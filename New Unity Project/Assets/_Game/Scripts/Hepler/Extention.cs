﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public static class Extention {
   public static int Claim10(this int Value) {
        while(Value >= 10) {
            Value = Value % 10;
        }
        return Value;
    }

    public static void AutoSize(this Image image, Vector2 size, PerfectSize.FollowType type = PerfectSize.FollowType.X) {
        if (image == null) { return; }
        var perfectSize = image.GetComponent<PerfectSize>();
        if (perfectSize == null) { return; }
        perfectSize.AutoSize(size, type);
    }

    public static void SetText(TextMeshProUGUI text, string str) {
        if (text == null) {
            Logs.LogError(string.Format("Text Null"));
            return; 
        }
        text.text = str;
    }

    public static void SetSpriteImage(Image image, Sprite icon, PerfectSize.FollowType type = PerfectSize.FollowType.X) {
        if(image == null) {
            Logs.LogError(string.Format("Image Null"));
            return; 
        }
        if(icon == null) {
            Logs.LogError(string.Format("Sprite Null"));
            return;
        }
        image.sprite = icon;
        var perfectSize = image.GetComponent<PerfectSize>();
        if(perfectSize == null) { return; }
        perfectSize.AutoSize(icon.rect.size, type);
    }

    public static void SetActiveObject(GameObject go, bool active) {
        if (go == null) {
            Logs.LogError(string.Format("GameObject Null"));
            return; 
        }
        go.SetActive(active);
    }
}
