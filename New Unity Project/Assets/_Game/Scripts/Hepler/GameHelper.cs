﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Permissions;
using System.Globalization;
//using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public static class GameHelper {
    private static Vector3[] direction = new Vector3[]{ Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.zero};
    public static Vector3 ConvertDirect(this EnumDefine.Direct direct) {
        return direction[(int)direct];
    }
    public static Vector2 WorldToCanvas(this Canvas canvas, Vector3 world_position, Camera camera = null) {
        if(camera == null) {
            camera = Camera.main;
        }

        var viewport_position = camera.WorldToViewportPoint(world_position);
        var canvas_rect = canvas.GetComponent<RectTransform>();

        return new Vector2((viewport_position.x * canvas_rect.sizeDelta.x) - (canvas_rect.sizeDelta.x * 0.5f),
            (viewport_position.y * canvas_rect.sizeDelta.y) - (canvas_rect.sizeDelta.y * 0.5f));
    }

    //public static Vector2 vTemp;
    public static void MoveXY(this Transform t, Vector2 d) {
        d.x += t.position.x;
        d.y += t.position.y;
        t.position = d;
    }

    public static void SetBool(this PlayerPrefs p, string key, bool value) {

    }

    public static void RemoveAllChildOnEditor(this Transform t) {
        int c = t.childCount;
        for(int i = 0; i < c; i++)
            Object.DestroyImmediate(t.GetChild(0).gameObject);
    }
    public static List<GameObject> GetAllChilds(this Transform t) {
        List<GameObject> childs = new List<GameObject>();
        int c = t.childCount;
        for(int i = 0; i < c; i++) { childs.Add(t.GetChild(i).gameObject); }
        return childs;
    }

    public static void RemoveAllChild2(this Transform t) {
        List<Transform> lst = new List<Transform>();
        for(int i = 0; i < t.childCount; i++)
            lst.Add(t.GetChild(i));
        for(int i = 0; i < lst.Count; i++)
            GameObject.Destroy(lst[i].gameObject);
    }

    public static void RemoveAllChild(this Transform t) {
        int c = t.childCount;
        for(int i = 0; i < c; i++)
            Object.Destroy(t.GetChild(0).gameObject);
    }

    public static void SetSizeFollowWidth(this Image img, int maxWidth) {
        if(img.sprite == null)
            return;
        float aspect = img.sprite.bounds.size.y / img.sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(maxWidth, maxWidth * aspect);
    }

    public static void SetSizeFollowHeight(this Image img, int maxHeight) {
        if(img.sprite == null)
            return;
        float aspect = img.sprite.bounds.size.y / img.sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(maxHeight / aspect, maxHeight);
    }

    public static void DeleteAllChilds(this Transform t) {
        int count = t.childCount;
        for(int i = 0; i < count; i++)
            Object.Destroy(t.GetChild(0).gameObject);
    }

    public static void DeleteImmediateAllChilds(this Transform t) {
        int count = t.childCount;
        for(int i = 0; i < count; i++)
            Object.DestroyImmediate(t.GetChild(0).gameObject);
    }

    public static void AlignWithPivot(this Image i) {
        if(i.sprite == null)
            return;
        Vector2 size = i.GetComponent<RectTransform>().rect.size;
        Vector2 pixelPivot = i.sprite.pivot;
        Vector2 percentPivot = new Vector2(pixelPivot.x / size.x, pixelPivot.y / size.y);
        i.GetComponent<RectTransform>().pivot = percentPivot;
    }
    private static System.Random rng = new System.Random();
    public static void Shuffle<T>(this T[] array) {
        int i = array.Length;
        while(i > 1) {
            int num = rng.Next(i--);
            T t = array[i];
            array[i] = array[num];
            array[num] = t;
        }
    }

    //public static float GetAnimationLeng(this SkeletonAnimation spine, string aniName)
    //{
    //    return spine.skeletonDataAsset.GetSkeletonData(true).FindAnimation(aniName).duration;
    //}

    //public static float GetAnimationLeng(this SkeletonGraphic spine, string aniName)
    //{
    //    return spine.skeletonDataAsset.GetSkeletonData(true).FindAnimation(aniName).duration;
    //}

    private const float DegToRad = Mathf.PI / 180;

    public static Vector2 Rotate(this Vector2 v, float degrees) {
        return v.RotateRadians(degrees * DegToRad);
    }

    public static Vector2 RotateRadians(this Vector2 v, float radians) {
        var ca = Mathf.Cos(radians);
        var sa = Mathf.Sin(radians);
        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }

    public static Vector3 Truncate(this Vector3 original, float max) {
        original.Normalize();
        original *= max;
        return original;
    }

    public static float GetAngle(this Transform t, Vector3 target) {
        return Mathf.Atan2(target.y - t.position.y, target.x - t.position.x) * Mathf.Rad2Deg - 90;
    }

    public static float GetAngle(this Vector3 t, Vector3 target) {
        return Mathf.Atan2(target.y - t.y, target.x - t.x) * Mathf.Rad2Deg - 90;
    }
    public static float GetAngle(this Vector3 t) {
        return Mathf.Atan2(t.y, t.x) * Mathf.Rad2Deg;
    }
    public static float GetAngle(this Vector2 t) {
        return Mathf.Atan2(t.y, t.x) * Mathf.Rad2Deg;
    }

    public static float GetAngleZero(this Vector3 t, Vector3 target) {
        return Mathf.Atan2(target.y - t.y, target.x - t.x) * Mathf.Rad2Deg + 90;
    }

    public static List<Vector3> FlipList(this List<Vector3> l) {
        return l.Select(s => new Vector3(-s.x, s.y)).ToList();
    }

    public static T DeepClone<T>(T obj) {
        using(var ms = new MemoryStream()) {
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, obj);
            ms.Position = 0;

            return (T)formatter.Deserialize(ms);
        }
    }

    public static void SetSizeX(this SpriteRenderer render, int size) {
        float scale = size / render.sprite.rect.width;
        render.transform.localScale = new Vector3(scale, scale);
    }

    public static void SetSizeY(this SpriteRenderer render, int size) {
        float scale = size / render.sprite.rect.height;
        render.transform.localScale = new Vector3(scale, scale);
    }

    public static Vector3 Rotate(this Vector3 v, float degrees) {
        return v.RotateRadians(degrees * DegToRad);
    }

    public static Vector3 RotateRadians(this Vector3 v, float radians) {
        var ca = Mathf.Cos(radians);
        var sa = Mathf.Sin(radians);
        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }

#if UNITY_EDITOR

    public static List<Object> GetAllAssets(string path) {
        string[] paths = { path };
        var assets = UnityEditor.AssetDatabase.FindAssets(null, paths);
        var assetsObj = assets.Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<Object>(UnityEditor.AssetDatabase.GUIDToAssetPath(s))).ToList();
        return assetsObj;
    }

    public static void PingObj(string path) {
        var obj = UnityEditor.AssetDatabase.LoadAssetAtPath<Object>(path);
        UnityEditor.Selection.activeObject = obj;
        UnityEditor.EditorGUIUtility.PingObject(obj);
    }

#endif
}