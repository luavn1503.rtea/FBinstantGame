﻿using System.Diagnostics;
using Gemmob;
using UnityEngine;

namespace Gemmob {
	public static class ResourceLogger {
		private static Stopwatch stopwatch = new Stopwatch();

		public static T Load<T>(string path) where T : Object {
			stopwatch.Reset();
			stopwatch.Start();
			T obj = Resources.Load<T>(path);
			stopwatch.Stop();
			if (obj == null) {
				Logs.LogError(string.Format("Can not load resource {0} in {1}", typeof(T).Name, path));
			} else {
				Logs.Log(string.Format("Found {0} object at {1} in {2}s", typeof(T).Name, path, stopwatch.Elapsed.TotalSeconds));
			}

            return obj;
		}

		public static T[] LoadAll<T>(string path) where T : Object {
			stopwatch.Reset();
			stopwatch.Start();
			T[] objs = Resources.LoadAll<T>(path);
            stopwatch.Stop();
			if (objs == null || objs.Length == 0) {
				Logs.LogError(string.Format("Can not load all resources {0} in {1}", typeof(T).Name, path));
			} else {
				Logs.Log(string.Format("Found {0} {1} object at {2} in {3}s", objs.Length, typeof(T).Name, path, stopwatch.Elapsed.TotalSeconds));
			}

			return objs;
		}
	}
}