﻿using Gemmob;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Spine;

public static class SpineExtention {
    public static float SetAnimation(this SkeletonAnimation skeleton, string animationKey, bool loop = false) {
        var animation = skeleton.Skeleton.Data.FindAnimation(animationKey);
        skeleton.AnimationState.SetAnimation(0, animation, loop);
        return animation.Duration;
    }

	public static void SetSkin(this SkeletonAnimation skeleton, string skinKey) {
        if(skeleton == null) {
            Debug.LogError("SkeletonAnimation null");
            return; 
        }
        skeleton.Skeleton.SetSkin(skinKey);
        skeleton.Skeleton.SetSlotsToSetupPose();
        skeleton.LateUpdate();
    }
    public static float SetAnimation(this SkeletonGraphic skeleton, string animationKey, bool loop = false) {
        var animation = skeleton.SkeletonData.FindAnimation(animationKey);
        skeleton.AnimationState.SetAnimation(0, animation, loop);
        return animation.Duration;
    }

    public static void SetSkin(this SkeletonGraphic skeleton, string skinKey) {
        if(skeleton == null) {
            Debug.LogError("SkeletonAnimation null");
            return;
        }
        skeleton.Skeleton.SetSkin(skinKey);
        skeleton.Skeleton.SetSlotsToSetupPose();
        skeleton.LateUpdate();
    }


}
