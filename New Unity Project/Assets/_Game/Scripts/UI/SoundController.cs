﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SoundController : MonoBehaviour
{
    public static SoundController Instance;
    public bool OnSound = true, OnMusic = true;
    [HideInInspector] public List<AudioSource> PoolsAudioSources;
    [HideInInspector] public List<AudioSource> SpawnsAudioSources;

    public AudioSource EnemyExSound;
    public AudioSource BackgroundSoundSource;

    [SerializeField] GameObject _emptyGameObject;

    void Awake()
    {
        Instance = this;

        LoadSetting();

        var transBackGround = transform.Find("BackgroundSoundSource");
        if (!transBackGround)
        {
            var tran = Instantiate(_emptyGameObject, transform);
            tran.transform.name = "BackgroundSoundSource";
            BackgroundSoundSource = tran.AddComponent<AudioSource>();
        }
        else
        {
            BackgroundSoundSource = transBackGround.GetComponent<AudioSource>();
        }

        var transEnemyExSound = transform.Find("EnemyExSound");
        if (!transEnemyExSound)
        {
            var tran = Instantiate(_emptyGameObject, transform);
            tran.transform.name = "EnemyExSound";
            EnemyExSound = tran.AddComponent<AudioSource>();
        }
        else
        {
            EnemyExSound = transBackGround.GetComponent<AudioSource>();
        }

        //GameEvent.OnMusicStateChanged += ChangeMusic;
    }

    

    private void ChangeMusic()
    {
        if (OnMusic)
        {
            if (CurrentBgClip != null)
                PlayBg(CurrentBgClip);
            else
                PlayBackgroundSound();
        }
        else
        {
            StopBackgroundSound();
        }
    }

void SaveSetting()
    {
        PlayerPrefs.SetInt("Music", OnMusic ? 1 : 0);
        PlayerPrefs.SetInt("Sound", OnSound ? 1 : 0);
    }

    void TurnOffAll()
    {
        OnMusic = false;
        OnSound = false;
    }

    void LoadSetting()
    {
        OnMusic = PlayerPrefs.GetInt("Music", 1) == 1;
        OnSound = PlayerPrefs.GetInt("Sound", 1) == 1;
    }

    void OnApplicationPause(bool isPause)
    {
        if (isPause)
        {
            SaveSetting();
            StopBackgroundSound();
            MuteAllSound();
        }
        else
        {
            LoadSetting();
            PlayBackgroundSound();
        }
    }

    void OnDestroy()
    {
       // GameEvent.OnMusicStateChanged -= ChangeMusic;
        SaveSetting();
    }

    public void PlayBg(AudioClip clip, float volume = 1f, bool loop = true)
    {
        CurrentBgClip = clip;
        if (OnMusic)
        {
            BackgroundSoundSource.Stop();
            BackgroundSoundSource.clip = clip;
            BackgroundSoundSource.volume = volume;
            BackgroundSoundSource.loop = loop;
            BackgroundSoundSource.Play();
        }
    }

    public static AudioClip CurrentBgClip;

    public static void PlayBackgroundSound(AudioClip clip, float volume = 1, bool loop = true)
    {
        Instance.PlayBg(clip, volume, loop);
    }

    public static void PlayBackgroundSound(SoundInfor infor = null, bool loop = true)
    {
        if (infor == null)
            Instance.BackgroundSoundSource.volume = Instance.OnMusic ? 1 : 0;
        else
            Instance.PlayBg(infor.Clip, Instance.OnMusic ? infor.Volume : 0, loop);
    }

    public static void StopBackgroundSound()
    {
        Instance.BackgroundSoundSource.volume = 0;
    }

    public void PlaySound(AudioClip clip, float volume = 1)
    {
        if (!OnSound)
            return;

        if (PoolsAudioSources.Count > 0)
        {
            PoolsAudioSources[0].clip = clip;
            PoolsAudioSources[0].volume = volume;
            PoolsAudioSources[0].Play();
            SpawnsAudioSources.Add(PoolsAudioSources[0]);
            PoolsAudioSources.RemoveAt(0);
        }
        else
        {
            int count = SpawnsAudioSources.Count;
            bool has = false;
            for (int i = 0; i < count;)
            {
                if (!SpawnsAudioSources[i].isPlaying)
                {
                    has = true;
                    PoolsAudioSources.Add(SpawnsAudioSources[i]);
                    SpawnsAudioSources.RemoveAt(i);
                    count--;
                }
                else i++;
            }
            if (has)
            {
                PlaySoundEffect(clip, volume);
            }
            else
            {
                AudioSource sound = gameObject.AddComponent<AudioSource>();
                sound.clip = clip;
                sound.volume = volume;
                sound.Play();
                SpawnsAudioSources.Add(sound);
            }
        }
    }

    private static float _lastTimeRunEnemyExpSound = 0;
    public static void PlayEnemyExpSound(SoundInfor infor)
    {
        if (Time.time - _lastTimeRunEnemyExpSound > .3f)
        {
            if (infor.Clip == null)
                return;
            Instance.PlaySound(infor.Clip, infor.Volume);
            _lastTimeRunEnemyExpSound = Time.time;
        }
    }

    public static void PlaySoundEffect(SoundInfor infor)
    {
        if (infor.Clip == null)
            return;
        Instance.PlaySound(infor.Clip, infor.Volume);
    }
    public static void PlaySoundEffect(AudioClip clip, float volume = 1)
    {
        Instance.PlaySound(clip, volume);
    }

    public static void StopSoundEffect()
    {
        int count = Instance.SpawnsAudioSources.Count;
        for (int i = 0; i < count; i++)
        {
            Instance.SpawnsAudioSources[0].Stop();
            Instance.PoolsAudioSources.Add(Instance.SpawnsAudioSources[0]);
            Instance.SpawnsAudioSources.RemoveAt(0);
        }
    }

    public static void MuteAllSound()
    {
        int count = Instance.SpawnsAudioSources.Count;
        for (int i = 0; i < count; i++)
            Instance.SpawnsAudioSources[0].volume = 0;
    }

    public static void StopAll()
    {
        StopBackgroundSound();
        StopSoundEffect();
    }
}

[Serializable]
public class SoundInfor
{
    public AudioClip Clip;
    [Range(0, 1)]
    public float Volume = 1;
}