﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SoundController
{
    [Header("Background music")]
    public SoundInfor Menu;
    public SoundInfor GamePlay;

    [Header("Button sounds")]
    public SoundInfor Click;

    [Header("Enemy Exposive sounds")]
    public List<SoundInfor> EnemyExp;

    [Header("Eat Item sounds")]
    public SoundInfor EatItem;

    private SoundInfor _soundPrevious;

    public void RunClickSound()
    {
        PlaySoundEffect(Click.Clip, Click.Volume);
    }

	[Header("Open box")] public SoundInfor OpenBox;

    public void RunSound()
    {
        Invoke("RunClickSound", 0);
    }
}