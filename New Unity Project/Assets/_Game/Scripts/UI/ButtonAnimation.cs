﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonAnimation : UIBehaviour, IPointerUpHandler, IPointerDownHandler,IPointerExitHandler,IPointerEnterHandler,IPointerClickHandler {
	private bool _isPointerOut;
	[SerializeField] private float SizeScaleClick = 0.9f;

	public bool interactable = true;
	public bool canUseAni = true;
	private Vector3 originScale;
	[SerializeField] private Button.ButtonClickedEvent m_OnClick = new Button.ButtonClickedEvent();

	protected override void Awake() {
		originScale = gameObject.transform.localScale;
	}

	public Image BtnSkin {
		get { return GetComponent<Image>(); }
		set {
			Image img = GetComponent<Image>();
			img = value;
		}
	}

	public Button.ButtonClickedEvent onClick {
		get { return m_OnClick; }
		set { m_OnClick = value; }
	}

	public virtual void OnPointerDown(PointerEventData eventData) {
		_isPointerOut = false;
		if (eventData.button != PointerEventData.InputButton.Left || !interactable || !canUseAni) 
			return;
		gameObject.transform.localScale = originScale * SizeScaleClick;
	}


	public virtual void OnPointerUp(PointerEventData eventData) {
		gameObject.transform.localScale = originScale;
	}

	IEnumerator WaitToIn(PointerEventData eventData) {
		//yield return Yielders.Get(0.1f);
		if (!_isPointerOut) {
			if (eventData.button != PointerEventData.InputButton.Left || !interactable)
				yield break;
			Press();
		}
	}

	private void Press() {
		if (!IsActive())
			return;
		if (m_OnClick != null)
			m_OnClick.Invoke();
	}
	[ContextMenu("switchClick")]
	public void SwitchClick() {

		m_OnClick = GetComponent<Button>().onClick;
		DestroyImmediate(GetComponent<Button>());
	}

	public void OnPointerExit(PointerEventData eventData) {
		_isPointerOut = true;
	}

	public void OnPointerEnter(PointerEventData eventData) {
		_isPointerOut = false;
	}

	public void OnPointerClick(PointerEventData eventData) {
		gameObject.transform.localScale = originScale;
		if (!_isPointerOut) {
			if (eventData.button != PointerEventData.InputButton.Left || !interactable)
				return;
			Press();
		} else {
			StartCoroutine(WaitToIn(eventData));
		}
	}
}