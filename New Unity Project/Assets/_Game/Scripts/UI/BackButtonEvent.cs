﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BackButtonEvent : MonoBehaviour
{
    public UnityEvent BackEvent;

    public void BackButtonClicked()
    {
        if (BackEvent != null)
            BackEvent.Invoke();
    }
}
