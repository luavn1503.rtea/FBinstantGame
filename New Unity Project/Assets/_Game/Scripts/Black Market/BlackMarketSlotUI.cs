﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Gemmob.Common;
using UnityEngine;
using UnityEngine.UI;

public class BlackMarketSlotUI : MonoBehaviour {
	[SerializeField] private int _id;
	[SerializeField] private Image _itemIcon;
    [SerializeField] private Image _itemCategory;
	[SerializeField] private Text _txtItemNum;
	[SerializeField] private ButtonAnimation _btnBuy;
	[SerializeField] private Text _txtCurrencyNum;
	[SerializeField] private Image _currencyIcon;
	[SerializeField] private GameObject _soldPanel;

	[ContextMenu("Init")]
	void Init() {
		_itemIcon = transform.Find("Icon").GetComponent<Image>();
		_txtItemNum = transform.Find("txtNum").GetComponent<Text>();
		_btnBuy = transform.Find("btnBuy").GetComponent<ButtonAnimation>();
		_txtCurrencyNum = _btnBuy.transform.Find("txtNumber").GetComponent<Text>();
		_currencyIcon = _btnBuy.transform.Find("Icon").GetComponent<Image>();
		_soldPanel = transform.Find("SoldPanel").gameObject;
	}

	void Start() {
		_btnBuy.onClick.AddListener(OnBuyClicked);
	}

	void SetBtnGray() {
		//bool isSold=GameData
		//if (GameData.Instance.BlackMarketSlotData.GetSold(_id)) {
		//	_btnBuy.
		//}
	}

	void BuyWhenAdsEnable(bool isUnlock, PlayerInfo player, BlackMarketSlot data, BlackMarketSlotData slotData, BlackMarketSlotInfor infor) {
		bool useCurrency = true;
        if (isUnlock) {
			if (infor.CurrencyType == AllCurrency.Ads) {
				//[Tracking-Position]-[VideoAds]
				AdsModify.ShowRewardVideo("GetItem_BlackMarket", AdsType.VideoReward, this.name, "null", "null",
					() => {
						slotData.WatchAds(_id);
						if (slotData.GetAdsWatched(_id) >= infor.CurrencyNum) {
							player.GetAllRewardType(infor.ItemType, infor.ItemExtendId, infor.ItemNum, this.name);
							DoSold();
							
						}
						else {
							_txtCurrencyNum.text = string.Format("{0}/{1}", slotData.GetAdsWatched(_id), infor.CurrencyNum);
						}
					});
			}
			else {
				if (useCurrency) {
					player.GetAllRewardType(infor.ItemType, infor.ItemExtendId, infor.ItemNum, this.name);
					DoSold();
					
				}
			}
		}
		else {
			if (data.UnlockCurrency == AllCurrency.Ads) {
				
				AdsModify.ShowRewardVideo("UnlockSlot_BlackMarket", AdsType.VideoReward, this.name, "null", "null", () => {
					slotData.WatchAds(_id);
					if (slotData.GetAdsWatched(_id) >= data.UnlockNum) {
						slotData.UnlockSlot(_id);
						SetSlot(_id);
					}
					else {
						_txtCurrencyNum.text = string.Format("{0}/{1}", slotData.GetAdsWatched(_id), data.UnlockCurrency);
					}
				});
			}
			else {
				if (useCurrency) {
					slotData.UnlockSlot(_id);
					SetSlot(_id);
				}
			}
		}
		if (!useCurrency) {

		}
	}

	void BuyWhenAdsDisable(bool isUnlock, PlayerInfo player, BlackMarketSlot data, BlackMarketSlotData slotData, BlackMarketSlotInfor infor) {
		bool useCurrency;
        if (isUnlock) {
			useCurrency = infor.NoAdsCurrencyType == AllCurrency.Coin;
			if (useCurrency) {
                player.GetAllRewardType(infor.ItemType, infor.ItemExtendId, infor.ItemNum, this.name);
				DoSold();
				
			}
		}
		else {
			useCurrency = data.NoAdsUnlockCurrency == AllCurrency.Coin;
			if (useCurrency) {
				slotData.UnlockSlot(_id);
				SetSlot(_id);
			}
		}
    }

	void OnBuyClicked() {
		PlayerInfo player = ManagerData.Instance.PlayerInfo;
		BlackMarketSlot data = ManagerData.Instance.BlackMarketData.Slot[_id];
		BlackMarketSlotData slotData = ManagerData.Instance.BlackMarketSlotData;
		BlackMarketSlotInfor infor = data.SlotInfor[slotData.GetSlotInforId(_id)];
		bool isUnlock = slotData.CheckUnlock(_id);
		if (Config.IsAdsEnable) {
			BuyWhenAdsEnable(isUnlock, player, data, slotData, infor);
		}
		else {
			BuyWhenAdsDisable(isUnlock, player, data, slotData, infor);
		}
	}

	void SetBtnShow(Sprite btnSkin, Sprite currencyIcon, string txtCurrencyNum, Color color) {
		_btnBuy.GetComponent<Image>().sprite = btnSkin;
		_currencyIcon.sprite = currencyIcon;
		_currencyIcon.SetSizeFollowWidth(50);
		_txtCurrencyNum.text = txtCurrencyNum;
		_txtCurrencyNum.color = color;
	}

	private Sprite GetCurrencyIcon(AllCurrency cur) {
		switch (cur) {
			case AllCurrency.Ads:
				return DataConfig.Instance.AdsSprite;
			case AllCurrency.Coin:
				return DataConfig.Instance.CoinSprite;
			default:
				return DataConfig.Instance.GemSprite;
		}
	}

	void SetBtn(BlackMarketSlotInfor infor, BlackMarketSlot slot, int adsWatched, bool isUnlock) {
		_btnBuy.gameObject.SetActive(!ManagerData.Instance.BlackMarketSlotData.GetSold(_id));
		Sprite btnSkin, currencyIcon;
		string txtCurrencyNum;
		Color color;
		if (Config.IsAdsEnable) {
			if (isUnlock) {
				btnSkin = infor.CurrencyType != AllCurrency.Coin
					? DataConfig.Instance.BtnActiveBlue
					: DataConfig.Instance.BtnActiveYellow;
				currencyIcon = GetCurrencyIcon(infor.CurrencyType);
				txtCurrencyNum = infor.CurrencyType == AllCurrency.Ads
					? string.Format("{0}/{1}", adsWatched, infor.CurrencyNum)
					: infor.CurrencyNum.ToString();
				color = infor.CurrencyType != AllCurrency.Coin ? DataConfig.Instance.BlueTxtColor : DataConfig.Instance.DolarColor;
			}
			else {
				btnSkin = slot.UnlockCurrency != AllCurrency.Coin
					? DataConfig.Instance.BtnActiveBlue
					: DataConfig.Instance.BtnActiveYellow;
				currencyIcon = GetCurrencyIcon(slot.UnlockCurrency);
				txtCurrencyNum = slot.UnlockCurrency == AllCurrency.Ads
					? string.Format("{0}/{1}", adsWatched, slot.UnlockNum)
					: slot.UnlockNum.ToString();
				color = slot.UnlockCurrency != AllCurrency.Coin ? DataConfig.Instance.BlueTxtColor : DataConfig.Instance.DolarColor;
			}
		}
		else {
			if (isUnlock) {
				btnSkin = infor.NoAdsCurrencyType != AllCurrency.Coin
					? DataConfig.Instance.BtnActiveBlue
					: DataConfig.Instance.BtnActiveYellow;
				currencyIcon = GetCurrencyIcon(infor.NoAdsCurrencyType);
				txtCurrencyNum = infor.NoAdsCurrencyNum.ToString();
				color = infor.NoAdsCurrencyType != AllCurrency.Coin ? DataConfig.Instance.BlueTxtColor : DataConfig.Instance.DolarColor;
			}
			else {
				btnSkin = slot.NoAdsUnlockCurrency != AllCurrency.Coin
					? DataConfig.Instance.BtnActiveBlue
					: DataConfig.Instance.BtnActiveYellow;
				currencyIcon = GetCurrencyIcon(slot.NoAdsUnlockCurrency);
				txtCurrencyNum = slot.NoAdsUnlockNum.ToString();
				color = slot.NoAdsUnlockCurrency != AllCurrency.Coin ? DataConfig.Instance.BlueTxtColor : DataConfig.Instance.DolarColor;
			}
		}
		SetBtnShow(btnSkin, currencyIcon, txtCurrencyNum, color);
	}

	public void SetSlot(int id) {
		//BlackMarketData data = ManagerData.Instance.BlackMarketData;
		_id = id;
		BlackMarketSlot data = ManagerData.Instance.BlackMarketData.Slot[_id];
		BlackMarketSlotData slotData = ManagerData.Instance.BlackMarketSlotData;
		BlackMarketSlotInfor infor = data.SlotInfor[slotData.GetSlotInforId(_id)];
		bool isUnlock = slotData.CheckUnlock(_id);
		if (isUnlock) {
			_itemIcon.sprite = infor.ItemIcon;
			_itemIcon.SetSizeFollowWidth(160);
			_txtItemNum.text = infor.ItemNum.ToString();
            _itemCategory.gameObject.SetActive(true);

        }
		else {
			_itemIcon.SetSizeFollowWidth(210);
			_txtItemNum.text = string.Empty;
            _itemCategory.gameObject.SetActive(false);

		}

		SetBtn(infor, data, slotData.GetAdsWatched(id), isUnlock);
		_soldPanel.SetActive(ManagerData.Instance.BlackMarketSlotData.GetSold(_id));
	}

	void DoSold(float time = 0.5f, float size = 20) {
		_btnBuy.gameObject.SetActive(false);
		_soldPanel.SetActive(true);
		ManagerData.Instance.BlackMarketSlotData.Sold(_id);
		transform.DOShakePosition(time, new Vector3(size, size, 0));
	}
}
