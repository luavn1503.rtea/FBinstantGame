﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackMarket : Frame {
    public Sprite locksprite;
    [SerializeField] private BlackMarketSlotUI _slot;
    [SerializeField] private List<BlackMarketSlotUI> _isSlot;
    private bool isCreate;

    private void Start() {
        CreateSlot();
    }
    private void OnEnable() {
        UpdateSlot();
    }
    void CreateSlot() {
        for(int i = 0; i < ManagerData.Instance.BlackMarketData.Slot.Count; i++) {
            if(i == 0) {
                _slot.SetSlot(i);
                _isSlot.Add(_slot);
                continue;
            }
            BlackMarketSlotUI slot = Instantiate(_slot, _slot.transform.parent);
            slot.SetSlot(i);
            _isSlot.Add(slot);

        }
        isCreate = true;
    }
    public void UpdateSlot() {
        if(isCreate) {
            for(int i = 0; i < _isSlot.Count; i++) {
                _isSlot[i].SetSlot(i);
            }
        }
    }
}
