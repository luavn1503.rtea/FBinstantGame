﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gemmob.Common;
using UnityEngine;
using UnityEngine.UI;

public class BlackMarketResetPanel : MonoBehaviour {
	[SerializeField] private Text _txtResetTime;
	[SerializeField] private ButtonAnimation _btnWatch;
	

	[ContextMenu("Init")]
	void Init() {
		_txtResetTime = transform.Find("txtReset").GetComponent<Text>();
		_btnWatch = transform.Find("btnWatch").GetComponent<ButtonAnimation>();
		
	}

	void Start() {
		SetResetTime();
		SetWatchBtn();
	}

	void SetWatchBtn(bool isInteractable, bool isActive, float effect) {
		_btnWatch.interactable = isInteractable;
		
		//_btnWatch.GetComponentsInChildren<_2dxFX_GrayScale>().ToList().ForEach(_ => _._EffectAmount = effect);
	}

	void SetWatchBtn() {
		LocalCountDown resetByAds = CountDownController.Instance.CountDownResetMarketByAds;
		if (!resetByAds.Done) {
           
			SetWatchBtn(false, true, 1);
            resetByAds.OnComplete += () => {
				_btnWatch.onClick.AddListener(OnWatchClicked);
				SetWatchBtn(true, false, 0);
			};
		}
		else {
			_btnWatch.onClick.AddListener(OnWatchClicked);
			SetWatchBtn(true, false, 0);
		}
	}

	void OnWatchClicked() {
        LocalCountDown resetByAds = CountDownController.Instance.CountDownResetMarketByAds;
		AdsModify.ShowRewardVideo("ResetMarket", AdsType.VideoReward, this.name, "null", "null", () => {
			resetByAds.StartCountDown();
			SetWatchBtn(false, true, 1);
			ResetMarket();
		});
	}

	void SetResetTime() {
		LocalCountDown resetTime = CountDownController.Instance.CountDownResetMarket;
		if (resetTime.Done) {
			resetTime.StartCountDown();
			resetTime.OnTick += () => { _txtResetTime.text = string.Format("Reset in: {0}", resetTime.GetTime()); };
			ResetMarket();
		}
		else {
			resetTime.StartCountDown(false);
			resetTime.OnTick += () => { _txtResetTime.text = string.Format("Reset in: {0}", resetTime.GetTime()); };
		}
		resetTime.OnComplete += () => {
			resetTime.StartCountDown();
			ResetMarket();
		};
	}

	void ResetMarket() {
		ManagerData.Instance.BlackMarketSlotData.ResetMarket();

	}
}
