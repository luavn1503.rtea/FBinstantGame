﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static EnumDefine;

[Serializable]
public class RewardsData {
    public List<RewardData> data = new List<RewardData>();
}

[Serializable]
public class RewardData {
    [SerializeField] private RewardsType rewardsType = RewardsType.Currency;
    [SerializeField] private IDCurrency idCurrency = IDCurrency.Coin;
    [SerializeField] private IDShip idShip = IDShip.Ship_01;
    [SerializeField] private int amount = 0;

    public RewardData(RewardsType rewardsType, int amount, IDCurrency idCurrency = IDCurrency.Coin, IDShip idShip = IDShip.Ship_01) {
        this.rewardsType = rewardsType;
        this.amount = amount;
        this.idCurrency = idCurrency;
        this.idShip = idShip;
    }

    public EnumDefine.RewardsType TYPE => rewardsType;

    public EnumDefine.IDCurrency IdCurrency => idCurrency;

    public EnumDefine.IDShip IdShip => idShip;

    public int Amount => amount;
}
