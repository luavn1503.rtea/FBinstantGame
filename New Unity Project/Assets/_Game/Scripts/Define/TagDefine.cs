﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagDefine {
    public const string Player = "Player";
    public const string Enemy = "Enemy";
    public const string PlayerBullet = "PlayerBullet";
}
