﻿
public class EnumDefine {

    public enum IDScene {
        Home,
        Game
    }

    public enum View {
        Hide,
        Show,
    }

    public enum Direct {
        Up = 0, Down = 1, Left = 2, Right = 3, Center = 4,
    }

    public enum TypeAttack { 
        None,
        Laser
    }
    public enum CurrencyType {
        Coin,
        Gem,
        Crystal,
        Pumpkin1,
        Pumpkin2,
        Pumpkin3,
        Pumpkin4,
        Pumpkin5,
        Energy,
    }

    public enum IDBullet {
        Bullet_None_1 = 0,
        Bullet_None_1b = 1,
        Bullet_None_2 = 2,
        Bullet_None_2b = 3,
        Bullet_None_3 = 4,
        Bullet_None_3b = 5,
        Bullet_None_4 = 6,
        Bullet_None_4b = 7,
        Bullet_None_5 = 8,
        Bullet_None_5b = 9,
        Bullet_None_6 = 10,
        Bullet_None_6b = 11,
        Bullet_None_7 = 12,
        Bullet_None_7b = 13,
        Bullet_None_8 = 14,
        Bullet_None_8b = 15,
        Bullet_Laser_1 = 16,
        Bullet_Laser_2 = 17,
        Bullet_Laser_3 =18,
        Drone_3_Bullet = 19,
        Drone_4_Bullet = 20,
        Drone_5_Bullet_laser = 21,
        Drone_6_Bullet =22
    }
    public enum Rank {
        D, C, B, A, S
    }

    public enum RewardsType {
        Currency,
        Debris,
        Ship
    }

    public enum IDCurrency {
        Coin,
        Gem,
        Energy,
        Crystal,
    }

    public enum IDShip {
        Ship_01 = 0,
        Ship_02 = 1,
        Ship_03 = 2,
        Ship_04 = 3,
        Ship_05 = 4,
        Ship_06 = 5,
        Ship_07 = 6,
        Ship_08 = 7,
        Drone_01 = 8,
        Drone_02 = 9,
        Drone_03 = 10,
        Drone_04 = 11,
        Drone_05 = 12,
        Drone_06 = 13,
        Drone_07 = 14,
        Drone_08 = 15,
        Ship_All = 99,
        Drone_All = 100,
    }

    public enum TypeShip {
        Ship,
        Drone,
        None,
    }

    public enum StarType {
        Common = 1,
        Rare = 2,
        Epic = 3,
        Heroic = 4,
        Legend = 5
    }

    public enum ModePlay {
        Normal = 0,
        Endless = 1,
        Boss = 2,
        Halloween = 3
    }

    public enum TypeSkill {
        Talen,
        Active,
        None,
    }

    public enum IDSkill {
        Shield = 0,
        Pow = 1,
        Drone = 2,
        Rocket = 3,
        RocketChase = 4,
        LaserGloble = 5,
        Shoot = 6,
        Freeze = 7,
        CombatPower = 8,
        FireRate = 9,
        Coin = 10,
        SpeedBullet = 11,
        Cooldown = 12,
        DroneDamage = 13,
        DroneFireRate = 14,
        Super = 15,
    }


    public enum TypeItem {
        Ship,
        Skill,
        Currency,
    }

    public enum TypeMission {
        Mission = 0, 
        Achivement = 1,
    }
    public enum TypeDaily {
        Reward
    }


    public enum IDStats {
        Power = 0,
        Damage = 1,
        Firerate = 2,
        Speedbullet = 3,
    }

    public enum IDModePlay {
        Easy = 0,
        Medium = 1,
        Hard = 2,
    }

    public enum ShopType {
        Coin = 0,
        Gem = 1, 
        HotDeal = 2,
    }
}


#region Data Type


public enum CardType {
    CombatPower,
    Shield,
    CooldownBomb,
    POW,
    Missiles,
    Life,
    CoinBonus,
    Drone,
    None = 99,
}

//public enum CoinShopType {
//    Coin, 
//    Gem, 
//    HotDeal,
//}

public enum ShopCoinBuyType {
    Gold, 
    Gem, 
    Dolar, 
    Ads
}

public enum PackType {
	UsaIndependence, 
    JulyTheFourth, 
    StarterPack4, 
    StarterPack5
}

#endregion

#region Game play

#endregion

#region UI
public enum MenuButton {
    Level,
    Pet,
    Achievement,
    MaterialBag,
    Sword,
    Hero,
    Shop,
    Setting,
    Minimode,
    DailyReward,
    Mail
}

public enum ChangeCurrencyArea {
	Achievement, Mission, DailyReward, TreasurePage, BlackMarket, Shop, Revive, Endless,
	BossBattle, Endgame, StartPage, IAP, DailyChallenge, Halloween, GiftCode, LuckySpin, GiveAway, BackupData,
	Vip, UpgradeShip, EvolveShip, UpgradeDrone, EvolveDrone, UpgradeCard, Rating, Ads, BuyShipIap
}

public enum NotifiKey {
	Treasure, DailyReward, Spin, Vip, Quest, BlackMarket, Storage, DailyMode, HalloweenMode, BossMode, EndlessMode
}
#endregion

