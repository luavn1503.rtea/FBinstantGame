﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePerSecendsBase : MonoBehaviour, IDamage {
    protected float baseDamage = 1f;
    protected Action onDamage = null;
    protected Collider2DBase coll2D  = null;
    protected Dictionary<int, IEnumerator> obInside = new Dictionary<int, IEnumerator>();
    public void Initialize(Collider2DBase coll2D, Action onDamage = null, float BaseDamage = 1f) {
        this.coll2D = coll2D;
        this.baseDamage = BaseDamage;
        this.onDamage = onDamage;
        coll2D.AddTriggerEnterListener(OnEnterColliderDamage);
        coll2D.AddTriggerExitListener(OnExitColliderDamage);
    }

    public void Remove() {
        coll2D.RemoveTriggerEnterListener(OnEnterColliderDamage);
        coll2D.RemoveTriggerExitListener(OnExitColliderDamage);
        foreach(var item in obInside.Values) { StopCoroutine(item); }
        obInside.Clear();
    }

    public void OnEnterColliderDamage(Collider2D collision) {
        if(collision.gameObject.activeInHierarchy == false) { return; }
        var idInstance = collision.GetInstanceID();
        var exist = obInside.TryGetValue(idInstance, out var ieDamagePerSec);
        if(exist) {
            Logs.LogError("Check login here: Tại sao lại có trùng");
        }
        else {
            var obj = collision.GetComponent<ITakeDamage>();
            if(obj != null) {
                ieDamagePerSec = TimeDamagePerSeconds(obj);
                obInside.Add(idInstance, ieDamagePerSec);
                StartCoroutine(ieDamagePerSec);
            }
        }
    }

    public void OnExitColliderDamage(Collider2D collision) {
        var idInstance = collision.GetInstanceID();
        var exist = obInside.TryGetValue(idInstance, out var ieDamagePerSec);
        if(exist) {
            StopCoroutine(ieDamagePerSec);
            obInside.Remove(idInstance);
        }
    }

    protected IEnumerator TimeDamagePerSeconds(ITakeDamage obj) {
        while(true) {
            ActiveDamage(obj);
            yield return Yielder.Wait(1f);
        }
    }

    protected void ActiveDamage(ITakeDamage obj) {
        obj?.TakeDamage(baseDamage);
        onDamage?.Invoke();
    }
}
