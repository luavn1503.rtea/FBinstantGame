﻿using UnityEngine;

public class AutoRotate : MonoBehaviour {
    private Transform _transform = null;
    private Vector3 _rotateSpeed = Vector3.zero;
    [SerializeField]  private float _speedRotate = 100;
    void Awake() {
        _transform = transform;
        _rotateSpeed = Vector3.forward * _speedRotate;
    }

    void Update() {
        _transform.Rotate(_rotateSpeed * Time.deltaTime);
    }
}
