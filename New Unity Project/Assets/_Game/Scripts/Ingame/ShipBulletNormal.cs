﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DamagePerSecendsBase))]
public class ShipBulletNormal : ShipBulletBase {
    [SerializeField] protected float speedMovement = 10f;
    [SerializeField] protected Collider2DBase coll2D = null;
    [SerializeField] protected DamagePerSecendsBase damagePerSecends = null;
    [SerializeField] protected GameObject efExplosion = null;
    public override ShipBulletBase Initialized() {
        base.Initialized();
        damagePerSecends.Initialize(coll2D, Recycle);
        return this;
    }
    public void Recycle() {
        efExplosion.Spawn(transform.position);
        damagePerSecends.Remove();
        gameObject.Recycle();
    }

    protected virtual void Update() {
        transform.Translate(Vector3.up * speedMovement * Time.deltaTime);
        CheckOutScene();
    }

    protected void CheckOutScene() {
        Vector2 origin = transform.position;
        if(Mathf.Abs(origin.x) >= maxSize.x+1f || Mathf.Abs(origin.y) >= maxSize.y+1f) { Recycle(); }
    }


}
