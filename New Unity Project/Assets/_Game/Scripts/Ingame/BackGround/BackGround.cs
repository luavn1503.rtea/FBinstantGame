﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGround : MonoBehaviour
{
    [SerializeField] private float speed,speedStart,timeChange;
    [SerializeField] private  List<Transform> layers;
    private bool IsActive = false;
    private Vector3 distance,vector3;
    private int indexCheck,indexBack;

    public void Init() {
        DOVirtual.Float(speedStart,speed,timeChange,value=> { speedStart = value; });
        distance = layers[1].position- layers[0].position;
        indexCheck = 1;
        indexBack = 0;
        IsActive = true;
    }


    private void Update() {
        vector3 = layers[0].position;
        layers[0].position = vector3 + Vector3.down * speedStart * Time.deltaTime;
        vector3 = layers[1].position;
        layers[1].position = vector3 + Vector3.down * speedStart * Time.deltaTime;
        if(layers[indexCheck].position.y <= 0) {
            layers[indexBack].position = layers[indexCheck].position + distance;
            SwapIndex();
        }
    }

    private void SwapIndex() {
        if(indexCheck == 1) {
            indexCheck = 0;
            indexBack = 1;
        }
        else {
            indexCheck = 1;
            indexBack = 0;
        }
        
    }
    [ContextMenu("PrintOffSet")]
    public void PrintOffSet() {
        Debug.Log("Khoang cach cua 2 img"+ Vector3.Distance(layers[0].position, layers[1].position)+"Vi tri cur"+ distance);
        ;
    }
}
