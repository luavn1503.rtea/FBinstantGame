﻿using System.Collections.Generic;
using DG.Tweening;
using Gemmob;
using UnityEngine;

public class EnemyMoveHandler : SingletonFree<EnemyMoveHandler> {
    private Vector2 sceneSize;
    private Vector2 smallSceneSize;
    private readonly List<Sequence> sequences = new List<Sequence>();


    protected override void OnAwake() {
        sceneSize = new Vector2(Camera.main.orthographicSize / Camera.main.aspect, Camera.main.orthographicSize);
        smallSceneSize = sceneSize;
        sceneSize.x -= .1f;
        sceneSize.y -= .1f;
        EventDispatcher.Instance.AddListener<GameEvent.OnStopGame>(Stop);
    }

    protected override void OnDestroy() {
        EventDispatcher.Instance.RemoveListener<GameEvent.OnStopGame>(Stop);
        base.OnDestroy();
    }


    public void Stop() {
        foreach(var sequence in sequences) {
            sequence.Kill();
        }

        sequences.Clear();
    }

    /// <summary>
    /// Set action after appear move finished
    /// </summary>
    public void SetMoveEnd(List<Transform> InGameEnemies, WaveData wave) {
        int numEnemies = InGameEnemies.Count;
        float timeMove = wave.TimeMove;
        Ease ease = Ease.Linear;
        switch(wave.IdleType) {
            case IdleType.StayAndJump:
                for(int i = 0; i < numEnemies; i++) {
                    InGameEnemies[i].transform.DOMoveY(-.5f, timeMove).SetEase(ease).SetLoops(-1, LoopType.Yoyo)
                        .SetRelative(true);
                }

                break;
            case IdleType.TwoPartX:
                for(int i = 0; i < numEnemies; i++) {
                    if(InGameEnemies[i].transform.position.x > 0) {
                        InGameEnemies[i].transform.DOMoveX(.5f, timeMove).SetEase(ease).SetLoops(-1, LoopType.Yoyo)
                            .SetRelative(true);
                    }
                    else {
                        InGameEnemies[i].transform.DOMoveX(-.5f, timeMove).SetEase(ease).SetLoops(-1, LoopType.Yoyo)
                            .SetRelative(true);
                    }
                }

                break;

            case IdleType.TwoPartY:
                for(int i = 0; i < numEnemies; i++) {
                    if(InGameEnemies[i].transform.position.y > transform.position.y) {
                        InGameEnemies[i].transform.DOMoveY(.5f, timeMove).SetEase(ease).SetLoops(-1, LoopType.Yoyo)
                            .SetRelative(true);
                    }
                    else {
                        InGameEnemies[i].transform.DOMoveY(-.5f, timeMove).SetEase(ease).SetLoops(-1, LoopType.Yoyo)
                            .SetRelative(true);
                    }
                }

                break;
            case IdleType.WaveJump:
                for(int l = 0; l < numEnemies; l++) {
                    if(InGameEnemies[l] != null) {
                        InGameEnemies[l].transform.DOMoveY(-0.5f, timeMove).SetRelative(true)
                            .SetLoops(-1, LoopType.Yoyo).SetEase(ease).SetDelay(l * 0.2f);
                    }
                }

                break;
            case IdleType.Random:
                Vector3 randomPosition;
                for(int i = 0; i < numEnemies; i++) {
                    randomPosition = InGameEnemies[i].transform.position;
                    randomPosition.x += Random.Range(-1f, 1f);
                    randomPosition.y += Random.Range(-1f, 1f);
                    randomPosition.x = Mathf.Clamp(randomPosition.x, -smallSceneSize.x, smallSceneSize.x);
                    randomPosition.y = Mathf.Clamp(randomPosition.y, -smallSceneSize.y, smallSceneSize.y);
                    InGameEnemies[i].transform.DOMove(randomPosition, Random.Range(timeMove, timeMove * 2f))
                        .SetEase(ease).SetLoops(-1, LoopType.Yoyo);
                }

                break;
            case IdleType.Switch:
                Vector3[] array = new Vector3[numEnemies];
                for(int i = 0; i < numEnemies; i++) {
                    array[i] = InGameEnemies[i].transform.position;
                }

                for(int i = 0; i < numEnemies; i++) {
                    InGameEnemies[i].transform
                        .DOMove(array[numEnemies - 1 - i], Random.Range(timeMove, timeMove * 2f))
                        .SetEase(ease).SetLoops(-1, LoopType.Yoyo);
                }

                break;

            case IdleType.RandomSwitch:
                Vector3[] array2 = new Vector3[numEnemies];
                for(int i = 0; i < numEnemies; i++) {
                    array2[i] = InGameEnemies[i].transform.position;
                }

                array2.Shuffle();
                for(int i = 0; i < numEnemies; i++) {
                    InGameEnemies[i].transform
                        .DOMove(array2[numEnemies - 1 - i], Random.Range(timeMove, timeMove * 2f))
                        .SetEase(ease).SetLoops(-1, LoopType.Yoyo);
                }

                break;

            case IdleType.Around:

                for(int num6 = 0; num6 < InGameEnemies.Count; num6++) {
                    if(InGameEnemies[num6] != null) {
                        Sequence sequence = DOTween.Sequence();
                        sequence.Append(InGameEnemies[num6].transform
                            .DOMoveX(-InGameEnemies[num6].transform.position.x + 2f * transform.position.x,
                                timeMove)
                            .SetDelay(Mathf.Abs(InGameEnemies[num6].transform.position.x)));
                        sequence.Append(InGameEnemies[num6].transform
                            .DOMoveY(-InGameEnemies[num6].transform.position.y + 2f * 2.26f,
                                timeMove));
                        sequence.Append(InGameEnemies[num6].transform
                            .DOMoveX(InGameEnemies[num6].transform.position.x, timeMove));
                        sequence.Append(InGameEnemies[num6].transform
                            .DOMoveY(InGameEnemies[num6].transform.position.y, timeMove));
                        sequence.SetLoops(-1, LoopType.Yoyo);
                        InGameEnemies[num6].GetComponent<EnemyBase>().MySequence = sequence;
                        sequences.Add(sequence);
                    }
                }

                break;

            case IdleType.MoveX:
                float num7 = 0.5f;
                for(int num8 = 0; num8 < InGameEnemies.Count; num8++) {
                    if(InGameEnemies[num8] != null) {
                        Sequence sequence = DOTween.Sequence();
                        sequence.Append(InGameEnemies[num8].transform
                            .DOMoveX(InGameEnemies[num8].transform.position.x + num7, timeMove)).SetEase(ease);
                        sequence.Append(InGameEnemies[num8].transform
                            .DOMoveX(InGameEnemies[num8].transform.position.x - num7, timeMove * 2f)).SetEase(ease);
                        sequence.Append(InGameEnemies[num8].transform
                            .DOMoveX(InGameEnemies[num8].transform.position.x - 0.05f, timeMove)).SetEase(ease);
                        sequence.SetLoops(-1, 0);
                        InGameEnemies[num8].GetComponent<EnemyBase>().MySequence = sequence;
                        sequences.Add(sequence);
                    }
                }

                break;
            case IdleType.MoveXY:

                Vector3 moveOnStartOffset = new Vector3(1f, 1f, 0f);
                Vector3 moveOnEndOffset = new Vector3(-1f, -1f, 0f);
                for(int num9 = 0; num9 < InGameEnemies.Count; num9++) {
                    if(InGameEnemies[num9] != null) {
                        Sequence sequence = DOTween.Sequence();
                        sequence.Append(InGameEnemies[num9].transform
                                .DOMove(InGameEnemies[num9].transform.position + moveOnStartOffset, timeMove))
                            .SetEase(ease);
                        sequence.Append(InGameEnemies[num9].transform
                            .DOMove(InGameEnemies[num9].transform.position, timeMove)).SetEase(ease);
                        sequence.Append(InGameEnemies[num9].transform
                                .DOMove(InGameEnemies[num9].transform.position + moveOnEndOffset, timeMove))
                            .SetEase(ease);
                        sequence.Append(InGameEnemies[num9].transform
                            .DOMove(InGameEnemies[num9].transform.position, timeMove)).SetEase(ease);
                        sequence.SetLoops(-1, 0);
                        InGameEnemies[num9].GetComponent<EnemyBase>().MySequence = sequence;
                        sequences.Add(sequence);
                    }
                }

                break;
        }
    }
}
