﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRecycle : MonoBehaviour {
    [SerializeField] private float timeRecycle = 1f;
    private void OnEnable() {
        DOVirtual.DelayedCall(timeRecycle, () => {
            gameObject.Recycle();
        });
    }
}
