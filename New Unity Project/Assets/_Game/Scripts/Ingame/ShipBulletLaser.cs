﻿using DG.Tweening;
using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBulletLaser : ShipBulletBase {
    protected Dictionary<int, IEnumerator> obInside = new Dictionary<int, IEnumerator>();
    [SerializeField] protected float startWidth = 0.2f;
    [SerializeField] protected LayerMask layerDetect = 0;
    [SerializeField] protected LineRenderer lineRenderer = null;
    [SerializeField] protected Transform efHit = null;

    public override void Remove() {
        lineRenderer.positionCount = 0;
        base.Remove();
    }

    protected virtual void FixedUpdate() {
        CreateLaser();
    }


    protected virtual void CreateLaser() {
        var origin = transform.position;
        lineRenderer.positionCount = 2;
        lineRenderer.startWidth = startWidth;
        lineRenderer.SetPosition(0, origin);
        Vector2 startBox = lineRenderer.transform.position;
        Vector2 sizeBox = new Vector2(startWidth * 0.7f, 0.1f);
        var hit = Physics2D.BoxCast(startBox, sizeBox, 0, Vector2.up, 15f, layerDetect);

        if(hit&&hit.transform.root.CompareTag(TagDefine.Enemy)) {
            var hitPoint = hit.point;
            efHit.position = hitPoint;
            lineRenderer.SetPosition(1, hitPoint);
            var idInstance = hit.transform.GetInstanceID();
            var exist = obInside.TryGetValue(idInstance, out var ieDamagePerSec);
            if(exist) {

            }
            else {
                var obj = hit.collider.GetComponent<ITakeDamage>();
                if(obj != null) {
                    ieDamagePerSec = TimeDamagePerSeconds(obj);
                    obInside.Add(idInstance, ieDamagePerSec);
                    StartCoroutine(ieDamagePerSec);
                }
            }
        }
        else {
            var hitPoint = new Vector3(origin.x, maxSize.y, origin.z);
            efHit.position = hitPoint;
            lineRenderer.SetPosition(1, hitPoint);
            foreach(var item in obInside.Values) {  StopCoroutine(item); }
            obInside.Clear();
        }
    }

    public void SetStartWith(float v) {
        startWidth = v;
    }

    protected IEnumerator TimeDamagePerSeconds(ITakeDamage obj) {
        while(true) {
            obj?.TakeDamage(10);
            yield return Yielder.Wait(1f);
        }
    }
}
