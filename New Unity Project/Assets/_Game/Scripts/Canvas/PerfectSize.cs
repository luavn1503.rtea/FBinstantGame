﻿//using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerfectSize : MonoBehaviour {
    public void AutoSize(Vector2 size, FollowType follow = FollowType.X) {
        var rect = GetComponent<RectTransform>();
        if(rect == null) { return; }
        switch(follow) {
            case FollowType.X:
                var ratioX = rect.sizeDelta.x/size.x;
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, ratioX * size.y);
                break;
            case FollowType.Y:
                var ratioY = rect.sizeDelta.y/size.y;
                rect.sizeDelta = new Vector2(ratioY * size.x, rect.sizeDelta.y);
                break;
        }
    }

    public enum FollowType {
        X, Y
    }
}
