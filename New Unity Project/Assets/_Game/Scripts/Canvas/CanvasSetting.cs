﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasSetting : MonoBehaviour {

	[SerializeField] private CanvasScaler _scaler;
	private float _width;
	private float _height;

	void Reset() {
		_scaler = transform.GetComponent<CanvasScaler>();
	}

	void Awake() {
		_width = Camera.main.pixelWidth;
		_height = Camera.main.pixelHeight;
        SetMathch();

    }

    void Start() {
	}

	void SetMathch() {
		float ratio = _width / _height;
        float limit = 10f / 16f;

        //Debug.Log(limit);
        _scaler.matchWidthOrHeight = ratio <= limit ? 0 : 1;
        //_scaler.matchWidthOrHeight = ratio;
		//Debug.Log(SystemInfo.deviceUniqueIdentifier);
		//e97f1e72bdd0a948ab72ed6387c05ff8
	}
}
