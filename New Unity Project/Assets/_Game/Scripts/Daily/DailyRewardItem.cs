﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewardItem : MonoBehaviour {
    private DailyRewardPage dailyRewardPage = null;
    [HideInInspector] public int dayIndex = 0;
    [HideInInspector] public DailyRewardInfor hardData = null;
    [Header("Background")]
    [SerializeField] private Image imgIcon = null;
    [SerializeField] private TextMeshProUGUI txtNameDay;
    [SerializeField] private TextMeshProUGUI txtAmount;
    [SerializeField] private GameObject goSelected = null;
    [SerializeField] private GameObject goClaimed  = null;

    [SerializeField] private ButtonAnimation btaClaim;


    private DailyRewardPage dailyRewardPage1;
    private UserDailyRewardInfor userDailyRewardInfor;
    public void Initialize(DailyRewardPage dailyRewardPage, int dayIndex, DailyRewardInfor hardData) {
        this.dayIndex = dayIndex;
        this.dailyRewardPage = dailyRewardPage;
        this.hardData = hardData;
        UpdateUI();
    }

    public void UpdateUI() {
        Extention.SetSpriteImage(imgIcon, hardData.Icon);
        Extention.SetText(txtAmount, hardData.Quantity.ToString());
        Extention.SetText(txtNameDay, string.Format("DAY {0}", dayIndex + 1));
        Extention.SetActiveObject(goClaimed, ManagerData.Instance.dailyRewardInfor.IsDayClaimed(dayIndex));
        Extention.SetActiveObject(goSelected, ManagerData.Instance.dailyRewardInfor.IsDayCliam(dayIndex));
    }

    public void OnClaimed() {
        UpdateUI();
    }
}



public enum StatusDailyReward {
    Claimed, CanClaim, WillClaim
}