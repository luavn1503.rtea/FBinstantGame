﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gemmob.Common;
using UnityEngine;

[Serializable]
public class UserDailyRewardInfor {
    private string keyDailyRewards = "dailyRewardsData";
    [SerializeField] private int curDay = 0;
    [SerializeField] private long curTime = long.MinValue;

    public UserDailyRewardInfor() {
        curDay = 0; ;
        curTime = DateTime.MinValue.Ticks;
    }

    public UserDailyRewardInfor LoadData() {
        UserDailyRewardInfor data = this;
        if(PlayerPrefs.HasKey(keyDailyRewards)) {
            var jsonData = PlayerPrefs.GetString(keyDailyRewards);
            data = JsonUtility.FromJson<UserDailyRewardInfor>(jsonData);

        }
        else {
            Save();
        }
        return data;
    }
    public void Save() {
        PlayerPrefs.SetString(keyDailyRewards, JsonUtility.ToJson(this));
    }


    public bool HasRewards(out int day) {
        day = -1;
        var timeNow = DateTime.Now.Ticks;
        var sleepTime = timeNow - curTime;
        TimeSpan timeSpan = new TimeSpan(sleepTime);
        if(timeSpan.Ticks >= TimeSpan.TicksPerDay) {
            day = curDay;
            return true;
        }
        else {
            return false;
        }
    }

    public void OnClaimed() {
        curDay += 1;
        curTime = DateTime.Now.Ticks;
        Save();
    }

    public bool IsDayCliam(int dayIndex) {
        var timeNow = DateTime.Now.Ticks;
        var sleepTime = timeNow - curTime;
        TimeSpan timeSpan = new TimeSpan(sleepTime);
        if(timeSpan.Ticks >= TimeSpan.TicksPerDay) {
            return dayIndex == curDay;
        }
        return false;
    }

    public bool IsDayClaimed(int dayIndex) {
        return dayIndex < curDay;
    }

}
