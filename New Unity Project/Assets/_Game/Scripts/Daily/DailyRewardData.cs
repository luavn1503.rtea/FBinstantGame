﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DailyRewardData", menuName = "Datas/DailyRewardData")]
public class DailyRewardData : ScriptableObject {

    private static DailyRewardData instance;
    public static DailyRewardData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<DailyRewardData>("DailyRewardData");
            }
            return instance;
        }
    }

    public List<DailyRewardInfor> Items = new List<DailyRewardInfor>();
    public List<DailyRewardInfor> Rookies = new List<DailyRewardInfor>();
    public List<DailyRewardInfor> AfterRookies = new List<DailyRewardInfor>();
}

[Serializable]
public class DailyRewardInfor {
    public Sprite Icon;
    public Sprite CategoryIcon;
    public int Quantity = 1;
    public AllRewardType RewardType;
    public int ExtendId;
    public bool IsSpecial;
}
