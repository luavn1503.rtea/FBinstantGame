﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeMainPanel : MonoBehaviour {
    public _2dxFX_ColorChange ShipUpgradeIcon;
    public _2dxFX_ColorChange DroneUpgradeIcon;

    public SkeletonGraphic ShipIcon;
    //public SkeletonGraphic ShipShadow;
    public SkeletonGraphic DroneIcons;
    //public SkeletonGraphic DroneShadow;
    public SkeletonGraphic DroneIcons2;
    //public SkeletonGraphic DroneShadow2;

    public SkeletonDataAsset[] ShipAniData;
    public SkeletonDataAsset[] DroneAniData;

    //[Header("For card equip")]
    //public Sprite BaseCardSprite;
    //public Image IconImage;
    //public Image BoundImage;

    void OnEnable() {
        PlayerInfo.AfterLoadData += LoadIcon;
        GameEvent.OnGemChanged += UpdateIcon;
        GameEvent.OnGoldChanged += UpdateIcon;
        LoadIcon();
    }

    void OnDisable() {
        PlayerInfo.AfterLoadData -= LoadIcon;
        GameEvent.OnGemChanged -= UpdateIcon;
        GameEvent.OnGoldChanged -= UpdateIcon;
    }

    void Start() {
        LoadIcon();
    }

    public void LoadIcon() {
        var playerData = ManagerData.Instance.PlayerInfo;


        //DroneIcons.transform.parent.gameObject.SetActive(playerData.IndexDrone != -1);

        if(DroneIcons.gameObject.activeInHierarchy) {
            //int curDroneRank = ManagerData.Instance.CurrentAircraftData.Drones[playerData.IndexDrone].Rank;
            //DroneIcons.skeletonDataAsset = DroneSkins[playerData.IndexDrone >= DroneSkins.Length ? DroneSkins.Length - 1 : playerData.IndexDrone];
            //DroneIcons.skeletonDataAsset = DroneShadow.skeletonDataAsset =
            //	DroneAniData[playerData.IndexDrone >= DroneAniData.Length ? DroneAniData.Length - 1 : playerData.IndexDrone];
            //DroneIcons.initialSkinName = DroneShadow.initialSkinName = string.Format("Skin_Lv{0}", curDroneRank - 1);
            DroneIcons.Initialize(true);
            //DroneShadow.Initialize(true);

            //int curDroneRank1 = ManagerData.Instance.CurrentAircraftData.Drones[playerData.IndexDrone].Rank;
            ////DroneIcons.skeletonDataAsset = DroneSkins[playerData.IndexDrone >= DroneSkins.Length ? DroneSkins.Length - 1 : playerData.IndexDrone];
            //DroneIcons2.skeletonDataAsset = DroneShadow2.skeletonDataAsset =
            //	DroneAniData[playerData.IndexDrone >= DroneAniData.Length ? DroneAniData.Length - 1 : playerData.IndexDrone];
            //DroneIcons2.initialSkinName = DroneShadow.initialSkinName = string.Format("Skin_Lv{0}", curDroneRank - 1);
            //DroneIcons2.Initialize(true);
            //DroneShadow2.Initialize(true);
        }

        UpdateIcon(0);

        //DroneIcons2.transform.parent.gameObject.SetActive(playerData.IndexDrone != -1);
        if(DroneIcons2.gameObject.activeInHierarchy) {
            //int curDroneRank1 = ManagerData.Instance.CurrentAircraftData.Drones[playerData.IndexDrone].Rank;
            //DroneIcons.skeletonDataAsset = DroneSkins[playerData.IndexDrone >= DroneSkins.Length ? DroneSkins.Length - 1 : playerData.IndexDrone];
            //DroneIcons2.skeletonDataAsset = DroneShadow2.skeletonDataAsset =
            //DroneAniData[playerData.IndexDrone >= DroneAniData.Length ? DroneAniData.Length - 1 : playerData.IndexDrone];
            //DroneIcons2.initialSkinName = DroneShadow.initialSkinName = string.Format("Skin_Lv{0}", curDroneRank1 - 1);
            DroneIcons2.Initialize(true);
            //DroneShadow2.Initialize(true);
        }
        UpdateIcon(1);

        var cardInfo = playerData.GetCardInfo;

        if(cardInfo != null && DataConfig.Instance) {
            //BoundImage.sprite = DataConfig.Instance.BoundRewardIconCards[cardInfo.NumberStarShow - 1];
            //BoundImage.color = DataConfig.Instance.BoundCardColor[cardInfo.NumberStarShow - 1];
            //IconImage.sprite = cardInfo.IconShow;
        }
        else if(DataConfig.Instance) {
            //BoundImage.sprite = DataConfig.Instance.BoundRewardIconCards[0];
            //BoundImage.color = DataConfig.Instance.BoundCardColor[0];
            //IconImage.sprite = BaseCardSprite;
        }
    }

    public void UpdateIcon(int number) {
        //var playerData = ManagerData.Instance.PlayerInfo;
        //var shipData = ManagerData.Instance.ShipsUpgradeData.Ships;
        //var currentShipLevel = ManagerData.Instance.CurrentAircraftData.Ships[playerData.IndexShip].Level;
        //int curShipRank = ManagerData.Instance.CurrentAircraftData.Ships[playerData.IndexShip].Rank;

        ////ShipIcon.skeletonDataAsset = ShipSkins[playerData.IndexShip >= ShipSkins.Length ? ShipSkins.Length - 1 : playerData.IndexShip];
        ////ShipIcon.skeletonDataAsset = ShipShadow.skeletonDataAsset =
        ////	ShipAniData[playerData.IndexShip >= ShipAniData.Length ? ShipAniData.Length - 1 : playerData.IndexShip];
        ////ShipIcon.initialSkinName = ShipShadow.initialSkinName = string.Format("Skin_Lv{0}", curShipRank - 1);
        //ShipIcon.Initialize(true);
        ////ShipShadow.Initialize(true);

        //if(currentShipLevel < 0)
        //    return;

        //bool activeUpgradeShip = currentShipLevel >= shipData[playerData.IndexShip].Levels.Count ? false :
        //    playerData.CheckGold(shipData[playerData.IndexShip].Levels[currentShipLevel].Coin) ||
        //    playerData.CheckGem(shipData[playerData.IndexShip].Levels[currentShipLevel].Gem);
        //ActiveIconUpgrade(ShipUpgradeIcon.gameObject, activeUpgradeShip);
        //if(playerData.IndexDrone >= 0)
        //    if(DroneIcons.gameObject.activeInHierarchy) {
        //        var droneData = ManagerData.Instance.DronesUpgradeData.Drones;
        //        var currentDroneLevel = ManagerData.Instance.CurrentAircraftData.Drones[playerData.IndexDrone].Level;

        //        bool activeUpgradeDrone = currentDroneLevel >= droneData[playerData.IndexDrone].Levels.Count
        //            ? false
        //            : playerData.CheckGold(droneData[playerData.IndexDrone].Levels[currentDroneLevel].Coin) ||
        //              playerData.CheckGem(droneData[playerData.IndexDrone].Levels[currentDroneLevel].Gem);
        //        ActiveIconUpgrade(DroneUpgradeIcon.gameObject, activeUpgradeDrone);
        //    }
        //    else {
        //    }
        //else
        //    DroneUpgradeIcon._Saturation = 0;
    }
    private void ActiveIconUpgrade(GameObject icon, bool active) {
        icon.gameObject.SetActive(active);
    }
}
