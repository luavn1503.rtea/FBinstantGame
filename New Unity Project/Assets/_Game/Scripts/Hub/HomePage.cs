﻿using System;
using System.Collections;
using Gemmob.Common;
using UnityEngine;
using UnityEngine.UI;
using Polaris.Tutorial;
using UnityEngine.Events;
using System.Linq;
using Gemmob;
//using Boo.Lang;

public partial class HomePage : Frame {
    [SerializeField] private ButtonAnimation _btaStartGame;

    [SerializeField] private ButtonAnimation _btaActiveSkill;
    [SerializeField] private ButtonAnimation _btaTalenSkill;

    [SerializeField] private ButtonAnimation _btaSetting;
    [SerializeField] private ButtonAnimation _btaDaily;
    [SerializeField] private ButtonBase _btnChest;
    [SerializeField] private ButtonAnimation _btaProfile;
    [SerializeField] private ButtonAnimation _btaExitGame;
    [SerializeField] private ShipPositionPreviewItem[] shipPositionPreviews;

    [Header("Pack")]
    [Space(20)]
    [SerializeField] private ButtonAnimation _btaBlackPanel;
    [SerializeField] private ButtonAnimation _btaUSAPack;
    [SerializeField] private Transform _rightPanel;

    [Header("Vip")]
    [SerializeField] private ButtonAnimation _btaVip;

    [Header("Watch ads")]
    [SerializeField] private ButtonAnimation _btaWatchAds;
    [SerializeField] private Text _txtCountDown;


    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        AssignActionToButton();
        SetAvatar();
        CountDown();
        foreach(var item in shipPositionPreviews) { item.Initialize(); }
    }

    private void ButtonSettingClicked() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<SettingPopup>();
    }
    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        MainCamera.Instance.main.orthographic = true;
        foreach(var item in shipPositionPreviews) { item.UpdateUI(); }
    }

    protected override void OnHide(Action onCompleted = null, bool instant = false) {
        MainCamera.Instance.main.orthographic = false;
        base.OnHide(onCompleted, instant);
    }

    private void AssignActionToButton() {
        _btaActiveSkill.onClick.AddListener(ButtonActiveSkill);
        _btaTalenSkill.onClick.AddListener(ButtonTalenSkill);

        _btaSetting.onClick.AddListener(ButtonSettingClicked);
        _btaStartGame.onClick.AddListener(ButtonPlayClicked);
        _btaDaily.onClick.AddListener(ButtonDailyRewardClicked);

        _btaExitGame.onClick.AddListener(() => ExitGame(true));
        _btnChest.onClick.AddListener(ButtonChest);

    }

    private IEnumerator ShowOrNotPackSale() {
        yield return new WaitUntil(() => RemoteConfigExtra.RemoteFetched == true);
    }

    private void ButtonChest() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<ChestPage>();
    }

    private void ButtonPlayClicked() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<SelectLevelPage>();
    }


    public void ButtonSpinClicked() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<SpinPage>();
    }

    public void ButtonDailyRewardClicked() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<DailyRewardPage>();
    }

    public void OpenShop(bool isShip) {
        //if (UIManager.IsLockClick)
        //	return;

        //if (isShip)
        //	UIManager.Instance.ShopPage.OpenAircraftShop();
        //else
        //	UIManager.Instance.ShopPage.OpenDroneShop();

        //SoundController.Instance.RunClickSound();
        //UIManager.Instance.ShopPage.ShowPage();
        UIHomeManager.Instance.Show<ShopPage>();
    }

    public void OpenTreasure() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<TreasurePage>();
    }

    public void ButtonActiveSkill() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<EquipmentPage>().SetupEquipment(EnumDefine.TypeItem.Skill, EnumDefine.Direct.Left);
    }

    public void ButtonTalenSkill() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<EquipmentPage>().SetupEquipment(EnumDefine.TypeItem.Skill, EnumDefine.Direct.Right);
    }

    public void OpenMarket() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<ShopPage>();
    }

    public void OpenStorage() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<StoragePage>();
    }

    private void ExitGame(bool state) {
        if(state) {
            Application.Quit();
        }
        else {
            
        }
    }
    public void ButtonExit() {
        Application.Quit();
    }

    void OpenVipPopup() {

    }

    void OnWatchAdsClick() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        //UIHomeManager.Instance.Show<ShopPage>();
    }

    public void SetAvatar() {
        //_btaProfile.GetComponent<Image>().sprite = ManagerData.Instance.AvatarData
        //    .AvatarInfor[ManagerData.Instance.PlayerInfo.PlayerProfile.AvatarID].AvatarIcon;
    }

    #region for sale pack

    public void ShowPack(Transform pack) {
        _btaBlackPanel.gameObject.SetActive(true);
        pack.SetParent(_btaBlackPanel.transform);
    }

    public void TurnOffPack() {
        //_btaBlackPanel.gameObject.SetActive(false);
        //_packController.transform.SetParent(_rightPanel);
        //_packController.TurnOffSale();
    }

    #endregion

    #region Watch ads

    void CountDown() {
        //LocalCountDown reward = CountDownController.Instance.CountDownHomeRandomReward;
        //reward.StartCountDown(false);
        //if(reward.Done) {
        //    SetBtnWatchAds(true, 0);
        //    //_txtCountDown.gameObject.SetActive(false);
        //    _txtCountDown.text = "Free Gift";
        //}
        //else {
        //    //_txtCountDown.gameObject.SetActive(true);
        //    SetBtnWatchAds(false, 1);
        //    reward.OnTick += () => { _txtCountDown.text = reward.GetTime(); };
        //    reward.OnComplete += () => {
        //        SetBtnWatchAds(true, 0);
        //        //_txtCountDown.gameObject.SetActive(false);
        //        _txtCountDown.text = "Free Gift";
        //    };
        //}
    }

    public void SetBtnWatchAds(bool interactable, float grayScale) {
        //_btaWatchAds.interactable = interactable;
        //   _btaWatchAds.GetComponent<GiveAwayEvent>().SetBtnWatchAds(interactable, grayScale);
    }

    public void ResetWatchAds() {
        LocalCountDown reward = CountDownController.Instance.CountDownHomeRandomReward;
        reward.StartCountDown();
        //_txtCountDown.gameObject.SetActive(true);
        SetBtnWatchAds(false, 1);
        reward.OnTick += () => { _txtCountDown.text = reward.GetTime(); };
        reward.OnComplete += () => {
            SetBtnWatchAds(true, 0);
            //_txtCountDown.gameObject.SetActive(false);
            _txtCountDown.text = "Free Gift";
        };
    }

    #endregion
}
