﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpinItem : MonoBehaviour {
    [SerializeField] private Image imgIcon = null;
    [SerializeField] private TextMeshProUGUI txtAmount = null;
    [HideInInspector] public SpinItemData hardData = null;
    public void Initialize(SpinItemData hardData) {
        this.hardData = hardData;
        Extention.SetSpriteImage(imgIcon, hardData.Icon);
        Extention.SetText(txtAmount, hardData.Number.ToString());
    }
}
