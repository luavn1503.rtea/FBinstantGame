﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using DG.Tweening.Core;
using Gemmob.Common;
using Random = UnityEngine.Random;
using Gemmob;
using DG.Tweening.Plugins.Options;
using UnityEditor;
using System.Collections.Generic;
//using Boo.Lang;

public class SpinPage : Frame {
    private bool isSpinning = false;
    private Tween tweenSpin = null;
     private List<SpinItem> listItem = null;
    [SerializeField] private ButtonAnimation btnSpin;
    [SerializeField] private RectTransform spin;
    [SerializeField] private SpinItem itemPrefab = null;
    [SerializeField] private RectTransform contents = null;

    [SerializeField] ButtonAnimation btnVideo;
    [SerializeField] Text timeFree;
    [SerializeField] Text timeVideo;
   
    private bool isHasVideo = false;
    [SerializeField] private ButtonAnimation backSpin;
    private LocalCountDown CountDownFree;
    private LocalCountDown CountDownVideo;

    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        LoadItem();
        btnSpin.onClick.AddListener(RunSpin);
    }
    private void ChangeStateBtnSpin(ButtonAnimation btnSpin, bool availb) {
        if(availb) {
            btnSpin.GetComponent<Image>().color = Color.white;
            btnSpin.interactable = true;
        }
        else {
          //  btnSpin.GetComponent<Image>().color = Color.gray;
           // btnSpin.interactable = false;
        }
    }
    private void LoadItem() {
        listItem = new List<SpinItem>();
        var data = SpinData.Instance.Items;
        for(int i = 0; i < data.Count; i++) {
            var itemCreate = itemPrefab.Spawn(contents);
            itemCreate.transform.localScale = Vector3.one;
            itemCreate.transform.localPosition = Vector3.one;
            itemCreate.transform.localEulerAngles = new Vector3(0f, 0f, -i * 36f);
            itemCreate.Initialize(data[i]);
            listItem.Add(itemCreate);
        }
    }



    private void RunSpin() {
        tweenSpin.Kill();

        var data = SpinData.Instance.Items;
        Vector3 vtAngle = Vector3.zero;
        Vector3 vtAngleWheel2 = Vector3.zero;
        var deltaAngle = 360 / data.Count;
        var angle_default = 360;

        int index = GetRandomIndex(data.Select(spinData => spinData.Rate).ToArray());
        int indexMulti = GetRandomIndex(new[] {80, 15, 5});

        isSpinning = true;
        //Rotate Wheel 1
        tweenSpin = DOTween.To(value => {
            vtAngle.z = value;
            spin.eulerAngles = vtAngle;
        }, spin.eulerAngles.z, index * deltaAngle - angle_default * 3, 2.0f)
            .SetEase(Ease.InSine).OnComplete(
                () => {
                    DOTween.To(value => {
                        vtAngle.z = value;
                        spin.eulerAngles = vtAngle;
                    }, spin.eulerAngles.z, (index * deltaAngle - angle_default * 6), 5.0f).SetEase(Ease.OutQuad).OnComplete(
                        () => {
                            isSpinning = false;
                            Logs.Log("Done");
                        });
                });
        //CheckSpinFree();
        RunSpinVideo();
        SetCountDownFreeAndVideoAction();
    }




        private int GetRandomIndex(int[] percent) {
        int numberRandom = Random.Range(0, 100);

        int indexItem;
        for(indexItem = 0; indexItem < percent.Length; indexItem++) {
            if(numberRandom < percent[indexItem])
                break;
            numberRandom -= percent[indexItem];
        }

        return indexItem;
    }
   
    //private bool CheckSpinFree() {
    //    if(!CountDownFree.Done && !(CountDownVideo.Done && isHasVideo)) {
    //        return false;
    //    }
            
    //    if(isSpinning) {
    //        return false;
    //    }
    //    if(CountDownFree.Done) {

    //        CountDownFree.StartCountDown();
    //    }
    //    return true;
    //}
    private void SetCountDownFreeAndVideoAction() {
      

      //  CountDownVideo.OnTick += CheckVideo;

       // CountDownFree.OnTick += CheckFree;
    }
    private void RunSpinVideo() {
       // if(!CountDownVideo.Done || !isHasVideo)
          //  return;
        ManagerData.Instance.DontLoadAds();
        bool isSkipFailVideo = false;

        if(isSkipFailVideo) {
            return;
        }
       // CountDownVideo.StartCountDown();
        ChangeStateBtnSpin(btnVideo, false);
    }
    private void CheckVideo() {
        if(isHasVideo) {
            timeVideo.text = "" + CountDownVideo.GetTime();

        }
    }
    private void CheckFree() {
        timeFree.text = "Next free in " + CountDownFree.GetTime();
    }
    public bool CheckCanSpin() {
        if(!CountDownController.Instance) {
            return false;
        }

        CountDownVideo = CountDownController.Instance.countDownVideo;
        CountDownFree = CountDownController.Instance.countDownFree;


        return CountDownFree.Done || ((Mediation.Instance.HasRewardVideo || isHasVideo) && CountDownVideo.Done);
    }

}
