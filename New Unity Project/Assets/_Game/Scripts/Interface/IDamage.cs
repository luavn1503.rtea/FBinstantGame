﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamage {
    void OnEnterColliderDamage(Collider2D collision);
    void OnExitColliderDamage(Collider2D collision);
}
