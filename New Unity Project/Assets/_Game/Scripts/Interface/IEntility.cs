﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEntility   {

    void StartEntility();

    void RemoveEntility();
}
