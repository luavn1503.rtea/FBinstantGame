﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoriaPanel : Frame {
    [SerializeField] private ButtonBase btnX2Rewards = null;
    [SerializeField] private ButtonBase btnReplay = null;
    [SerializeField] private ButtonBase btnMap = null;
    [SerializeField] private ButtonBase btnNextLevel = null;
    [SerializeField] private RewardsPanel rewardsPanel = null;

    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        rewardsPanel.Initialize();
        btnX2Rewards.onClick.AddListener(ButtonX2Rewards);
        btnReplay.onClick.AddListener(ButtonReplay);
        btnMap.onClick.AddListener(ButtonMap);
        btnNextLevel.onClick.AddListener(ButtonNextLevel);
    }

    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        ManagerData.Instance.PlayerInfo.UpgradeLevelUnlock();
        var levelSelect = ManagerGame.Instance.levelSelected;
        var data = ManagerData.Instance.GetLevelData(levelSelect + 1);
        string strTitle = string.Format("LEVEL {0}", levelSelect + 1);
        EnumDefine.IDShip idShip = (EnumDefine.IDShip)data.EvolveShipId;
        EnumDefine.IDShip idDrone = (EnumDefine.IDShip)(data.EvolveShipId + 8);
        RewardData rewardCoin = new RewardData(EnumDefine.RewardsType.Currency, data.CoinReward, EnumDefine.IDCurrency.Coin);
        RewardData rewardDebrisShip = new RewardData(EnumDefine.RewardsType.Debris, data.EvolveShipNum, idShip: idShip);
        RewardData rewardDebrisDrone = new RewardData(EnumDefine.RewardsType.Debris, data.EvolveDroneNum, idShip: idDrone);
        RewardsData rewards = new RewardsData();
        rewards.data.Add(rewardCoin);
        rewards.data.Add(rewardDebrisShip);
        rewards.data.Add(rewardDebrisDrone);
        rewardsPanel.ShowRewards(rewards);
        base.OnShow(onCompleted, instant);
    }

    private void ButtonX2Rewards() {

    }

    private void ButtonReplay() {

    }

    private void ButtonMap() {
        ManagerScene.Instance.Load(IDScene.Home);
    }

    private void ButtonNextLevel() {
        ManagerGame.Instance.levelSelected += 1;
        ManagerScene.Instance.Load(IDScene.Ingame);
    }
}
