﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefeatPanel : Frame {
    private List<RewardItem> Items = new List<RewardItem>();
    [SerializeField] private Image imgProcess = null;
    [SerializeField] private ButtonBase btnX2Rewards = null;
    [SerializeField] private ButtonBase btnReplay = null;
    [SerializeField] private ButtonBase btnMap = null;
    [SerializeField] private ButtonBase btnUpgrade = null;
    [SerializeField] private RectTransform contentsItem = null;
    [SerializeField] private RewardItem itemPrefab = null;


    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        GenererRewards(null);
        btnX2Rewards.onClick.AddListener(ButtonX2Rewards);
        btnReplay.onClick.AddListener(ButtonReplay);
        btnMap.onClick.AddListener(ButtonMap);
        btnUpgrade.onClick.AddListener(ButtonUpgrade);
    }


    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
    }

    private void GenererRewards(List<RewardData> rewards) {
        for(int i = 0; i < 3; i++) {
            var itemCreate = itemPrefab.Spawn(contentsItem);
            itemCreate.transform.localScale = Vector3.one;
            itemCreate.transform.localPosition = Vector3.zero;
            Items.Add(itemCreate);
        }
    }

    private void ButtonX2Rewards() {

    }

    private void ButtonReplay() {

    }

    private void ButtonMap() {
        ManagerScene.Instance.Load(IDScene.Home);
    }

    private void ButtonUpgrade() {
        ManagerScene.Instance.Load(IDScene.Home);
    }
}
