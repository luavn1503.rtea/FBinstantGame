﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameShipInfo : MonoBehaviour {

    [SerializeField] private List<ActiveBase> levelsActive = new List<ActiveBase>();

    public void ChangeLevel(int level) {
        for(int i = 0; i < levelsActive.Count; i++) {
            if (i < level) {
                levelsActive[i].Active(true);
            }
            else {
                levelsActive[i].Active(false);
            }
        }
    }
}
