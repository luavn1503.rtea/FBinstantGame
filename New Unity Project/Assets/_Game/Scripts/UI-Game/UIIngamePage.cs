﻿using DG.Tweening;
using Gemmob;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIIngamePage : Frame
{
    [SerializeField] private ButtonBase btnPause = null;
    [SerializeField] private ButtonBase btnSkillShip = null;
    [SerializeField] private ButtonBase btnSkillActive = null;
    [Space, Header("Notify EndWave")]
    [SerializeField] private Transform tfNotify = null;
    [SerializeField] private SkeletonGraphic skeNotify = null;
    [SerializeField, SpineAnimation] private string keyWarning = string.Empty;
    [SerializeField, SpineAnimation] private List<string> listNotifyEndWave = new List<string>();
    [Space, Header("Notify Wave")]
    [SerializeField] private RectTransform rectWave = null;
    [SerializeField] private TextMeshProUGUI txtWave = null;
    [Header("BossHPBar")]
    [SerializeField] private HpBarManager bossHpBar;
    [Space, Header("ShipInfo")]
    [SerializeField] private IngameShipInfo shipInfo = null;

    protected override void OnInitialize(HUD hud)
    {
        base.OnInitialize(hud);
        tfNotify.localScale = Vector3.zero;
        var xReference = MainCanvasScaler.Instance.main.referenceResolution.x;
        rectWave.anchoredPosition = new Vector2(-xReference, rectWave.anchoredPosition.y);
        btnPause.AddListener(ButtonPause);
        btnSkillShip.AddListener(ButtonSkillShip);
        btnSkillActive.AddListener(ButtonSkillActive);
    }

    private void ButtonPause()
    {
        UIIngameManager.Instance.Show<PausePanel>();
    }

    private void ButtonSkillShip()
    {
        ManagerPlayer.Instance.ActiveSkill(EnumDefine.IDSkill.Drone);

    }

    private void ButtonSkillActive()
    {
        ManagerPlayer.Instance.ActiveSkill(EnumDefine.IDSkill.Shield);
    }
    public float ShowNotifyEndWave()
    {
        if (skeNotify == null) { return 0f; }
        tfNotify.localScale = Vector3.one;
        int random = UnityEngine.Random.Range(0, listNotifyEndWave.Count);
        var timeLive = skeNotify.SetAnimation(listNotifyEndWave[random]);
        DOVirtual.DelayedCall(timeLive, () =>
        {
            DOVirtual.DelayedCall(5f, () => { tfNotify.localScale = Vector3.zero; });
        });
        return timeLive;
    }

    public float ShowNotifyWarning()
    {
        if (skeNotify == null) { return 0f; }
        tfNotify.localScale = Vector3.one;
        var timeLive = skeNotify.SetAnimation(keyWarning);
        DOVirtual.DelayedCall(timeLive, () =>
        {
            tfNotify.localScale = Vector3.zero;
        });
        return timeLive;
    }

    public void ShowNotifyWave(int totalWave, int currentWave)
    {
        txtWave.text = string.Format("Wave {0}/{1}", currentWave + 1, totalWave);
        var xReference = MainCanvasScaler.Instance.main.referenceResolution.x;
        rectWave.anchoredPosition = new Vector2(-xReference, rectWave.anchoredPosition.y);
        rectWave.DOAnchorPosX(0f, 1f).SetEase(Ease.Flash).OnComplete(() =>
        {
            DOVirtual.DelayedCall(0.2f, () =>
            {
                rectWave.DOAnchorPosX(xReference, 0.6f).SetEase(Ease.Flash);
            });
        });
    }

    public HpBarManager GetHpBarManager()
    {
        return bossHpBar;
    }

    public void SetLevelShip(int level)
    {
        shipInfo.ChangeLevel(level);
    }
}
