﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Polaris.Tutorial {
	public class TutorialPointer : MonoBehaviour {
		[SerializeField] private float _secondsForOneLength = 20;
		[SerializeField] private float _distance = 1;
		private Vector2 _rootPos;
		private Vector2 _front;
		private Vector2 _back;
		private float _baseAngle;

		void Awake() {
			_baseAngle = transform.localRotation.eulerAngles.z;
		}

		public void SetRootPos(PointerPos pointerPos, Vector2 pointedObjPos) {
			Active(true);
			transform.SetAsLastSibling();
			float radAngle = Mathf.Abs(_baseAngle) * Mathf.Deg2Rad;
			Vector2 tempRootPos = new Vector2(pointedObjPos.x - 1 * Mathf.Sin(radAngle),
				pointedObjPos.y + 1 * Mathf.Cos(radAngle));
			switch (pointerPos) {
				case PointerPos.TopRight:
					transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, _baseAngle);
					_rootPos = tempRootPos;
					break;
				case PointerPos.BottomLeft:
					transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y,
						_baseAngle + 180);
					_rootPos = new Vector2(1.75f * pointedObjPos.x - tempRootPos.x, 1.75f * pointedObjPos.y - tempRootPos.y);
					break;
			}
			_back = new Vector2(_rootPos.x - _distance * Mathf.Sin(radAngle), _rootPos.y + _distance * Mathf.Cos(radAngle));
			_front = new Vector2(_rootPos.x + _distance * Mathf.Sin(radAngle), _rootPos.y - _distance * Mathf.Cos(radAngle));
		}

		public void Active(bool active) {
			gameObject.SetActive(active);
		}

		void DoMove() {
			//_back = new Vector2(_rootPos.x - _distance * Mathf.Sin(_angle), _rootPos.y + _distance * Mathf.Cos(_angle));
			//_front = new Vector2(_rootPos.x + _distance * Mathf.Sin(_angle), _rootPos.y - _distance * Mathf.Cos(_angle));
			transform.position = Vector3.Lerp(_front, _back,
				Mathf.SmoothStep(0f, 1f, Mathf.PingPong(Time.unscaledTime / _secondsForOneLength, 1f)));
		}

		void Update() {
			DoMove();
		}
	}

	public enum PointerPos {
		TopRight, BottomLeft
	}
}

