﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftBase : EnemyBase
{
    [SerializeField] private Coin coinPref;
    [SerializeField] private Vector2 direct;
    [SerializeField] private float moveSpeed;
    private Vector2 maxSize = Vector2.zero; 


    public void SetMoveSpeed(float moveSpeed) {
        this.moveSpeed = moveSpeed;
    }
    protected override void Awake() {
        enemyData = EnemiesData.Instance.Gifts[ID];
    }

    public override void TakeDamage(float damage) {
        curHP -= damage;
        if(curHP <= 0 && IsLive) {
            SpawnCoin();
            EnemyDie();
        }
    }

    public override void EnemyDie() {
        enemyCol2D.OnEnemyDie();
        if(IsLive) {
            IsLive = false;
            SpawnDieEffect();
            ManagerEnemies.Instance.EnemyDeath(transform.position);
            GamePlayState.Instance.AddInGameScore(GetEnemyData().Score);
            gameObject.Recycle();
        }
    }

    private void Update() {
        transform.Translate(direct * Time.deltaTime * moveSpeed);
        CheckOutScene();
    }

    protected void CheckOutScene() {
        Vector2 origin = transform.position;
        if(Mathf.Abs(origin.x) >= maxSize.x + 2f || Mathf.Abs(origin.y) >= maxSize.y + 2f) { EnemyDie(); }
    }

    protected override void OnValidate() {
        //Get data Enemy
        enemyData = EnemiesData.Instance.Gifts[ID];
        rg2D = GetComponent<Rigidbody2D>();
        eMove = GetComponent<EnemyMoveBase>();
        enemyCol2D = GetComponentInChildren<EnemyCollider2D>();
    }

    public override void Init(float scaleHp) {
        maxSize = MainCamera.Instance.max;
        moveSpeed = moveSpeed;
        base.Init(scaleHp);
    }

    private void SpawnCoin() {
        int a = UnityEngine.Random.Range(5,12);
        for(int i = 0; i <= a; i++) {
            var coin = coinPref.Spawn();
            Vector2 offset = new Vector2(UnityEngine.Random.Range(-0.3f,0.3f),UnityEngine.Random.Range(0,0.2f));
            coin.transform.position = (Vector2)transform.position + offset;
            coin.Init();
        }
    }
}
