﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E8Bullet5Laser : BulletBase
{
    [SerializeField] protected float timeDrop = 1f;
    [SerializeField] protected BulletBase bulletLaser;
    protected float angle;
    protected override void Active() {
        base.Active();
        DOVirtual.DelayedCall(timeDrop, () => { SpawnerBullet(); });
    }

    protected virtual void SpawnerBullet() {
        rg2D.velocity = Vector2.zero;
        angle = Random.Range(0,360);
        for(int i = 0; i < 5 ; i++ ) {
            var bullet = bulletLaser.Spawn(transform.position);
            bullet.SetDirect(angle);
            bullet.Init();
            angle += 72f;
        }
        DOVirtual.DelayedCall(0.2f, () => { tweenTimeLife.Kill(); gameObject.Recycle(); });
    }
}
