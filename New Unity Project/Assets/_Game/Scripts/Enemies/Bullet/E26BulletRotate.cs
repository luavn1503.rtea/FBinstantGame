﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E26BulletRotate : BulletBase
{
    [SerializeField] private Transform bullet;
    [SerializeField] private float speedRotatate;
    [SerializeField] private float radius;
    [SerializeField]private float startAngle =90f;

    protected override void Active() {
        base.Active();
    }

    private void Update() {
        //transform.Translate(Vector3.up * Time.deltaTime * speedRotatate / 144);
        //transform.Rotate(Vector3.back, speedRotatate * Time.deltaTime);
        //transform.position += (Vector3)direction * Time.deltaTime;
        //transform.position += (Vector3)direction * Time.deltaTime * speed;
        startAngle += speedRotatate * Time.deltaTime;
        var offset = new Vector2(Mathf.Sin(startAngle), Mathf.Cos(startAngle)) * radius;
        bullet.position = transform.position + (Vector3)offset;
    }
}
