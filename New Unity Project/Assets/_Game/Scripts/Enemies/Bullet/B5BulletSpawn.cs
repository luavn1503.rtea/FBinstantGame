﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B5BulletSpawn : E8Bullet5Laser
{
    // Start is called before the first frame update
    [SerializeField] protected int numberTurn = 2;
    [SerializeField] protected float timeTurnDelay = 1f;
    protected int curNumberTurn = 2;

    public override void Init() {
        curNumberTurn = numberTurn;
        base.Init();
    }
    protected override void SpawnerBullet() {
        curNumberTurn--;
        rg2D.velocity = Vector2.zero;
        angle = Random.Range(0, 360);
        for(int i = 0; i < 36; i++) {
            var bullet = bulletLaser.Spawn(transform.position);
            bullet.SetDirect(angle);
            bullet.Init();
            angle += 10f;
        }
        if(curNumberTurn == 0) {
            DOVirtual.DelayedCall(0.2f, () => { tweenTimeLife.Kill(); gameObject.Recycle(); });
        }
        else {
            DOVirtual.DelayedCall(1f, () => { SpawnerBullet(); });
        }
    }





}
