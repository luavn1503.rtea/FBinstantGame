﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E5BulletTarget : BulletBase
{
    public override void Init() {
        direction = (ManagerPlayer.Instance.GetShipPossition().possition - transform.position).normalized;
        base.Init();
    }
}
