﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E7BulletLaser : BulletBase
{
    [SerializeField] private float timeWarning;
    [SerializeField] private float timeLaser;
    [SerializeField] private GameObject warningLine;
    [SerializeField] private GameObject laserLine;

    public override void Init() {
        warningLine.SetActive(false);
        laserLine.SetActive(false);
        base.Init();
    }
    protected override void Active() {
        rg2D.velocity = Vector2.zero;
        StartCoroutine(ActiveLaser());
    }

    IEnumerator ActiveLaser() {
        //Start waring
        warningLine.SetActive(true);
        yield return Yielder.Wait(timeWarning);

        //End waring => Start laser
        laserLine.SetActive(true);
        yield return Yielder.Wait(timeLaser);

        //recycle
        RecycleBullet();

    }
}
