﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E6Bullet : BulletBase
{
    [SerializeField] private float timeTaget;
    private bool target = true;

    public override void Init() {
        base.Init();
        target = true;
        DOVirtual.DelayedCall(timeTaget, () => { target = false; });
    }

    protected override void Active() {
        StartCoroutine(MoveTarget());
    }

    IEnumerator MoveTarget() {
        while(target) {
            // transform.up = Play
            yield return null;
        }
        rg2D.velocity = transform.up * speed;
    }
    // Start is called before the first frame update


}
