﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Gemmob;

public class B10BulletFollow : BulletBase
{
    [SerializeField] private float timeFollow = 3f;
    [SerializeField] private float delayFollow = 0.2f;
    private bool IsFollow = true;
    //private Coroutine corFollowPlayer = null; 

    public override void Init() {
        IsFollow = true;
        base.Init();
    }

    protected override void Active() {
        StartCoroutine(FollowPlayer());
        DOVirtual.DelayedCall(timeFollow, () => { IsFollow = false; });
    }

    IEnumerator FollowPlayer() {
        while(IsFollow) {
            direction = DirectTargetPlayer();
            rg2D.velocity = direction.normalized * speed;
            yield return Yielder.Wait(delayFollow);
        }
    }

}
