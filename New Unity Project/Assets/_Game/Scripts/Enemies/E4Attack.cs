﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E4Attack : EnemyAttackBase
{
    [SerializeField] private int numberBulletCol;
    [SerializeField] private float diffTimeBulletCol;

    protected override IEnumerator FireBulletPoint(GameObject parSys) {
        if(parSys.GetComponent<ParticleSystem>() != null) {
            parSys.GetComponent<ParticleSystem>().Play();
        }
        yield return Yielder.Wait(1f);
        BulletBase bullet = new BulletBase();
        float angleBullet = -90-(numberBulletCol/2)*diffTimeBulletCol; // start angle of bullet
        for(int i = 0; i < numberBulletCol; i++) {
            StartCoroutine(FireBulletCol(bullet, parSys, angleBullet));
            angleBullet += numberBulletCol;
            yield return Yielder.Wait(diffTimeBulletCol);
        }
    }

    IEnumerator FireBulletCol(BulletBase bullet, GameObject parSys, float angleBullet) {
        for(int y = 0; y < numberBulletCol; y++) {
            bullet = bulletPre.Spawn(parSys.transform.position);
            bullet.SetDirect(angleBullet);
            bullet.Init();
            yield return Yielder.Wait(diffTimeBulletCol);
        }
    }
}
