﻿using DG.Tweening;
using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBase : EnemyBase
{
    // Start is called before the first frame update
    [SerializeField] private GameObject render;
    [SerializeField] private Vector3 direct = Vector3.down;
    [SerializeField] protected float moveSpeed = 2f;
    protected Vector2 maxSize = Vector2.zero;

    public void SetDirect(Vector3 direct) {
        this.direct = direct;
    }

    public void SetMoveSpeed(float moveSpeed) {
        this.moveSpeed = moveSpeed;
    }
    protected override void Awake() {
        enemyData = EnemiesData.Instance.Rocks[ID];
    }

    public override void TakeDamage(float damage) {
        curHP -= damage;
        if(curHP <= 0 && IsLive) {
            EnemyDie();
        }
    }

    public override void EnemyDie() {
        enemyCol2D.OnEnemyDie();
        if(IsLive) {
            IsLive = false;
            SpawnDieEffect();
            ManagerEnemies.Instance.EnemyDeath(transform.position);
            GamePlayState.Instance.AddInGameScore(GetEnemyData().Score);
            gameObject.Recycle();
        }
    }

    private void Update() {
        render.transform.Rotate(Vector3.forward, Time.deltaTime*90f);
        transform.Translate(direct * Time.deltaTime * moveSpeed);
        //CheckOutScene();
    }


    protected override void OnValidate() {
        //Get data Enemy
        enemyData = EnemiesData.Instance.Rocks[ID];
        rg2D = GetComponent<Rigidbody2D>();
        eMove = GetComponent<EnemyMoveBase>();
        enemyCol2D = GetComponentInChildren<EnemyCollider2D>();
    }

    public override void Init(float scaleHp) {
        maxSize = MainCamera.Instance.max;
        moveSpeed = 0f;
        DOVirtual.DelayedCall(5f, () => { EnemyDie(); });
        base.Init(scaleHp);
    }

    protected void CheckOutScene() {
        Vector2 origin = transform.position;
        if(Mathf.Abs(origin.x) >= maxSize.x + 2f || Mathf.Abs(origin.y) >= maxSize.y + 2f) { EnemyDie(); }
    }
}
