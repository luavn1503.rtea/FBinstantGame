﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B7AnimationController : AnimationController
{
    [SerializeField] protected SkeletonAnimation skeleton2;

    public override float SetAnimation(string animationKey, bool loop = false) {
        skeleton.state.SetAnimation(0, animationKey, loop);
        skeleton2.state.SetAnimation(0, animationKey, loop);
        return skeleton.state.GetCurrent(0).Animation.Duration;
    }
}
