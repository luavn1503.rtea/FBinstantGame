﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B7ProcessAttack1 : ProcessAttack
{
    [Header("RangeBullet")]
    [SerializeField]private float range = 0.5f;
    protected override IEnumerator FireBulletPoint(GameObject parSys) {
        if(parSys.GetComponent<ParticleSystem>() != null) {
            parSys.GetComponent<ParticleSystem>().Play();
            yield return Yielder.Wait(1f);
        }

        float angleBullet = 0;
        Vector2 perpen = Vector2.Perpendicular(DirectTargetPlayer(parSys));
        Vector2 offSet = perpen*range*(colBullet-1)*0.5f;
        //Start fix goc Defaul  vs  DefaulTarget 
        if(styleBullet == StyleBullet.DEFAUL) {
            angleBullet = -90; // start angle of bullet
        }
        else if(styleBullet == StyleBullet.DEFAULTARGET) {
            angleBullet = AngleTargetPlayer(parSys);
        }
        //

        for(int i = 0; i < colBullet; i++) {
            if(i == colBullet - 1) {
                yield return StartCoroutine(SpawnColBullet(parSys, angleBullet,offSet));
            }
            else {
                StartCoroutine(SpawnColBullet(parSys, angleBullet, offSet));
                if(timeColDelay > 0.001) {
                    yield return Yielder.Wait(timeColDelay);
                }

            }

            // Góc mới 
            if(styleBullet == StyleBullet.DEFAULTARGET) {
                offSet -= perpen*range;
            }

        }
    }

    protected virtual IEnumerator SpawnColBullet(GameObject ParSys, float AngleBullet, Vector2 Offset) {
        for(int y = 0; y < rowBullet; y++) {

            bullet = bulletPre.Spawn(ParSys.transform.position+(Vector3)Offset);
            if(styleBullet == StyleBullet.TARGET) {
                bullet.SetDirect(AngleTargetPlayer(ParSys));
            }
            else if(styleBullet == StyleBullet.RANDOM) {
                bullet.SetDirect(UnityEngine.Random.Range(0, 360));
            }
            else if(styleBullet == StyleBullet.DEFAUL || styleBullet == StyleBullet.DEFAULTARGET) {
                bullet.SetDirect(AngleBullet);
            }
            bullet.Init();
            if(timeRowDelay > 0.001) {
                yield return Yielder.Wait(timeRowDelay);
            }
        }
    }
}
