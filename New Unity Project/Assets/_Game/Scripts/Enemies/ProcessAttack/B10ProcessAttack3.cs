﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B10ProcessAttack3 : ProcessAttack
{
    // Start is called before the first frame update
    [SerializeField] private float angleRotate = 10f;

    override protected IEnumerator SpawnColBullet(GameObject ParSys, float AngleBullet) {
        float plusAngle = 0f;
        for(int y = 0; y < rowBullet; y++) {

            bullet = bulletPre.Spawn(ParSys.transform.position);
            if(styleBullet == StyleBullet.TARGET) {
                bullet.SetDirect(AngleTargetPlayer(ParSys));
            }
            else if(styleBullet == StyleBullet.RANDOM) {
                bullet.SetDirect(UnityEngine.Random.Range(0, 360));
            }
            else if(styleBullet == StyleBullet.DEFAUL || styleBullet == StyleBullet.DEFAULTARGET) {
                bullet.SetDirect(AngleBullet+plusAngle);
                plusAngle += angleRotate;
            }
            bullet.SetDamage(eBase.GetEnemyData().BulletDam);
            bullet.Init();
            if(timeRowDelay > 0.001) {
                yield return Yielder.Wait(timeRowDelay);
            }
        }
    }

    protected override void endProcess() {
        curNumberTurn--;
        if(curNumberTurn > 0) {
            DOVirtual.DelayedCall(timeTurnDelay, () => { StartCoroutine(Fire()); });
            angleRotate = -angleRotate;
        }
        else {
            if(string.IsNullOrEmpty(endAnim)) {
                bossAttackBase.NextProcess();
            }
            else {
                time = animController.SetAnimation(endAnim);
                DOVirtual.DelayedCall(time, () => { bossAttackBase.NextProcess(); });
            }
        }
    }
}
