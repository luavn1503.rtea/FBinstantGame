﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class B9ProcessAttack3 : ProcessAttack
{
    // Start is called before the first frame update
    protected override IEnumerator Fire() {
        animController.SetAnimation(liveAnim, true);
        foreach(GameObject attackEffect in attackPoints) {
            if(attackEffect == attackPoints[attackPoints.Count - 1]) {
                yield return StartCoroutine(FireBulletPoint(attackEffect, Hand.Left));
                endProcess();
            }
            else {
                StartCoroutine(FireBulletPoint(attackEffect, Hand.Right));
                if(timePointDelay > 0.001) {
                    yield return Yielder.Wait(timePointDelay);
                }
            }
        }
    }

    protected virtual IEnumerator FireBulletPoint(GameObject parSys, Hand hand) {
        if(parSys.GetComponent<ParticleSystem>() != null) {
            parSys.GetComponent<ParticleSystem>().Play();
            yield return Yielder.Wait(1f);
        }

        float angleBullet = 0;

        //Start fix goc Defaul  vs  DefaulTarget 
        if(styleBullet == StyleBullet.DEFAUL) {
            if(Hand.Right == hand) {
                angleBullet = (colBullet - 1) * diffAngleCol * 0.5f; // start angle of bullet
            }
            else {
                angleBullet = -180 + (colBullet - 1) * diffAngleCol * 0.5f; // start angle of bullet
            }
            
        }
        else if(styleBullet == StyleBullet.DEFAULTARGET) {
            angleBullet = AngleTargetPlayer(parSys) + (colBullet - 1) * diffAngleCol * 0.5f;
        }
        //

        for(int i = 0; i < colBullet; i++) {
            if(i == colBullet - 1) {
                yield return StartCoroutine(SpawnColBullet(parSys, angleBullet));
            }
            else {
                StartCoroutine(SpawnColBullet(parSys, angleBullet));
                if(timeColDelay > 0.001) {
                    yield return Yielder.Wait(timeColDelay);
                }

            }

            // Góc mới 
            if(styleBullet == StyleBullet.DEFAUL || styleBullet == StyleBullet.DEFAULTARGET) {
                angleBullet -= diffAngleCol;
            }

        }
    }

    protected enum Hand {
        Right,
        Left
    }
}
