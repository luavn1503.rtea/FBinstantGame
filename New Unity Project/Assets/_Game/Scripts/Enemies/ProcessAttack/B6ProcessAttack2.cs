﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B6ProcessAttack2 : ProcessAttack {



    public override void Init() {
        curNumberTurn = numberTurn;
        base.Init();
    }
    protected override IEnumerator Fire() {
        animController.SetAnimation(liveAnim, true);
        foreach(GameObject attackEffect in attackPoints) {
            if(attackEffect == attackPoints[attackPoints.Count - 1]) {
                StartCoroutine(FireBulletPointSide(attackEffect, Side.LEFT));
                yield return StartCoroutine(FireBulletPointSide(attackEffect, Side.RIGHT));
                endProcess();
            }
            else {
                //StartCoroutine(FireBulletPointSide(attackEffect, Side.LEFT));
                //if(timePointDelay > 0.001) {
                //    yield return Yielder.Wait(timePointDelay);
                //}
            }
        }
    }

    protected virtual IEnumerator FireBulletPointSide(GameObject parSys, Side side) {
        if(parSys.GetComponent<ParticleSystem>() != null) {
            parSys.GetComponent<ParticleSystem>().Play();
            yield return Yielder.Wait(1f);
        }

        float angleBullet = 0;

        //Start fix goc Defaul  vs  DefaulTarget 
        if(styleBullet == StyleBullet.DEFAUL && side == Side.LEFT) {
            angleBullet = -90 + (colBullet - 1) * diffAngleCol * 0.5f; // start angle of bullet
        }
        else if(styleBullet == StyleBullet.DEFAUL && side == Side.RIGHT) {
            angleBullet = -90 - (colBullet - 1) * diffAngleCol * 0.5f;
        }
        //

        for(int i = 0; i < colBullet; i++) {
            if(i == colBullet - 1) {
                yield return StartCoroutine(SpawnColBullet(parSys, angleBullet));
            }
            else {
                StartCoroutine(SpawnColBullet(parSys, angleBullet));
                if(timeColDelay > 0.001) {
                    yield return Yielder.Wait(timeColDelay);
                }

            }

            // Góc mới 
            if(styleBullet == StyleBullet.DEFAUL && side == Side.LEFT) {
                angleBullet -= diffAngleCol;
            }
            else if(styleBullet == StyleBullet.DEFAUL && side == Side.RIGHT) {
                angleBullet += diffAngleCol;
            }
        }
    }


    protected override void endProcess() {
        curNumberTurn--;
        if(curNumberTurn > 0) {
            //DOVirtual.DelayedCall(timeTurnDelay, () => { StartCoroutine(Fire()); });
            StartCoroutine(Fire());
        }
        else {
            if(string.IsNullOrEmpty(endAnim)) {
                bossAttackBase.NextProcess();
            }
            else {
                time = animController.SetAnimation(endAnim);
                DOVirtual.DelayedCall(time, () => { bossAttackBase.NextProcess(); });
            }
        }
    }
    protected enum Side {
        RIGHT,
        LEFT
    }
}
