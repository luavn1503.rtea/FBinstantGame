﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E11ProcessAttack : ProcessAttack {
    [SerializeField] private float spacing;

    protected override IEnumerator FireBulletPoint(GameObject parSys) {
        float speedBullet = bulletPre.GetSpeed();
        BulletBase bullet = null;
        ;
        foreach(GameObject atkPoint in attackPoints) {
            for(int row = 0; row < 5; row++) {
                if(row == 0 || row == 4) {
                    bullet = bulletPre.Spawn(atkPoint.transform.position);
                    bullet.SetDirect(Vector2.down);
                    bullet.Init();
                }
                else if(row == 1 || row == 3) {
                    Vector2 start = atkPoint.transform.position;
                    start.x -= spacing * 1.5f;
                    for(int i=0;i<4;i++) {
                        bullet = bulletPre.Spawn(start);
                        bullet.SetDirect(Vector2.down);
                        bullet.Init();
                        start.x += spacing;
                    }
                }
                else {
                    Vector2 start = atkPoint.transform.position;
                    start.x -= spacing;
                    for(int i = 0; i < 2; i++) {
                        bullet = bulletPre.Spawn(start);
                        bullet.SetDirect(Vector2.down);
                        bullet.Init();
                        start.x += spacing*2;
                    }
                }

                yield return Yielder.Wait(spacing / speedBullet);
                //yield return new WaitForSeconds(0.5f);
            }
        }

    }
}
