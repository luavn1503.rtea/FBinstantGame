﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B7ProcessAttack2 : B7ProcessAttack1
{
    [Header("RangeHighWide")]
    [SerializeField]private float high = 2f;
    [SerializeField]private float wide = 1.5f;
    // Start is called before the first frame update
    protected override IEnumerator SpawnColBullet(GameObject ParSys, float AngleBullet, Vector2 Offset) {
        return base.SpawnColBullet(ParSys, AngleBullet, RandomOffset());
    }

    protected Vector2 RandomOffset() {
        float x = Random.Range(-wide,wide);
        float y = Random.Range(0,high);
        return new Vector2(x, y);
    }
}
