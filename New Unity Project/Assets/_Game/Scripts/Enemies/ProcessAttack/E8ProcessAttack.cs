﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E8ProcessAttack : ProcessAttack
{
    protected override IEnumerator FireBulletPoint(GameObject parSys) {
        if(parSys.GetComponent<ParticleSystem>() != null) {
            parSys.GetComponent<ParticleSystem>().Play();
            yield return Yielder.Wait(1f);
        }
        BulletBase bullet;
        float angleBullet = Random.Range(0,360); // start angle of bullet
        for(int i = 0; i < colBullet; i++) {
            bullet = bulletPre.Spawn(parSys.transform.position);
            bullet.SetDirect(angleBullet);
            bullet.Init();
            angleBullet = Random.Range(0, 360);
            yield return Yielder.Wait(timeRowDelay);
        }
    }
}
