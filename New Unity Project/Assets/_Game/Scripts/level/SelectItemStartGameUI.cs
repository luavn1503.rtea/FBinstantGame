﻿using System;
using DG.Tweening;
using Gemmob;
using Gemmob.Common;
using UnityEngine;
using UnityEngine.UI;

public class SelectItemStartGameUI : MonoBehaviour {
	[SerializeField] private PopupBuyItem _buyItem;
	[Header("PLAYER INFO")]
	[SerializeField] private Text _playerCp;//
	[SerializeField] private int _itemSelect;
	[Header("MAP INFO")]
	[SerializeField] private Text _level;
	[SerializeField] private Text _goldGet;
	[SerializeField] private Text _levelCombatPower;///
	[SerializeField] private GameObject _warning;
	[SerializeField] private Text _fuelCost;
	[SerializeField] private RectTransform _selectPanel;
	[SerializeField] private GameObject _groupReward;

	[SerializeField] private Image _imgItemBomb;
	private bool _isLoaded = false;

	public Action BackButton;
	public ButtonUseItem[] Items;
	//[SerializeField] private AircraftPartShow[] _partShows;
	[SerializeField] private ButtonAnimation _btnExit;
	[SerializeField] private ButtonAnimation _btnPlay;

	[Header("Endless")]
	[SerializeField] private Transform _endlessItemSelect;
	[SerializeField] private GameObject _endlessItemGroup;
	[SerializeField] private Transform _itemInfor;

	private float _currentLevelCp;

	public int ItemSelect {
		get {
			return _itemSelect;
		}
	}

	void OnEnable() {
		if(_isLoaded)
			LoadData();

		PlayerInfo.AfterLoadData += SetupShipAndDroid;

		_itemSelect = -1;
		_selectPanel.gameObject.SetActive(false);
		_selectPanel.GetComponent<Image>().SetSizeFollowWidth(145);
	}

	void Start() {
		_btnExit.onClick.AddListener(() => {

			BackButton();

		});
		_btnPlay.onClick.AddListener(PlayGame);
		LoadData();
		_isLoaded = true;
	}

	public void LoadData() {
		LoadDataForButton();
		LoadDataMap();
		SetupShipAndDroid();
		var dataCard = ManagerData.Instance.GetInformationCard();

		_imgItemBomb.sprite = dataCard != null && dataCard.Name == CardType.Missiles ? dataCard.IconInGame : DataConfig.Instance.BombSprite;
		_imgItemBomb.SetNativeSize();
		_itemSelect = -1;
	}

	void OnDisable() {
		PlayerInfo.AfterLoadData -= SetupShipAndDroid;
	}

	void LoadDataForButton() {
		StartItemData data = ManagerData.Instance.StartItemData;
		for(int i = 0; i < Items.Length; i++)
			if(i < data.Items.Count) {
				var index = i;
				Items[i].SetData(data.Items[i].Icon, ManagerData.Instance.PlayerInfo.NumberItem[i],
					() => {
						LoadBuyItem(data.Items[index], index);
					},
					SetBoundSelectItem,
					() => {
						//var newItemSelect = _itemSelect != index && ManagerData.Instance.PlayerInfo.CanUseItem(index);
						//_itemSelect = newItemSelect ? index : -1;
						//_selectPanel.gameObject.SetActive(newItemSelect);

						//if(!ManagerData.Instance.PlayerInfo.CanUseItem(index)) {
						//	LoadBuyItem(data.Items[index], index);

						//	//[Tracking - Position]-[MenuShowup]
						//	string locationMenu = UIManager.Instance.startPage.CurrentNameMenu();
						//	GameTracking.Instance.TrackMenuShowup(locationMenu);
						//}
					});
				Items[i].gameObject.SetActive(true);
			}
			else
				Items[i].gameObject.SetActive(false);
	}

	void LoadDataMap() {
		var levelSelect = GamePlayState.Instance.CurrentLevelSelect;
		if(levelSelect < 0)
			return;

		var levelData = LevelService.Instance.GetById(levelSelect);

		int fuelUse = 0;
		int goldReward = 0;
		var playMode = GamePlayState.Instance.ModePlay;
		if(playMode == EnumDefine.ModePlay.Normal) {
			fuelUse = levelData.FuelCost;
			goldReward = levelSelect < ManagerData.Instance.PlayerInfo.LevelUnlock
				? levelData.ReFightCoinReward
				: levelData.CoinReward;
			//for(int i = 0; i < _partShows.Length; i++) {
			//	_partShows[i].ShowPart(levelData, levelSelect);
			//}
		}
		//else if(playMode == EnumDefine.ModePlay.Endless)
		//	//fuelUse = EndlessModeControler.FuelEndless;
		//else if(playMode == EnumDefine.ModePlay.Boss)
			//fuelUse = BossBattleLoader.Instance.GetFuelNeed();


		_fuelCost.text = fuelUse == 0 ? "" : fuelUse.ToString();
		_goldGet.text = goldReward == 0 ? "" : goldReward.ToString();

		_levelCombatPower.gameObject.SetActive(GamePlayState.Instance.ModePlay == EnumDefine.ModePlay.Normal);

		var pcp = ManagerData.Instance.GetPlayerCp();
		var isMore = true;

		if(_levelCombatPower.gameObject.activeInHierarchy) {
			isMore = pcp >= levelData.CombatPower;

			if(!isMore) {
				_levelCombatPower.transform.DOScale(1.25f, .5f).OnComplete(() => {
					_levelCombatPower.transform.DOScale(1f, .3f);
				});
			}
			DOTween.To(value => {
				//_levelCombatPower.text = "Enemies Power: " + value.ToString("0");
				_levelCombatPower.text = string.Format("ENEMIES POWER: {0:0}", value);
			}, 0, levelData.CombatPower, 1f);
			//_levelCombatPower.color = isMore ? Color.white : DataConfig.Instance.WarningColor;
		}

		_warning.SetActive(!isMore && GamePlayState.Instance.ModePlay == EnumDefine.ModePlay.Normal);
		_groupReward.SetActive(_levelCombatPower.gameObject.activeInHierarchy);

		_endlessItemGroup.SetActive(GamePlayState.Instance.ModePlay == EnumDefine.ModePlay.Endless);
		//_endlessItemGroup.SetActive(false);

		switch(GamePlayState.Instance.ModePlay) {
			case EnumDefine.ModePlay.Normal:
				_level.text = "LEVEL   " + LevelManager.GetLevel;
				if(ManagerData.Instance.DailyChallengeSaver.IsChallenge(LevelManager.GetLevel)) {
					Debug.Log("[Daily challenge] Challenge level");
				}

				break;
			case EnumDefine.ModePlay.Endless:
				_level.text = "ENDLESS";
				break;
			case EnumDefine.ModePlay.Boss:
				_level.text = "BOSS   " + 1;
				break;
			default:
				_level.text = "BOSS   " + LevelManager.GetLevel;
				break;
		}
	}

	void SetupShipAndDroid() {
		if(GamePlayState.Instance.CurrentLevelSelect < 0)
			return;

		//Set data for load ship
		//int indexShip = ManagerData.Instance.PlayerInfo.IndexShip;
		//int indexDroid = ManagerData.Instance.PlayerInfo.IndexDrone;

		//var s = ManagerData.Instance.ShipsUpgradeData.Ships[indexShip];

		_playerCp.text = string.Format("COMBAT POWER : {0}", ManagerData.Instance.GetPlayerCp().ToString());

		//GamePlayState.Instance.CurPlayerIndex = ManagerData.Instance.PlayerInfo.IndexShip;
		//GamePlayState.Instance.CurDroneIndex = ManagerData.Instance.PlayerInfo.IndexDrone;

		if(GamePlayState.Instance.ModePlay != EnumDefine.ModePlay.Endless) {
			var pcp = ManagerData.Instance.GetPlayerCp();
			var isMore = pcp >= LevelService.Instance.GetById(GamePlayState.Instance.CurrentLevelSelect).CombatPower;
			_playerCp.color = isMore ? Color.green : DataConfig.Instance.WarningColor;
			_warning.SetActive(!isMore);
		}
	}

	void SetBoundSelectItem(Transform newParent) {
		_selectPanel.SetParent(newParent);
		_selectPanel.localPosition = Vector2.zero;
		_selectPanel.transform.SetSiblingIndex(1);
	}

	public void LoadBuyItem(StartItemInfo data, int index) {


		_buyItem.LoadDataBuy(data, index, BackButton);
		gameObject.SetActive(false);
		_buyItem.gameObject.SetActive(true);


	}



	public void PlayGame() {
		SoundController.Instance.RunClickSound();

		if(GamePlayState.Instance.ModePlay != EnumDefine.ModePlay.Boss) {
			//TODO: remove fuel
			//var levelData = LevelService.Instance.GetById(GamePlayState.Instance.CurrentLevelSelect);
			//var fuelUse = GamePlayState.Instance.ModePlay == EnumDefine.ModePlay.Normal ? levelData.FuelCost :
				//GamePlayState.Instance.ModePlay == EnumDefine.ModePlay.Endless ?
				//	EndlessModeControler.FuelEndless : 10;

			//var isEnoughFuel = ManagerData.Instance.PlayerInfo.CheckFuel(fuelUse);

		//	if(isEnoughFuel) {
		//		var canPlay = false;
		//		if(GamePlayState.Instance.ModePlay == EnumDefine.ModePlay.Normal) {
		//			if(levelData.CombatPower > ManagerData.Instance.GetPlayerCp())
		//				UIManager.Instance.startPage.ShowWanring();
		//			else
		//				canPlay = true;
		//		}
		//		else if(GamePlayState.Instance.ModePlay == EnumDefine.ModePlay.Endless)
		//			canPlay = true;

		//		if(canPlay)
		//			LoadGame(fuelUse);
		//	}
		//	else {
		//		UIManager.Instance.PopupAnnounce.StartAnnounce("Dont enough fuel", TypeReward.Fuel);

		//		//[Tracking - Position]-[MenuShowup]
		//		GameTracking.Instance.TrackMenuShowup(UIManager.Instance.PopupAnnounce.CurrentNameMenu());
		//	}
		//}
		//else {

			///////////////////////
			//UIManager.Instance.BossBattlePage.PlayGameAction.Invoke();
			//if (UIManager.IsLockClick)
			//	return;

			//GamePlayState.Instance.CurPlayerIndex = ManagerData.Instance.PlayerInfo.IndexShip;
			//GamePlayState.Instance.CurDroneIndex = ManagerData.Instance.PlayerInfo.IndexDrone;
			//GamePlayState.Instance.ItemUse = -2;
			//if(GamePlayState.Instance.CurrentLevelSelect == GameData.Instance.PlayerInfo.CurrentLevel) {
			//    GameData.Instance.PlayerInfo.IsPlayedCurrentLevel = true;
			//}
			//TODO: remove fuel

			//int fuelUse = BossBattleLoader.Instance.GetFuelNeed();

			//var isEnoughFuel = ManagerData.Instance.PlayerInfo.CheckFuel(fuelUse);

			//if(isEnoughFuel) {
			//	//LoadGame(fuelUse);
			//}
			//else {
			//	UIManager.Instance.PopupAnnounce.StartAnnounce("Dont enough fuel", TypeReward.Fuel);
			//	//[Tracking - Position]-[MenuShowup]
			//	GameTracking.Instance.TrackMenuShowup(UIManager.Instance.PopupAnnounce.CurrentNameMenu());
			//}
		}
	}

	public void LoadGame(int fuelUse) {
		//[Tracking - Position]-[MenuShowup]
		string locationMenu = "Ingame";
		string gameMode = "null";
		string level = "null";
		string wave = "null";
		Action onComplete = () => {
			switch (GamePlayState.Instance.ModePlay) {
				case EnumDefine.ModePlay.Normal: {
					level = (LevelManager.GetLevel).ToString();
					//wave = (NormalModeControler.Instance.CurrentWave + 1).ToString();
					if (ManagerData.Instance.DailyChallengeSaver.IsChallenge(LevelManager.GetLevel)) {
						gameMode = "Trial";
                        //break;
                    }
					gameMode = "Campagin";
					break;
				}
				case EnumDefine.ModePlay.Endless:
					//wave = (EndlessModeControler.Instance.CurrentEndless + 1).ToString();
					gameMode = "Endless";
					break;
				case EnumDefine.ModePlay.Boss:
                    //wave = (BossBattleController.Instance.CurrentEndless + 1).ToString();
                    gameMode = "Boss";
					break;
				case EnumDefine.ModePlay.Halloween:
					//wave = (HalloweenModeController.Instance.CurrentWave + 1).ToString();
					gameMode = "Weekend";
					break;
				default:
					gameMode = "Boss";
					break;
			}
			GameTracking.Instance.TrackMenuShowup(locationMenu, gameMode, level, wave);
		};


		ManagerData.Instance.PlayerInfo.UseFuel(fuelUse, this.name);
		GamePlayState.Instance.ShipIndex = -2;
		GamePlayState.Instance.DroneIndex = -2;

		ManagerData.Instance.OnReset();

		GamePlayState.Instance.CalculateShowAdsInStart(40);
		//if(GamePlayState.Instance.IsShowAdsInStart && Mediation.Instance.HasInterstitial) {
		//	//[Tracking-Position]-[VideoAds]
		//	AdsModify.ShowInterstitial("start_normal", locationMenu, gameMode, level, () => LoadingController.Instance.LoadGamingScene(onComplete));
		//}
		//else {
		//	LoadingController.Instance.LoadGamingScene(onComplete);
		//}
	}

	public void SetEndlessItemSelect(bool isActive, Transform parent) {
		_endlessItemSelect.SetParent(parent);
		_endlessItemSelect.gameObject.SetActive(isActive);
		_endlessItemSelect.localPosition = Vector3.zero;
	}

	public void SetItemInfor(Transform item, bool isActive, string infor) {
		_itemInfor.gameObject.SetActive(isActive);
		if(isActive) {
			_itemInfor.GetComponentInChildren<Text>().text = infor;
			_itemInfor.position = item.position + Vector3.up * 1.5f;
		}
	}
}
