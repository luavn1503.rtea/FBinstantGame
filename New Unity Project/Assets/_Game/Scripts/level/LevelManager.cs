﻿using Gemmob;
using Gemmob.Common;

public class LevelManager : SingletonBind<LevelManager> {

	public static int GetLevel {
		get { return GamePlayState.Instance.CurrentLevelSelect + 1; }
	}

	

	public void StopGame() {
		

		StopAllCoroutines();
	}



	#region Testing

	/// <summary>
	/// Killed all enemy
	/// </summary>
	public void KillAllEnemies() {
//		StopCoroutines();
//		_countPathDone = Int32.MaxValue;
//		foreach (var enemy in InGameEnemies) {
//			if (enemy.GetComponent<EnemyBase>() != null)
//				enemy.GetComponent<EnemyBase>().Killed();
//		}
//
//		InGameEnemies.Clear();
	}

	public void ReplayCurentWave() {
//		StopCoroutines();
//		_countPathDone = Int32.MaxValue;
//		_waveIndex--;
//		if (_waveIndex < -1) {
//			_waveIndex = -1;
//		}
//
//		KillAllEnemies();
	}

	public void PlayerPreviousWave() {
//		StopCoroutines();
//		_countPathDone = Int32.MaxValue;
//		_waveIndex -= 2;
//		if (_waveIndex < -1) {
//			_waveIndex = -1;
//		}
//
//		KillAllEnemies();
	}

	#endregion
    
}
