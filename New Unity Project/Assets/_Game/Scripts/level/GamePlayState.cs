﻿using System;
using System.Collections.Generic;
using Gemmob.Common;

namespace Gemmob {
	public enum TypeGame {
		Normal,
		CombatPower
	}

	public class GamePlayState : Singleton<GamePlayState> {		
		public bool IsStarting;
		public bool IsTry;
        public List<NotifiItemData> notifiItemDatas;

		protected override void Initialize() {
            notifiItemDatas = new List<NotifiItemData>();
		}

		public void TryShip() {
			IsTry = true;
		}

		public void Play() {
		}

		public void SetInGameScore(int score) {
			InGameScore = score;
			if (OnInGameScoreChanged != null) {
				OnInGameScoreChanged();
			}
		}

		public void AddInGameScore(int score) {
			InGameScore += score;
			if (OnInGameScoreChanged != null) {
				OnInGameScoreChanged();
			}
		}

        public void SetActiveNotifiStore(AllRewardType type, int idItem)
        {
            notifiItemDatas.Add(new NotifiItemData() { typeReward = type, id = idItem});
        }

        public bool CheckNotifiReward()
        {
            return notifiItemDatas.Count > 0;
        }

        public void ClearNotifiItemData()
        {
            notifiItemDatas.Clear();
        }

		public int ShipIndex;
		public int DroneIndex;
		public int InGameScore;
		public Action OnInGameScoreChanged;
		public int InGameCoin;
		public int EndlessSkipItemId = -1;
		public TypeGame TypeGame = TypeGame.CombatPower;
		public List<int> AircraftPart;
        public bool HasNotifiItemStore;

		#region For tracking

		public int CurDroneIndex;
		public int CurPlayerIndex;
		public int ItemUse;
		public EnumDefine.ModePlay ModePlay; // 0: Level mode, 1: Endless mode, 2: Battle mode
		public int CurrentLevelSelect = 0;
		public bool IsRevive;
		public int BombCount = 0;
		public int CardId;
		public int Wave;

		#endregion

		#region ForAds

		public bool IsShowAdsInStart;

		public void CalculateShowAdsInStart(float percent) {
			float random = UnityEngine.Random.Range(0, 99);
			IsShowAdsInStart = random < percent;
		}

		#endregion
	}

    public class NotifiItemData
    {
        public AllRewardType typeReward;
        public int id;
    }
}