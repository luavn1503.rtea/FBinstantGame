﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RatingPopup : Frame {
	[SerializeField] private ButtonAnimation _btnConfirm;
	[SerializeField] private ButtonAnimation[] _btnStars;
	[SerializeField] private GameObject _rewardSet;
	private bool _isRated;
	private bool _isRatedMax;

	void Start() {
		SetAllStarEvent();
		_btnConfirm.onClick.AddListener(OnConfirmClick);
	}

	void OnEnable() {
		SetGrayAllStar();
		_isRated = false;
		SetConfirmBtn(false, 1, DataConfig.Instance.ColorLevelCantPlay);
	}

	void SetConfirmBtn(bool interactable, float grayScale, Color txtColor) {
		_btnConfirm.interactable = interactable;
	//	_btnConfirm.GetComponent<_2dxFX_GrayScale>()._EffectAmount = grayScale;
		_btnConfirm.GetComponentInChildren<Text>().color = txtColor;
	}

	void SetAllStarEvent() {
		for (int i = 0; i < _btnStars.Length; i++) {
			int id = i;
			_btnStars[i].onClick.AddListener(() => OnStarClick(id));
		}
	}

	void SetGrayAllStar() {
		for (int i = 0; i < _btnStars.Length; i++) {
		//	_btnStars[i].GetComponent<_2dxFX_GrayScale>()._EffectAmount = 1;
			_btnStars[i].interactable = true;
		}
	}

	void OnStarClick(int id) {
		if (!_isRated) {
			_isRated = true;
			for (int i = 0; i < _btnStars.Length; i++) {
				_btnStars[i].interactable = false;
				if (i <= id) {
				//	_btnStars[i].GetComponent<_2dxFX_GrayScale>()._EffectAmount = 0;
				}
			}
			if (id == _btnStars.Length - 1) {
				_rewardSet.gameObject.SetActive(true);
				_isRatedMax = true;
			}
			SetConfirmBtn(true, 0, DataConfig.Instance.DolarColor);
		}
	}

	void OnConfirmClick() {
		if (_isRatedMax) {
			Application.OpenURL(string.Format("market://details?id={0}", Application.identifier));
			ManagerData.Instance.PlayerInfo.AddCurrency(EnumDefine.IDCurrency.Gem, 100);
			RatingController.Instance.SetRatedMax();
		}
		Hide();
	}
}
