﻿using Gemmob;
using Gemmob.Common;
using UnityEngine;

public class LoadingController : Singleton<LoadingController> {

	private const string ResourcePath = "Prefabs/Loading";
	private Loadding _loadingPage;

	public void Reload() {
		_loadingPage = Object.Instantiate(ResourceLogger.Load<Loadding>(ResourcePath));
		_loadingPage.name = typeof(LoadingController).Name;
		Object.DontDestroyOnLoad(_loadingPage);
	}

	protected override void Initialize() {
		Reload();
	}

	public void LoadUIScene() {
		//_loadingPage.LoadScene("UIScene");
		//_loadingPage.LoadScene("LoadingScene", () => _loadingPage.LoadSceneNoBg("UIScene"));
	}

	public void LoadGamingScene(System.Action onComplete = null) {
		// TODO: Remove this function when change game controller to event because we registered open game UI 
		//UIManager.OpenGameScene();
		if (GamePlayState.Instance.ModePlay == EnumDefine.ModePlay.Normal) {
			//EnemiesPreloader.Instance.GetAllEnemies();
			if (ManagerData.Instance.DailyChallengeSaver.IsChallenge(LevelManager.GetLevel) && ManagerData.Instance.PlayerInfo.UnlockModeData.isUnlocDailyMode) {
				
			}
		}
		else {
			
		}

		//_loadingPage.LoadScene("InGameScene", onComplete);
		//_loadingPage.LoadScene("LoadingScene", () => _loadingPage.LoadSceneNoBg("InGameScene"));
	}

	//public void LoadTutorial() {
	//	GamePlayState.Instance.IsTutorial = true;
	//	UIManager.OpenGameScene();
	//	_loadingPage.LoadScene("IngameScene");
	//}

	public void LoadTutorial() {
		//UIManager.OpenGameScene();
		GamePlayState.Instance.ModePlay = EnumDefine.ModePlay.Normal;
		//EnemiesPreloader.Instance.GetAllEnemies();
		//_loadingPage.LoadScene("IngameScene");
	}
}