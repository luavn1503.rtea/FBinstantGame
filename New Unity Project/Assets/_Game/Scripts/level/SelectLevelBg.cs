﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SelectLevelBg : MonoBehaviour {
    [SerializeField] private float bgSpeed = 0.03f;
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private Image bg;

    private Vector2 oldPosition;
    private readonly string mainTex = "_MainTex";

    private void Awake() {
        oldPosition = scrollRect.content.position;
    }
    private void OnEnable() {
        scrollRect.onValueChanged.AddListener(delta => {
            // bg.material.GetTextureOffset()
        });
    }

    private void Update() {
        bg.material.SetTextureOffset(mainTex, (Vector2.up) * bgSpeed * Time.deltaTime * 3f + bg.material.GetTextureOffset(mainTex));
    }
}
