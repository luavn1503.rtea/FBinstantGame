﻿using System;
using Gemmob;
using Gemmob.Common;
using UnityEngine;
using UnityEngine.UI;

public class CombatPowerAnnoune : MonoBehaviour
{
    public Text EnemyPower;
    public Text PlayerPower;
	[SerializeField] private ButtonAnimation _btnExit;
	[SerializeField] private ButtonAnimation _btnUpgrade;
	[SerializeField] private ButtonAnimation _btnYes;
	[SerializeField] private ButtonAnimation _btnTapToBack;
	public Action OnUpgradeClick;

	void Start() {
		_btnExit.onClick.AddListener(Exit);
		_btnUpgrade.onClick.AddListener(() => OnUpgradeClick());
		_btnYes.onClick.AddListener(StartGame);
		_btnTapToBack.onClick.AddListener(Exit);
	}

    private void Exit() {
        //[Tracking - Position]-[ButtonPress]
     //   string location = UIManager.Instance.StartPage.CurrentNameMenu();
        string btnID = "btn_back";
       // GameTracking.Instance.TrackButtonPress(location, btnID);

        gameObject.SetActive(false);

        //[Tracking - Position]-[MenuShowup]
       // string locationMenu = UIManager.Instance.StartPage.CurrentNameMenu();
     //   GameTracking.Instance.TrackMenuShowup(locationMenu);
    }

    public void SetData()
    {
        var pcp = ManagerData.Instance.GetPlayerCp();
        var lcp = LevelService.Instance.GetById(GamePlayState.Instance.CurrentLevelSelect).CombatPower;
        EnemyPower.text = lcp.ToString();
        PlayerPower.text = pcp.ToString();
        //MainText.text =
        //    string.Format("Your total power ({0}) is lower than level power ({1}). Do you want to continue? ", pcp, lcp);
    }

    public void StartGame()
    {
        //[Tracking - Position]-[ButtonPress]
     //   string location = UIManager.Instance.StartPage.CurrentNameMenu();
        string btnID = "btn_start";
      //  GameTracking.Instance.TrackButtonPress(location, btnID);

      //  GameData.Instance.PlayerInfo.UseFuel(LevelService.Instance.GetById(GamePlayState.Instance.CurrentLevelSelect).FuelCost, location);

        //GameData.Instance.PlayerInfo.UseFuel(fuelUse);
       // GamePlayState.Instance.ItemUse = UIManager.Instance.StartPage.ItemPage.ItemSelect;
        GamePlayState.Instance.ShipIndex = -2;
        GamePlayState.Instance.DroneIndex = -2;

		GamePlayState.Instance.CalculateShowAdsInStart(40);
	    //if (GamePlayState.Instance.IsShowAdsInStart && Mediation.Instance.HasInterstitial) {
     //       string location1 = "Ingame";
     //       string gameMode = "Campagin";
     //       string level = (GamePlayState.Instance.CurrentLevelSelect + 1).ToString();
     //       AdsModify.ShowInterstitial("start_normal", location1, gameMode, level, () => LoadingController.Instance.LoadGamingScene());
	    //} else {
		   // LoadingController.Instance.LoadGamingScene();
	    //}

        //[Tracking - Position]-[MenuShowup]
        string locationMenu = "Ingame";
        GameTracking.Instance.TrackMenuShowup(locationMenu);
    }
}
