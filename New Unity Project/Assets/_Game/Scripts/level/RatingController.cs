﻿using UnityEngine;

public class RatingController : MonoBehaviour {
	private static RatingController _instance;
	private const string _ratedKey = "RatedMax";
	public bool _isRatedMax;
	public int LvlCount;
	public bool IsBoss;

	public static RatingController Instance {
		get {
			if (_instance == null) {
				GameObject obj = new GameObject(typeof(RatingController).Name);
				_instance = obj.AddComponent<RatingController>();
				DontDestroyOnLoad(obj);
			}
			
			return _instance;
		}
	}

	void Awake() {
		_isRatedMax = PlayerPrefs.GetInt(_ratedKey) == 1;
		if (_isRatedMax) {
			//Destroy(UIManager.Instance.RatingPopup.gameObject);
		}
	}

	public void ShowRatingPopup() {
		if (!_isRatedMax) {
			IsBoss = false;
			LvlCount = 0;
			UIHomeManager.Instance.Show<RatingPopup>();
		}
	}

	public void CheckShowByCount() {
		if (!_isRatedMax) {
			LvlCount += 1;
			if (LvlCount >= 5) {
				ShowRatingPopup();
			}
		}
	}

	public void SetRatedMax() {
		_isRatedMax = true;
		PlayerPrefs.SetInt(_ratedKey, 1);
		PlayerPrefs.Save();
	}
}
