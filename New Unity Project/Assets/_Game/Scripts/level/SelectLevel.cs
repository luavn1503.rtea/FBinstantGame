﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using DG.Tweening;
//using Gemmob;
//using JetBrains.Annotations;
//using Spine.Unity;
//using UnityEngine;
//using UnityEngine.UI;
//public class SelectLevel : PageBase {
//    public static SelectLevel Instance;

//    [Header("Make map")]
//    [SerializeField]
//    private RectTransform group;

//    [SerializeField]
//    private GameObject levelIteamPrefab;
//    [SerializeField]
//    private GameObject dotPrefab;
//    [SerializeField]
//    private DOTweenPath levelPath;
//    public List<LevelItemUI> levels;
//    [SerializeField]
//    private Transform scroll;

//    [Header("SELECT")]
//    [SerializeField]
//    private RectTransform selectRect;
//    [SerializeField]
//  //  private LevelSelectMark LevelselectRectMark;

//   // [SerializeField]
//    private Sprite _normalLevelSprite, _bossLevelSprite;
//    [Header("Modes Play")]
//    [SerializeField] private ModePlayUI modePlayUI;

//    public override void Awake() {
//        Instance = this;
//        base.Awake();
//        scroll.eulerAngles = new Vector3(38, 0, 0);
//        _btnBack.onClick.RemoveAllListeners();
//        _btnBack.onClick.AddListener(HideToBackToPrePage);

//    }
//    private void OnEnable() {
//        var currenLevel = ManagerData.Instance.PlayerInfo.CurrentLevel;
//        float positionVertical = levels[currenLevel< levels.Count? currenLevel:levels.Count -1].transform.localPosition.y;
//        group.transform.localPosition = new Vector2(group.transform.localPosition.x, -positionVertical + 200f);
//        SetSelect(levels[currenLevel >= levels.Count ? levels.Count - 1 : currenLevel], null);
//    }
//    public override void FinishAnim() {
//        base.FinishAnim();
//        CheckNewUnlock();
//        ShowUpOpenModel();


//    }
//    private void ShowUpOpenModel() {
//        ShowUpOpenEndless();
//        ShowUpOpenHalloweenMode();
//        ShowUpOpenTrial();
//        ShowUpBossBattle();
//        modePlayUI.UnlockModes(ManagerData.Instance.PlayerInfo.CurrentLevel);
//    }
//    private void ShowUpOpenEndless() {
//        //ShowUpOpenEndless();
//        var currentLvel = ManagerData.Instance.PlayerInfo.CurrentLevel;
//        UnlockLevelMode unlockLevel = ManagerData.Instance.UnlockLevelModeData.UnlockLevelEndlessMode;
//        PlayerInfo playerInfo = ManagerData.Instance.PlayerInfo;
//        if(currentLvel >= unlockLevel.LevelUnlock && !playerInfo.UnlockModeData.isUnlockEndlessMode) {
//            playerInfo.UnlockModeData.isUnlockEndlessMode = true;
//            playerInfo.SaveData();
//        }

//    }
//    private void ShowUpOpenHalloweenMode() {
//        var currentLevel = ManagerData.Instance.PlayerInfo.CurrentLevel;
//        UnlockLevelMode unlockLevel = ManagerData.Instance.UnlockLevelModeData.UnlockLevelTrialMode;
//        PlayerInfo playerInfo = ManagerData.Instance.PlayerInfo;
//        if(currentLevel >= unlockLevel.LevelUnlock && !playerInfo.UnlockModeData.isUnlocDailyMode) {
//            playerInfo.UnlockModeData.isUnlockBossBattleMode = true;
//            playerInfo.SaveData();
//        }
//    }
//    private void ShowUpOpenTrial() {
//        var currentlevel = ManagerData.Instance.PlayerInfo.CurrentLevel;
//        UnlockLevelMode unlockLevel = ManagerData.Instance.UnlockLevelModeData.UnlockLevelTrialMode;
//        PlayerInfo playerInfo = ManagerData.Instance.PlayerInfo;
//        if(currentlevel >= unlockLevel.LevelUnlock && !playerInfo.UnlockModeData.isUnlocDailyMode) {
//            playerInfo.UnlockModeData.isUnlocDailyMode = true;
//            playerInfo.SaveData();
//        }
//    }
//    private void ShowUpBossBattle() {
//        var currentlevel = ManagerData.Instance.PlayerInfo.CurrentLevel;
//        UnlockLevelMode unlockLevel = ManagerData.Instance.UnlockLevelModeData.UnlockLevelBossBattleMode;
//        PlayerInfo playerInfo = ManagerData.Instance.PlayerInfo;
//        if(currentlevel >= unlockLevel.LevelUnlock && !playerInfo.UnlockModeData.isUnlocDailyMode) {
//            playerInfo.UnlockModeData.isUnlockBossBattleMode = true;
//            playerInfo.SaveData();
//        }
//    }
//    private void CheckNewUnlock() {
//        CheckNewUnlockShipOwn();
//        var newShip = ManagerData.Instance.CheckShipUnlock();
//        if(newShip.Count > 0) {
//            if(UIManager.Instance.ratingPopup.gameObject.activeInHierarchy) {
//                UIManager.Instance.ratingPopup.HideToBackToPrePage();
//            }
//        }
//        foreach(var ship in newShip) {
//            UIManager.Instance.PopupAnnounce.StartAnnounce(ship.Item1, "", ship.Item2);
//        }

//        var newDrone = ManagerData.Instance.CheckDroneUnlock();
//        if(newDrone.Count > 0) {

//            if(UIManager.Instance.ratingPopup.gameObject.activeInHierarchy) {
//                UIManager.Instance.ratingPopup.HideToBackToPrePage();

//            }
//            foreach(var drone in newDrone) {
//                UIManager.Instance.PopupAnnounce.StartAnnounce(drone.Item1, "You have had unlock new drone", drone.Item2);
//            }
//        }
//    }

//    private void CheckNewUnlockShipOwn() {
//        //List<ShipUpgradeData> ships = GameData.Instance.ShipsUpgradeData.Ships;
//        //PlayerInfo player = GameData.Instance.PlayerInfo;
//        //for(int i = 0; i < ships.Count; ++i) {
//        //    if(player.CurrentLevel >= ships[i].LevelUnlock) {
//        //        ShipDataInfor ship = GameData.Instance.CurrentAircraftData.Ships[i];
//        //        if (!ship.IsGetRewardUnlock && ship.Level > 0) {
//        //            foreach(var shipIAP in GameData.Instance.IapShipData.shopShipInfos) {
//        //                if(shipIAP.ExtendId == ships[i].Id) {
//        //                    // get reward
//        //                    foreach(var item in shipIAP.rewards) {
//        //                        GameData.Instance.PlayerInfo.GetAllRewardType(item.RewardType, item.ExtendId, item.Number, ChangeCurrencyArea.Endgame);
//        //                        UIManager.Instance.Announce.StartAnnounce(item.Name, "You hava had new evolve part", item.Icon, item.Number, true, "", item.CategoryIcon);
//        //                        ship.IsGetRewardUnlock = true;
//        //                        GameData.Instance.CurrentAircraftData.SaveShip();
//        //                    }
//        //                    break;
//        //                }
//        //            }
//        //        }
//        //    }
//        //}

//        ManagerData.Instance.GetShipRewardIfUnlocked();
//        ManagerData.Instance.GetDroneRewardIfUnlocked();
//    }
//    public void LevelItemClicked(LevelItemUI levelItemUI) {
//        if(UIManager.IsLockClick) {
//            GamePlayState.Instance.ModePlay = EnumDefine.ModePlay.Normal;
//            GamePlayState.Instance.CurrentLevelSelect = levelItemUI.transform.GetSiblingIndex();
//            SetSelect(levelItemUI, () => {
//                HideToNextToNewPage();

//                GameController.Instance.StartGame();
//                UIManager.Instance.TopBarController.gameObject.SetActive(true);
//            });
//        }
//    }
//    public override void HideToBackToPrePage() {
//        modePlayUI.TapToHideAll();
//        if(UIManager.IsLockClick) {
//            return;
//        }
//        string location =CurrentNameMenu();
//        string btnID = "btn_back";
//        modePlayUI.TapToHideAll();
//        base.HideToBackToPrePage();

//        string locationMenu = UIManagerStatic.PageStack.PeekCustome().CurrentNameMenu();
//        GameTracking.Instance.TrackMenuShowup(locationMenu);
//    }
//    public void SetSelect(LevelItemUI levelItem, Action callback) {
//        if(selectRect) {
//            selectRect.SetParent(levelItem.GetComponent<RectTransform>());
//            selectRect.anchoredPosition = Vector2.zero;
//            selectRect.SetAsLastSibling();
//        }
//        levelItem.SetMyState();
//        if((levelItem.Level + 1) % 6 == 0) {

//        }

//    }
//    public void ClosePage() {
//        HideToBackToPrePage();
//    }
//    [ContextMenu("InitLevelItems")]
//    private void InitLevelItems() {
//        levels.Clear();
//        Transform container = levelIteamPrefab.transform.parent;
//        int count = container.childCount -1;
//        while(container.childCount > 1 && count > 0) {
//            DestroyImmediate(container.GetChild(0).gameObject, true);
//            count--;

//        }
//        levels.Add(levelIteamPrefab.GetComponent<LevelItemUI>());
//        int numLevelOfGame = FindObjectOfType<GameConfig>().NumLevelOfGame;
//        for(int i = 1; i < numLevelOfGame; i++)
//            levels.Add(Instantiate(levelIteamPrefab, container, true).GetComponent<LevelItemUI>());
//        if(levelPath.wps.Count > 0) {
//            levelPath.transform.position = levelPath.wps[0];
//        }
//        if(levelPath.wps.Count == numLevelOfGame) {
//            for(int i = 0; i < numLevelOfGame; i++) {
//                Vector3 pos = levelPath.wps[i];
//                levels[i].Level = i;
//                levels[i].transform.position = new Vector3(pos.x, pos.y, transform.position.z);
//                levels[i].TxtLevel.text = (i + 1).ToString();
//                levels[i].name = "level" + (i + 1);
//                if((i + 1) % 6 == 0) {

//                }
//                else {
//                    levels[i].GetComponent<Image>().sprite = _normalLevelSprite;
//                    levels[i].GetComponent<Image>().SetNativeSize();
//                }

//                levels[i].Skeleton = levels[i].transform.GetComponentsInChildren<SkeletonGraphic>();

//            }
//        }
//        else {
//            Tween pathtween = levelPath.transform.DOPath(levelPath.wps.ToArray(), .01f, PathType.Linear);
//            pathtween.ForceInit();
//            for(int i = 0; i < numLevelOfGame; i++) {
//                Vector3 pos = pathtween.PathGetPoint(i/(float)numLevelOfGame);
//                levels[i].TxtLevel.text = (i + 1).ToString();
//                levels[i].transform.position = new Vector3(pos.x, pos.y, transform.position.z);
//            }
//        }
//        for(int i = levels.Count - 1; 0 <= i; i--) {
//            levels[i].transform.SetAsFirstSibling();
//        }
//        CreateDots();
//        group.sizeDelta = new Vector2(group.rect.width, levels[levels.Count - 1].GetComponent<RectTransform>().anchoredPosition.y + 1500);


//    }
//    [ContextMenu("Create Dot")]
//    private void CreateDots() {
//        Transform dotContainer = dotPrefab.transform.parent;
//        var cout = dotContainer.childCount;
//        while(dotContainer.childCount > 1 && cout > 0) {
//            DestroyImmediate(dotContainer.GetChild(1).gameObject);
//            cout--;
//        }
//        foreach(var levelItemUi in levels)
//            levelItemUi.Dots.Clear();
//        if(levelPath.wps.Count > 0)
//            levelPath.transform.position = levelPath.wps[0];

//        var pathTweem = levelPath.transform.DOPath(levelPath.wps.ToArray(), 1f, PathType.Linear);
//        pathTweem.ForceInit();
//        var number = pathTweem.PathLength() * 1.8f;
//        List<GameObject> l = new List<GameObject>() { dotPrefab };
//        for(int i = 1; i < number; i++) {
//            GameObject obj = Instantiate(dotPrefab);
//            obj.transform.SetParent(dotContainer);
//            obj.transform.localScale = Vector3.one;
//            l.Add(obj);
//        }
//        int count2 = 1;
//        List<float> list = new List<float>();
//        for(int i = 0; i < number; i++) {
//            list.Clear();
//            for(int j = 0; j < count2; j++)
//                list.Add(levelPath.path.wpLengths[j]);


//            if((list.Sum() - levelPath.path.wpLengths[1]) / pathTweem.PathLength() < i / (float)number) {
//                count2++;
//            }
//            Vector3 pos = pathTweem.PathGetPoint(i / (float)number);
//            l[i].transform.position = new Vector3(pos.x, pos.y, transform.position.z);
//            if(count2 - 3 >= 0)
//                levels[count2 - 3].Dots.Add(l[i].GetComponent<Image>());
//        }


//        float radius = .4f;
//        for(var index = 0; index < levels.Count; index++) {

//            var levelItemUi = levels[index];
//            var lst = levelItemUi.Dots.FindAll(s =>
//                Vector2.Distance(levelItemUi.transform.position, s.transform.position) <= radius);
//            if(lst.Count > 0) {
//                foreach(var image in lst) {
//                    levelItemUi.Dots.Remove(image);
//                    DestroyImmediate(image.gameObject);
//                }
//            }


//            if(index > 0) {
//                var lst2 = levels[index - 1].Dots.FindAll(s =>
//                      Vector2.Distance(levelItemUi.transform.position, s.transform.position) <= radius);
//                if(lst2.Count > 0) {
//                    foreach(var image in lst2) {
//                        levels[index - 1].Dots.Remove(image);
//                        DestroyImmediate(image.gameObject);
//                    }
//                }

//            }
//        }
//    }
//    public override string CurrentNameMenu() {
//        return "MenuLevelSelect";
//    }
//}






