﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonUseItem : MonoBehaviour
{
    public Image Icon;
    public Text Number;

    public Button BuyMore;
    public Button BaseButton;
    public void SetData(Sprite icon, int currentNumber, Action actionButton, Action<Transform> setBound, Action setItemUse)
    {
        Icon.sprite = icon;
        Icon.SetNativeSize();
        Number.text = currentNumber.ToString();

        BuyMore.onClick.RemoveAllListeners();
        BuyMore.onClick.AddListener(actionButton.Invoke);

        BaseButton.onClick.RemoveAllListeners();
        BaseButton.onClick.AddListener(() =>
        {
            setBound.Invoke(transform);
        });
        BaseButton.onClick.AddListener(setItemUse.Invoke);
    }
}
