﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AircraftPartShow : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    [SerializeField] private Aircraft _aircraft;
    [SerializeField] private Text _txtNum;
    [SerializeField] private Image _partIcon;
    [SerializeField] private GameObject _checkedPanel;
    private LevelData _lvlData;

    [ContextMenu("Init")]
    void Init() {
        _txtNum = transform.Find("txtCurrent").GetComponent<Text>();
        _partIcon = transform.Find("ImgIcon").GetComponent<Image>();
        _checkedPanel = transform.Find("Checked").gameObject;
    }

    public void ShowPart(LevelData lvlData, int lvlSelect) {
        _lvlData = lvlData;
        _checkedPanel.SetActive(lvlSelect < ManagerData.Instance.PlayerInfo.LevelUnlock);
        if(_aircraft == Aircraft.Ship) {
            //SetPartShow(lvlData.EvolveShipNum, ManagerData.Instance.ShipsUpgradeData.GetUpdateShip((EnumDefine.IDShip)lvlData.EvolveShipId).Icon);
        }
        else {
           // SetPartShow(lvlData.EvolveDroneNum,
             //   ManagerData.Instance.DronesUpgradeData.Drones[lvlData.EvolveDroneId].Icon);
        }
    }

    public void ShowPartInGame(LevelData lvlData, int lvlSelect) {
        _lvlData = lvlData;
        this.gameObject.SetActive(lvlSelect >= ManagerData.Instance.PlayerInfo.LevelUnlock - 1); // becasuse win + 1
        if(_aircraft == Aircraft.Ship) {
           // SetPartShow(lvlData.EvolveShipNum, ManagerData.Instance.ShipsUpgradeData.GetUpdateShip((EnumDefine.IDShip)lvlData.EvolveShipId).Icon);
        }
        else {
            //SetPartShow(lvlData.EvolveDroneNum,
             //   ManagerData.Instance.DronesUpgradeData.Drones[lvlData.EvolveDroneId].Icon);
        }
    }


    void SetPartShow(int number, Sprite icon) {
        _txtNum.text = number.ToString();
        _partIcon.sprite = icon;
        _partIcon.SetSizeFollowWidth(111);
        //_partIcon.SetNativeSize();
    }

    public void OnPointerDown(PointerEventData eventData) {
        //string aircraftName =
        //    _aircraft == Aircraft.Ship
                //? ManagerData.Instance.ShipsUpgradeData.GetUpdateShip((EnumDefine.IDShip)_lvlData.EvolveShipId).Name
                //: ManagerData.Instance.DronesUpgradeData.Drones[_lvlData.EvolveDroneId].Name;
        //string infor = string.Format("Use to evolve\n{0}", aircraftName);
        //UIManager.Instance.startPage.ItemPage.SetItemInfor(transform, true, infor);
    }

    public void OnPointerUp(PointerEventData eventData) {
        //UIManager.Instance.StartPage.ItemPage.SetItemInfor(transform, false, string.Empty);
    }

    enum Aircraft {
        Ship, Drone
    }
}
