﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using Gemmob;
public enum IDScene {
    Home,
    Ingame
}
public class ManagerScene : SingletonResourcesPrefabAlive<ManagerScene> {
    [SerializeField] private GameObject goLoading = null;
    protected override void OnAwake() {
        base.OnAwake();
        goLoading.SetActive(false);
    }

    public void Load(string name) {
        PoolManager.Instance.DestroyAll();
        LoadAsync(name);
    }

    public void Load(int index) {
        PoolManager.Instance.DestroyAll();
        LoadAsync(index);
    }

    public void Load(IDScene ID) {
        PoolManager.Instance.DestroyAll();
        goLoading.SetActive(true);
        LoadAsync((int)ID, null, ()=> { goLoading.SetActive(false); });
    }

    public AsyncOperation LoadAsync(string name, Action<float> onLoadding = null, Action onFinish = null, float delayTime = 0) {
        AsyncOperation async = LoadAsync(name);
        StartCoroutine(IEWaitForDone(async, onLoadding, onFinish, delayTime));
        return async;
    }

    public AsyncOperation LoadAsync(int index, Action<float> onLoadding = null, Action onFinish = null, float delayTime = 0) {
        AsyncOperation async = LoadAsync(index);
        StartCoroutine(IEWaitForDone(async, onLoadding, onFinish, delayTime));
        return async;
    }

    public AsyncOperation LoadAsync(string name) {
        return SceneManager.LoadSceneAsync(name);
    }

    public AsyncOperation LoadAsync(int index) {
        return SceneManager.LoadSceneAsync(index);
    }

    #region IEnumerator
    private IEnumerator IEWaitForDone(AsyncOperation async, Action<float> onLoadding = null, Action onFinish = null, float delayTime = 0) {
        if (delayTime > 0) async.allowSceneActivation = false;
        while (delayTime > 0) {
            delayTime -= Time.deltaTime;
            onLoadding?.Invoke(async.progress);
            yield return Yielders.EndOfFrame;
        }
        async.allowSceneActivation = true;
		while(!async.isDone) {
            onLoadding?.Invoke(async.progress);
            yield return Yielders.EndOfFrame;
		}
        onLoadding?.Invoke(1f);
        onFinish?.Invoke();
    }
    #endregion
}