﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using Gemmob;
using UnityEngine;

public class PathManager : SingletonBind<PathManager> {
    [Serializable]
    public class Points {
        public List<Vector3> Positions;
        public List<Vector3> FlipXPositions;
    }

    public List<Points> Paths;
    protected override void OnAwake() {
        InitPathNormal();
    }

    [ContextMenu("Init Path")]
    public void InitPathNormal() {
        Paths.Clear();
        foreach (Transform child in transform) {
            child.DOKill();
            List<Vector3> lst = new List<Vector3>();
            lst.AddRange(child.GetComponent<DOTweenPath>().wps);
            Paths.Add(new Points { Positions = lst });
        }

        if (Application.isPlaying) {
            transform.DeleteImmediateAllChilds();
        }

        foreach (var p in Paths) {
            for (int i = 0; i < p.Positions.Count; i++) {
                p.FlipXPositions = p.Positions.FlipList();
            }
        }
    }

}