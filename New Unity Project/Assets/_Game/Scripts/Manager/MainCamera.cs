﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : SingletonBind<MainCamera> {
    private Vector2 maxSize = Vector2.zero;
    [SerializeField] private Camera mainCamera = null;

    protected override void OnAwake() {
        base.OnAwake();
        maxSize = mainCamera.orthographicSize * new Vector2((float)Screen.width / Screen.height, 1f);
    }
    public Vector2 max => maxSize;
    public Vector2 maxBullet => maxSize;
    public Camera main => mainCamera;
    private void OnValidate() {
        mainCamera = transform.GetComponentInChildren<Camera>();
    }

    public void ShakeCamera(float time) {
        Vector3 shakeVector = new Vector3(1f, 1f, 0);
        transform.DOShakePosition(time, shakeVector, 5, 10f, false, true);
    }

#if UNITY_EDITOR
    void OnDrawGizmos() {

        //Gizmos.color = Color.cyan;
        //Gizmos.DrawLine(new Vector3(-BulletMoveRangX, BulletMoveRangY), new Vector3(BulletMoveRangX, BulletMoveRangY));
        //Gizmos.DrawLine(new Vector3(BulletMoveRangX, BulletMoveRangY), new Vector3(BulletMoveRangX, -BulletMoveRangY));
        //Gizmos.DrawLine(new Vector3(BulletMoveRangX, -BulletMoveRangY), new Vector3(-BulletMoveRangX, -BulletMoveRangY));
        //Gizmos.DrawLine(new Vector3(-BulletMoveRangX, -BulletMoveRangY), new Vector3(-BulletMoveRangX, BulletMoveRangY));
        //UnityEditor.Handles.Label(new Vector3(-BulletMoveRangX + .2f, BulletMoveRangY), "Bullet bounds");

        Gizmos.color = Color.yellow;
        for(int i = 0; i < GameConfig.numberNodeX; i++) {
            for(int j = 0; j < GameConfig.numberNodeY; j++) {
                var x = GameConfig.distanceX *(i - GameConfig.numberNodeX/2);
                var y = GameConfig.distanceY * j;
                Gizmos.DrawWireSphere(GameConfig.customCenter + new Vector3(x, y), 0.05f);
            }
        }
    }

#endif
}
