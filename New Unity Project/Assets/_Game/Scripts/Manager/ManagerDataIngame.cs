﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerDataIngame : SingletonBind<ManagerDataIngame> {
    [Header("TAIL"), Space]
    [SerializeField] private List<GameObject> listTail = new List<GameObject>();
    [Header("PLAYER BULLET HIT EFFECT")]
    public List<GameObject> PlayerBulletHitEffects;
    [Header("PLAYER FIRE EFFECT")]
    public List<GameObject> PlayerFireEffects;
    [Header("PLAYER TAIL EFFECT")]
    public List<GameObject> PlayerTailEffects;
    [Header("ENEMY DIE EFFECT")]
    public List<GameObject> EnemyDieEffects;
    [Header("GET COIN EFFTECT")]
    public GameObject GetCoinEffects;
    [Header("ITEMS")]
    public List<ItemBase> Items;
    public GameObject GetTailShip(int index) {
        return listTail[index];
    }
}
