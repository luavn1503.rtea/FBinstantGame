﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMax : ItemBase {
    protected override void ActiveEffect() {
        ManagerPlayer.Instance.UpLevelShip(true);
    }
}
