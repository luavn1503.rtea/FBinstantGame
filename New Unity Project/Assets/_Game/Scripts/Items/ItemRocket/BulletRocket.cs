﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletRocket : BulletBase
{
    // Start is called before the first frame update
    [SerializeField] private Transform enemyTarget = null;

    [ContextMenu("Init")]
    public override void Init() {
        enemyTarget = null;
        base.Init();
    }

    protected override void Active() {
        rg2D.velocity = direction.normalized * speed;
        DOVirtual.DelayedCall(0.2f,()=> {
            enemyTarget = ManagerEnemies.Instance.GetEnemyClose();
        });    
    }

    protected override void FixedUpdate() {
        if(enemyTarget!=null) {
            if(!enemyTarget.GetComponent<EnemyBase>().IsLive || !enemyTarget.gameObject.activeInHierarchy) {
                enemyTarget = ManagerEnemies.Instance.GetEnemyClose();
                if(enemyTarget == null)
                    return;
            }
            direction = Vector2.Lerp(direction,(enemyTarget.position - transform.position).normalized,Time.deltaTime*2f);
            rg2D.velocity = direction.normalized * speed;
        }
        base.FixedUpdate();
    }

    protected override void OnTriggerEnter2D(Collider2D coll) {
        if(!gameObject.activeInHierarchy) {
            return;
        }
        if(coll.CompareTag(TagDefine.Enemy)) {
            coll.GetComponent<ITakeDamage>().TakeDamage(dame);
            RecycleBullet();
        } 
    }
}
