﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using static EnumDefine;

public class ShipEquipmentPanel : EquipmentPanelBase {
    private ShipEquipmentItem itemSelected = null;
    private ShipPositionEquipemntItem shipPosSelected = null;
    private SkillEquipmentPanel skillEquipment = null;
    private ShipEvolvePanel shipEvolve = null;
    private List<ShipEquipmentItem> listItems = new List<ShipEquipmentItem>();
    [SerializeField] private RectTransform contents = null;
    [SerializeField] private ShipEquipmentItem itemPrefab = null;
    [Header("Ship")]
    [SerializeField] private List<ShipPositionEquipemntItem> shipssPosEquipment = null;
    public override EnumDefine.TypeItem typeItem => EnumDefine.TypeItem.Ship;

    public void Initialize(EquipmentPage equipmentPage, InfoEquipmentPanel infoEquipment, SelectedEquipment selectedEquipment, SkillEquipmentPanel skillEquipment, ShipEvolvePanel shipEvolve) {
        this.equipmentPage = equipmentPage;
        this.infoEquipment = infoEquipment;
        this.selectedEquipment = selectedEquipment;
        this.skillEquipment = skillEquipment;
        this.shipEvolve = shipEvolve;
        foreach(var item in shipssPosEquipment) { item.Initialize(this); }
        var ships = ShipsData.Instance.Ships;
        LoadItems(ships);
    }

    public ShipData hardData {
        get {
            if(itemSelected == null) {
                Logs.Log("Not Select Item");
                return null;
            }
            return itemSelected.hardData;
        }
    }

    protected void LoadItems(List<ShipData> dataShip) {
        foreach(var item in dataShip) {
            var itemCreate = itemPrefab.Spawn(contents);
            itemCreate.Initialize(this, item);
            listItems.Add(itemCreate);
        }
    }

    public void Show() {
        foreach(var item in shipssPosEquipment) { item.Show(); }
    }

    public void Filter(EnumDefine.TypeShip type, EnumDefine.IDShip idShip = EnumDefine.IDShip.Ship_01) {
        foreach(var item in listItems) {
            if(item.hardData.TYPE == type) {
                if(!item.gameObject.activeSelf) {
                    item.gameObject.SetActive(true);
                }
            }
            else {
                if(item.gameObject.activeSelf) {
                    item.gameObject.SetActive(false);
                }
            }
        }
        if(type != EnumDefine.TypeShip.None) { 
            OnSelectItem(idShip); 
        }
        else {
            if(shipPosSelected != null) {
                shipPosSelected.OnUnSelect();
                shipPosSelected = null;
            }
        }
    }

    public void OnSelectItem(EnumDefine.IDShip ID) {
        var item = listItems.Find(x => x.hardData.ID == ID);
        OnSelectItem(item);
    }

    public void OnSelectItem(ShipEquipmentItem itemSelected) {
        if(this.itemSelected != null && this.itemSelected.hardData.ID == itemSelected.hardData.ID) {

        }
        else {
            itemSelected.OnSelect();
            if(this.itemSelected != null) { this.itemSelected.OnUnSelect(); }
            this.itemSelected = itemSelected;
        }
        UpdateUI();
    }

    public void UpdateUI() {
        shipPosSelected.UpdateUI(hardData.ID);
        var position = itemSelected.transform.position + Vector3.down * 0.5f;
        var softData = ManagerData.Instance.AircraftData.GetDataShipInfo(hardData.ID);
        if(softData.IsUnlock()) {
            var curLevel = hardData.GetLevelData(softData.Level);
            if(softData.Level + 1 == hardData.MaxLevelInRank(softData.Rank)) {
                var strPower = string.Format("<color=\"white\">POWER </color><color=\"blue\"> {0}</color>", curLevel.statses.GetStats(IDStats.Power).Value);
                infoEquipment.SetInfo(softData.Level, strPower);
                infoEquipment.SetButtonUpgrade("MAX", false, null);
            }
            else {
                var nextLevel = hardData.GetLevelData(softData.Level + 1);
                var increaseCp = nextLevel.statses.GetStats(IDStats.Power).Value - curLevel.statses.GetStats(IDStats.Power).Value;
                var strPower = string.Format("<color=\"white\">POWER </color><color=\"yellow\"> {0}</color><color=\"green\"> +{1}</color>", curLevel.statses.GetStats(IDStats.Power).Value, increaseCp);
                var strRequireUpgrade = string.Format("<sprite=\"coin_icon\" name=\"coin_icon\"> {0}", nextLevel.Coin);
                int coinCurrency = ManagerData.Instance.PlayerInfo.GetCurrency(IDCurrency.Coin);
                infoEquipment.SetInfo(softData.Level, strPower);
                infoEquipment.SetButtonUpgrade(strRequireUpgrade, true, OnUpgrade);

            }
            if(softData.Rank == hardData.MaxRank) {
                infoEquipment.SetButtonEvovle("MAX", false, null);
            }
            else {
                var dataEvolve = hardData.GetDrebrisEvolve(softData.Rank + 1);
                int gemCurrency = ManagerData.Instance.PlayerInfo.GetCurrency(IDCurrency.Gem);
                var strRequireEvolve = string.Format("<sprite=\"icon_pzpiece\" name=\"icon_pzpiece\"> {0}/{1}", softData.Debris, dataEvolve.ItemNum);
                infoEquipment.SetButtonEvovle(strRequireEvolve, true, OnEvolve);
            }
            var shipEquiped = ManagerData.Instance.PlayerInfo.IsShipEquiped(hardData.ID, out var shipEquipedInfo);
            if(hardData.TYPE == EnumDefine.TypeShip.Drone) {
                if(shipEquiped) {
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("UNPICK", OnUnPick).SetActive(true, position);
                }
                else {
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("PICK", OnPick).SetActive(true, position);
                }
            }
            else {
                if(shipEquiped) {
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("EQUIPED", Equiped).SetActive(true, position);
                }
                else {
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("PICK", OnPick).SetActive(true, position);
                }
            }
        }
        else {
            var curLevel = hardData.GetLevelData(0);
            selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("BUY", OnBuy).SetActive(true, position);
            var strPower = string.Format("<color=\"white\">POWER </color><color=\"blue\"> {0}</color>", curLevel.statses.GetStats(IDStats.Power).Value);
            infoEquipment.SetInfo(softData.Level, strPower).SetButtonUpgrade("BUY", false, null).SetButtonEvovle("BUY", false, null);
        }
    }

    private void OnInformation() {
        UIHomeManager.Instance.Show<InformationPage>();
    }

    private void OnPick() {
        selectedEquipment.Active(false);
        ManagerData.Instance.PlayerInfo.SetShipEquiped(shipPosSelected.Direct, true, hardData.ID);


    }

    private void OnUnPick() {
        selectedEquipment.Active(false);
        ManagerData.Instance.PlayerInfo.SetShipEquiped(shipPosSelected.Direct, false, hardData.ID);

    }

    private void Equiped() {

    }

    private void OnBuy() {
        selectedEquipment.Active(false);
        var shipHardData = ShipsData.Instance.GetUpgradeShip(hardData.ID);
        var shipValue = -shipHardData.GetLevelData(0).Coin;
        ManagerData.Instance.PlayerInfo.AddCurrency(IDCurrency.Coin, shipValue);
        ManagerData.Instance.AircraftData.BuyShip(hardData.ID);
        UpdateUI();
    }

    private void OnUpgrade() {
        var softData = ManagerData.Instance.AircraftData.GetDataShipInfo(hardData.ID);
        if(softData.IsUnlock()) {
            var curLevel = hardData.GetLevelData(softData.Level);
            var nextLevel = hardData.GetLevelData(softData.Level + 1);
            if(softData.Level + 1 < hardData.MaxLevelInRank(softData.Rank)) {
                int coinCurrency = ManagerData.Instance.PlayerInfo.GetCurrency(IDCurrency.Coin);
                if(coinCurrency >= nextLevel.Coin) {
                    ManagerData.Instance.PlayerInfo.AddCurrency(IDCurrency.Coin, -nextLevel.Coin);
                    ManagerData.Instance.AircraftData.UpgradeShip(hardData.ID);
                    UpdateUI();
                }
                else {
                    Logs.Log(string.Format("You Not Enough {0}", IDCurrency.Coin));
                }
            }
            else {
                Logs.Log("Max Level");
            }
        }
        else {
            Logs.Log("You Need Unlock Ship");
        }
    }

    private void OnEvolve() {
        shipEvolve.RegisterEvelvo();
    }

    public void RegisterEquipShip(Direct direct) {
        var item = shipssPosEquipment.Find(x => x.Direct == direct);
        RegisterEquipShip(item);
    }

    public void RegisterEquipShip(ShipPositionEquipemntItem shipPosSelected) {
        if(this.shipPosSelected == null) {
            shipPosSelected?.OnSelect();
            this.shipPosSelected = shipPosSelected;
            ShowShips();
        }
        else {
            if(this.shipPosSelected.Direct == shipPosSelected.Direct) {
                Logs.Log(string.Format("Direct {0} Selected", shipPosSelected.Direct));
            }
            else {
                shipPosSelected?.OnSelect();
                this.shipPosSelected?.OnUnSelect();
                this.shipPosSelected = shipPosSelected;
                ShowShips();
            }
        }
    }

    private void ShowShips() {
        var type = shipPosSelected.Direct == EnumDefine.Direct.Center ? EnumDefine.TypeShip.Ship : EnumDefine.TypeShip.Drone;
        Filter(type, shipPosSelected.IDShip);
        skillEquipment.Filter(TypeSkill.None);
        selectedEquipment.Active(false);
    }
}
