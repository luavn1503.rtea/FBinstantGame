﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SelectedEquipment : MonoBehaviour {
    private Action onInfo = null;
    private Action onPick = null;
    [SerializeField] private TextMeshProUGUI txtInfo = null;
    [SerializeField] private TextMeshProUGUI txtPick = null;
    [SerializeField] protected ButtonBase btnInfo = null;
    [SerializeField] protected ButtonBase btnPick = null;
    public void Initialize() {
        btnPick.onClick.AddListener(OnPick);
        btnInfo.onClick.AddListener(OnInformation);
    }

    public void Active(bool enable, Vector3 position = default, Action onInfo = default, String strPick = default, Action onPick = default) {
        this.onInfo = onInfo;
        this.onPick = onPick;
        txtPick.text = strPick;
        transform.position = position;
        gameObject.SetActive(enable);
    }
    
    public SelectedEquipment SetActive(bool active, Vector3 position = default) {
        transform.position = position;
        gameObject.SetActive(active);
        return this;
    }

    public SelectedEquipment SetButtonInfo(string strTile, Action callBack, bool active = true) {
        txtInfo.text = strTile;
        onInfo = callBack;
        btnInfo.gameObject.SetActive(active);
        return this;
    }

    public SelectedEquipment SetButtonPick(string strTile, Action callBack, bool active = true) {
        txtPick.text = strTile;
        onPick = callBack;
        btnPick.gameObject.SetActive(active);
        return this;
    }

    public virtual void OnInformation() {
        onInfo?.Invoke();
    }

    public virtual void OnPick() {
        onPick?.Invoke();
    }
}
