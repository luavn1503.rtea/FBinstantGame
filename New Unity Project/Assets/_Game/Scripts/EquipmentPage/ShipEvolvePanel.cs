﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipEvolvePanel : MonoBehaviour {
    private ShipEquipmentPanel shipEquipment = null;
    [SerializeField] private ButtonBase btnEvolve = null;
    [SerializeField] private Button btnBack = null;
    [SerializeField] private StatsPanel statsPanel = null;
    [SerializeField] private List<ShipTranformItem> shipTranforms = new List<ShipTranformItem>();
    [SerializeField] private List<ShipDebrisItem> shipDebris = new List<ShipDebrisItem>();
    public void Initialize(ShipEquipmentPanel shipEquipment) {
        this.shipEquipment = shipEquipment;
        btnEvolve.onClick.AddListener(ButtonEvolve);
        btnBack.onClick.AddListener(ButtonBack);
        Active(false);
    }

    public void RegisterEvelvo() {
        UpdateUI();
        Active(true);
    }

    public void UpdateUI() {
        var hardData = ShipsData.Instance.GetUpgradeShip(shipEquipment.hardData.ID);
        var softData = ManagerData.Instance.AircraftData.GetDataShipInfo(shipEquipment.hardData.ID);
        statsPanel.RegisterStatses(hardData.GetLevelData(softData.Level).statses, hardData.GetLevelData(softData.Level + 1).statses);
        if(softData.Rank >= shipEquipment.hardData.MaxRank) {

        }
        else {
            foreach(var item in shipTranforms) {
                item.ChangeShip(shipEquipment.hardData.ID);
            }
            foreach(var item in shipDebris) {
                item.ChangeShip(shipEquipment.hardData.ID);
            }
        }
    }

    private void ButtonEvolve() {
        var softData = ManagerData.Instance.AircraftData.GetDataShipInfo(shipEquipment.hardData.ID);
        if(softData.IsUnlock()) {
            int gemCurrency = ManagerData.Instance.PlayerInfo.GetCurrency(EnumDefine.IDCurrency.Gem);
            var evolveData = shipEquipment.hardData.GetDrebrisEvolve(softData.Rank + 1);
            if(softData.Debris >= evolveData.ItemNum) {
                var gemRequire = evolveData.GemNum;
                if(gemCurrency >= gemRequire) {
                    ManagerData.Instance.PlayerInfo.AddCurrency(EnumDefine.IDCurrency.Gem, -gemRequire);
                    ManagerData.Instance.AircraftData.AddShipDebris(shipEquipment.hardData.ID, -evolveData.ItemNum);
                    ManagerData.Instance.AircraftData.EvolveAircraft(shipEquipment.hardData.ID);
                    shipEquipment.UpdateUI();
                    UpdateUI();
                }
                else {
                    Logs.Log(string.Format("You Not Enough {0}", EnumDefine.IDCurrency.Gem));
                }
            }
            else {
                Logs.Log(string.Format("You Not Enough Debris"));
            }
        }
        else {
            Logs.Log("You Need Unlock Ship");
        }
    }

    private void ButtonBack() {
        Active(false);
    }

    private void Active(bool active) {
        gameObject.SetActive(active);
    }
}
