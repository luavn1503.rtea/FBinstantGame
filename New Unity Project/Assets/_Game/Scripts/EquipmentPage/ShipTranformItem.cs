﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShipTranformItem : MonoBehaviour {
    [SerializeField] private EnumDefine.Direct direct = EnumDefine.Direct.Left;
    [SerializeField] protected Image imgIcon = null;
    [SerializeField] protected Image imgRank = null;
    [SerializeField] protected TextMeshProUGUI txtRank = null;
    [SerializeField] protected ButtonBase btnSelect = null;
    [SerializeField] protected GameObject goSelected = null;
    [SerializeField] protected GameObject goEquiped = null;

    public EnumDefine.Direct Direct => direct;

    public void ChangeShip(EnumDefine.IDShip id) {
        var hardData = ShipsData.Instance.GetUpgradeShip(id);
        var softData = ManagerData.Instance.AircraftData.GetDataShipInfo(id);
        var rank = direct == EnumDefine.Direct.Left ? softData.Rank : softData.Rank + 1;
        var isEquiped = ManagerData.Instance.PlayerInfo.IsShipEquiped(id, out var shipEquipedInfo);
        SetRank(rank);
        SetEquiped(isEquiped);
        SetIcon(hardData.GetIcon(rank));
    }

    public void SetIcon(Sprite sprIcon) {
        if(imgIcon == null) {
            Logs.Log("Image Icon Null");
            return;
        }
        if(sprIcon == null) {
            Logs.Log("Sprite Icon Null");
            return;
        }
        imgIcon.sprite = sprIcon;
        var perfectSize = imgIcon.GetComponent<PerfectSize>();
        if(perfectSize) {
            var sizeFix = sprIcon.rect.size;
            perfectSize.AutoSize(sizeFix);
        }
    }

    public void SetRank(EnumDefine.Rank rank) {
        if(txtRank == null) {
            Logs.Log("Text Rank Null");
            return;
        }
        txtRank.text = rank.ToString();
        imgRank.sprite = SpritesData.Instance.GetBorderRank(rank);
    }

    public void SetEquiped(bool active) {
        if(goEquiped == null) {
            Logs.Log("Equiped Null");
            return;
        }
        goEquiped.SetActive(active);
    }
}
