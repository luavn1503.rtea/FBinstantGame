﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEquipmentItem : ItemEquipmentBase {
    private ShipEquipmentPanel equipmentPanel = null;
    [HideInInspector] public ShipData hardData = null;

    public ShipEquipmentItem Initialize(ShipEquipmentPanel equipmentPanel, ShipData hardData) {
        this.hardData = hardData;
        this.equipmentPanel = equipmentPanel;
        btnSelect.onClick.AddListener(ButtonSelect);
        UpdateUI();
        return this;
    }
    public void UpdateUI() {
        var softData = ManagerData.Instance.AircraftData.GetDataShipInfo(hardData.ID);
        if(softData.IsUnlock()) {
            var sprIcon = hardData.GetIcon(softData.Rank);
            SetIcon(sprIcon);
            SetRank(softData.Rank);
            SetActiveDebris(false);
            var shipEquiped = ManagerData.Instance.PlayerInfo.IsShipEquiped(hardData.ID, out var shipEquipedInfo);
            SetEquiped(shipEquiped);
        }
        else {
            var sprIcon = hardData.GetIcon(EnumDefine.Rank.D);
            SetIcon(sprIcon);
            SetRank(EnumDefine.Rank.D);
            SetActiveDebris(false);
            SetEquiped(false);
        }
    }

    protected override void ButtonSelect() {
        base.ButtonSelect();
        equipmentPanel.OnSelectItem(this);
    }
}
