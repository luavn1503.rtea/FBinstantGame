﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gemmob;
using Polaris.Tutorial;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static EnumDefine;

public class EquipmentPage : Frame {
    [SerializeField] private ButtonBase btnBack = null;
    [SerializeField] private ShipEquipmentPanel shipEquipment = null;
    [SerializeField] private SkillEquipmentPanel skillEquipment = null;
    [SerializeField] private SelectedEquipment selectedEquipment = null;
    [SerializeField] private InfoEquipmentPanel infoEquipment = null;
    [SerializeField] private ShipEvolvePanel shipEvolve = null;

    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        shipEvolve.Initialize(shipEquipment);
        shipEquipment.Initialize(this, infoEquipment, selectedEquipment, skillEquipment, shipEvolve);
        skillEquipment.Initialize(this, infoEquipment, selectedEquipment, shipEquipment);
        selectedEquipment.Initialize();
        infoEquipment.Initialize();
        btnBack.AddListener(ButtonBack);
    }

    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        UIHomeManager.Instance.SetActiveBot(false);
        shipEquipment.Show();
    }

    protected override void OnHide(Action onCompleted = null, bool instant = false) {
        base.OnHide(onCompleted, instant);
        UIHomeManager.Instance.SetActiveBot(true);
        UIHomeManager.Instance.Show<HomePage>();
    }

    public void SetupEquipment(TypeItem typeItem, Direct direct = Direct.Center) {
        switch(typeItem) {
            case TypeItem.Ship:
                
                shipEquipment.RegisterEquipShip(direct);
                //Debug.Log(IDShip.Ship_01);
                break;
            case TypeItem.Skill:
                skillEquipment.RegisterEquipShip(direct);
                break;
        }
    }

    private void ButtonBack() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        Hide();
    }
}






