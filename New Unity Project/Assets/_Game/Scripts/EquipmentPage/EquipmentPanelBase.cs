﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EquipmentPanelBase : MonoBehaviour {
    protected EquipmentPage equipmentPage = null;
    protected InfoEquipmentPanel infoEquipment = null;
    protected SelectedEquipment selectedEquipment = null;
    public abstract EnumDefine.TypeItem typeItem { get; }

}
