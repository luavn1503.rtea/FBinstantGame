﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillEquipmentItem : ItemEquipmentBase {
    private SkillEquipmentPanel skillEquipment = null;
    public SkillData hardData = null;
    public SkillEquipmentItem Initialize(SkillEquipmentPanel skillEquipment, SkillData hardData) {
        this.skillEquipment = skillEquipment;
        this.hardData = hardData;
        btnSelect.onClick.AddListener(ButtonSelect);
        UpdateUI();
        return this;
    }
    public void UpdateUI() {
        var softData = ManagerData.Instance.skillsInfo.GetSkillInfo(hardData.ID);
        if(softData.IsUnlock()) {
            SetIcon(hardData.Icon);
            SetRank(EnumDefine.Rank.D);
            SetActiveDebris(false);
            var shipEquiped = ManagerData.Instance.PlayerInfo.IsSkillEquiped(hardData.ID, out var shipEquipedInfo);
            SetEquiped(shipEquiped);
        }
        else {
            SetIcon(hardData.Icon);
            SetRank(EnumDefine.Rank.D);
            SetActiveDebris(false);
            SetEquiped(false);
        }
    }

    protected override void ButtonSelect() {
        base.ButtonSelect();
        skillEquipment.OnSelectItem(this);
    }
}
