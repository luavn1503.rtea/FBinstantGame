﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipPositionEquipemntItem : MonoBehaviour {
    private UIShipPreview shipPreviewCreate = null;
    private ShipEquipmentPanel shipEquipment = null;
    [SerializeField] private EnumDefine.Direct direct = EnumDefine.Direct.Up;
    [SerializeField] private EnumDefine.IDShip idShip = EnumDefine.IDShip.Ship_01;
    [SerializeField] private ButtonBase btnSelect = null;
    [SerializeField] private GameObject goSelected = null;
    public EnumDefine.Direct Direct => direct;
    public EnumDefine.IDShip IDShip => idShip;

    public void Initialize(ShipEquipmentPanel shipEquipment) {
        this.shipEquipment = shipEquipment;
        btnSelect.AddListener(ButtonSelect);
    }

    public void Show() {
        var equiped  = ManagerData.Instance.PlayerInfo.ShipEquipedAtDirect(Direct, out var shipEquipedInfo);
        if(equiped) { UpdateUI(shipEquipedInfo.IDShip); }
        else { if(shipPreviewCreate != null) { shipPreviewCreate.Recycle(); } }
    }

    public void UpdateUI(EnumDefine.IDShip idShip) {
        this.idShip = idShip;
        if(shipPreviewCreate != null) { shipPreviewCreate.Recycle(); }
        var shipPreviewPrefab = ManagerData.Instance.GetShipPreview(idShip);
        if(shipPreviewPrefab == null) { return; }
        shipPreviewCreate = shipPreviewPrefab.Spawn(transform);
        shipPreviewCreate.transform.localScale = Vector3.one;
        shipPreviewCreate.transform.localPosition = Vector3.zero;
        var shipDataInfo = ManagerData.Instance.AircraftData.GetDataShipInfo(idShip);
        shipPreviewCreate.SetSkin(string.Format("Skin_Lv{0}", (int)shipDataInfo.Rank));
    }


    private void ButtonSelect() {
        shipEquipment.RegisterEquipShip(this);
    }

    public void OnSelect() {
        if(goSelected != null) { goSelected.SetActive(true); }
    }

    public void OnUnSelect() {
        if(goSelected != null) { goSelected.SetActive(true); }
    }
}
