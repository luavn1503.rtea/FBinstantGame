﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillPositionEquipmentItem : MonoBehaviour {
    private SkillEquipmentPanel skillEquipment = null;
    [SerializeField] private Image imgIcon = null;
    [SerializeField] private EnumDefine.Direct direct = EnumDefine.Direct.Up;
    [SerializeField] private EnumDefine.IDSkill idSkill = EnumDefine.IDSkill.Shield;
    [SerializeField] private ButtonBase btnSelect = null;
    [SerializeField] private GameObject goSelected = null;
    public EnumDefine.Direct Direct => direct;
    public EnumDefine.IDSkill IDSkill => idSkill;
    public void Initialize(SkillEquipmentPanel skillEquipment) {
        this.skillEquipment = skillEquipment;
        btnSelect.AddListener(ButtonSelect);
        var equiped  = ManagerData.Instance.PlayerInfo.SkillEquipedAtDirect(Direct, out var skillEquipedInfo);
        if(equiped) { UpdateUI(skillEquipedInfo.IDSkill); }
    }

    public void UpdateUI(EnumDefine.IDSkill idSkill) {
        this.idSkill = idSkill;
        var hardData = SkillsData.Instance.GetSkillData(idSkill);
        SetIcon(hardData.Icon);
    }

    private void ButtonSelect() {
        skillEquipment.RegisterEquipSkill(this);
    }

    public void OnSelect() {
        if(goSelected != null) { goSelected.SetActive(true); }
    }

    public void OnUnSelect() {
        if(goSelected != null) { goSelected.SetActive(true); }
    }

    public void SetIcon(Sprite sprIcon) {
        if(imgIcon == null) {
            Logs.Log("Image Icon Null");
            return;
        }
        if(sprIcon == null) {
            Logs.Log("Sprite Icon Null");
            return;
        }
        imgIcon.sprite = sprIcon;
        var perfectSize = imgIcon.GetComponent<PerfectSize>();
        if(perfectSize) {
            var sizeFix = sprIcon.rect.size;
            perfectSize.AutoSize(sizeFix);
        }
    }
}
