﻿using DG.Tweening;
using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float lerpSpeed = 10f;
    private bool isMoveToPlayer  = false;

    public void Init() {
        isMoveToPlayer = false;
        Active();
    }

    private void Active() {
        transform.DOMove((Vector2)transform.position+Vector2.down*2, 3f).OnComplete(() => { isMoveToPlayer = true; });
    }

    void Update() {
        if(isMoveToPlayer) {
            transform.position = Vector3.Lerp(transform.position, ManagerPlayer.Instance.GetShipPossition().possition, Time.deltaTime * lerpSpeed);
            if(Vector2.Distance(transform.position, ManagerPlayer.Instance.GetShipPossition().possition) < .5f)
                gameObject.Recycle();
        }

    }

    void OnDisable() {
        transform.DOKill();
    }
}
