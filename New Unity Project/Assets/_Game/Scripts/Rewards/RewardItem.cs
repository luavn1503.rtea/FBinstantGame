﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static EnumDefine;

public class RewardItem : MonoBehaviour {
    [SerializeField] private Image imgIcon = null;
    [SerializeField] private TextMeshProUGUI txtAmount = null;
    [SerializeField] private GameObject goDebris = null;
    public void Initialize(RewardData reward) {

    }

    public void UpdateUI(RewardData reward) {
        var icon = ManagerData.Instance.GetIconReward(reward.TYPE, reward.IdCurrency, reward.IdShip);
        Extention.SetSpriteImage(imgIcon, icon);
        Extention.SetText(txtAmount, reward.Amount.ToString());
        Extention.SetActiveObject(goDebris, reward.TYPE == RewardsType.Debris);
    }

    private void SetAmount(int amount) {
        if(txtAmount == null) { return; }
        txtAmount.text = amount.ToString();
    }

    private void SetIcon(Sprite icon) {
        if (imgIcon == null) { return; }
        imgIcon.sprite = icon;
        var perfectSize = imgIcon.GetComponent<PerfectSize>();
        if (perfectSize == null) { return; }
        var sizeIcon = icon.rect.size;
        perfectSize.AutoSize(sizeIcon);
    }

    private void SetDebris(bool active) {
        if (goDebris == null) { return; }
        goDebris.SetActive(active);
    }
}
