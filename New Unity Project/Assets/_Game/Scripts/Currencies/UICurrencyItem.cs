﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UICurrencyItem : MonoBehaviour {
    private CurrencyData hardData = null;
    [SerializeField] private EnumDefine.IDCurrency idCurrency = EnumDefine.IDCurrency.Coin;
    [SerializeField] private TextMeshProUGUI txtAmount = null;
    [SerializeField] private Image imgIcon = null;

    private void Start() {
        hardData = CurrenciesData.Instance.GetCurrency(idCurrency);
        imgIcon.sprite = hardData.Icon;
        imgIcon.AutoSize(hardData.Icon.rect.size);
    }

    private void OnEnable() {
        var currencyValue = ManagerData.Instance.PlayerInfo.GetCurrency(idCurrency);
        UpdateUI(currencyValue);
        EventDispatcher.Instance.AddListener<StructDefine.CurrencyChange>(ChangeCurrency);
    }

    private void OnDisable() {
        EventDispatcher.Instance.RemoveListener<StructDefine.CurrencyChange>(ChangeCurrency);
    }

    private void ChangeCurrency(StructDefine.CurrencyChange change) {
        if (change.idCurrency == idCurrency) {
            UpdateUI(change.amount);
        }
    }

    private void UpdateUI(int amountChange) {
        if(txtAmount == null) { return; }
        var currencyValue = ManagerData.Instance.PlayerInfo.GetCurrency(idCurrency);
        txtAmount.text = currencyValue.ToString();
    }
}
