﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXHome : SingletonBind<SFXHome> {
    //[SerializeField]
   // private List<AudioClip> sourceMusic = new List<AudioClip>();
    [SerializeField]
    private List<AudioClip> sourceAudio = new List<AudioClip>();
    private void Start() {
        //int id = Random.Range(0, sourceMusic.Count);
        //SFX.Instance.PlayBackgroundMusic(sourceMusic[id]);
    }
    public void PlaySound(Audio id) {
        SFX.Instance.PlaySoundEffect(sourceAudio[(int)id]);
    }
    public enum Audio {
        Click_Button_01,
        Click_Button_02,
        Click_OnBox,
        Cell_Stuff,
        Not_Enough_Material,
        Paying_Stuff,
        Material_Open_01,
        Material_Open_02,
        Wait_Board_01,
        IncomeCoin_01,
        Upgrade_Weapon,
        None,
    }
}
