﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModePlayPanel : MonoBehaviour {
    private ModePlayItem itemSelected = null;
    private List<ModePlayItem> listItem = null;
    [SerializeField] private ModePlayItem itemPrefab = null;
    [SerializeField] private RectTransform contents = null;

    public void Initialize() {
        LoadItems();
    }

    protected void LoadItems() {
        var data = SmallsData.Instance.modesPlay.modesPlay;
        listItem = new List<ModePlayItem>();
        foreach(var item in data) {
            var itemCreate = itemPrefab.Spawn(contents);
            itemCreate.Initialize(this, item);
            listItem.Add(itemCreate);
        }
    }

    public void UpdateUI() {
        var level = ManagerGame.Instance.levelSelected;
        var softData = ManagerData.Instance.levelInfo.GetLevelDataInfo(level);
        foreach(var item in listItem) {
            item.UpdateUI(EnumDefine.IDModePlay.Medium);
        }
    }

    public void OnSelectItem(ModePlayItem itemSelected) {
        if(this.itemSelected) { this.itemSelected.OnSelect(false); }
        this.itemSelected = itemSelected;
        this.itemSelected.OnSelect(true);
    }
}
