﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class ModePlayItem : MonoBehaviour {
    private ModePlayData hardData = null;
    private ModePlayPanel modePlayPanel = null;
    [SerializeField] private Image imgIcon = null;
    [SerializeField] private TextMeshProUGUI txtName = null;
    [SerializeField] private GameObject goLock = null;
    [SerializeField] private GameObject goSelected = null;
    [SerializeField] private ButtonBase btnSelect = null;

    public void Initialize(ModePlayPanel modePlayPanel, ModePlayData hardData) {
        this.modePlayPanel = modePlayPanel;
        this.hardData = hardData;
        Extention.SetText(txtName, hardData.Name);
        Extention.SetSpriteImage(imgIcon, hardData.Icon);
    }

    public void UpdateUI(EnumDefine.IDModePlay curMode) {
        btnSelect.RemoveAllListener();
        var unlock = hardData.ID <= curMode;
        Extention.SetActiveObject(goLock, !unlock);
        if(unlock) {
            btnSelect.AddListener(ButtonSelect);
        }
        else {
            btnSelect.AddListener(ButtonLock);
        }
    }

    private void ButtonLock() {

    }

    private void ButtonSelect() {
        modePlayPanel.OnSelectItem(this);
    }

    public void OnSelect(bool select) {
        Extention.SetActiveObject(goSelected, select);
    }
}
