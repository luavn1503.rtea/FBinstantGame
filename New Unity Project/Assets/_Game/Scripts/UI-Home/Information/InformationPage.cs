﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InformationPage : Frame {
    [SerializeField] private ButtonBase btnPreview = null;
    [SerializeField] private ButtonBase btnTryit = null;

    private void Start() {
        btnPreview.AddListener(ButtonPreview);
        btnTryit.AddListener(ButtonTryIt);
    }

    private void ButtonPreview() {

    }

    private void ButtonTryIt() {
        Hide();
    }
}
