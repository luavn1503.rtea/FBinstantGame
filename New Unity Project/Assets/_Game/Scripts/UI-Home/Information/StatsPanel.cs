﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsPanel : MonoBehaviour {
    private List<StatsItem> listItem = null;
    [SerializeField] private StatsItem itemPrefab = null;
    [SerializeField] private RectTransform contents = null;

    public void RegisterStatses(Statses curStatses, Statses lastStatses = null) {
        if(listItem == null) { listItem = new List<StatsItem>(); }
        int maxCount = curStatses.data.Count > listItem.Count ? curStatses.data.Count : listItem.Count;
        for(int i = 0; i < maxCount; i++) {
            if(i < listItem.Count) {
                if(i < curStatses.data.Count) {
                    listItem[i].UpdateUI(curStatses.data[i], lastStatses == null ? null : lastStatses.data[i]);
                }
                else {
                    listItem[i].gameObject.SetActive(false);
                }
            }
            else {
                var itemCreate = itemPrefab.Spawn(contents);
                itemCreate.transform.localScale = Vector3.one;
                itemCreate.UpdateUI(curStatses.data[i], lastStatses == null ? null : lastStatses.data[i]);
                listItem.Add(itemCreate);
            }
        }
    }
}
