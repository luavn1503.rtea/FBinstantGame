﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ContainsChestPanel : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI txtButtonBuy = null;
    [SerializeField] private RectTransform rectMain = null;
    [SerializeField] private ButtonBase btnClose = null;
    [SerializeField] private ButtonBase btnBuy = null;
    [SerializeField] private RewardsPanel rewardsGuar = null;
    [SerializeField] private RewardsPanel rewardsChange = null;

    public void Initialize() {
        rewardsGuar.Initialize();
        rewardsChange.Initialize();
        btnClose.onClick.AddListener(ButtonClose);
        btnBuy.onClick.AddListener(ButtonBuy);
    }

    public void UpdateUI(ChestData hardData) {
        rectMain.localScale = Vector3.zero;
        rectMain.DOScale(Vector3.one, 0.2f);
        Active(true);
    }

    private void ButtonBuy() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
    }

    private void ButtonClose() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        Active(false);
    }

    private void Active(bool active) {
        gameObject.SetActive(active);
    }
}
