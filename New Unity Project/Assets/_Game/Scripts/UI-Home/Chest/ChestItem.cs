﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChestItem : MonoBehaviour {
    private ChestPage chestPage = null;
    private ChestData hardData = null;
    private ContainsChestPanel containsChest = null;
    [SerializeField] private Image imgIcon = null;
    [SerializeField] private TextMeshProUGUI txtTitle = null;
    [SerializeField] private TextMeshProUGUI txtPrice = null;
    [SerializeField] private ButtonBase btnSelect = null;

    public void Initialize(ChestPage chestPage, ChestData hardData, ContainsChestPanel containsChest) {
        this.chestPage = chestPage;
        this.hardData = hardData;
        this.containsChest = containsChest;
        btnSelect.onClick.AddListener(ButtonSelect);

        UpdateUI();
    }

    public void UpdateUI() {
        Extention.SetSpriteImage(imgIcon, hardData.Icon);
        Extention.SetText(txtTitle, hardData.Name);
        if(hardData.Price == 0) {
            Extention.SetText(txtPrice, "FREE");
        }
        else {
            var strCurrency = hardData.IdCurrency == EnumDefine.IDCurrency.Coin ? "icon_coin"
                : hardData.IdCurrency == EnumDefine.IDCurrency.Gem ? "icon_gem" : string.Empty;
            var strPrice = string.Format("<sprite=\"{0}\" name=\"{1}\"> {2}", strCurrency, strCurrency, hardData.Price);
            Extention.SetText(txtPrice, strPrice);
        }
    }

    private void ButtonSelect() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        containsChest.UpdateUI(hardData);
    }
}
