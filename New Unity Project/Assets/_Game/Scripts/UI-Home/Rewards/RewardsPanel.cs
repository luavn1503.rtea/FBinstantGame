﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardsPanel : MonoBehaviour {
    private List<RewardItem> listItem = new List<RewardItem>();
    [SerializeField] private RectTransform contents = null;
    [SerializeField] private RewardItem itemPrefab = null;

    public void Initialize() {
        GenererRewards();
    }

    private void GenererRewards() {
        for(int i = 0; i < 3; i++) {
            var itemCreate = itemPrefab.Spawn(contents);
            itemCreate.transform.localScale = Vector3.one;
            itemCreate.transform.localPosition = Vector3.zero;
            listItem.Add(itemCreate);
        }
    }

    public void ShowRewards(RewardsData rewards) {
        if(listItem == null) { listItem = new List<RewardItem>(); }
        int maxCount = rewards.data.Count > listItem.Count ? rewards.data.Count : listItem.Count;
        for(int i = 0; i < maxCount; i++) {
            if(i < listItem.Count) {
                if(i < rewards.data.Count) {
                    listItem[i].UpdateUI(rewards.data[i]);
                }
                else {
                    listItem[i].gameObject.SetActive(false);
                }
            }
            else {
                var itemCreate = itemPrefab.Spawn(contents);
                itemCreate.transform.localScale = Vector3.one;
                itemCreate.UpdateUI(rewards.data[i]);
                listItem.Add(itemCreate);
            }
        }
    }
}
