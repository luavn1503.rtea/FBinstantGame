﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionTag : MonoBehaviour {
    private MissionPage missionPage = null;
    [SerializeField] private EnumDefine.TypeMission typeMission = EnumDefine.TypeMission.Mission;
    [SerializeField] private ButtonBase btnSelect = null;
    [SerializeField] private GameObject goSelected = null;
    [SerializeField] private GameObject goUnSelected = null;

    public EnumDefine.TypeMission TYPE => typeMission;

    public void Initialize(MissionPage missionPage) {
        this.missionPage = missionPage;
        OnSelect(false);
        btnSelect.onClick.AddListener(ButtonSelect);
    }

    private void ButtonSelect() {
        missionPage.OnSelectMission(this);
    }

    public void OnSelect(bool selected) {
        goSelected.SetActive(selected);
        goUnSelected.SetActive(!selected);
    }
}
