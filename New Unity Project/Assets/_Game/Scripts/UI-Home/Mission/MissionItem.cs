﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MissionItem : MonoBehaviour {
    private float sizeX = 0f;
    private EnumDefine.TypeMission typeMission = EnumDefine.TypeMission.Mission;
    private MissionPage missionPage = null;
    [SerializeField] private Image imgIcon = null;
    [SerializeField] private RectTransform rectProcess = null;
    [SerializeField] private Image imgButtonCliam = null;
    [SerializeField] private TextMeshProUGUI txtAmountReward = null;
    [SerializeField] private TextMeshProUGUI txtTitle = null;
    [SerializeField] private TextMeshProUGUI txtProcess = null;
    [SerializeField] private ButtonBase btnClaim = null;
    [Space, Header("Sprite")]
    [SerializeField] private Sprite sprClaim = null;
    [SerializeField] private Sprite sprUnClaim = null;
    [HideInInspector] public QuestData hardData = null;

    public EnumDefine.TypeMission TYPE => typeMission;


    public void Initialize(MissionPage missionPage, EnumDefine.TypeMission typeMission, QuestData hardData) {
        this.missionPage = missionPage;
        this.typeMission = typeMission;
        this.hardData = hardData;
        sizeX = MainCanvasScaler.Instance.main.referenceResolution.x - 430f;
        UpdateUI();
    }

    public void UpdateUI() {
        var softData = ManagerData.Instance.missionInfo.GetQuestInfo(typeMission, hardData.ID);
        SetTitle(hardData.Name);
        var isComplete = hardData.IsDone(softData.IDQuest);
        QuestInfor quest = null;
        if(isComplete) { quest = hardData.Infor[softData.IDQuest - 1]; }
        else { quest = hardData.Infor[softData.IDQuest]; }
        SetIcon(CurrenciesData.Instance.GetCurrency(quest.RewardType).Icon);
        var process01 = Mathf.Clamp01((float)(softData.Process/quest.Needed));
        SetProcessRect(process01);
        SetAmountReward(quest.RewardNum);
        SetProcessText(quest.Description, softData.Process, quest.Needed);
        SetButtonClaim(isComplete, quest.IsDone(softData.Process));
    }

    private void SetIcon(Sprite sprIcon) {
        if(imgIcon == null) { return; }
        if(sprIcon == null) { return; }
        imgIcon.sprite = sprIcon;
        imgIcon.AutoSize(sprIcon.rect.size);
    }

    private void SetAmountReward(int amount) {
        if(txtAmountReward == null) { return; }
        txtAmountReward.text = amount.ToString();
    }

    private void SetTitle(string strTitle) {
        if(txtTitle == null) { return; }
        txtTitle.text = strTitle;
    }

    private void SetProcessRect(float process01) {
        if(rectProcess == null) { return; }
        var sizeXNew = (-1)*(sizeX - sizeX * process01);
        rectProcess.sizeDelta = new Vector2(sizeXNew, rectProcess.sizeDelta.y);
    }

    private void SetProcessText(string description,int process, int total) {
        if(txtProcess == null) { return; }
        txtProcess.text = string.Format("{0} {1}/{2}", description, process > total ? total : process, total);
    }


    private void SetButtonClaim(bool complete, bool claim = false) {
        btnClaim.RemoveAllListener();
        if(complete) {
            imgButtonCliam.sprite = sprUnClaim;
            btnClaim.AddListener(OnComplete);
        }
        else {
            if(claim) {
                imgButtonCliam.sprite = sprClaim;
                btnClaim.AddListener(OnClaim);
            }
            else {
                imgButtonCliam.sprite = sprUnClaim;
                btnClaim.AddListener(OnUnClaim);
            }
        }
    }

    private void OnClaim() {
        Logs.Log("RewardHere");
        ManagerData.Instance.missionInfo.ClaimQuest(TYPE, hardData.Type);
        UpdateUI();
    }

    private void OnUnClaim() {
        Logs.Log("OnUnClaim");
    }

    private void OnComplete() {
        Logs.Log("OnComplete");
    }

}
