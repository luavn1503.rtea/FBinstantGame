﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHomeManager : HUD<UIHomeManager> {
    [SerializeField] private RectTransform rectButtonBot = null;

    public void SetActiveBot(bool active) {
        rectButtonBot.DOKill();
        rectButtonBot.DOAnchorPosY(active ? 0f : (-1) * rectButtonBot.sizeDelta.y, 0.5f);
    }
}
