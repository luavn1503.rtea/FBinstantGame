﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PrebatlePanel : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI txtLevel = null;
    [SerializeField] private TextMeshProUGUI txtButtonPlay = null;
    [SerializeField] private ButtonBase btnBack = null;
    [SerializeField] private ButtonBase btnPlay = null;
    [SerializeField] private ModePlayPanel modePlayPanel = null;
    [SerializeField] private RewardsPanel rewardsPanel = null;
    public void Initialize() {
        modePlayPanel.Initialize();
        rewardsPanel.Initialize();
        btnBack.AddListener(ButtonBack);
        btnPlay.AddListener(ButtonPlay);
    }

    public void ShowPrebatle() {
        var levelSelect = ManagerGame.Instance.levelSelected;
        var data = ManagerData.Instance.GetLevelData(levelSelect + 1);
        string strTitle = string.Format("LEVEL {0}", levelSelect + 1);
        Extention.SetText(txtLevel, strTitle);
        EnumDefine.IDShip idShip = (EnumDefine.IDShip)data.EvolveShipId;
        EnumDefine.IDShip idDrone = (EnumDefine.IDShip)(data.EvolveShipId + 8);
        RewardData rewardCoin = new RewardData(EnumDefine.RewardsType.Currency, data.CoinReward, EnumDefine.IDCurrency.Coin);
        RewardData rewardDebrisShip = new RewardData(EnumDefine.RewardsType.Debris, data.EvolveShipNum, idShip: idShip);
        RewardData rewardDebrisDrone = new RewardData(EnumDefine.RewardsType.Debris, data.EvolveDroneNum, idShip: idDrone);
        RewardsData rewards = new RewardsData();
        rewards.data.Add(rewardCoin);
        rewards.data.Add(rewardDebrisShip);
        rewards.data.Add(rewardDebrisDrone);
        rewardsPanel.ShowRewards(rewards);
        modePlayPanel.UpdateUI();
        Active(true);
    }

    private void Active(bool active) {
        gameObject.SetActive(active);
    }

    private void ButtonBack() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_01);
        Active(false);
    }
    
    private void ButtonPlay() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_01);
        ManagerScene.Instance.Load(IDScene.Ingame);
    }
}
