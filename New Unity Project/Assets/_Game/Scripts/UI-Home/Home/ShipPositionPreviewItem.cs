﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipPositionPreviewItem : MonoBehaviour {
    private UIShipPreview shipPreviewCreate = null;
    [SerializeField] private EnumDefine.Direct direct = EnumDefine.Direct.Up;
    [SerializeField] private ButtonBase btnSelect = null;

    public void Initialize() {
        btnSelect.AddListener(ButtonSelect);
    }

    public void UpdateUI() {
        var equiped  = ManagerData.Instance.PlayerInfo.ShipEquipedAtDirect(direct, out var shipEquipedInfo);
        Debug.Log(equiped);
        if(shipPreviewCreate != null) { shipPreviewCreate.Recycle(); }
        if(equiped) {
            Debug.Log(shipEquipedInfo.IDShip);
            var idShip = shipEquipedInfo.IDShip;
            var shipPreviewPrefab = ManagerData.Instance.GetShipPreview(idShip);
            if(shipPreviewPrefab == null) { return; }
            shipPreviewCreate = shipPreviewPrefab.Spawn(transform);
            shipPreviewCreate.transform.localScale = Vector3.one;
            shipPreviewCreate.transform.localPosition = Vector3.zero;
            var shipDataInfo = ManagerData.Instance.AircraftData.GetDataShipInfo(idShip);
            shipPreviewCreate.SetSkin(string.Format("Skin_Lv{0}", (int)shipDataInfo.Rank));
            Debug.Log(string.Format("Skin_Lv{0}", (int)shipDataInfo.Rank));
        }
    }

    private void ButtonSelect() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        UIHomeManager.Instance.Show<EquipmentPage>().SetupEquipment(EnumDefine.TypeItem.Ship, direct);
    }
}
