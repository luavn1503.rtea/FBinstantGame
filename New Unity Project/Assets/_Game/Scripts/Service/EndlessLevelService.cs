﻿using Gemmob;
using System.Collections.Generic;

namespace Gemmob {
	public class EndlessLevelService : Singleton<EndlessLevelService> {
		public const string ResourcePath = "Halloween";
		public const string FullPath = "Assets/SpaceShooter/Resources/Halloween";

		public List<LevelData> Levels;

		public void Reload() {
			Levels = new List<LevelData>(ResourceLogger.LoadAll<LevelData>(ResourcePath));
		}

		protected override void Initialize() {
			Reload();
		}

		public LevelData GetById(int id) {
			return Levels[id];
		}
	}
}