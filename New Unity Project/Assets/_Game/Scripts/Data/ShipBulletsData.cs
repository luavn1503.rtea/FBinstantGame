﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletsData", menuName = "Data/BulletsData")]
public class ShipBulletsData : ScriptableObject {
    [SerializeField] private List<ShipBulletBase> bullets = new List<ShipBulletBase>();

    public ShipBulletBase GetBullet(EnumDefine.IDBullet idBulet) {
        var bullet = bullets.Find(x => x.idBullet == idBulet);
        if(bullet == null) {
            Logs.LogError("Bullet Không tồn tại hoặc chưa được Setup ID " + idBulet);
            return null;
        }
        return bullet;
    }
}
