﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StatsesData", menuName = "Data/StatsesData")]
public class StatsesData : ScriptableObject {
    private static StatsesData instance = null;
    public List<StatsData> statses = new List<StatsData>();
    public static StatsesData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<StatsesData>("Data/StatsesData");
                return instance;
            }
            return instance;
        }
    }

    public StatsData GetStatsData(EnumDefine.IDStats ID) {
        var data = statses.Find(x => x.ID == ID);
        if(data == null) {
            Debug.Log(string.Format("Drone {0} not yet", ID));
            return new StatsData(ID);
        }
        return data;
    }


}


[Serializable]
public class StatsData {
    [SerializeField] private EnumDefine.IDStats idStats  = EnumDefine.IDStats.Power;
    [SerializeField] private Sprite icon = null;
    public StatsData(EnumDefine.IDStats idStats) {
        this.idStats = idStats;
    }

    public EnumDefine.IDStats ID => idStats;


    public Sprite Icon => icon;
}

[Serializable]
public class Statses {
    public List<Stats> data = new List<Stats>();

    public Statses() {
        data = new List<Stats>();
    }

    public Stats GetStats(EnumDefine.IDStats ID) {
        var stats = data.Find(x => x.ID == ID );
        if (stats == null) {
            Logs.Log(string.Format("Stats {0} not yet", ID));
            return new Stats(ID, 0);
        }
        return stats;
    }
}

[Serializable]
public class Stats {
    [SerializeField] private EnumDefine.IDStats idStats  = EnumDefine.IDStats.Power;
    [SerializeField] private int value = 0;
    public Stats(EnumDefine.IDStats idStats, int value) {
        this.idStats = idStats;
        this.value = value;
    }

    public EnumDefine.IDStats ID => idStats;

    public int Value => value;
}
