﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RankAttack_?", menuName = "Data/RankAttackData")]
public class RanksAttackData : ScriptableObject {
    public List<RankAttackData> ranks = new List<RankAttackData>();
    public RankAttackData GetAttackRank(EnumDefine.Rank rank){
        var data = ranks.Find(x => x.rank == rank);
        if(data == null) {
            Debug.Log("Rank Không tồn tại");
            return new RankAttackData();
        }
        return data;
    }
}

[System.Serializable]
public class RankAttackData {
    public EnumDefine.Rank rank = EnumDefine.Rank.D;
    public float timeSuper = 5f;
    public List<LevelAttackData> levels = new List<LevelAttackData>();
    public LevelAttackData GetLevelAttack(int level) {
        var data = levels.Find(x => x.level == level);
        if(data == null) {
            Debug.Log("Level Không tồn tại");
            return new LevelAttackData();
        }
        return data;
    }
}

[System.Serializable]
public class LevelAttackData {
    public int level = 0;
    //public MuzzleData muzzleData = new MuzzleData();
    public List<TypeAttackData> attacks = new List<TypeAttackData>();

}

[System.Serializable]
public class TypeAttackData {
    public float rateAttack = 0.2f;
    public float scaleDamage = 1.0f;
    public float withBulletLaser = 0.1f;
    public EnumDefine.TypeAttack typeAttack = EnumDefine.TypeAttack.None;
    public List<AttackData> dataAttack = new List<AttackData>();

    public TypeAttackData() {
        rateAttack = 0.2f;
        scaleDamage = 1.0f;
        typeAttack = EnumDefine.TypeAttack.None;
        withBulletLaser = 0.1f;
        dataAttack = new List<AttackData>();
    }

    public float GetWithBulletLaser() {
        return withBulletLaser;
    }

}

[System.Serializable]
public class AttackData {
    public EnumDefine.IDBullet idBullet = EnumDefine.IDBullet.Bullet_None_1;
    public float timeDelay = 0f;
    public Vector3 offset = Vector3.zero;
    public Vector3 eulerAngle = Vector2.zero;
}

//[System.Serializable]
//public class MuzzleData {
//    public Vector3 offset = Vector3.zero;
//    public GameObject muzzlePrefab = null;
//}