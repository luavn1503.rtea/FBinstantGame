﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static EnumDefine;

[CreateAssetMenu(fileName = "CurrenciesData", menuName = "Data/CurrenciesData")]
public class CurrenciesData : ScriptableObject {
    private static CurrenciesData instance = null;
    public List<CurrencyData> currencies = new List<CurrencyData>();
    public static CurrenciesData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<CurrenciesData>("Data/CurrenciesData");
                return instance;
            }
            return instance;
        }
    }
    public CurrencyData GetCurrency(EnumDefine.IDCurrency idCurrency) {
        var currency = currencies.Find(x => x.IDCurrency == idCurrency);
        if (currency == null) {
            Logs.LogError(string.Format("Tiền tệ {0} chưa được cập nhật", idCurrency));
            return new CurrencyData();
        }
        return currency;
    }
}

[System.Serializable]
public class CurrencyData {
    [SerializeField] private EnumDefine.IDCurrency idCurrency = EnumDefine.IDCurrency.Coin;
    [SerializeField] private float rate = 1; //Tỉ giá so với tiền tệ thấp nhất (Coin).
    [SerializeField] private Sprite icon = null;
    public EnumDefine.IDCurrency IDCurrency => idCurrency;
    public float Rate => rate;
    public Sprite Icon => icon;
}
[Serializable]
public class CurrencyInfo {
    [SerializeField] private IDCurrency idCurrency = IDCurrency.Coin;
    [SerializeField] private int amount = 0;
    public CurrencyInfo(IDCurrency idCurrency, int amount) {
        this.idCurrency = idCurrency;
        this.amount = amount;
    }
    public IDCurrency IDCurrency => idCurrency;
    public int Amount => amount;

    public int AddCurrency(int value) {
        amount += value;
        return amount;
    }
}
