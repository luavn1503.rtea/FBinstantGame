﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSkillDrone : ActiveSkillComponentBase {
    public override EnumDefine.IDSkill idActiveSkill => EnumDefine.IDSkill.Drone;

    public override ActiveSkillComponentBase Initialize() {

        return this;
    }

    public override void StartSkill() {
        var droneCreate = ManagerPlayer.Instance.CreateDrone(EnumDefine.IDShip.Drone_01, EnumDefine.Direct.Down);
        droneCreate.InitializedActiveSkill(10f);
    }

    public override void PauseSkill() {
        
    }
}
