﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSkillShield : ActiveSkillComponentBase {
    private ShieldBase sheildCreate = null;
    [SerializeField] private ShieldBase shieldPrefab = null;
    public override EnumDefine.IDSkill idActiveSkill => EnumDefine.IDSkill.Shield;

    public override ActiveSkillComponentBase Initialize() {

        return this;
    }

    public override void StartSkill() {
        sheildCreate = shieldPrefab.Spawn(transform);
        sheildCreate.transform.localPosition = Vector3.zero;
        sheildCreate.Initialize(5f);
    }

    public override void PauseSkill() {
        
    }
}
