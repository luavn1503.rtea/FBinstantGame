﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreJump : MonoBehaviour
{
    [SerializeField] private TextMesh _text;
    [SerializeField] private MeshRenderer _mesh;
    private Transform _trans;

    [ContextMenu("Set start variable")]
    public void Reset() {
        _text = GetComponent<TextMesh>();
        _mesh = GetComponent<MeshRenderer>();
    }

    void Awake() {
        _trans = transform;
    }



    public void Init(Vector2 startpos, int score) {
        _text.text = score.ToString();
        _mesh.material.color = Color.white;
        _trans.position = startpos;
        _trans.localScale = Vector3.one * .0f;
        _trans.DOScale(.17f, 2f).SetSpeedBased().OnComplete(() =>
        {
            _trans.DOScale(.1f, .5f).SetSpeedBased().OnComplete(() =>
            {
                _trans.DOMoveY(1.5f, 5).SetRelative().SetSpeedBased().SetEase(Ease.OutBack).OnComplete(() =>
                {
                    _mesh.material.DOFade(0, .6f).OnComplete(() =>
                    {
                        gameObject.Recycle();
                    });
                });
            });
        });
    }
}
