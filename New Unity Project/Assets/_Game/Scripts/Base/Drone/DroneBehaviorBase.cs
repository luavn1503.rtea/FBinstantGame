﻿using DG.Tweening;
using Gemmob;
using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneBehaviorBase : BehaviorBase {
    [SerializeField] protected DroneMovementBase droneMovement = null;
    [SerializeField] private GameObject psBottom;
    public void Initialized(EnumDefine.Direct archon, float timeLive = -1) {
        this.archon = archon;
        droneMovement.Initialize(archon);
    }

    public void InitializedActiveSkill(float timeLive) {
        archon = EnumDefine.Direct.Down;
        droneMovement.Initialize(archon);
        OnStartAttack();
        DOVirtual.DelayedCall(timeLive, () => {
            gameObject.Recycle();
        });
    }

    public void OnMoveStart() {
        var target = droneMovement.GetTarget();
        transform.DOMove(target, 0.5f).OnComplete(() => {
            ManagerPlayer.Instance.DroneMoveStartDone();
        });
    }

    public void OnStartAttack() {
        Attack();
        droneMovement.EnableMove(true);
    }


    public void OnFinishAttack() {
        PauseAttack();
    }

    public void OnDeath() {
        //Turn off + blur
        TurnOnOffRender(false);
        PauseAttack();
        droneMovement.EnableMove(false);
    }


    public void TurnOnOffRender(bool on) {
        if(on) {
            SetColor(Color.white, 0f);
        }
        else {
            Color fideColor = Color.white;
            fideColor.a = 0f;
            SetColor(fideColor, 0.2f);
        }
        psBottom.SetActive(on);
    }
    public void OnRevive() {
        Attack();
    }
}
