﻿using System;
using System.Collections;
using Gemmob;

using UnityEngine;

public class GameController : MonoBehaviour {
	public static GameController Instance;
	private int _touchIndex = -1;

	void Awake() {
		Application.targetFrameRate = 60;
        GC.Collect();
		Instance = this;

		GamePlayState.Instance.TypeGame = TypeGame.CombatPower;
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
			_touchIndex = 0;

	}

	private void Start() {
		var gamePlayState = GamePlayState.Instance;
		gamePlayState.CardId = ManagerData.Instance.PlayerInfo.Card.Index;
		gamePlayState.BombCount = 0;
		if (GamePlayState.Instance.ModePlay == EnumDefine.ModePlay.Halloween) {
			StartGame(shipUse: 6, droneUse: -1);
		} else {
			StartGame(gamePlayState.ItemUse, gamePlayState.ShipIndex, gamePlayState.DroneIndex);
		}
	}

	public void StartGame(int itemUse = -2, int shipUse = -2, int droneUse = -2) {
		SoundController.PlayBackgroundSound(SoundController.Instance.GamePlay);
		GamePlayState.Instance.IsStarting = false;
		EventDispatcher.Instance.Dispatch(new GameEvent.OnStartGame(itemUse, shipUse, droneUse));

		StartCoroutine(StartNewLevel());
	}

	private IEnumerator StartNewLevel(bool isRevive = false) {
		yield return Yielders.Get(1.5f);
		Debug.Log("Start level by revive?: " + isRevive);
		if (!isRevive) {
			GamePlayState.Instance.IsStarting = true;
			EventDispatcher.Instance.AddListener<GameEvent.OnRevive>(CallOnRevive);
			//PlayerController.Instance.StartAttack();
			//var modePlay = GamePlayState.Instance.ModePlay;
			//switch (modePlay) {
			//	case ModePlay.Normal:
			//		NormalModeControler.Instance.StartGame();
			//		break;
			//	case ModePlay.Endless:
			//		EndlessModeControler.Instance.StartGame();
			//		break;
			//	case ModePlay.Boss:
			//		BossBattleController.Instance.StartGame();
			//		break;
			//	case ModePlay.Halloween:
			//		HalloweenModeController.Instance.StartGame();
			//		break;
			//}
		}

		EventDispatcher.Instance.Dispatch<GameEvent.OnStartAttack>();
	}

	private void CallOnRevive() {
		if (gameObject == null) return;
		StartCoroutine(StartNewLevel(true));
	}

	public void StopGame() {
		EventDispatcher.Instance.Dispatch<GameEvent.OnStopGame>();
		EventDispatcher.Instance.RemoveListener<GameEvent.OnRevive>(CallOnRevive);
		//ItemPool.Instance.Pool.DespawnAll();
		//EffectPool.Instance.Pool.DespawnAll();
		//EnemyPool.Instance.Pool.DespawnAll();
		//BulletPool.Instance.Pool.DespawnAll();
		//EnemyBulletPool.Instance.Pool.DespawnAll();
		//DroneBulletPool.Instance.Pool.DespawnAll();
	}

	void OnDestroy() {
		EventDispatcher.Instance.RemoveListener<GameEvent.OnRevive>(CallOnRevive);
		GamePlayState.Instance.OnInGameScoreChanged = null;
	}

	[ContextMenu("Change type game")]
	public static void ChangeTypeGame() {
		GamePlayState.Instance.TypeGame = GamePlayState.Instance.TypeGame == TypeGame.CombatPower ? TypeGame.Normal : TypeGame.CombatPower;
		Debug.Log("Change to type: " + GamePlayState.Instance.TypeGame);
	}


}

