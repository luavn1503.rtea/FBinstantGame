﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ItemBase : MonoBehaviour
{
    private Vector2 moveDirect = Vector2.down;
    [SerializeField] private float speed = 1f;
    [SerializeField] private float speedRotate = 1f;
    [SerializeField] private Transform icon, inRotate, outRotate;
    [SerializeField] private Rigidbody2D rg2D;
    protected Vector2 maxSize = Vector2.zero;
    [ContextMenu("Init")]
    public virtual void Init() {
        maxSize = MainCamera.Instance.max;
        Active();
    }

    protected virtual void Active() {
        rg2D.velocity = moveDirect * speed;
    }

    private void Update() {
        inRotate.Rotate(Vector3.forward, speedRotate, Space.Self);
        outRotate.Rotate(Vector3.forward, -speedRotate, Space.Self);
        CheckOutScene();
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision) {
        if(collision.CompareTag(TagDefine.Player)) {
            ManagerPlayer.Instance.GetShip().EffectGetItem();
            ActiveEffect();
            gameObject.Recycle();
        }
    }

    protected virtual void ActiveEffect() {
        ManagerPlayer.Instance.UpLevelShip();
    }

    protected void CheckOutScene() {
        Vector2 origin = transform.position;
        if(Mathf.Abs(origin.x) >= maxSize.x + 1f || Mathf.Abs(origin.y) >= maxSize.y + 1f) { gameObject.Recycle(); }
    }
}
