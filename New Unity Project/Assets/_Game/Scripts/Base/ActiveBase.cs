﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveBase : MonoBehaviour {
    [SerializeField] private GameObject obActive = null;
    public void Active(bool active) {
        if(obActive == null) return;
        if (obActive.activeSelf == active) { return; }
        obActive.SetActive(active);
    }
}
