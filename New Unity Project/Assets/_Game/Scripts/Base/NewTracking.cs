﻿using UnityEngine;
using Gemmob.Common;

public class NewTracking : MonoBehaviour
{
    private static readonly string BUTTON_PRESS = "buttonPress";
    private static readonly string MENU_SHOWUP = "menuShowUp";
    private static readonly string VIDEO_ADS = "videoAds";
    private static readonly string DAILY_QUEST_COMPLETED = "dailyQuestCompleted";
    private static readonly string DAILY_QUEST_CLAIM = "dailyQuestClaim";
    private static readonly string ACHIEVEMENT_COMPLETED = "achievementCompleted";
    private static readonly string ACHIEVEMENT_CLAIM = "achievementClaimed";
    private static readonly string IAP_PROMOTION = "iapPromotion";
    private static readonly string IAP_PURCHASED = "iapPurchased";
    private static readonly string EARN_VIRTUAL_CURRENCY = "earn_virtual_currency";
    private static readonly string SPEND_VIRTUAL_CURRENCY = "spend_virtual_currency";
    private static readonly string WEAPON_PIECE_CHANGE = "weaponPieceChange";
    private static readonly string ITEMS_CHANGE = "itemsChange";
    private static readonly string CARDS_CHANGE = "cardsChange";
    private static readonly string WEAPON_UNLOCKED = "weaponUnlocked";
    private static readonly string WEAPON_UPGRADED = "weaponUpgraded";
    private static readonly string CAMPAIGN_START = "campaignStart";
    private static readonly string CAMPAIGN_FINISH = "campaignFinish";
    private static readonly string ENDLESS_START = "endlessStart";
    private static readonly string ENDLESS_FINISH = "endlessFinish";
    private static readonly string BOSSBATTLE_START = "bossStart";
    private static readonly string BOSSBATTLE_FINISH = "bossFinish";
    private static readonly string WEEKEND_START = "EventStart";
    private static readonly string WEEKEND_FINISH = "EventFinish";
    private static readonly string TUTORIAL_BEGIN = "tutorial_begin";
    private static readonly string TUTORIAL_COMPLETE = "tutorial_complete";




    private static void LogEvent(string eventName, Events.ParameterBuilder builder)
    {
#if FIREBASE_ENABLE
        Events.Log(eventName, builder);
#endif
    }

    public static void LogButtonPress(string prvLocation, string location, string btnID, string gameMode, string level, string wave) {
        LogEvent(BUTTON_PRESS, Events.ParameterBuilder.Create()
                                                        .Add("prvLocation", prvLocation)
                                                        .Add("location", location)
                                                        .Add("btnID", btnID)
                                                        .Add("gameMode", gameMode)
                                                        .Add("level", level)
                                                        .Add("wave", wave)
                                                        );
        Logs.Log(string.Format("eventTracking: {0} ; prvLocation: {1} ; location: {2} ; btnID: {3} ; gameMode: {4} ; level: {5} ; wave: {6}",
            BUTTON_PRESS, prvLocation, location, btnID, gameMode, level, wave));
    }

    public static void LogMenuShowup(string prvLocation, string location, string gameMode, string level, string wave) {
        LogEvent(MENU_SHOWUP, Events.ParameterBuilder.Create()
                                                      .Add("prvLocation", prvLocation)
                                                      .Add("location", location)
                                                      .Add("gameMode", gameMode)
                                                      .Add("level", level)
                                                      .Add("wave", wave)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; prvLocation: {1} ; location: {2} ; gameMode: {3} ; level: {4} ; wave: {5}",
            MENU_SHOWUP, prvLocation, location, gameMode, level, wave));
    }

    public static void LogVideoAds(string adsType, string location, string adsStatus, string gameMode, string level)
    {
        LogEvent(VIDEO_ADS, Events.ParameterBuilder.Create()
                                                      .Add("adsType", adsType)
                                                      .Add("location", location)
                                                      .Add("adsStatus", adsStatus)
                                                      .Add("gameMode", gameMode)
                                                      .Add("level", level)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; adsType: {1} ; location: {2} ; adsStatus: {3} ; gameMode: {4} ; level: {5}",
            VIDEO_ADS, adsType, location, adsStatus, gameMode, level));
    }

    public static void LogDailyQuestCompleted(string questId)
    {
        LogEvent(DAILY_QUEST_COMPLETED, Events.ParameterBuilder.Create()
                                                      .Add("questId", questId)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; questId: {1}",
            DAILY_QUEST_COMPLETED, questId));
    }

    public static void LogDailyQuestClaim(string questId)
    {
        LogEvent(DAILY_QUEST_CLAIM, Events.ParameterBuilder.Create()
                                                      .Add("questId", questId)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; questId: {1}",
            DAILY_QUEST_CLAIM, questId));
    }

    public static void LogAchievementCompleted(string questId)
    {
        LogEvent(ACHIEVEMENT_COMPLETED, Events.ParameterBuilder.Create()
                                                      .Add("questId", questId)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; questId: {1}",
            ACHIEVEMENT_COMPLETED, questId));
    }

    public static void LogAchievementQuestClaim(string questId)
    {
        LogEvent(ACHIEVEMENT_CLAIM, Events.ParameterBuilder.Create()
                                                      .Add("questId", questId)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; questId: {1}",
            ACHIEVEMENT_CLAIM, questId));
    }

    public static void LogIapPromotion(string item_category, string item_id, string item_name)
    {
        LogEvent(IAP_PROMOTION, Events.ParameterBuilder.Create()
                                                      .Add("item_category", item_category)
                                                      .Add("item_id", item_id)
                                                      .Add("item_name", item_name)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; item_category: {1} ; item_id: {2} ; item_name: {3}",
            IAP_PROMOTION, item_category, item_id, item_name));
    }

    public static void LogIapPurchased(string item_id, string item_name, string item_category)
    {
        LogEvent(IAP_PURCHASED, Events.ParameterBuilder.Create()
                                                      .Add("item_id", item_id)
                                                      .Add("item_name", item_name)
                                                      .Add("item_name", item_category)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; item_id: {1} ; item_name: {2} ; item_category: {3}",
            IAP_PURCHASED, item_id, item_name, item_category));
    }

    public static void LogEarnVirtualCurrency(string virtual_currency_name, string value, string location, string gameMode, string level)
    {
        LogEvent(EARN_VIRTUAL_CURRENCY, Events.ParameterBuilder.Create()
                                                      .Add("virtual_currency_name", virtual_currency_name)
                                                      .Add("value", value)
                                                      .Add("location", location)
                                                      .Add("gameMode", gameMode)
                                                      .Add("level", level)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; virtual_currency_name: {1} ; value: {2} ; location: {3} ; gameMode: {4} ; level: {5}",
            EARN_VIRTUAL_CURRENCY, virtual_currency_name, value, location, gameMode, level));
    }

    public static void LogSpendVirtualCurrency(string item_name, string virtual_currency_name, string value, string location, string gameMode, string level)
    {
        LogEvent(SPEND_VIRTUAL_CURRENCY, Events.ParameterBuilder.Create()
                                                      .Add("item_name", item_name)
                                                      .Add("virtual_currency_name", virtual_currency_name)
                                                      .Add("value", value)
                                                      .Add("location", location)
                                                      .Add("gameMode", gameMode)
                                                      .Add("level", level)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; item_name: {1} ; virtual_currency_name: {2} ; value: {3} ; location: {4} ; gameMode: {5} ; level: {6}",
            SPEND_VIRTUAL_CURRENCY, item_name, virtual_currency_name, value, location, gameMode, level));
    }

    public static void LogWeaponPieceChange(string pieceID, string action, string value, string location) {
        LogEvent(WEAPON_PIECE_CHANGE, Events.ParameterBuilder.Create()
                                                     .Add("pieceID", pieceID)
                                                     .Add("action", action)
                                                     .Add("value", value)
                                                     .Add("location", location)
                                                     );
        Logs.Log(string.Format("eventTracking: {0} ; pieceID: {1} ; action: {2} ; value: {3} ; location: {4}",
            WEAPON_PIECE_CHANGE, pieceID, action, value, location));
    }

    public static void LogItemsChange(string action, string itemsType, string itemsID, string quantity, string location, string gameMode, string level) {
        LogEvent(ITEMS_CHANGE, Events.ParameterBuilder.Create()
                                                      .Add("action", action)
                                                      .Add("itemsType", itemsType)
                                                      .Add("itemsID", itemsID)
                                                      .Add("quantity", quantity)
                                                      .Add("location", location)
                                                      .Add("gameMode", gameMode)
                                                      .Add("level", level)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; action: {1} ; itemsType: {2} ; itemsID: {3} ; quantity: {4} ; location: {5} ; gameMode: {6} ; level: {7}",
            ITEMS_CHANGE, action, itemsType, itemsID, quantity, location, gameMode, level));
    }

    public static void LogCardsChange(string action, string cardsLevel, string cardsID, string purpose) {
        LogEvent(CARDS_CHANGE, Events.ParameterBuilder.Create()
                                                      .Add("action", action)
                                                      .Add("cardsLevel", cardsLevel)
                                                      .Add("cardsID", cardsID)
                                                      .Add("purpose", purpose)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; action: {1} ; cardsLevel: {2} ; cardsID: {3} ; purpose: {4}",
            CARDS_CHANGE, action, cardsLevel, cardsID, purpose));
    }

    public static void LogWeaponUnlocked(string itemsID, string unlockWay) {
        LogEvent(WEAPON_UNLOCKED, Events.ParameterBuilder.Create()
                                                      .Add("itemsID", itemsID)
                                                      .Add("unlockWay", unlockWay)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; itemsID: {1} ; unlockWay: {2}",
            WEAPON_UNLOCKED, itemsID, unlockWay));
    }

    public static void LogWeaponUpgraded(string itemsID, string currency, string level) {
        LogEvent(WEAPON_UPGRADED, Events.ParameterBuilder.Create()
                                                      .Add("itemsID", itemsID)
                                                      .Add("currency", currency)
                                                      .Add("level", level)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; itemsID: {1} ; currency: {2} ; level: {3}",
            WEAPON_UPGRADED, itemsID, currency, level));
    }

    public static void LogCampaignStart(string level, string shipId, string shipLevel, string droneId, string droneLevel, string cardID, string cardLevel) {
        LogEvent(CAMPAIGN_START, Events.ParameterBuilder.Create()
                                                      .Add("level", level)
                                                      .Add("shipId", shipId)
                                                      .Add("shipLevel", shipLevel)
                                                      .Add("droneId", droneId)
                                                      .Add("droneLevel", droneLevel)
                                                      .Add("cardID", cardID)
                                                      .Add("cardLevel", cardLevel)

                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; level: {1} ; shipId: {2} ; shipLevel: {3} ; droneId: {4} ; droneLevel: {5} ; cardID: {6} ; cardLevel: {7}",
            CAMPAIGN_START, level, shipId, shipLevel, droneId, droneLevel, cardID, cardLevel));
    }

    public static void LogCampaignFinish(string level, string wave, string status, string firstWinCount, string duration, string shipId, string shipLevel, string droneId, string droneLevel, string cardID, string cardLevel) {
        LogEvent(CAMPAIGN_FINISH, Events.ParameterBuilder.Create()
                                                      .Add("level", level)
                                                      .Add("wave", wave)
                                                      .Add("status", status)
                                                      .Add("firstWinCount", firstWinCount)
                                                      .Add("duration", duration)
                                                      .Add("shipId", shipId)
                                                      .Add("shipLevel", shipLevel)
                                                      .Add("droneId", droneId)
                                                      .Add("droneLevel", droneLevel)
                                                      .Add("cardID", cardID)
                                                      .Add("cardLevel", cardLevel)

                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; level: {1} ; wave: {2} ; status: {3} ; firstWinCount: {4} ; duration: {5} ; shipId: {6} ; shipLevel: {7} ; droneId: {8} ; droneLevel: {9} ; cardID: {10} ; cardLevel: {11}",
            CAMPAIGN_FINISH, level, wave, status, firstWinCount, duration, shipId, shipLevel, droneId, droneLevel, cardID, cardLevel));
    }

    public static void LogEndlessStart(string shipId, string shipLevel, string droneId, string droneLevel, string cardID, string cardLevel) {
        LogEvent(ENDLESS_START, Events.ParameterBuilder.Create()
                                                      .Add("shipId", shipId)
                                                      .Add("shipLevel", shipLevel)
                                                      .Add("droneId", droneId)
                                                      .Add("droneLevel", droneLevel)
                                                      .Add("cardID", cardID)
                                                      .Add("cardLevel", cardLevel)

                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; shipId: {1} ; shipLevel: {2} ; droneId: {3} ; droneLevel: {4} ; cardID: {5} ; cardLevel: {6}",
            ENDLESS_START, shipId, shipLevel, droneId, droneLevel, cardID, cardLevel));
    }

    public static void LogEndlessFinish(string wave ,string duration, string shipId, string shipLevel, string droneId, string droneLevel, string cardID, string cardLevel) {
        LogEvent(ENDLESS_FINISH, Events.ParameterBuilder.Create()
                                                      .Add("wave", wave)
                                                      .Add("duration", duration)
                                                      .Add("shipId", shipId)
                                                      .Add("shipLevel", shipLevel)
                                                      .Add("droneId", droneId)
                                                      .Add("droneLevel", droneLevel)
                                                      .Add("cardID", cardID)
                                                      .Add("cardLevel", cardLevel)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; wave: {1} ; duration: {2} ; shipId: {3} ; shipLevel: {4} ; droneId: {5} ; droneLevel: {6} ; cardID: {7} ; cardLevel: {8}",
            ENDLESS_FINISH, wave, duration, shipId, shipLevel, droneId, droneLevel, cardID, cardLevel));
    }

    public static void LogBossStart(string bossId, string shipId, string shipLevel, string droneId, string droneLevel, string cardID, string cardLevel) {
        LogEvent(BOSSBATTLE_START, Events.ParameterBuilder.Create()
                                                      .Add("bossId", bossId)
                                                      .Add("shipId", shipId)
                                                      .Add("shipLevel", shipLevel)
                                                      .Add("droneId", droneId)
                                                      .Add("droneLevel", droneLevel)
                                                      .Add("cardID", cardID)
                                                      .Add("cardLevel", cardLevel)

                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; bossId: {1} ; shipId: {2} ; shipLevel: {3} ; droneId: {4} ; droneLevel: {5} ; cardID: {6} ; cardLevel: {7}",
            BOSSBATTLE_START, bossId, shipId, shipLevel, droneId, droneLevel, cardID, cardLevel));
    }

    public static void LogBossFinish(string bossId, string status, string duration, string shipId, string shipLevel, string droneId, string droneLevel, string cardID, string cardLevel) {
        LogEvent(BOSSBATTLE_FINISH, Events.ParameterBuilder.Create()
                                                      .Add("bossId", bossId)
                                                      .Add("status", status)
                                                      .Add("duration", duration)
                                                      .Add("shipId", shipId)
                                                      .Add("shipLevel", shipLevel)
                                                      .Add("droneId", droneId)
                                                      .Add("droneLevel", droneLevel)
                                                      .Add("cardID", cardID)
                                                      .Add("cardLevel", cardLevel)

                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; bossId: {1} ; status: {2} ; duration: {3} ; shipId: {4} ; shipLevel: {5} ; droneId: {6} ; droneLevel: {7} ; cardID: {8} ; cardLevel: {9}",
            BOSSBATTLE_FINISH, bossId, status, duration, shipId, shipLevel, droneId, droneLevel, cardID, cardLevel));
    }

    public static void LogEventStart(string eventType, string shipId, string shipLevel, string droneId, string droneLevel, string cardID, string cardLevel) {
        LogEvent(WEEKEND_START, Events.ParameterBuilder.Create()
                                                      .Add("eventType", eventType)
                                                      .Add("shipId", shipId)
                                                      .Add("shipLevel", shipLevel)
                                                      .Add("droneId", droneId)
                                                      .Add("droneLevel", droneLevel)
                                                      .Add("cardID", cardID)
                                                      .Add("cardLevel", cardLevel)

                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; eventType: {1} ; shipId: {2} ; shipLevel: {3} ; droneId: {4} ; droneLevel: {5} ; cardID: {6} ; cardLevel: {7}",
            WEEKEND_START, eventType, shipId, shipLevel, droneId, droneLevel, cardID, cardLevel));
    }

    public static void LogEventFinish(string eventType, string wave, string status, string duration, string shipId, string shipLevel, string droneId, string droneLevel, string cardID, string cardLevel) {
        LogEvent(WEEKEND_FINISH, Events.ParameterBuilder.Create()
                                                      .Add("eventType", eventType)
                                                      .Add("wave", wave)
                                                      .Add("status", status)
                                                      .Add("duration", duration)
                                                      .Add("shipId", shipId)
                                                      .Add("shipLevel", shipLevel)
                                                      .Add("droneId", droneId)
                                                      .Add("droneLevel", droneLevel)
                                                      .Add("cardID", cardID)
                                                      .Add("cardLevel", cardLevel)

                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; eventType: {1} ; wave: {2} ; status: {3} ; duration: {4} ; shipId: {5} ; shipLevel: {6} ; droneId: {7} ; droneLevel: {8} ; cardID: {9} ; cardLevel: {10}",
            WEEKEND_FINISH, eventType, wave, status, duration, shipId, shipLevel, droneId, droneLevel, cardID, cardLevel));
    }

    public static void LogTutorialBegin(string tutorialId) {
        LogEvent(TUTORIAL_BEGIN, Events.ParameterBuilder.Create()
                                                     .Add("tutorialId", tutorialId)
                                                     );
        Logs.Log(string.Format("eventTracking: {0} ; tutorialId: {1}",
            TUTORIAL_BEGIN, tutorialId));
    }

    public static void LogTutorialComplete(string tutorialId, string duration) {
        LogEvent(TUTORIAL_COMPLETE, Events.ParameterBuilder.Create()
                                                      .Add("tutorialId", tutorialId)
                                                      .Add("duration", duration)
                                                      );
        Logs.Log(string.Format("eventTracking: {0} ; tutorialId: {1} ; duration: {2}",
            TUTORIAL_COMPLETE, tutorialId, duration));
    }
}
