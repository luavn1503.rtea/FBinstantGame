﻿using DG.Tweening;
using Gemmob;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorBase : MonoBehaviour, IEntility {
    protected int level = 0;
    protected EnumDefine.Rank rank = EnumDefine.Rank.D;
    protected List<ShipAttackerBase> listAttackComponent = new List<ShipAttackerBase>();
    protected List<ActiveSkillComponentBase> listActiveSkillComponent = new List<ActiveSkillComponentBase>();
    [SerializeField] protected EnumDefine.Direct archon = EnumDefine.Direct.Left;
    [SerializeField] protected EnumDefine.IDShip idShip = EnumDefine.IDShip.Ship_01;
    [SerializeField] protected StatusShip status = StatusShip.Live;
    [SerializeField] protected SkeletonAnimation skeAnimation = null;
    [SerializeField] protected ShipBulletsData bulletsData = null;
    [SerializeField] protected RanksAttackData rankAttackData = null;
    [SerializeField] protected BulletLevelsPositionData bulletLevelsPositionData = null;
    [SerializeField] protected ParticleSystem psFire = null;

    public EnumDefine.Direct Archon => archon;

    public EnumDefine.IDShip IDShip => idShip;

    public void StartEntility() {

    }

    public void RemoveEntility() {

    }

    protected void Attack() {
        var rankAttack = rankAttackData.GetAttackRank(rank);
        var levelAttack = rankAttack.GetLevelAttack(level);
        //var muzzleData = levelAttack.muzzleData;
        ////Spawer Fire_Ship
        //if(muzzleData.muzzlePrefab != null) {
        //    if(muzzle!=null) {
        //        muzzle.Recycle();
        //        muzzle = null;
        //    }
        //    muzzle = muzzleData.muzzlePrefab.Spawn();
        //    muzzle.transform.parent = transform;
        //    muzzle.transform.localPosition = muzzleData.offset;
        //}
        //
        foreach(var item in levelAttack.attacks) {
            var component = listAttackComponent.Find(x => x.typeAttack == item.typeAttack);
            if(component && component.isActiveAndEnabled) {
                component.Initialize(item, bulletsData, this).StartAttack();
            }
            else {
                var componentPrefab = ManagerPlayer.Instance.GetAttackComponent(item.typeAttack);
                component = componentPrefab.Spawn(transform);
                component.transform.localPosition = Vector3.zero;
                component.Initialize(item, bulletsData, this).StartAttack();
                listAttackComponent.Add(component);
            }
        }
    }

    protected void PauseAttack() {
        foreach(var item in listAttackComponent) { item?.PauseAttack(); }
    }
    protected virtual void ChangeLevel(int level) {
        SetLevel(level);
        PauseAttack();
        Attack();
        Logs.Log("Level: " + this.level.ToString());
    }

    protected virtual void SetLevel(int level) {
        this.level = level;
    }

    protected virtual void ChangeRank(EnumDefine.Rank rank) {
        this.rank = rank;
        skeAnimation.SetSkin(string.Format("Skin_Lv{0}", (int)rank));
        PauseAttack();
        Attack();
    }


    public StructDefine.ShipPossition GetShipPossition() {
        var origin = transform.position;
        return new StructDefine.ShipPossition() { view = EnumDefine.View.Show, possition = origin };
    }

    public void ActiveSkill(EnumDefine.IDSkill idSkill) {
        var component =  listActiveSkillComponent.Find(x => x.idActiveSkill == idSkill);
        if(component) {
            component.Initialize().StartSkill();
        }
        else {
            var componentPrefab = ManagerPlayer.Instance.GetActiveSkillComponent(idSkill);
            component = componentPrefab.Spawn(transform);
            component.transform.localPosition = Vector3.zero;
            listActiveSkillComponent.Add(component);
            component.Initialize().StartSkill();
        }
    }
    protected EnumDefine.Rank MaxRank() {
        return EnumDefine.Rank.S;
    }

    protected int MaxLevelInRank() {
        return rankAttackData.GetAttackRank(rank).levels.Count;
    }

    //Start_TungNS
    protected IEnumerator FlashSkeleTon(float timeDuration,float timeChange) {
        Color yourColor = skeAnimation.Skeleton.GetColor();
        yourColor.a = 0.2f;
        bool onFlash = true;
        DOVirtual.DelayedCall(timeDuration,()=> { onFlash = false; });
        while(onFlash) {
            SetColor(yourColor, timeChange);
            yield return Yielder.Wait(timeChange);
            SetColor(Color.white, timeChange);
            yield return Yielder.Wait(timeChange);
        }
    }

    public Tween SetColor(Color colorTarget, float time) {
        if(skeAnimation!=null) {
            Color yourColor = skeAnimation.Skeleton.GetColor();
            return DOTween.To(() => yourColor, (co) => { skeAnimation.Skeleton.SetColor(co); }, colorTarget, time);
        }
        return null;
    }

    public void PlayPsFire() {
        psFire.Play();
    }
    //End_Tungs
    public enum StatusShip {
        Hello,
        Live,
        Death,
        Finish,
    }
}
