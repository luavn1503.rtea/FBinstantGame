﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovementBase : MonoBehaviour {
    protected bool isDown = false;
    protected bool enableMove = false;
    protected Vector3 lastPosition = Vector3.zero;
    protected Camera mainCamera = null;
    protected Vector3 lastMousePosition = Vector3.zero;

    //Start fix mvoe out scene
    [SerializeField] private Collider2D col2D;
    private float maxX = 0,maxY = 0;
    //
    public void Initialize() {
        enableMove = false;
    }

    public void EnableMove(bool enable) {
        enableMove = enable;
    }

    private void Start() {
        mainCamera = MainCamera.Instance.main;
    }

    protected Vector3 GetMousePosition() {
#if UNITY_EDITOR
        return mainCamera.ScreenToWorldPoint(Input.mousePosition);
#else
        return mainCamera.ScreenToWorldPoint(Input.touches[0].position);
#endif
    }


    protected virtual void Update() {
        if(enableMove) {
            if(Input.GetMouseButtonDown(0)) {
                isDown = true;
                lastPosition = transform.position;
                lastMousePosition = GetMousePosition();
                lastMousePosition.z = transform.position.z;
            }

            if(Input.GetMouseButtonUp(0)) {
                isDown = false;
            }

            if(Input.GetMouseButton(0)) {
                var originMousePosition = GetMousePosition();
                originMousePosition.z = transform.position.z;
                var distance = originMousePosition - lastMousePosition;
                var targetPosition = lastPosition + distance;
                //Start fix move out scene
                maxX = MainCamera.Instance.maxBullet.x;
                maxY = MainCamera.Instance.maxBullet.y - 0.5f;
                if(targetPosition.x >= maxX) {
                    targetPosition.x = maxX;
                }else if(targetPosition.x <= -maxX) {
                    targetPosition.x = -maxX;
                }

                if(targetPosition.y >= maxY) {
                    targetPosition.y = maxY;
                }
                else if(targetPosition.y <= -maxY) {
                    targetPosition.y = -maxY;
                }
                //End
                transform.position = targetPosition; 
            }
        }
    }
}
