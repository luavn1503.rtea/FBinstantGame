﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShipBulletBase : MonoBehaviour {
    protected float damage = 0f;
    protected Vector2 maxSize = Vector2.zero;
    public EnumDefine.IDBullet idBullet = EnumDefine.IDBullet.Bullet_None_1;

    public virtual ShipBulletBase Initialized() {
        maxSize = MainCamera.Instance.max;
        return this;
    }

    public virtual void Remove() {
        gameObject.Recycle();
    }
}

