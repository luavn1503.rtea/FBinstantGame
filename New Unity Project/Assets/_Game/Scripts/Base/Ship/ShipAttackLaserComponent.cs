﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAttackLaserComponent : ShipAttackerBase {
    private List<ShipBulletLaser> bulletLasers =new List<ShipBulletLaser>();
    private ShipBulletLaser bulletCreate = null;
    public override EnumDefine.TypeAttack typeAttack => EnumDefine.TypeAttack.Laser;


    protected override void Fire() {
        if(ship != null) {
            ship.PlayPsFire();
        }
        foreach(var attackData in attackData.dataAttack) {
            if(attackData.timeDelay == 0) {
                CreateBullet(attackData,this.attackData.GetWithBulletLaser());
            }
            else {
                StartCoroutine(TimeDelay(attackData.timeDelay, attackData));
            }
        }
    }

    public override void PauseAttack() {
        foreach(ShipBulletLaser b in bulletLasers) {
            b.Remove();
        }
        bulletLasers.Clear();
    }

    protected void CreateBullet(AttackData attackData,float withLaser) {
        var bulletPrefab = bulletData.GetBullet(attackData.idBullet);
        if(bulletPrefab == null) { return; }
        var origin = transform.position;
        var possition = origin + attackData.offset;
        bulletCreate = bulletPrefab.Spawn(transform, possition) as ShipBulletLaser;
        bulletCreate.transform.localEulerAngles = attackData.eulerAngle;
        bulletCreate.SetStartWith(withLaser);
        bulletCreate.Initialized();
        bulletLasers.Add(bulletCreate);
    }
}

