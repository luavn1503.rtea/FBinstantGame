﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAttackNormalComponent : ShipAttackerBase {
    private IEnumerator IETimeFire = null;
    public override EnumDefine.TypeAttack typeAttack => EnumDefine.TypeAttack.None;

    public override void StartAttack() {
        IETimeFire = TimeFire();
        StartCoroutine(IETimeFire);
    }

    public override void PauseAttack() {
        if (IETimeFire != null) {
            StopCoroutine(IETimeFire);
            IETimeFire = null;
        }
    }

    private IEnumerator TimeFire() {
        while(true) {
            Fire();
            yield return Yielder.Wait(attackData.rateAttack);
        }
    }

}
