﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipStatsBase : MonoBehaviour {
    protected bool isDeath = false;
    [SerializeField] protected ShipBehaviorBase shipBehavior = null;
    [SerializeField] protected float healthBase = 1f;
    [SerializeField] protected float healthTemp = 0f;
    public void Initialize(ShipBehaviorBase shipBehavior) {
        isDeath = false;
        healthTemp = healthBase;
        this.shipBehavior = shipBehavior;
    }

    public void TakeDamage(float damage) {
        if(!isDeath && shipBehavior.GetShipCollider().EnebleCollider) {
            healthTemp -= damage;
            if(healthTemp <= 0) {
                isDeath = true;
                ManagerPlayer.Instance.OnDeath();
            }
        }
    }

    public void OnRevive() {
        isDeath = false;
        healthTemp = healthBase;
    }
}
