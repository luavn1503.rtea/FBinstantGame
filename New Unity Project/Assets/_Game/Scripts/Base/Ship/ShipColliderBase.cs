﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipColliderBase : Collider2DBase, ITakeDamage {
    [SerializeField] protected ShipStatsBase shipStats = null;
    [SerializeField] protected ShipBehaviorBase shipBehavior = null;
    public void Initialize(ShipBehaviorBase shipBehavior, ShipStatsBase shipStats) {
        this.shipBehavior = shipBehavior;
        this.shipStats = shipStats;
    }

    public void TakeDamage(float damage) {
        shipStats.TakeDamage(damage);
    }
}
