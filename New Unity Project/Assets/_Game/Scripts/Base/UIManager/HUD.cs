﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gemmob {
    [DisallowMultipleComponent]
    public abstract class HUD : MonoBehaviour {
        private List<Frame> activeFrames = new List<Frame>();
        [SerializeField] private int order = 0;
        [SerializeField] private Frame defaultFrame = null;
        [Header("[Frames]")]
        [SerializeField] private Frame[] frames = new Frame[0];

        private void LogActiveFrames() {
            int index = 1;
            foreach(Frame frame in GetActiveFrames()) {
                Debug.Log($"[{GetType()}] {index}: {frame.gameObject.name}", frame.gameObject);
                index++;
            }
        }

        protected virtual void Reset() {
            frames = GetComponentsInChildren<Frame>(true);
            if(frames.Length > 0) {
                defaultFrame = frames[0];
            }
        }
        protected virtual void OnEnable() {
            HUDManager.Add(this);
        }

        protected virtual void OnDisable() {
            HUDManager.Remove(this);
        }

        protected virtual void Start() {
            activeFrames = new List<Frame>(frames.Length);
            foreach(Frame frame in frames) { InitializeFrame(frame); }
            Show(defaultFrame);
        }

        protected virtual void OnDestroy() {
            foreach(Frame frame in frames) {
                if(frame == null) { continue; }
                frame.Remove();
                frame.OnShowed.RemoveListener(OnFrameShowed);
                frame.OnHidden.RemoveListener(OnFrameHidden);
            }
        }

        public virtual bool OnUpdate() {
            if(Input.GetKeyDown(KeyCode.Escape) && GetActiveFrameCount() > 0) {
                OnBackButtonCLicked();
                return true;
            }
            return false;
        }

        #region Get States

        public int Order { get => order; }

        public int GetFrameCount() {
            return frames.Length;
        }

        public int GetActiveFrameCount() {
            return activeFrames.Count;
        }

        public Frame GetFrameActiveOnTop() {
            if(activeFrames.Count == 0) { return null; }
            return activeFrames[activeFrames.Count - 1];
        }

        public F GetFrameActiveOnTop<F>() where F : Frame {
            return GetFrameActiveOnTop() as F;
        }

        public bool IsFrameActiveOnTop(Frame target) {
            return GetFrameActiveOnTop() == target;
        }

        public IEnumerable<Frame> GetFrames() {
            foreach(Frame frame in frames) {
                yield return frame;
            }
        }

        public IEnumerable<Frame> GetActiveFrames() {
            foreach(Frame frame in activeFrames) {
                yield return frame;
            }
        }

        public F GetFrame<F>() where F : Frame {
            foreach(Frame frame in GetFrames()) {
                if(frame is F) {
                    return frame as F;
                }
            }
            return null;
        }

        public F GetActiveFrame<F>() where F : Frame {
            foreach(Frame frame in GetActiveFrames()) {
                if(frame is F) {
                    return frame as F;
                }
            }
            return null;
        }

        public bool ContainsFrame<F>() where F : Frame {
            foreach(Frame frame in GetFrames()) {
                if(frame is F) {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsFrame(Frame target) {
            foreach(Frame frame in GetFrames()) {
                if(frame == target) {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsActiveFrame<F>() where F : Frame {
            foreach(Frame frame in GetActiveFrames()) {
                if(frame is F) {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsActiveFrame(Frame target) {
            foreach(Frame frame in GetActiveFrames()) {
                if(frame == target) {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Show & Hide & Pause
        public virtual Frame ShowFrameActiveOnTop(Action onCompleted = null, bool instant = false) {
            return Show(GetFrameActiveOnTop(), onCompleted, instant);
        }

        public F Show<F>(Action onCompleted = null, bool instant = false) where F : Frame {
            return Show(GetFrame<F>(), onCompleted, instant) as F;
        }

        public virtual Frame Show(Frame frame, Action onCompleted = null, bool instant = false) {
            if(frame == null)
                return null;

            if(!frame.Initialized) {
                InitializeFrame(frame);
            }

            if(frame.Hud != this) {
                Debug.LogWarningFormat("[HUD] The trying to open a frame {0} is not within the scope of the current hud {1}.", frame.name, GetType().Name);
                return null;
            }

            if(ContainsActiveFrame(frame)) {
                Debug.LogWarningFormat("[HUD] The trying to open a frame {0} has been opened before.", frame.name);
                activeFrames.Remove(frame);
            }

            return frame.Show(onCompleted, instant);
        }

        public F Hide<F>(Action onCompleted = null, bool instant = false) where F : Frame {
            return Hide(GetFrame<F>(), onCompleted, instant) as F;
        }

        public Frame HideFrameActiveTop(Action onCompleted = null, bool instant = false) {
            return Hide(GetFrameActiveOnTop(), onCompleted, instant);
        }

        public virtual Frame Hide(Frame frame, Action onCompleted = null, bool instant = false) {
            if(frame == null) { return null; }
            if(!ContainsActiveFrame(frame)) {
                Debug.LogWarningFormat("[HUD] The frame {0} has not been opened before.", frame.name);
                return null;
            }

            if(IsFrameActiveOnTop(frame)) {
                Debug.LogWarningFormat("[HUD] The closing a frame {0} is not on the top.", frame.name);
            }

            return frame.Hide(onCompleted, instant);
        }

        public virtual Frame PauseFrameActiveOnTop(Action onCompleted = null, bool instant = false) {
            return Pause(GetFrameActiveOnTop(), onCompleted, instant);
        }

        public virtual Frame Pause(Frame frame, Action onCompleted = null, bool instant = false) {
            if(frame == null)
                return null;
            if(!ContainsActiveFrame(frame)) {
                Debug.LogWarningFormat("[HUD] The frame {0} has not been opened before.", frame.name);
                return null;
            }

            return frame.Pause(onCompleted, instant);
        }

        public virtual Frame ResumeFrameActiveOnTop(Action onCompleted = null, bool instant = false) {
            return Resume(GetFrameActiveOnTop(), onCompleted, instant);
        }

        public virtual Frame Resume(Frame frame, Action onCompleted = null, bool instant = false) {
            if(frame == null)
                return null;
            if(!ContainsActiveFrame(frame)) {
                Debug.LogWarningFormat("[HUD] The frame {0} has not been opened before.", frame.name);
                return null;
            }

            return frame.Resume(onCompleted, instant);
        }


        public int HideAll() {
            int hideCount = activeFrames.Count;
            for(int i = activeFrames.Count - 1; i >= 0; i--) {
                activeFrames[i].Hide(null, true);
            }
            return hideCount;
        }

        #endregion

        protected virtual void InitializeFrame(Frame frame) {
            if(frame == null) { return; }
            frame.gameObject.SetActive(false);
            frame.Initialize(this);
            frame.OnShowed.AddListener(OnFrameShowed);
            frame.OnHidden.AddListener(OnFrameHidden);
        }

        protected virtual void OnFrameShowed(Frame frame) {
            if(frame == null) { return; }
            if(frame == GetFrameActiveOnTop()) { return; }
            if(!ContainsFrame(frame)) { return; }
            activeFrames.Add(frame);
        }

        protected virtual void OnFrameHidden(Frame frame) {
            if(frame == null) { return; }
            if(!ContainsFrame(frame)) { return; }
            bool isFrameOnTop = frame == GetFrameActiveOnTop();
            activeFrames.Remove(frame);
            if(isFrameOnTop) { GetFrameActiveOnTop()?.Resume(); }
        }

        protected virtual void OnBackButtonCLicked() {
            Frame frameOnTop = GetFrameActiveOnTop();
            if(frameOnTop == null) { return; }
            frameOnTop.OnBack();
        }
    }

    public abstract class HUD<T> : HUD where T : HUD<T> {
        private static T instance;

        public static T Instance {
            get {
                if(instance == null) {
                    instance = FindObjectOfType<T>();
                    if(instance == null) {
                        Logs.LogError(string.Format("[SINGLETON] Không tìm thấy class {0} trong cảnh!", typeof(T)));
                    }
                }
                return instance;
            }
        }

        protected virtual void Awake() {
            if(instance == null) {
                instance = this as T;
            }
            else if(instance != this) {
                Logs.LogError(string.Format("[SINGLETON] Có nhiều hơn một thể hiện của class {0} trong cảnh!", typeof(T)));
                Destroy(this.gameObject);
            }
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            instance = null;
        }
    }
}
