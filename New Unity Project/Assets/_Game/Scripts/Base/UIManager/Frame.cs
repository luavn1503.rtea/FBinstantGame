﻿using System;
using UnityEngine;

namespace Gemmob {
    [DisallowMultipleComponent]
    public class Frame : MonoBehaviour {
        private bool initialized = false;
        private bool showed = false;
        private bool paused = false;
        private HUD hud = null;
        [Header("[Events]")]
        [SerializeField] private FrameEvent onShowed = null;
        [SerializeField] private FrameEvent onHidden = null;
        [SerializeField] private FrameEvent onPaused = null;
        [SerializeField] private FrameEvent onResumed = null;
        public FrameEvent OnShowed { get { return onShowed; } set { onShowed = value; } }
        public FrameEvent OnHidden { get { return onHidden; } set { onHidden = value; } }
        public FrameEvent OnPaused { get { return onPaused; } set { onPaused = value; } }
        public FrameEvent OnResumed { get { return onResumed; } set { onResumed = value; } }

        public HUD Hud { get => hud; private set => hud = value; }
        public bool Initialized { get => initialized; private set => initialized = value; }
        public bool Showed {
            get => showed;
            private set {
                showed = value;

                if (showed) {
                    OnShowed?.Invoke(this);
                }
                else {
                    OnHidden?.Invoke(this);
                }
            }
        }

        public bool Paused {
            get => paused;
            private set {
                paused = value;

                if (paused) {
                    OnPaused?.Invoke(this);
                }
                else {
                    OnResumed?.Invoke(this);
                }
            }
        }

        public Frame Initialize(HUD hud) {
            if (!Initialized) {
                this.Hud = hud;

                Initialized = true;
                Showed = false;
                Paused = false;

                OnInitialize(hud);
            }
            return this;
        }

        public void Remove() {
            OnRemove();
        }

        public Frame Show(Action onCompleted = null, bool instant = false) {
            Showed = true;
            Paused = false;
            OnShow(onCompleted, instant);
            return this;
        }

        public Frame Hide(Action onCompleted = null, bool instant = false) {
            Showed = false;
            OnHide(onCompleted, instant);
            return this;
        }

        public Frame Pause(Action onCompleted = null, bool instant = false) {
            if (!Paused) {
                Paused = true;
                onPaused?.Invoke(this);
                OnPause(onCompleted, instant);
            }
            return this;
        }

        public Frame Resume(Action onCompleted = null, bool instant = false) {
            if (Paused) {
                Paused = false;
                onResumed?.Invoke(this);
                OnResume(onCompleted, instant);
            }
            return this;
        }

        public virtual Frame OnBack() {
            return Hide();
        }

        protected virtual void OnInitialize(HUD hud) { }
        protected virtual void OnRemove() { }

        protected virtual void OnShow(Action onCompleted = null, bool instant = false) {
            gameObject.SetActive(true);
            onCompleted?.Invoke();
        }

        protected virtual void OnHide(Action onCompleted = null, bool instant = false) {
            gameObject.SetActive(false);
            onCompleted?.Invoke();
        }

        protected virtual void OnPause(Action onCompleted = null, bool instant = false) {
            onCompleted?.Invoke();
        }

        protected virtual void OnResume(Action onCompleted = null, bool instant = false) {
            onCompleted?.Invoke();
        }
    }
}
