﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBase : MonoBehaviour, IDamage, ITakeDamage {
    protected float damageRecived = 0f;
    protected float maxDamageRecive = float.MaxValue;
    protected Tween tweenDestroy = null;
    [SerializeField] protected Collider2DBase coll2D = null;
    [SerializeField] protected DamagePerSecendsBase damagePerSecends = null;

    public void Initialize(float timeLive, float maxDamageRecive = float.MaxValue) {
        this.maxDamageRecive = maxDamageRecive;
        damagePerSecends.Initialize(coll2D, BaseDamage: float.MaxValue);
        tweenDestroy = DOVirtual.DelayedCall(timeLive, ()=> {
            Recycle();
        });
    }

    public void Recycle() {
        damagePerSecends.Remove();
        gameObject.Recycle();
    }

    public void OnEnterColliderDamage(Collider2D collision) {
        
    }

    public void OnExitColliderDamage(Collider2D collision) {
        
    }

    public void TakeDamage(float damage) {
        damageRecived += damage;
        if (damageRecived >= maxDamageRecive) {
            Recycle();
        }
    }
}
