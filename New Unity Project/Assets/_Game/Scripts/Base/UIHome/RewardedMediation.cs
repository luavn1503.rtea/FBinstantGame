﻿//using Gemmob.API.Ads;
using System;
using System.Diagnostics;

public partial class Mediation {
    public const string RewardedCondition = "ADS_REWARDED";

#if ADS_ENABLE
    RewardedAdmob admobRewarded = null;
#else
   // Ad admobRewarded = null;
#endif
    //Ad backupRewarded = null;

    [Conditional(RewardedCondition)]
    private void InitRewardedVideo() {
#if ADS_ENABLE
        admobRewarded = new RewardedAdmob(AdmobInfo.video, Ad.Type.Rewarded);
#if ADS_BACKUP_UNITY
        backupRewarded = new RewardedUnity(BackupInfo.unityads, Ad.Type.Rewarded);
#endif
        Logs.Log("[Mediation] RewardVideo initialized.");
#endif
    }

    [Conditional(RewardedCondition)]
    public void ShowRewardVideoUnity(string position, Action onCompleted = null, Action onFailed = null, float delayTime = 0) {
        //Show(position, backupRewarded, null, onCompleted, onFailed, delayTime);
    }

    [Conditional(RewardedCondition)]
    public void ShowRewardVideo(string position, Action onCompleted = null, Action onFailed = null, float delayTime = 0) {
       //Show(position, admobRewarded, backupRewarded, onCompleted, onFailed, delayTime);
    }

    [Conditional(RewardedCondition)]
    private void RequestRewardVideo() {
       // if (admobRewarded != null) admobRewarded.Request();
    }

    public bool HasRewardVideo {
        get {
#if ADS_ENABLE && ADS_REWARDED
            return (admobRewarded != null && admobRewarded.IsLoaded) || (backupRewarded != null && backupRewarded.IsLoaded);
#else
            Logs.Log("Your project config is not useRewardVideo.");
            return false;
#endif
        }
    }
}
