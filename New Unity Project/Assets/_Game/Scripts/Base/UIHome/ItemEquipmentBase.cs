﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public abstract class ItemEquipmentBase : MonoBehaviour {
    [SerializeField] protected Image imgIcon = null;
    [SerializeField] protected Image imgRank = null;
    [SerializeField] protected TextMeshProUGUI txtRank = null;
    [SerializeField] protected ButtonBase btnSelect = null;
    [SerializeField] protected GameObject goDebris = null;
    [SerializeField] protected GameObject goSelected = null;
    [SerializeField] protected GameObject goEquiped = null;

    public void SetIcon(Sprite sprIcon) {
        if(imgIcon == null) {
            Logs.Log("Image Icon Null");
            return;
        }
        if (sprIcon == null) {
            Logs.Log("Sprite Icon Null");
            return;
        }
        imgIcon.sprite = sprIcon;
        var perfectSize = imgIcon.GetComponent<PerfectSize>();
        if(perfectSize) {
            var sizeFix = sprIcon.rect.size;
            perfectSize.AutoSize(sizeFix);
        }
    }

    public void SetRank(EnumDefine.Rank rank) {
        if(txtRank == null) {
            Logs.Log("Text Rank Null");
            return;
        }
        txtRank.text = rank.ToString();
        imgRank.sprite = SpritesData.Instance.GetBorderRank(rank);
    }

    public void SetActiveDebris(bool active) {
        if(goDebris == null) {
            Logs.Log("Debris Null");
            return;
        }
        goDebris.SetActive(active);
    }

    public void SetEquiped(bool active) {
        if(goEquiped == null) {
            Logs.Log("Equiped Null");
            return;
        }
        goEquiped.SetActive(active);
    }

    public virtual void OnSelect() {
        goSelected.SetActive(true);
    }

    public virtual void OnUnSelect() {
        goSelected.SetActive(false);
    }

    protected virtual void ButtonSelect() {

    }
}
