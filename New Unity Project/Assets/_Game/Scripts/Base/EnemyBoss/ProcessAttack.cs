﻿using DG.Tweening;
using Gemmob;
using Spine.Unity;
using System.Collections;
using UnityEngine;

public class ProcessAttack : EnemyAttackBase {
    [SerializeField,SpineAnimation] protected string startAnim,liveAnim,endAnim;
    [Header("NameProcess")] public string nameProcess;
    [SerializeField] protected BossAttackBase bossAttackBase;
    [SerializeField] protected AnimationController animController;
    [SerializeField] protected bool canMove = true;
    [SerializeField] protected int numberTurn = 1;
    protected int curNumberTurn;
    private Tween tween = null;
    protected float time = 0f;
    public void SetAnim(AnimationController Anim) {
        animController = Anim;
    }

    public void SetBossAtk(BossAttackBase BossAtk) {
        bossAttackBase = BossAtk;
    }

    public override void Init() {
        curNumberTurn = numberTurn;
        if(string.IsNullOrEmpty(startAnim)) {
            base.Init();
        }
        else {
            time = animController.SetAnimation(startAnim);
            tween = DOVirtual.DelayedCall(time, () => { base.Init(); });
        }
    }


    protected override IEnumerator Fire() {
        if(!string.IsNullOrEmpty(liveAnim)) {
            animController.SetAnimation(liveAnim, true);
        }
        foreach(GameObject attackEffect in attackPoints) {
            if(attackEffect == attackPoints[attackPoints.Count - 1]) {
                yield return StartCoroutine(FireBulletPoint(attackEffect));
                endProcess();
            }
            else {
                StartCoroutine(FireBulletPoint(attackEffect));
                if(timePointDelay > 0.001) {
                    yield return Yielder.Wait(timePointDelay);
                }
            }
        }  
    }
    protected virtual void endProcess() {
        curNumberTurn--;
        if(curNumberTurn > 0) {
            tween = DOVirtual.DelayedCall(timeTurnDelay, () => { StartCoroutine(Fire()); });
        }
        else {
            if(string.IsNullOrEmpty(endAnim)) {
                bossAttackBase.NextProcess();
            }
            else {
                time = animController.SetAnimation(endAnim);
                tween =  DOVirtual.DelayedCall(time, () => { bossAttackBase.NextProcess(); });
            }
        }
    }

    private void OnValidate() {
        bossAttackBase = GetComponent<BossAttackBase>();
        animController = GetComponent<AnimationController>();
    }

    public bool GetCanMove() {
        return canMove;
    }

    public void KillTween() {
        tween.Kill();
        StopCoroutine(coroutine);
    } 
}
