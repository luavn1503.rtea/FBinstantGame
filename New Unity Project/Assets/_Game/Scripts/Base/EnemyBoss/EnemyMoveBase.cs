﻿
using DG.Tweening;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveBase : MonoBehaviour {
    public bool IsAttacking;
    private bool canPush = false;
    [SerializeField] private bool _isMoveAttack = true;


    private TweenCallback _moveOnFinish;

    private Tween tween;

    private Transform objView;

    public virtual void Awake() {
        //_trans = transform;
        //_enemyBase = GetComponent<EnemyBase>();
    }

    public void MoveAppear(List<Vector3> movePath, float speed, Cell cell, float delay, TweenCallback moveFinished) {
        canPush = false;
        IsAttacking = false;
        _moveOnFinish = moveFinished;

        transform.position = movePath[0];
        transform.DOPath(movePath.ToArray(), speed, PathType.CatmullRom, PathMode.TopDown2D).SetSpeedBased(true)
            .SetEase(Ease.Linear).SetLookAt(.1f).SetDelay(delay).OnStart(() => {
                transform.GetComponentInChildren<SkeletonAnimation>().state.SetAnimation(0,
                    transform.GetComponentInChildren<SkeletonAnimation>().AnimationName, true);
            }).OnComplete(() => {
                transform.DORotate(new Vector3(0, 0, 0), .5f);
                SetMoveDone();
                //GetComponent<EnemyBase>().NoDam = false;
            });
    }

    public void MoveLoop(List<Vector3> movePath, float speed, Cell cell, float delay, TweenCallback moveFinished) {
        _moveOnFinish = moveFinished;

        transform.position = movePath[0];
        transform.DOPath(movePath.ToArray(), speed, PathType.CatmullRom, PathMode.TopDown2D).SetSpeedBased(true)
            .SetEase(Ease.Linear).SetLookAt(.1f).SetLoops(-1).SetDelay(delay);
        SetMoveDone();
    }

    public void MoveAndDestroyOnFinish(List<Vector3> movePath, float speed, Cell cell, float delay,
        TweenCallback moveFinished) {
        _moveOnFinish = moveFinished;

        transform.position = movePath[0];
        transform.DOPath(movePath.ToArray(), speed, PathType.CatmullRom, PathMode.TopDown2D).SetSpeedBased(true)
            .SetEase(Ease.Linear).SetLookAt(.1f).SetDelay(delay).OnComplete(() => {
                GetComponent<EnemyNormal>().EnemyDie();
                SetMoveDone();
            });
    }

    public void OnEnemyDie() {
        SetMoveDone();
        transform.DOKill();
    }

    void SetMoveDone() {
        if(_moveOnFinish != null) {
            _moveOnFinish();
            _moveOnFinish = null;
        }
    }

    public void MoveAttackPlayer(Ease ease) {
        //Start- Stop Enemy when move attack 
        SetCanPush(false);
        var a = transform.GetComponent<EnemyAttackBase>();
        if(a!=null) {
            a.StopFire();
        }
        //End
        if(!_isMoveAttack)
            return;

        List<Vector3> pathMove = new List<Vector3>();
        pathMove.Add(transform.position);

        Vector3 middlePos = transform.position;
        middlePos.y += .5f;
        Vector3 ppos =  ManagerPlayer.Instance.GetShipPossition().possition;
        //Start_ Style 2
        middlePos.x += (ppos.x - middlePos.x) / 4;

        pathMove.Add(middlePos);
        pathMove.Add(ManagerPlayer.Instance.GetShipPossition().possition);

        objView = transform.GetComponent<EnemyNormal>().ViewObj;
        tween = objView.DOPath(pathMove.ToArray(), 5, PathType.CatmullRom).SetSpeedBased(true).SetEase(ease).OnComplete(
            () => {
                objView.DOLocalMove(Vector3.zero, 5).SetSpeedBased(true).SetEase(ease)
                    .OnComplete(() => { IsAttacking = false; });
            });

        //End

        //Start_ Style 1
        //objView = transform.GetComponent<EnemyNormal>().ViewObj;
        //objView.DOMove(ppos,3f) .OnComplete(
        //    () => {
        //        objView.DOLocalMove(Vector3.zero, 5).SetSpeedBased(true).SetEase(ease)
        //            .OnComplete(() => { IsAttacking = false; });
        //});
        //End
    }

    public void SetCanPush(bool IsCan) {
        canPush = IsCan;
    }

    public void PushBack() {
        if(canPush) {
            objView = transform.GetComponent<EnemyNormal>().ViewObj;
            if(tween != null) {
                tween.Kill();
            }
            tween = objView.DOLocalMove((Vector3)Vector2.up * 0.2f, 0.1f).OnComplete(() => {
                tween = objView.DOLocalMove(Vector3.zero, 0.1f).OnComplete(() => { tween = null; });
            });
        }
    }
}
