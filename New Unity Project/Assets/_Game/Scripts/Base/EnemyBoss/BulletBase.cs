﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;

[RequireComponent(typeof(Rigidbody2D))]
public class BulletBase : MonoBehaviour
{
    [SerializeField] protected Rigidbody2D rg2D;
    [SerializeField] protected float speed = 5;
    [SerializeField] protected Vector2 direction = Vector2.down;
    [SerializeField] protected float dame = 1;
    [SerializeField] protected float timeLife = 5f;
    [SerializeField] protected GameObject psRecycle = null;
    protected Tween tweenTimeLife;
    // Start is called before the first frame update
    public void SetDirect(Vector2 Direction) {
        direction = Direction.normalized;
    }

    public void SetDirect(float angle) {
        var rad = angle * Mathf.Deg2Rad;
        direction.x = Mathf.Cos(rad);
        direction.y = Mathf.Sin(rad);
    }

    public void SetDamage(float Damage) {
        dame = Damage;
    }
    public float GetSpeed() {
        return speed;
    }

    [ContextMenu("StartInit")]
    public virtual void Init() {
        Init(direction, speed,dame);
    }

    protected virtual void Init(Vector2 Direction,float Speed, float Dame) { 
        direction = Direction;
        transform.up = Direction;
        speed = Speed;
        dame = Dame;
        Active();
        tweenTimeLife = DOVirtual.DelayedCall(timeLife, () => { gameObject.Recycle(); });
    }

    protected virtual void Active() {
        rg2D.velocity = direction.normalized * speed;
    }

    protected virtual void OnTriggerEnter2D(Collider2D coll) {
        if(!gameObject.activeInHierarchy) {
            return;
        }

        if(coll.CompareTag(TagDefine.Player)) {
            Debug.Log("Player get "+ dame + "_Dame");
            if(coll.GetComponent<ITakeDamage>()!=null) {
                coll.GetComponent<ITakeDamage>().TakeDamage(dame); 
            }
            RecycleBullet();
        }
       
    }

    private void OnValidate() {
        rg2D = GetComponent<Rigidbody2D>();
    }

    protected void RecycleBullet() {
        psRecycle.Spawn(transform.position);
        tweenTimeLife.Kill();
        gameObject.Recycle();
    }

    protected virtual void FixedUpdate() {
        transform.up = direction;
    }

    //Strat -Support find the target
    protected Vector2 DirectTargetPlayer() {
        return (ManagerPlayer.Instance.GetShipPossition().possition - transform.position).normalized;
    }
    //End 
}
