﻿using DG.Tweening;
using Gemmob;
using System;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D)),
 RequireComponent(typeof(EnemyAttackBase)),
 RequireComponent(typeof(EnemyMoveBase))]
public class EnemyNormal : EnemyBase {
    [SerializeField] protected EnemyAttackBase eAttack;
    [SerializeField] public Transform ViewObj;
    [SerializeField] private int itemIndex = -1;


    public virtual void SetItemBase(int Item) {
        itemIndex = Item;
    }

    protected override void Awake() {
        ViewObj = transform.Find("Render");
        base.Awake();
    }

    public override void Init() {
        base.Init();
    }

    protected override void OnValidate() {
        base.OnValidate();
        eAttack = GetComponent<EnemyAttackBase>();
    }

    //public override void StartAttack() {
    //    base.StartAttack();
    //    if(enemyData.CanAttack) {
    //        eAttack.Init();
    //    }
    //}

    private void OnDisable() {
        EventDispatcher.Instance.RemoveListener<StructDefine.EnemyAttack>();
    }

    public override void EnemyDie() {
        OnDropItem();
        base.EnemyDie();

    }

    protected virtual void OnDropItem() {
        if(itemIndex >= ManagerDataIngame.Instance.Items.Count || itemIndex < 0) {
            return;
        }
        var item = ManagerDataIngame.Instance.Items[itemIndex].Spawn(transform.position);
        item.Init();
    }

    public override void TakeDamage(float damage) {
        eMove.PushBack();
        base.TakeDamage(damage);
    }
}
