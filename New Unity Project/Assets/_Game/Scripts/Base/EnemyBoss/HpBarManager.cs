﻿using DG.Tweening;
using DG.Tweening.Core.Easing;
using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpBarManager : MonoBehaviour {
    public static HpBarManager Intance;
    [SerializeField] private GameObject hpBar;
    [SerializeField] private Image imgCurHP;
    private Color curColor;
    private Coroutine coroutine;
    public void Init(float perHP = 1f) {
        curColor = Color.red;
        hpBar.SetActive(true);
        UpDateInfo(perHP);
    }



    public void UpDateInfo(float curHP) {
        ChangeColorBar(curHP);
        if(coroutine != null) {
            StopCoroutine(coroutine);
        }
        coroutine = StartCoroutine(FadeBar());
        imgCurHP.fillAmount = curHP;
    }

    private void ChangeColorBar(float curHP) {
        if(curHP < 0.6f) {
            curColor = Color.green;
        }
        else if(curHP < 0.4f) {
            curColor = Color.yellow;
        }
        else if(curHP < 0.1f) {
            curColor = Color.white;
        }

    }

    IEnumerator FadeBar() {
        Color fadeColor = curColor;
        fadeColor.a = 0.5f;
        imgCurHP.color = fadeColor;
        yield return Yielder.Wait(0.2f);
        imgCurHP.color = curColor;
    }

    public void SetActive(bool IsActive) {
        if(coroutine != null) {
            StopCoroutine(coroutine);
        }
        hpBar.SetActive(IsActive);
    }
}
