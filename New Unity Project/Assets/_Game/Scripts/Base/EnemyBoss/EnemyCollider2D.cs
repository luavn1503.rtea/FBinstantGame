﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DamagePerSecendsBase)),
 RequireComponent(typeof(Collider2D))   ]
public class EnemyCollider2D : Collider2DBase, ITakeDamage {
    [SerializeField] private EnemyBase enemyBase = null;
    [SerializeField] private DamagePerSecendsBase damePerSec = null;
    public void Init(EnemyBase EnemyBase) {
        enemyBase = EnemyBase;
        damePerSec.Initialize(this,null,enemyBase.GetEnemyData().BodyDam);
    }
    
    public void TakeDamage(float damage) {
        if(enemyBase.IsLive) {
            enemyBase?.TakeDamage(damage);
        }
    }

    protected override void OnValidate() {
        base.OnValidate();
        damePerSec = GetComponent<DamagePerSecendsBase>();
        if(enemyBase == null) {
            enemyBase =  transform.GetComponentInParent<EnemyBase>();
        }
    }

    internal void OnEnemyDie() {
        damePerSec.Remove();
    }
}
