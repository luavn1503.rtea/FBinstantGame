﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackBase : MonoBehaviour {
    protected BulletBase bullet;
    //For 1 turn fire
    [Header("Turn")]
    [SerializeField] protected float timeTurnDelay = 3f; // time next turn 
    [Header("Point")]
    [SerializeField] protected List<GameObject> attackPoints; // List point spawner
    [SerializeField] protected float timePointDelay; // Time between hand
    //For 1 point fire
    [Header("Col And Row")]
    [SerializeField] protected int colBullet = 1;
    [SerializeField] protected float timeColDelay = 0;
    [SerializeField] protected float diffAngleCol = 10f;
    [SerializeField] protected int rowBullet = 1;
    [SerializeField] protected float timeRowDelay = .2f;// Time between bullet in turn;
    [Header("Bullet")]
    [SerializeField] protected StyleBullet styleBullet;
    [SerializeField] protected BulletBase bulletPre; // Loai dan

    protected EnemyBase eBase = null;
    protected Coroutine coroutine =null;

    protected virtual void Awake() {
        eBase = GetComponent<EnemyBase>();
    }


    [ContextMenu("Init")]
    public virtual void Init() {
        coroutine = StartCoroutine(Fire());
    }

    protected virtual IEnumerator Fire() {
        //yield return Yielder.Wait(UnityEngine.Random.Range(timeTurnDelay, 10));
        while(true) {
            foreach(GameObject attackEffect in attackPoints) {
                if(attackEffect == attackPoints[attackPoints.Count - 1]) {
                    yield return StartCoroutine(FireBulletPoint(attackEffect));
                }
                else {
                    StartCoroutine(FireBulletPoint(attackEffect));
                    if(timePointDelay>0.001) {
                        yield return Yielder.Wait(timePointDelay);
                    } 
                }
            }
            yield return Yielder.Wait(timeTurnDelay);
        }
    }

    protected virtual IEnumerator FireBulletPoint(GameObject parSys) {
        if(parSys.GetComponent<ParticleSystem>() != null) {
            parSys.GetComponent<ParticleSystem>().Play();
            yield return Yielder.Wait(1f);
        }

        float angleBullet = 0;

        //Start fix goc Defaul  vs  DefaulTarget 
        if(styleBullet == StyleBullet.DEFAUL) {
            angleBullet = -90 + (colBullet - 1) * diffAngleCol * 0.5f; // start angle of bullet
        }
        else if(styleBullet == StyleBullet.DEFAULTARGET) {
            angleBullet = AngleTargetPlayer(parSys) + (colBullet - 1) * diffAngleCol * 0.5f;
        }
        //

        for(int i = 0; i < colBullet; i++) {
            if(i == colBullet - 1) {
                yield return StartCoroutine(SpawnColBullet(parSys, angleBullet));
            }
            else {
                StartCoroutine(SpawnColBullet(parSys, angleBullet));
                if(timeColDelay > 0.001) {
                    yield return Yielder.Wait(timeColDelay);
                }

            }

            // Góc mới 
            if(styleBullet == StyleBullet.DEFAUL || styleBullet == StyleBullet.DEFAULTARGET) {
                angleBullet -= diffAngleCol;
            }

        }
    }


    protected virtual IEnumerator SpawnColBullet(GameObject ParSys, float AngleBullet) {
        for(int y = 0; y < rowBullet; y++) {

            bullet = bulletPre.Spawn(ParSys.transform.position);
            if(styleBullet == StyleBullet.TARGET) {
                bullet.SetDirect(AngleTargetPlayer(ParSys));
            }
            else if(styleBullet == StyleBullet.RANDOM) {
                bullet.SetDirect(UnityEngine.Random.Range(0, 360));
            }
            else if(styleBullet == StyleBullet.DEFAUL || styleBullet == StyleBullet.DEFAULTARGET) {
                bullet.SetDirect(AngleBullet);
            }
            bullet.SetDamage(eBase.GetEnemyData().BulletDam);
            bullet.Init();
            if(timeRowDelay > 0.001) {
                yield return Yielder.Wait(timeRowDelay);
            }
        }
    }


    protected float AngleTargetPlayer(GameObject obj) {
        Vector2 a = DirectTargetPlayer(obj);
        return -Vector2.Angle(new Vector2(1, 0), a);
    }

    protected Vector2 DirectTargetPlayer(GameObject obj) {
        return (ManagerPlayer.Instance.GetShipPossition().possition - obj.transform.position).normalized;
    }

    public void StopFire() {
        if(coroutine!=null) {
            StopCoroutine(coroutine);
        }  
    }
}



public enum StyleBullet {
    DEFAUL,
    RANDOM,
    TARGET,
    DEFAULTARGET
}
