﻿using DG.Tweening;
using Gemmob;
using UnityEngine;
using UnityEngine.Events;

public class EnemyBase : MonoBehaviour {
    public int ID;
    public Sequence MySequence = null;
    [SerializeField] protected EnemyMoveBase eMove;
    [SerializeField] protected EnemyCollider2D enemyCol2D;
    [SerializeField] protected EnemyData enemyData;
    [SerializeField] protected float curHP,totalHP;
    [SerializeField] protected Rigidbody2D rg2D;
    [SerializeField] protected GameObject psDie;
    [SerializeField] public bool IsLive = false;

    protected virtual void Awake() {
        enemyData = EnemiesData.Instance.Datas[ID];
    }

    #region Init

    public virtual void Init() {
        Init(1);
    }

    public virtual void Init(float scaleHp) {
        totalHP = enemyData.Hp * scaleHp * enemyData.NumberBarHp;
        curHP = enemyData.Hp * scaleHp * enemyData.NumberBarHp;
        if(curHP < enemyData.MinHp) {
            curHP = enemyData.MinHp;
        }
        IsLive = true;
        enemyCol2D.Init(this);
    }

    #endregion

    public virtual void TakeDamage(float damage) {
        curHP -= damage;
        if(curHP <= 0 && IsLive) {
            if(totalHP > 50) {
                MainCamera.Instance.ShakeCamera(1f);
            }
            EnemyDie();
        }
    }

    public virtual void SpawnDieEffect() {
        if(psDie != null) {
            var a = psDie.Spawn(transform.position);
            a.GetComponentInChildren<ParticleSystem>().Play();
        }
        //ManagerEnemies.Instance.SpawnerScoreJump(transform.position,enemyData.Score);
    }

    public virtual void EnemyDie() {
        eMove.OnEnemyDie();
        enemyCol2D.OnEnemyDie();
        if(IsLive) {
            IsLive = false;
            ManagerEnemies.Instance.EnemyDeath(transform.position);
            GamePlayState.Instance.AddInGameScore(GetEnemyData().Score);
            SpawnDieEffect();
            gameObject.Recycle();
        }
    }

    protected virtual void OnValidate() {
        //Get data Enemy
        enemyData = EnemiesData.Instance.Datas[ID];
        rg2D = GetComponent<Rigidbody2D>();
        eMove = GetComponent<EnemyMoveBase>();
        enemyCol2D = GetComponentInChildren<EnemyCollider2D>();
    }

    public EnemyData GetEnemyData() {
        return enemyData;
    }

    public void InitHp(int v) {

    }

    public virtual void StartAttack() {

    }
}
