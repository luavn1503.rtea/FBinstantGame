﻿using System;
using System.Diagnostics;
using UnityEngine;
using Object = UnityEngine.Object;

public static class Logs {
    public const string EnableLogsString = "ENABLE_LOGS";

    public static bool IsEnable {
        get {
#if ENABLE_LOGS
            return true;
#else
        return false;
#endif
        }
    }

    [Conditional(EnableLogsString)]
    public static void Log(string logMsg) {
        UnityEngine.Debug.Log(logMsg);
    }
    
    [Conditional(EnableLogsString)]
    public static void Log(object message, Object context) {
        UnityEngine.Debug.Log(message, context);
    }

    [Conditional(EnableLogsString)]
    public static void LogFormat(string format, params object[] args) {
        UnityEngine.Debug.LogFormat(format, args);
    }
    
    [Conditional(EnableLogsString)]
    public static void LogFormat(Object context, string format, params object[] args) {
        UnityEngine.Debug.LogFormat(context, format, args);
    }

    public static void LogError(object message) {
        UnityEngine.Debug.LogError(message);
    }

    public static void LogError(object message, Object context) {
        UnityEngine.Debug.LogError(message, context);
    }

    public static void LogErrorFormat(string format, params object[] args) {
        UnityEngine.Debug.LogErrorFormat(format, args);
    }

    public static void LogErrorFormat(Object context, string format, params object[] args) {
        UnityEngine.Debug.LogErrorFormat(context, format, args);
    }

    public static void LogException(Exception exception) {
        UnityEngine.Debug.LogException(exception);
    }

    public static void LogException(Exception exception, Object context) {
        UnityEngine.Debug.LogException(exception, context);
    }

    [Conditional(EnableLogsString)]
    public static void LogWarning(object message) {
        UnityEngine.Debug.LogWarning(message);
    }

    [Conditional(EnableLogsString)]
    public static void LogWarning(object message, Object context) {
        UnityEngine.Debug.LogWarning(message, context);
    }

    [Conditional(EnableLogsString)]
    public static void LogWarningFormat(string format, params object[] args) {
        UnityEngine.Debug.LogWarningFormat(format, args);
    }

    [Conditional(EnableLogsString)]
    public static void LogWarningFormat(Object context, string format, params object[] args) {
        UnityEngine.Debug.LogWarningFormat(context, format, args);
    }
    
    [Conditional(EnableLogsString)]
    public static void Log(string s, LogType type = LogType.Log) {
        if (!IsEnable) return;
        switch (type) {
            case LogType.Log:
                UnityEngine.Debug.Log(s);
                break;
            case LogType.Warning:
                UnityEngine.Debug.LogWarning(s);
                break;
            case LogType.Assert:
                UnityEngine.Debug.LogAssertion(s);
                break;
            case LogType.Error:
                UnityEngine.Debug.LogError(s);
                break;
            default:
                UnityEngine.Debug.Log(s);
                break;
        }
    }

    [Conditional(EnableLogsString)]
    public static void Log(Object context, string s, LogType type = LogType.Log) {
        if (!IsEnable) return;
        switch (type) {
            case LogType.Log:
                UnityEngine.Debug.Log(s, context);
                break;
            case LogType.Warning:
                UnityEngine.Debug.LogWarning(s, context);
                break;
            case LogType.Assert:
                UnityEngine.Debug.LogAssertion(s, context);
                break;
            case LogType.Error:
                UnityEngine.Debug.LogError(s, context);
                break;
            default:
                UnityEngine.Debug.Log(s, context);
                break;
        }
    }

    [Conditional(EnableLogsString)]
    public static void LogFormat(LogType type, string s, params object[] args) {
        if (!IsEnable) return;
        switch (type) {
            case LogType.Log:
                UnityEngine.Debug.LogFormat(s, args);
                break;
            case LogType.Warning:
                UnityEngine.Debug.LogWarningFormat(s, args);
                break;
            case LogType.Assert:
                UnityEngine.Debug.LogAssertionFormat(s, args);
                break;
            case LogType.Error:
                UnityEngine.Debug.LogErrorFormat(s, args);
                break;
            default:
                UnityEngine.Debug.LogFormat(s, args);
                break;
        }
    }

    [Conditional(EnableLogsString)]
    public static void LogFormat(Object context, LogType type, string s, params object[] args) {
        if (!IsEnable) return;
        switch (type) {
            case LogType.Log:
                UnityEngine.Debug.LogFormat(context, s, args);
                break;
            case LogType.Warning:
                UnityEngine.Debug.LogWarningFormat(context, s, args);
                break;
            case LogType.Assert:
                UnityEngine.Debug.LogAssertionFormat(context, s, args);
                break;
            case LogType.Error:
                UnityEngine.Debug.LogErrorFormat(context, s, args);
                break;
            default:
                UnityEngine.Debug.LogFormat(context, s, args);
                break;
        }
    }
}