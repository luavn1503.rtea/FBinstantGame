﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopItem : MonoBehaviour {
    [Serializable]
    public class EarnInfor {
        public TextMeshProUGUI Txt;
        public Image Icon;
    }
    public static event Action UpdateData;
    private LocalCountDown _cd;
    [SerializeField] private int _id;
    [SerializeField] private TypeReward _type;
    [SerializeField] private Image _bg;
    [SerializeField] private Image _frame;
    [SerializeField] private Image _icon;
    [SerializeField] private GameObject _bonusTag;
    [SerializeField] private GameObject _line;
    [SerializeField] private EarnInfor _bonusInfor;
    [SerializeField] private EarnInfor _originInfor;
    [SerializeField] private ButtonAnimation _btnPanel;

    [Header("Buy by iap")]
    [SerializeField] private ButtonAnimation _btnBuyIap;
    [SerializeField] private TextMeshProUGUI _txtPriceIap;

    [Header("Buy by other")]
    [SerializeField] private ButtonAnimation _btnBuyOther;
    [SerializeField] private GameObject _gemSet;
    [SerializeField] private GameObject _videoIcon;
    [SerializeField] private TextMeshProUGUI _txtPriceGem;
    [SerializeField] private TextMeshProUGUI _txtCd;
    private PlayerInfo playerInfor;
    private void Start() {
        playerInfor = ManagerData.Instance.PlayerInfo;
        UpdateData += UpdateGem;

    }

    private void OnDisable() {
        if(_cd != null) {
            _cd.OnTick -= UpdateTextCountdown;
            _cd.OnComplete -= CountdownComplete;
        }
    }

    private void OnDestroy() {
        UpdateData -= UpdateGem;
    }

    #region BuyEvent

    void AddEventBtn(ButtonAnimation btn, UnityEngine.Events.UnityAction action) {
        btn.onClick.AddListener(action);
        _btnPanel.onClick.AddListener(action);
    }

    void OnIapClick(ShopCoinInfor infor) {
        int addNum = 0;
        UnityIAP.Instance.Buy(infor.Iap, infor.Iap, () => {
            if(RemoteConfigExtra.IsVideoDoubleIap) {
                if(_type == TypeReward.Coin) {
                    addNum = infor.Value;
                    if(playerInfor.BonusBuyCoin[_id] > 1) {
                        Tracking.LogIapABTesting(RemoteConfigExtra.IsVideoDoubleIap);
                        //[Tracking - Position]-[MenuShowup]
                        GameTracking.Instance.TrackMenuShowup("MenuWatchAds");

                        // popup
                        ShopPage shopPage = UIHomeManager.Instance.Show<ShopPage>();
                        shopPage._pnlQuestion.SetActive(true);
                        shopPage._btnYes.onClick.RemoveAllListeners();
                        shopPage._btnNo.onClick.RemoveAllListeners();
                        shopPage._btnYes.onClick.AddListener(()=> {
                            //[Tracking - Position]-[ButtonPress]
                            GameTracking.Instance.TrackButtonPress("MenuWatchAds", "btn_watchAds");
                            //[Tracking-Position]-[VideoAds]
                            AdsModify.ShowRewardVideo("shop", AdsType.BonusReward, "MenuWatchAds", "null", "null", () => {
                                int addNumBonus = infor.Value * (playerInfor.BonusBuyCoin[_id] - 1);
                                playerInfor.AddGold(addNumBonus + addNum, "MenuWatchAds");
                                EffectCurrency.Instance.IconFlow(transform.position, addNumBonus + addNum, _type);
                                shopPage._pnlQuestion.SetActive(false);
                                playerInfor.CheckBonusBuy(_id, playerInfor.BonusBuyCoin, SetActiveBonusSet);
                                Tracking.LogPercentWatchedVideoDouble(true);
                            }, ()=> {
                                playerInfor.AddGold(addNum, "MenuWatchAds");
                                EffectCurrency.Instance.IconFlow(transform.position, addNum, _type);
                                shopPage._pnlQuestion.SetActive(false);
                                playerInfor.CheckBonusBuy(_id, playerInfor.BonusBuyCoin, SetActiveBonusSet);
                                Tracking.LogPercentWatchedVideoDouble(false);
                            });
                        });
                        shopPage._btnNo.onClick.AddListener(()=> {
                            //[Tracking - Position]-[ButtonPress]
                            GameTracking.Instance.TrackButtonPress("MenuWatchAds", "btn_back");

                            playerInfor.AddGold(addNum, "MenuWatchAds");
                            EffectCurrency.Instance.IconFlow(transform.position, addNum, _type);
                            shopPage._pnlQuestion.SetActive(false);
                            playerInfor.CheckBonusBuy(_id, playerInfor.BonusBuyCoin, SetActiveBonusSet);
                            Tracking.LogPercentWatchedVideoDouble(false);
                        });
                    }else {
                        playerInfor.AddGold(addNum, this.name);
                        EffectCurrency.Instance.IconFlow(transform.position, addNum, _type);
                    }
                } 
                else { // gem
                    addNum = infor.Value;
                    if (playerInfor.BonusBuyGem[_id] > 1) {
                        Tracking.LogIapABTesting(RemoteConfigExtra.IsVideoDoubleIap);
                        //[Tracking - Position]-[MenuShowup]
                        GameTracking.Instance.TrackMenuShowup("MenuWatchAds");

                        // popup
                        ShopPage shopPage = UIHomeManager.Instance.Show<ShopPage>();
                        shopPage._pnlQuestion.SetActive(true);
                        shopPage._btnYes.onClick.RemoveAllListeners();
                        shopPage._btnNo.onClick.RemoveAllListeners();
                        shopPage._btnYes.onClick.AddListener(() => {
                            //[Tracking - Position]-[ButtonPress]
                            GameTracking.Instance.TrackButtonPress("MenuWatchAds", "btn_watchAds");
                            //[Tracking-Position]-[VideoAds]
                            AdsModify.ShowRewardVideo("shop", AdsType.BonusReward, "MenuWatchAds", "null", "null", () => {
                                int addNumBonus = infor.Value * (playerInfor.BonusBuyGem[_id] - 1);
                                playerInfor.AddGem(addNumBonus + addNum, "MenuWatchAds");
                                EffectCurrency.Instance.IconFlow(transform.position, addNumBonus + addNum, _type);
                                shopPage._pnlQuestion.SetActive(false);
                                playerInfor.CheckBonusBuy(_id, playerInfor.BonusBuyGem, SetActiveBonusSet);
                                Tracking.LogPercentWatchedVideoDouble(true);
                            }, () => {
                                Tracking.LogPercentWatchedVideoDouble(false);
                            });


                        });
                        shopPage._btnNo.onClick.AddListener(() => {
                            //[Tracking - Position]-[ButtonPress]
                            GameTracking.Instance.TrackButtonPress("MenuWatchAds", "btn_back");

                            playerInfor.AddGem(addNum, "MenuWatchAds");
                            EffectCurrency.Instance.IconFlow(transform.position, addNum, _type);
                            shopPage._pnlQuestion.SetActive(false);
                    addNum = infor.Value * playerInfor.BonusBuyCoin[_id];
                            playerInfor.CheckBonusBuy(_id, playerInfor.BonusBuyGem, SetActiveBonusSet);
                            Tracking.LogPercentWatchedVideoDouble(false);
                        });
                        
                    }
                    else {
                        playerInfor.AddGem(addNum, this.name);
                        EffectCurrency.Instance.IconFlow(transform.position, addNum, _type);
                    }

                }
            }
            else {
                if (_type == TypeReward.Coin) {
                    if (playerInfor.BonusBuyCoin[_id] > 1) {
                        Tracking.LogIapABTesting(RemoteConfigExtra.IsVideoDoubleIap);
                    }
                    playerInfor.AddGold(addNum, this.name);
                    playerInfor.CheckBonusBuy(_id, playerInfor.BonusBuyCoin, SetActiveBonusSet);
                    
                } else {
                    if (playerInfor.BonusBuyGem[_id] > 1) {
                        Tracking.LogIapABTesting(RemoteConfigExtra.IsVideoDoubleIap);
                    }
                    addNum = infor.Value * playerInfor.BonusBuyGem[_id];
                    playerInfor.AddGem(addNum, this.name);
                    playerInfor.CheckBonusBuy(_id, playerInfor.BonusBuyGem, SetActiveBonusSet);
                }
                EffectCurrency.Instance.IconFlow(transform.position, addNum, _type);
            }
        });
        
    }
    void OnOtherClick(ShopCoinInfor infor, bool video) {
        PlayerInfo playerInfor = ManagerData.Instance.PlayerInfo;
        if (video) {
            AdsModify.ShowRewardVideo("shop", AdsType.VideoReward, this.name, "null", "null", () => {
                playerInfor.GetAllRewardType(ToAllReward(), 0, infor.Value, this.name);
                EffectCurrency.Instance.IconFlow(transform.position, infor.Value, _type);
                _cd.StartCountDown();
                SetBtnAds(DataConfig.Instance.BtnDeactiveGray, false, false);
                if (_cd.OnTick == null) {
                    _cd.OnTick += UpdateTextCountdown;
                } else {
                    _txtCd.text = _cd.GetTime();
                }
                if (_cd.OnComplete == null) {
                    _cd.OnComplete += CountdownComplete;
                }
            });
        } else {
            if (playerInfor.UseGem((int)infor.Price, "fuel", this.name)) {
                playerInfor.AddFuel(infor.Value, this.name);
                EffectCurrency.Instance.IconFlow(transform.position, infor.Value, _type);
                if (UpdateData != null) {
                    UpdateData.Invoke();
                }
            } else {
                
            }
        }
    }

    #endregion

    #region Loading

    AllRewardType ToAllReward() {
        if (_type == TypeReward.Coin) return AllRewardType.Coin;
        else if (_type == TypeReward.Gem) return AllRewardType.Gem;
        return AllRewardType.Fuel;
    }

    void UpdateGem() {
        ShopCoinInfor infor = ManagerData.Instance.ShopCoinData.HotDeal[_id];
        if (infor.BuyType != ShopCoinBuyType.Ads) {
            if (ManagerData.Instance.PlayerInfo.CheckGem(infor.Price)) {
                _btnBuyOther.GetComponent<Image>().sprite = DataConfig.Instance.BtnActiveYellow;
                _txtPriceGem.color = DataConfig.Instance.GemPriceColor;
            } else {
                _btnBuyOther.GetComponent<Image>().sprite = DataConfig.Instance.BtnDeactiveGray;
                _txtPriceGem.color = DataConfig.Instance.ColorLevelCantPlay;
            }
        }
    }

    void SetIcon(ShopCoinInfor infor) {
        _icon.sprite = infor.Icon;
        _icon.SetNativeSize();
        _icon.transform.localScale = Vector2.one * 1.5f;
    }

    void LoadSkin(ShopCoinInfor infor, Sprite frame) {
        //_bg.sprite = infor.BuyType == ShopCoinBuyType.Ads ? UIManager.Instance.CoinShopPage.AdsPanel : UIManager.Instance.CoinShopPage.NormalPanel;
        _frame.sprite = frame;
        SetIcon(infor);
    }

    void LoadEarnInfor(ShopCoinInfor infor, List<int> lsBonus, Sprite earnIcon) {
        StartCoroutine(RemoteConfigExtra.WaitUntilRemoteFetched(() => {
            if (RemoteConfigExtra.IsVideoDoubleIap) {
                _bonusTag.SetActive(false);
                _line.SetActive(false);
                _bonusInfor.Txt.gameObject.SetActive(false);
                _originInfor.Txt.text = string.Format("+{0:##,###}", infor.Value);
                _originInfor.Icon.sprite = earnIcon;
                _originInfor.Icon.SetSizeFollowHeight(53);
            } else {
                if (_id >= lsBonus.Count) {
                    _bonusTag.SetActive(false);
                } else {
                    ManagerData.Instance.PlayerInfo.GetFirstTimeBonus(_id, lsBonus, _bonusTag);
                }
                if (_type == TypeReward.Fuel) {
                    _bonusTag.SetActive(false);
                }
                _line.SetActive(_bonusTag.activeInHierarchy);
                _bonusInfor.Txt.gameObject.SetActive(_bonusTag.activeInHierarchy);
                float mulBonus = _id < playerInfor.BonusBuyCoin.Count ? playerInfor.BonusBuyCoin[_id] : 2;
                _bonusInfor.Txt.text = string.Format("+{0:##,###}", infor.Value * mulBonus);
                _originInfor.Txt.text = string.Format("+{0:##,###}", infor.Value);
                _originInfor.Icon.sprite = _bonusInfor.Icon.sprite = earnIcon;
                _originInfor.Icon.SetSizeFollowHeight(53);
                _bonusInfor.Icon.SetSizeFollowHeight(53);
            }
        }));


    }

    void LoadBtnBuy(ShopCoinInfor infor) {
        StartCoroutine(RemoteConfigExtra.WaitUntilRemoteFetched(() => {
            if (RemoteConfigExtra.IsVideoDoubleIap) {
                _btnBuyIap.onClick.RemoveAllListeners();
                _btnBuyOther.onClick.RemoveAllListeners();
                _btnPanel.onClick.RemoveAllListeners();
                bool isBuyByAds = infor.BuyType == ShopCoinBuyType.Ads;
                bool isFuel = _type == TypeReward.Fuel;
                _btnBuyIap.gameObject.SetActive(!isBuyByAds && !isFuel);
                _btnBuyOther.gameObject.SetActive(isBuyByAds || isFuel);
                _videoIcon.SetActive(isBuyByAds);
                _txtCd.gameObject.SetActive(isBuyByAds);
                _gemSet.SetActive(infor.BuyType == ShopCoinBuyType.Gem);
                _btnBuyOther.GetComponent<Image>().sprite = isBuyByAds ? DataConfig.Instance.BtnActiveBlue : DataConfig.Instance.BtnActiveYellow;
                if (isBuyByAds) {
                    AddEventBtn(_btnBuyOther, () => OnOtherClick(infor, true));
                    SetBtnAds();
                } else {
                    if (isFuel) {
                        AddEventBtn(_btnBuyOther, () => OnOtherClick(infor, false));
                        UpdateGem();
                        _txtPriceGem.text = infor.Price.ToString();
                    } else {
                        AddEventBtn(_btnBuyIap, () => OnIapClick(infor));
                        _txtPriceIap.text = Application.internetReachability != NetworkReachability.NotReachable
                            ? UnityIAP.Instance.GetLocalPrice(infor.Iap).localizedPriceString
                            : string.Format("$ {0}", infor.Price);
                    }
                }
            } else {
                _btnBuyIap.onClick.RemoveAllListeners();
                _btnBuyOther.onClick.RemoveAllListeners();
                _btnPanel.onClick.RemoveAllListeners();
                bool isBuyByAds = infor.BuyType == ShopCoinBuyType.Ads;
                bool isFuel = _type == TypeReward.Fuel;
                _btnBuyIap.gameObject.SetActive(!isBuyByAds && !isFuel);
                _btnBuyOther.gameObject.SetActive(isBuyByAds || isFuel);
                _videoIcon.SetActive(isBuyByAds);
                _txtCd.gameObject.SetActive(isBuyByAds);
                _gemSet.SetActive(infor.BuyType == ShopCoinBuyType.Gem);
                _btnBuyOther.GetComponent<Image>().sprite = isBuyByAds ? DataConfig.Instance.BtnActiveBlue : DataConfig.Instance.BtnActiveYellow;
                if (isBuyByAds) {
                    AddEventBtn(_btnBuyOther, () => OnOtherClick(infor, true));
                    SetBtnAds();
                } else {
                    if (isFuel) {
                        AddEventBtn(_btnBuyOther, () => OnOtherClick(infor, false));
                        UpdateGem();
                        _txtPriceGem.text = infor.Price.ToString();
                    } else {
                        AddEventBtn(_btnBuyIap, () => OnIapClick(infor));
                        _txtPriceIap.text = Application.internetReachability != NetworkReachability.NotReachable
                            ? UnityIAP.Instance.GetLocalPrice(infor.Iap).localizedPriceString
                            : string.Format("$ {0}", infor.Price);
                    }
                }
            }
        }));



                
    }

    void SetBtnAds(Sprite btnSkin, bool interactable, bool activeVideo) {
        _btnBuyOther.GetComponent<Image>().sprite = btnSkin;
        _btnPanel.interactable = _btnBuyOther.interactable = interactable;
        _videoIcon.SetActive(activeVideo);
        _txtCd.gameObject.SetActive(!activeVideo);
    }

    void SetBtnAds() {
        _cd.StartCountDown(false);
        if (_cd.Done) {
            SetBtnAds(DataConfig.Instance.BtnActiveBlue, true, true);
        } else {
            SetBtnAds(DataConfig.Instance.BtnDeactiveGray, false, false);
            if(_cd.OnTick == null) {
                _cd.OnTick += UpdateTextCountdown;
            }
        }
        if (_cd.OnComplete == null) {
            _cd.OnComplete += CountdownComplete;
        }
    }

    void SetActiveBonusSet(bool isActive) {
        _bonusTag.SetActive(isActive);
        _line.SetActive(isActive);
        _bonusInfor.Txt.gameObject.SetActive(isActive);
    }

    public void LoadItem(int id, TypeReward type, LocalCountDown cd, List<ShopCoinInfor> lsInfor, List<int> lsBonus,
        Sprite frame, Sprite icon) {
        _id = id;
        _type = type;
        _cd = cd;
        LoadSkin(lsInfor[_id], frame);
        LoadEarnInfor(lsInfor[_id], lsBonus, icon);
        LoadBtnBuy(lsInfor[_id]);
    }

    private void UpdateTextCountdown() {
        _txtCd.text = _cd.GetTime();
    }

    private void CountdownComplete() {
        SetBtnAds(DataConfig.Instance.BtnActiveBlue, true, true);
    }

    #endregion
}
