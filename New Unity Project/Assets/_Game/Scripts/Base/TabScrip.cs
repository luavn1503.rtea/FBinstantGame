﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabScrip : MonoBehaviour
{
    [SerializeField] private string tabName;
    [SerializeField] private Image image;
    [SerializeField] private Text textTitle;
    [SerializeField] private Sprite spriteSelect;
    [SerializeField] private Sprite spriteDeselect;

    public string TabName { get => tabName; }

    public void SetSelected()
    {
        image.sprite = spriteSelect;
        textTitle.color = DataConfig.Instance.SelectedTxtColor;
        transform.SetAsLastSibling();
    }
    public void SetDeselect()
    {
        image.sprite = spriteDeselect;
        textTitle.color = DataConfig.Instance.DeselectedTxtColor;
    }


}
