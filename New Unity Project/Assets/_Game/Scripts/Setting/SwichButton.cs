﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GameSystem.Common.Utilities;

public abstract class SliderListener: MonoBehaviour {
    [SerializeField] protected Slider sliderMusic;

    private void Reset() {
        sliderMusic = GetComponentInChildren<Slider>();

    }
    protected virtual void Start() {
        sliderMusic.onValueChanged.AddListener(OnSliderChang);
        OnSliderChang(sliderMusic.value);
    }
    protected abstract void OnSliderChang(float value);
}
public class SwichButton : SliderListener
{
    [SerializeField] protected Image sliderMusicImage;
    [SerializeField] protected TextMeshProUGUI sliderMusicConten;
    [SerializeField] protected ButtonBase swithButton;


    protected override void OnSliderChang(float value) {
        bool isOn = value >0f;
        sliderMusicConten.text = isOn ? "ON" : "OFF";
        sliderMusicImage.SetGray(!isOn);
    }
    protected override void Start() {
        base.Start();
        swithButton?.onClick.AddListener(OnWithButtonClick);
    }
    protected virtual void OnWithButtonClick() {
        sliderMusic.value = sliderMusic.value > 0f ? 0f : 1f;
    }
}
