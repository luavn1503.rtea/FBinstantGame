﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum ButtonMoveType {
    Center, Offset
}

public class ButtonMove : MonoBehaviour
{
    [SerializeField]
    private ButtonMoveType buttonMove;

    private void Awake() {
        //GetComponent<Button>().onClick.AddListener(() => {
        //    string location = UIManager.Instance.SettingPopup.CurrentNameMenu();
        //    string btnID = "btn_move";
        //    GameTracking.Instance.TrackButtonPress(location, btnID);

        //    if(buttonMove == ButtonMoveType.Center) {
        //        GameConfig.MoveMode = 0;
        //    }
        //    else {
        //        GameConfig.MoveMode = 1;
        //    }
        //    if(GameEvent.OnMoveModeChanged != null) {
        //        GameEvent.OnMoveModeChanged();
        //    }
        //    GameEvent.OnMoveModeChanged += UpDateState;
        //    UpDateState();
        //});
    }
    void UpDateState() {
        if(buttonMove == ButtonMoveType.Center) {
            transform.GetChild(0).gameObject.SetActive(GameConfig.MoveMode == 0);
            FadeBtn(GameConfig.MoveMode == 0);
        }
        else {
            transform.GetChild(0).gameObject.SetActive(GameConfig.MoveMode == 1);
            FadeBtn(GameConfig.MoveMode == 1);
        }
    }
    void FadeBtn(bool isAtive) {
        Image imgChoosed = transform.GetComponent<Image>();
        imgChoosed.color = isAtive
            ?new Color(imgChoosed.color.r, imgChoosed.color.g, imgChoosed.color.b, 1):
			 new Color(imgChoosed.color.r, imgChoosed.color.g, imgChoosed.color.b, 0.3f);

    }
    private void OnDestroy() {
        GameEvent.OnMoveModeChanged -= UpDateState;
    }
}
