﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPopup : Frame {
    //public static string
    [SerializeField] private ButtonAnimation btnRestore;
    [SerializeField] private ButtonAnimation btnTap;

    private void Start() {
        btnRestore.onClick.AddListener(ClickReStore);
        btnTap.onClick.AddListener(ClickTap);
    }
    public void ClickReStore() {

    }

    public void ClickTap() {

    }

}
