﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TabBase : MonoBehaviour {
    private int id = 0;
    private Action<TabBase> onClick = null;
    [SerializeField] private ButtonBase btnSelect = null;
    [SerializeField] private GameObject goSelected = null;
    [SerializeField] private GameObject goUnSelected = null;

    [HideInInspector] public int ID => id;

    public void Initialize(int id, Action<TabBase> onClick) {
        this.id = id;
        this.onClick = onClick;
        OnSelect(false);
        btnSelect.onClick.AddListener(ButtonSelect);
    }

    private void ButtonSelect() {
        onClick?.Invoke(this);
    }

    public void OnSelect(bool selected) {
        goSelected.SetActive(selected);
        goUnSelected.SetActive(!selected);
    }
}
