﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static EnumDefine;

public class ShopPage : Frame {

    private bool _createdItem;
    private TabBase tabSelected = null;
    private List<ShopItem> ShopItem = new List<ShopItem>();
    [SerializeField] private TextMeshProUGUI txtShopName = null;
    [SerializeField] private ResizeCoinShop _resizeShop;
    [SerializeField] private List<TabBase> listTag = null;

    [SerializeField] private ShopItem itemPrefab = null;
    [SerializeField] private RectTransform content = null;

    [Header("Panel")]
    public Sprite NormalPanel;
    //public Sprite AdsPanel;

    [Header("Frame")]
    public Sprite CoinFrame;
    public Sprite GemFrame;
    public Sprite HotDealFrame;

    [Header("Popup Y/N")]
    public ButtonAnimation _btnYes;
    public ButtonAnimation _btnNo;
    public GameObject _pnlQuestion;

    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        LoadShopItem();
        for(int i = 0; i < listTag.Count; i++) {
            listTag[i].Initialize(i, OnSelectTab);
        }
        OnSelectTab((int)ShopType.Coin);
    }

    protected override void OnShow(Action onCompleted = null, bool instant = false) {

        base.OnShow(onCompleted, instant);
    }
    private void LoadShopItem() {
        ShopItem = new List<ShopItem>();
        var items = ShopCoinData.Instance.Coins;
        for(int i = 0; i < items.Count; i++) {
            var itemCreate = itemPrefab.Spawn(content);
            itemCreate.transform.localScale = Vector3.one;
            ShopItem.Add(itemCreate);
        }

        ShopItem = new List<ShopItem>();
        var item = ShopCoinData.Instance.Gem;
        for(int i = 0; i < items.Count; i++) {
            var itemCreate = itemPrefab.Spawn(content);
            itemCreate.transform.localScale = Vector3.one;
            ShopItem.Add(itemCreate);
        }


    }

    public void CoinTabClick() {
        //string location = tabScript.TabName;
        //string btnID = "btn_goldMenu";
        //GameTracking.Instance.TrackButtonPress(location, btnID);

        //SelectTab(CoinTab, LoadCoin, true);
        //_resizeShop.SetActiveDescription(true);

    }
    public void CoinTabClickedWithoutTracking() {
        //SelectTab(CoinTab, LoadCoin, true);
        //_resizeShop.SetActiveDescription(true);
    }
    public void GemTabClicked() {

        // string location = tabScript.TabName;
        // string btnID = "btn_gemMenu";
        // GameTracking.Instance.TrackButtonPress(location, btnID);

        //SelectTab(GemTab, LoadGem, true);
        // _resizeShop.SetActiveDescription(true);

    }
    public void GemTabClickedWithoutTracking() {
        //SelectTab(GemTab, LoadGem, true);
        //_resizeShop.SetActiveDescription(true);
    }

    public void OnSelectTab(int id) {
        var tab = listTag.Find(x =>x.ID == id);
        if(tab == null) { return; }
        OnSelectTab(tab);
    }

    public void OnSelectTab(TabBase tab) {
        if(tab == null) { return; }
        if(tabSelected) {
            if(tab.ID == tabSelected.ID) {

            }
            else {
                tab.OnSelect(true);
                tabSelected.OnSelect(false);
                tabSelected = tab;
                Extention.SetText(txtShopName, ((ShopType)tab.ID).ToString().ToUpper());
            }
        }
        else {
            tab.OnSelect(true);
            tabSelected = tab;
            Extention.SetText(txtShopName, ((ShopType)tab.ID).ToString().ToUpper());
        }
    }


    void LoadCoin() {
        ShopItem.ForEach(item => item.gameObject.SetActive(true));
        for(int i = 0; i < ManagerData.Instance.ShopCoinData.Coins.Count; i++) {
            //ShopItem item = GameConfig.Instance.ItemPool(_item, ShopItem, _item.transform.parent);
            //item.LoadItem(i, TypeReward.Coin, CountDownController.Instance.CountDownFreeGold,
            //    ManagerData.Instance.ShopCoinData.Coins, ManagerData.Instance.PlayerInfo.BonusBuyCoin, CoinFrame,
            //    DataConfig.Instance.CoinSprite);
            //if(!_createdItem) {
            //    item.transform.SetAsFirstSibling();

            //}
        }
    }
    void LoadGem() {
        ShopItem.ForEach(item => item.gameObject.SetActive(true));
        for(int i = 0; i < ManagerData.Instance.ShopCoinData.Coins.Count; i++) {
            //ShopItem item = GameConfig.Instance.ItemPool(_item, ShopItem, _item.transform.parent);
            //item.LoadItem(i, TypeReward.Coin, CountDownController.Instance.CountDownFreeGem,
            //    ManagerData.Instance.ShopCoinData.Coins, ManagerData.Instance.PlayerInfo.BonusBuyCoin, GemFrame,
            //    DataConfig.Instance.CoinSprite);
            //if(!_createdItem) {
            //    item.transform.SetAsFirstSibling();

            //}
        }
    }
}






