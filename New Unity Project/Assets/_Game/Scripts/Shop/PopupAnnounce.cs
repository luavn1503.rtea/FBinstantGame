﻿using Gemmob;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PopupAnnounce : Frame {
    public GetRewardAnnounce RewardAnnounce;
    [SerializeField]
    private List<AnnounceData> _cacheAnnounce;

    public List<AnnounceData> CacheAnnounce {
        get {
            return _cacheAnnounce ?? new List<AnnounceData>();
        }
        set { _cacheAnnounce = value; }
    }
    //[Header("Customize")]
    //[Space(20)]
    //[SerializeField] private GetRewardBossBattlePopUp _getRewardBossBattlePopUp;

    void Start() {
        //_btnBack.onClick.RemoveAllListeners();
        //_btnBack.onClick.AddListener(HideToBackToPrePage);
    }

    public void ShowRewardBossBattlePopUp() {
        RewardAnnounce.gameObject.SetActive(false);

        //var giftWonModeInfo = JsonUtility.FromJson<GiftWonModeInfo>(PlayerPrefs.GetString(BossBattleController.IsWonModeKey));
        //if (giftWonModeInfo == null || !giftWonModeInfo.isWon) {
        //	gameObject.SetActive(false);
        //	//HideToBackToPrePage();
        //	return;
        //}
        //ShowPage();
        //_getRewardBossBattlePopUp.ShowBossBattleAnnounce(giftWonModeInfo, showPopupResult: HideToBackToPrePage);
    }

    public void StartAnnounce(string main = "", string description = "", Sprite mainObject = null, int number = 0, bool isWait = true, string infor = "", Sprite foreignImg = null) {
        //if(RewardAnnounce.gameObject.activeInHierarchy && isWait) {
        //    //TODO: Cache announce
        //    CacheAnnounce.Add(new AnnounceData() { Main = main, Description = description, Sprite = mainObject, Number = number, ForeignImg = foreignImg });
        //}
        //else {
        //    RewardAnnounce.StartAnnounce(main, description, mainObject, number, infor, foreignImg);
        //    RewardAnnounce.gameObject.SetActive(true);
        //    ShowPage();
        //}
    }

    /// <summary>
    /// Function for not enought currency, it have button to shop
    /// </summary>
    /// <param name="description"></param>
    /// <param name="typeLose">Type currenecy missint</param>
    /// <param name="isWait"></param>
    public void StartAnnounce(string description, TypeReward typeLose, bool isWait = true) {
        //if(RewardAnnounce.gameObject.activeInHierarchy && isWait) {
        //    //TODO: Cache announce
        //    CacheAnnounce.Add(new AnnounceData() { Description = description, TypeLose = typeLose });
        //}
        //else {
        //    RewardAnnounce.NotEnoughCurrency(description, typeLose, HideToBackToPrePage);
        //    RewardAnnounce.gameObject.SetActive(true);
        //    OnShow();
        //}
    }

    private void StartAnnounce(AnnounceData announceData) {
        if(announceData.Sprite != null || announceData.Number != 0 || string.IsNullOrEmpty(announceData.Main)) {
            StartAnnounce(announceData.Main, announceData.Description, announceData.Sprite, announceData.Number, false, "", announceData.ForeignImg);
        }
        else {
            StartAnnounce(announceData.Description, announceData.TypeLose, false);
        }
    }

    private void RemoveAll() {
        RewardAnnounce.gameObject.SetActive(false);
    }
}

[Serializable]
public class AnnounceData {
    public string Main;
    public string Description;
    public Sprite ForeignImg;
    public Sprite Sprite;
    public int Number;
    public TypeReward TypeLose;
}
