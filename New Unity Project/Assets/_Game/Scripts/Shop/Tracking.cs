﻿using System;
using Gemmob.API.Analytis;
using UnityEngine;

public class Tracking {
#region Tracking Method
    public static void LogIngame(int level, EnumDefine.ModePlay mode, int wave, bool firstWin, bool win, bool revived, int ship, int drone, int card, int item, int bomb) {
	    try {
		 //   PrefData.TriggerLevelPlayed(level);
		    Log(Category.Ingame, Events.ParameterBuilder.Create()
			    .Add("level", level)
			    .Add("mode", mode.ToString())
			    .Add("wave", wave)
			    .Add("firstWin", firstWin ? 1 : 0)
			    .Add("result", win ? 1 : 0)
			 //   .Add("entry", PrefData.TrackingPlayedTimes(level))
			    .Add("revived", revived ? 1 : 0)
			    .Add("ship", ship)
			    .Add("drone", drone)
			    .Add("card", card)
			    .Add("item", item)
			    .Add("bomb", bomb)
		    );
#if UNITY_EDITOR
		    Debug.LogFormat(
			    "Level: {0}\nModePlay: {1}\nWave: {2}\nIsWin: {3}\nIsRevive: {4}\nShipIndex: {5}\nDroneIndex: {6}\nCardIndex: {7}\nItemIndex: {8}\nBombCount: {9}\nFirstWin: {10}",
			    level, mode, wave, win, revived, ship, drone, card, item, bomb, firstWin);
#endif
		} catch (Exception e) {
#if UNITY_EDITOR
		    Debug.Log(e.Message);
#endif
	    }
    }

	public static void LogCurrency(int changeValue, EnumDefine.CurrencyType currency, ChangeCurrencyArea area, string detail = "") {
        if (changeValue == 0) return;
		try {
			Log(Category.Currency, Events.ParameterBuilder.Create()
				.Add("change", Mathf.Abs(changeValue))
				.Add("type", (int)currency)
				.Add("action", changeValue > 0 ? 0 : 1)
				.Add("button", string.Format("{0}{1}", area.ToString(), detail))
			);

			if (currency == EnumDefine.CurrencyType.Coin) {
				TrackingUserProperty(UserProperties.Coin, ManagerData.Instance.PlayerInfo.Gold.ToString());
			} else if (currency == EnumDefine.CurrencyType.Gem) {
				TrackingUserProperty(UserProperties.Gem, ManagerData.Instance.PlayerInfo.Gem.ToString());
			}
#if UNITY_EDITOR
			Debug.LogFormat("Change: {0}\nType: {1}\nAction: {2}\nButton :{3}", Mathf.Abs(changeValue), (int)currency,
				changeValue > 0 ? 0 : 1, string.Format("{0}{1}", area.ToString(), detail));
#endif
		} catch (Exception e) {
#if UNITY_EDITOR
			Debug.Log(e.Message);
#endif
		}
	}

    public static void LogRewardAds(string adsKey, bool watchSuccess) {
	    try {
		//    PrefData.TriggerAdsWatched(adsKey);
		    //PrefData.TrackingAdsWatchedTotalTimes++;
		    Log(Category.Reward_ads, Events.ParameterBuilder.Create()
			    .Add("id", adsKey)
			    .Add("result", watchSuccess ? 1 : 0)
			    //.Add("quantity", PrefData.TrackingAdsWatchedTimes(adsKey))
		    );
#if UNITY_EDITOR
		 ///   Debug.LogFormat("Id: {0}\nResult: {1}\nQuantity: {2}", adsKey, watchSuccess,
			 //   PrefData.TrackingAdsWatchedTimes(adsKey));
#endif
		} catch (Exception e) {
#if UNITY_EDITOR
		    Debug.Log(e.Message);
#endif
	    }
    }

    public static void LogIAP(string key, int saleOffPercent = 0) {
	    try {
		    Log(Category.Iap, Events.ParameterBuilder.Create()
			    .Add("id", key)
			    .Add("saleoff", saleOffPercent)
		    );
#if UNITY_EDITOR
		    Debug.LogFormat("Id: {0}\nSaleoff: {1}", key, saleOffPercent);
#endif
	    }
	    catch (Exception e) {
#if UNITY_EDITOR
		    Debug.Log(e.Message);
#endif
	    }
    }

    public static void LogMenuButton(MenuButton button) {
        //PrefData.TriggerButtonClicked(button);
        Log(Category.Menu, Events.ParameterBuilder.Create()
                                                    .Add("id", (int)button)
                                                    //.Add("quantity", PrefData.TrackingButtonClickedTimes(button))
            );
    }

	public static void LogAircraft(int aircraftId, bool isShip, int lvl) {
		try {
			Log(Category.Aircraft, Events.ParameterBuilder.Create()
				.Add("id", aircraftId)
				.Add("type", isShip ? 0 : 1)
				.Add("level", lvl));
		}
		catch (Exception e) {
#if UNITY_EDITOR
			Debug.Log(e.Message);
#endif
		}
	}

    public static void LogIapABTesting(bool isVideoIap) {
#if UNITY_EDITOR
        Debug.Log("Tracking type: "  + (isVideoIap ? "VideoDouble" : "Control Group"));
#else
        Log(Category.AbTestingIap, Events.ParameterBuilder.Create()
            .Add("type",isVideoIap ? "Control Group" : "VideoDouble"));
#endif
    }

    public static void LogPercentWatchedVideoDouble(bool isWatchedVideo) {
       // PrefData.TriggerPercentUsedVideoDouble(isWatchedVideo);
//#if UNITY_EDITOR
//        //Debug.Log("Tracking VideoDoubleIap(percent): " + PrefData.TrackingPercentUsedVideoDouble());
//#else
//        Events.Log("VideoDoubleIap", Events.ParameterBuilder.Create()
//            .Add("percent", PrefData.TrackingPercentUsedVideoDouble()));
//#endif
    }
    private static void Log(Category category, Events.ParameterBuilder paramBuilder) {
#if UNITY_EDITOR
#else
		Events.Log(category.ToString().ToLower(), paramBuilder);
#endif
    }

#endregion

#region User Properties
    public static void TrackingSession(float totalTime) {
        TrackingUserProperty(UserProperties.Sessions, totalTime.ToString());
    }

    public static void TrackingUserProperty(UserProperties key, string value) {
	    try {
		   // FirebaseService.Instance.SetUserProperty(key.ToString().ToLower(), value);
		}
	    catch (Exception e) {
#if UNITY_EDITOR
		    Debug.Log(e.Message);
#endif
	    }
    }

#endregion

#region Define Params
    public enum UserProperties {
        User_id, Sessions, Coin, Gem,
    }

    public enum Category {
        Ingame,
        Currency,
        Aircraft,
        Reward_ads,
        Iap,
        Menu,
        AbTestingIap,
    }

    public enum ItemType {
        Levelup3,
        Levelup5,
        Levelup10,
        Bomb,
    }

#endregion
}
