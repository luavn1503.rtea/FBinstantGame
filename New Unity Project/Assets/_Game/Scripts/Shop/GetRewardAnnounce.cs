﻿using System;
using Gemmob.Common;
using UnityEngine;
using UnityEngine.UI;

public class GetRewardAnnounce : MonoBehaviour {
	public Text MainAnnounce;
	public Image MainObject;
    public Image ForeignImg;
	public GameObject BoundImage;
	public Text TextInformation;
	public Text Number;

	public ButtonAnimation ShopButton;
	public ButtonAnimation ExitButton;
	public ButtonAnimation BackButton;

	void Start() {

	}

	void OnBackBtnClick() {

	}

	public void StartAnnounce(string main = "", string description = "", Sprite mainObject = null, int number = 0,
		string infor = "", Sprite foreignImg = null) {
		RemoveAll();

		MainObject.sprite = mainObject;
		BoundImage.gameObject.SetActive(mainObject != null);

		TextInformation.text = description;
		TextInformation.gameObject.SetActive(String.Compare(string.Empty, description, StringComparison.Ordinal) != 0);

		MainAnnounce.text = main;
		MainAnnounce.gameObject.SetActive(String.Compare(string.Empty, main, StringComparison.Ordinal) != 0);

		Number.text = "x" + number;
		Number.gameObject.SetActive(number != 0);

		Text txtInfor = MainObject.GetComponentInChildren<Text>();
		txtInfor.text = infor;
		MainObject.SetSizeFollowWidth(String.Compare(string.Empty, infor, StringComparison.Ordinal) != 0 ? 140 : 180);

        if(foreignImg != null) {
            ForeignImg.sprite = foreignImg;
            ForeignImg.gameObject.SetActive(true);
        }
        else {
            ForeignImg.gameObject.SetActive(false);
        }

		BackButton.gameObject.SetActive(true);
	}

	public void NotEnoughCurrency(string description, TypeReward typeLose, Action pageBaseBackClicked) {
		RemoveAll();

		ExitButton.gameObject.SetActive(true);
		ShopButton.gameObject.SetActive(true);

		ShopButton.onClick.RemoveAllListeners();
		ShopButton.onClick.AddListener(pageBaseBackClicked.Invoke);
		ShopButton.onClick.AddListener(()=> { UIHomeManager.Instance.Show<ShopPage>(); });
		ShopButton.onClick.AddListener(() => {
			switch (typeLose) {
				case TypeReward.Gem: {

                    break;
                }
				case TypeReward.Fuel: {

                    break;
                }
				default: {

                    break;
                }
			}
		});

		description = !Config.IsIapEnable ? "Not enough fund" : description;
		TextInformation.text = description;
		TextInformation.gameObject.SetActive(String.Compare(string.Empty, description, StringComparison.Ordinal) != 0);
	}

	private void RemoveAll() {
		BackButton.gameObject.SetActive(false);
		ExitButton.gameObject.SetActive(false);
		MainAnnounce.gameObject.SetActive(false);
		BoundImage.SetActive(false);
		TextInformation.gameObject.SetActive(false);
		Number.gameObject.SetActive(false);
		ShopButton.gameObject.SetActive(false);
	}
}
