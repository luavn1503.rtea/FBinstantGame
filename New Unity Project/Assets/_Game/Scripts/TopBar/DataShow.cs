﻿using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;
using UnityEngine.UI;

public class DataShow : MonoBehaviour
{
    [SerializeField] private DataType dataType;
    public Text textShow;
    public GameObject countDown;

    private void Reset() {
        textShow = GetComponent<Text>();
    }
    private void OnEnable() {
        switch(dataType) {
            case DataType.Fuel:
                //if(ManagerData.Instance.PlayerInfo.PlayerProfile.IsVip) {
                //    textShow.text = "";
                //    countDown.SetActive(false);
                //}
                //else {
                //    GameEvent.OnFuelChanged += SetData;
                //    SetData(ManagerData.Instance.PlayerInfo.Fuel, PlayerInfo.MaximumFuel);

                //}
                break;
            case DataType.Coin:
                GameEvent.OnGoldChanged += SetData;
                SetData(ManagerData.Instance.PlayerInfo.Gold);
                break;
            case DataType.Gem:
                GameEvent.OnGemChanged += SetData;
                SetData(ManagerData.Instance.PlayerInfo.Gem);
                break;
        }
    }
    private void OnDisable() {
        GameEvent.OnFuelChanged -= SetData;
        GameEvent.OnGoldChanged -= SetData;
        GameEvent.OnGemChanged -= SetData;
    }
    void SetData(int data) {
        textShow.DOText(data.ToString(), .5f, false, ScrambleMode.Numerals)
            .SetEase(Ease.Linear);
    }
    void SetData(int data, int maxData) {
        if(dataType == DataType.Fuel && ManagerData.Instance.PlayerInfo.PlayerProfile.IsVip) {
            return;
        }
        textShow.text = data + "/" + maxData;
    }
    public void SetTxtFuel() {
        if(ManagerData.Instance.PlayerInfo.PlayerProfile.IsVip) {
            return;
        }
        GameEvent.OnFuelChanged += SetData;
        SetData(ManagerData.Instance.PlayerInfo.Fuel, PlayerInfo.MaximumFuel);
    }
}
public enum DataType {
    Fuel,
    Coin,
    Gem
}
