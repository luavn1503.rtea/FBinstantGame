﻿using System.Collections.Generic;
using UnityEngine;

public static class Yielders
{
	public static WaitForSeconds Get(float seconds)
	{
		WaitForSeconds value;
		if (!TimeInterval.TryGetValue(seconds, out value)) {
		    value = new WaitForSeconds(seconds);
		    TimeInterval.Add(seconds, value);
		}

		return value;
	}

	private static readonly Dictionary<float, WaitForSeconds> TimeInterval = new Dictionary<float, WaitForSeconds>(100);

	public static readonly WaitForEndOfFrame EndOfFrame = new WaitForEndOfFrame();

	public static readonly WaitForFixedUpdate FixedUpdate = new WaitForFixedUpdate();
}