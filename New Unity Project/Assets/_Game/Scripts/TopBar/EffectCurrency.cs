﻿using System;
using System.ComponentModel;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class EffectCurrency : MonoBehaviour {
	public static EffectCurrency Instance;

	[SerializeField] private Transform _coinTrans;
	[SerializeField] private Transform _gemTrans;
	[SerializeField] private Transform _fuelTrans;

	[Header("Config")]
	[SerializeField] private float _alpha;
	[SerializeField] private float _duration = .5f;
	[SerializeField] private float _distance = -50f;

	[Header("Parameter time")]
	[SerializeField] private float _radius;
	[SerializeField] private float _sizeScaleOut = 1.1f;
	[SerializeField] private float _timeOut = 2f;
	[SerializeField] private float _timeTo = 5f;
	[SerializeField] private float _timeBlow = .2f;

	[Header("Efx")]
	[SerializeField] private ParticleSystem _coinEfx;
	[SerializeField] private ParticleSystem _gemEfx;

	[Header("Show")]
	public Text Fuel;
	public Text Gem;
	public Text Coin;

	public Transform Icon;

	[SerializeField] private int[] _numberUse = { 3, 10, 15, 20, 30, 50, 100 };
	void Awake() {
		Instance = this;
	}

#if UNITY_EDITOR
	void Update() {
		if (Input.GetKeyDown(KeyCode.A))
			IconFlow(Vector3.zero, 30, TypeReward.Gem);
		if (Input.GetKeyDown(KeyCode.S))
			IconFlow(Vector3.zero, 300, TypeReward.Gem);
		if (Input.GetKeyDown(KeyCode.D))
			IconFlow(Vector3.zero, 100, TypeReward.Coin);
		if (Input.GetKeyDown(KeyCode.F))
			IconFlow(Vector3.zero, 3000, TypeReward.Coin);
		if (Input.GetKeyDown(KeyCode.Q))
			IconFlow(Vector3.zero, 30000, TypeReward.Coin);
		if (Input.GetKeyDown(KeyCode.W))
			IconFlow(Vector3.zero, 2, TypeReward.Fuel);
		if (Input.GetKeyDown(KeyCode.E))
			IconFlow(Vector3.zero, 10, TypeReward.Fuel);
		if (Input.GetKeyDown(KeyCode.R))
			IconFlow(Vector3.zero, 30, TypeReward.Fuel);
	}
#endif

	#region Text flow
	public void CoinJump(int number) {
		if (number != 0)
			SetUp(Coin, number);
	}

	public void GemJump(int number) {
		if (number != 0)
			SetUp(Gem, number);
	}

	public void FuelJump(int number) {
		if (number != 0)
			SetUp(Fuel, number);
	}

	private void SetUp(Text textShow, int info) {
		if (info < 0)
			return;

		textShow.DOKill(true);
		textShow.transform.DOKill(true);
		textShow.text = info > 0 ? "+ " + info : info.ToString();
		Vector3 currentPosition = textShow.transform.localPosition;

		textShow.gameObject.SetActive(true);

		textShow.transform.DOLocalMoveY(_distance, _duration).SetRelative(true).onComplete +=
			() => {
				textShow.transform.localPosition = currentPosition;
				textShow.gameObject.SetActive(false);
			};

		textShow.DOFade(_alpha, _duration).onComplete += () => {
			Color c = textShow.color;
			c.a = 1;
			textShow.color = c;
		};
	}

	#endregion

	#region Icon flow
	public void IconFlow(Vector3 startPosition, int number, TypeReward typeCurrency) {
		var config = DataConfig.Instance;
		Sprite icon = typeCurrency == TypeReward.Coin ? config.CoinSprite :
			typeCurrency == TypeReward.Fuel ? config.FuelSprite :
			typeCurrency == TypeReward.Gem ? config.GemSprite : null;

		if (icon == null)
			return;

		Vector3 positionTo = typeCurrency == TypeReward.Coin ? _coinTrans.position :
			typeCurrency == TypeReward.Fuel ? _fuelTrans.position : _gemTrans.position;

		//var gameData = GameData.Instance.ShopCoinData;
		//var data = typeCurrency == TypeReward.Coin ? gameData.Coins : typeCurrency == TypeReward.Fuel ? gameData.Fule : gameData.Gem;
		//int numberUse = 0;

		//if (number < data[0].Value)
		//	numberUse = _numberUse[0];
		//else
		//	for (int i = 1; i < data.Count; i++)
		//		if (number > data[i - 1].Value)
		//			numberUse = _numberUse[i];

		//if (numberUse == 0)
		//	numberUse = _numberUse[_numberUse.Length - 1];

		//for (int i = 0; i < numberUse; i++) {
		//	Transform item = ImageEfxPool.Instance.Pool.Spawn(Icon).transform;
		//	item.position = startPosition;
		//	//item.localScale = Vector2.one * .7f;
		//	item.localScale = Vector2.one * 1.2f;
		//	Vector2 posOut = Vector2.zero;
		//	posOut.x = UnityEngine.Random.Range(-1f, 1f);
		//	posOut.y = UnityEngine.Random.Range(-1f, 1f);
		//	posOut = posOut.normalized * _radius;

		//	item.DOMove(posOut, UnityEngine.Random.Range(_timeOut / 2, _timeOut)).SetEase(Ease.InOutQuart).SetRelative(true).OnComplete(() => {
		//		item.DOMove(positionTo, _timeTo).SetEase(Ease.Linear).OnComplete(() => {
		//			if (typeCurrency == TypeReward.Coin && !_coinEfx.isPlaying)
		//				_coinEfx.Play();
		//			else if (typeCurrency == TypeReward.Gem && !_gemEfx.isPlaying)
		//				_gemEfx.Play();
		//			item.DOScale(_sizeScaleOut, _timeBlow).OnComplete(() => item.DOScale(Vector3.one, _timeBlow).OnComplete(() => ImageEfxPool.Instance.Pool.Despawn(item)));
		//		});
		//	});

		//	var img = item.GetComponent<Image>();
		//	img.sprite = icon;
		//	img.SetNativeSize();
		//}
	}
	#endregion
}
