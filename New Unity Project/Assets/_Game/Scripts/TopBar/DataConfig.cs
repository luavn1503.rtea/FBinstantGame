﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class DataConfig : MonoBehaviour {
	public static DataConfig Instance;
	public Sprite BombSprite;

	[Header("Color text")]
	public Color GemColor;
	public Color GemPriceColor;
	public Color GoldColor;
	public Color FuelColor;
	public Color VideoColor;
	public Color DolarColor;
	public Color WarningColor;
	public Color AlphaZeroColor;
	public Color ColorLevelCantPlay;
	public Color SelectedTxtColor;
	public Color DeselectedTxtColor;
	public Color BlueTxtColor;

	[Header("Button")]
	public Sprite BtnActiveBlue;
	public Sprite BtnActiveYellow;
	public Sprite BtnDeactiveGray;
	public Sprite BtnDeactiveDark;
	public Sprite BtnItalicYellow;
	public Sprite BtnItalicGray;

	[Header("Box Sprite")]
	public Sprite BadBoxSprite;
	public Sprite NormalBoxSprite;
	public Sprite GoodBoxSprite;

	[Header("Currency Icon")]
	public Sprite GemSprite;
	public Sprite CoinSprite;
	public Sprite FuelSprite;
	public Sprite AdsSprite;

	[Header("Bound card")]
	public Sprite[] BoundCards;
	public Color[] BoundCardColor;

	void Awake() {
		Instance = this;
	}

	[ContextMenu("Change")]
	public void ChangeToWhite() {
		GemColor = Color.white;
		GoldColor = Color.white;
		FuelColor = Color.white;
		VideoColor = Color.white;
		DolarColor = Color.white;
	}
}
