﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Drone5BulletLaser : ShipBulletLaser {
    private Transform enemyTarget = null;
    private Vector2 direction = Vector2.up;
    private float timeNextTarget = 0f;
    private bool statusOn = true;
    private Tween tween;

    public override ShipBulletBase Initialized() {
        enemyTarget = null;
        direction = Vector2.up;
        timeNextTarget = Time.time;
        return base.Initialized();
    }

    protected override void FixedUpdate() {
        if(timeNextTarget < Time.time) {
            enemyTarget = ManagerEnemies.Instance.GetEnemyClose();
            if(enemyTarget != null && Mathf.Abs(enemyTarget.position.x) < maxSize.x && Mathf.Abs(enemyTarget.position.y) <maxSize.y) {
                direction = (enemyTarget.position - transform.position).normalized;
                timeNextTarget += 1f;
                statusOn = true;
                tween = DOVirtual.DelayedCall(0.1f,()=>{ TurnOffLaser(); });
            }
        }
        CreateLaser();
    }

    private void TurnOffLaser() {
        statusOn = false;
        obInside.Clear();
        lineRenderer.SetPosition(1, transform.position);
    }


    protected override void CreateLaser() {
        var origin = transform.position;
        lineRenderer.positionCount = 2;
        lineRenderer.startWidth = startWidth;
        lineRenderer.SetPosition(0, origin);
        if(statusOn) {
            Vector2 startBox = lineRenderer.transform.position;
            Vector2 sizeBox = new Vector2(startWidth * 0.7f, 0.1f);
            var hit = Physics2D.BoxCast(startBox, sizeBox, 0, direction, 15f, layerDetect);
            if(hit && hit.transform.CompareTag(TagDefine.Enemy)) {
                var hitPoint = hit.point;
                efHit.position = hitPoint;
                lineRenderer.SetPosition(1, hitPoint);
                var idInstance = hit.transform.GetInstanceID();
                var exist = obInside.TryGetValue(idInstance, out var ieDamagePerSec);
                if(!exist) {
                    var obj = hit.collider.GetComponent<ITakeDamage>();
                    if(obj != null) {
                        ieDamagePerSec = TimeDamagePerSeconds(obj);
                        obInside.Add(idInstance, ieDamagePerSec);
                        StartCoroutine(ieDamagePerSec);
                    }
                }
            }
        }
        else {
            efHit.position = origin;
            lineRenderer.SetPosition(1, origin);
        }
    }

    public override void Remove() {
        tween.Kill();
        base.Remove();
    }
}
