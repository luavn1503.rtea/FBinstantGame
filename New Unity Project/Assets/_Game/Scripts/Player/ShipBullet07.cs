﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBullet07 : ShipBulletNormal {
    // Start is called before the first frame update

    [Header("Move Sin")]
    [SerializeField] private float speedAxisX = 0.2f;
    [SerializeField] private float distanceUpDown = 1f;
    [SerializeField] private Animator animator;
    private Vector2 startPosition;

    public override ShipBulletBase Initialized() {
        startPosition = transform.position;
        return base.Initialized();
    }
    // Update is called once per frame
    protected override void Update() {
        Vector3 mov = new Vector3 (transform.position.x + speedAxisX *Time.deltaTime, speedMovement * Time.deltaTime + transform.position.y,0);
        transform.position = mov;
        if(transform.position.x > startPosition.x + distanceUpDown) {
            speedAxisX = -Mathf.Abs(speedAxisX);
        }
        else if(transform.position.x < startPosition.x - distanceUpDown) {
            speedAxisX = Mathf.Abs(speedAxisX);
        }
        CheckOutScene();
    }
}
