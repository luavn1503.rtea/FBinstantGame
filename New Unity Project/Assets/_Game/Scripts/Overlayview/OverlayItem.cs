﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OverlayItem : MonoBehaviour {
    private BottomOverlayViewer overlayViewer = null;
    public string btnID;
    public string strName;
    public RectTransform myRect = null;
    public ButtonAnimation MyButton;
    public Image imgBG;
    public Image imgIcon;
    public TextMeshProUGUI txtName;
    [Header("Sprite")]
    public Sprite sprActive;
    public Sprite sprDeActive;
    [HideInInspector] public int idIndex;
    public void Initialize(BottomOverlayViewer overlayViewer, int idIndex) {
        this.idIndex = idIndex;
        this.overlayViewer = overlayViewer;
        txtName.text = strName;
        MyButton.onClick.AddListener(OnClick);
    }

    private void OnClick() {
        overlayViewer.OnClickButton(idIndex);
    }

    public void OnSelect(Sprite sprBG, int fontSize, Vector3 incsActiveScale, float timeScale, float highActiveIcon) {
        myRect.DOKill();
        imgIcon.transform.DOKill();

        imgBG.sprite = sprBG;
        txtName.fontSize = fontSize;
        imgIcon.sprite = sprActive;
        imgIcon.SetNativeSize();
        myRect.DOSizeDelta(incsActiveScale, timeScale);
        Vector3 targetPosIcon = Vector3.zero + Vector3.up * highActiveIcon;
        Vector3 targetPosIcon2 = targetPosIcon - Vector3.up * 10f;
        imgIcon.transform.DOLocalMove(targetPosIcon, timeScale).OnComplete(() => {
            imgIcon.transform.DOLocalMove(targetPosIcon2, timeScale * 0.3f);
        });
    }

    public void OnUnSelect(Sprite sprBG, int fontSize, Vector3 decsDeactiveValue, float timeScale) {
        myRect.DOKill();
        imgIcon.transform.DOKill();

        imgBG.sprite = sprBG;
        txtName.fontSize = fontSize;
        imgIcon.sprite = sprActive;
        imgIcon.SetNativeSize();
        imgIcon.transform.localPosition = Vector2.zero;
        myRect.DOSizeDelta(decsDeactiveValue, timeScale);
    }
}
