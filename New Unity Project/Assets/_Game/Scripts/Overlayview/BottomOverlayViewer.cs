﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Gemmob;
using UnityEngine;
using UnityEngine.UI;

public class BottomOverlayViewer : MonoBehaviour
{
    private List<RectTransform> _pageList = new List<RectTransform>();
    private Vector3 _btaOriginScale;
    private Vector3 _incsActiveScale;
    private Vector3 _decsDeactiveValue;
    private int _currentIndexPage = -1;

    [SerializeField] private List<OverlayItem> btaList;
    [Header("---------------")]
    [Space(10)]
    [SerializeField] private Sprite _bgDeactive;
    [SerializeField] private Sprite _bgActive;
    [Range(1f, 2f)]
    [SerializeField] private float _btaActiveScaleIncs = 1.2f;

    [SerializeField] private float _timeScale = 0.3f;
    [SerializeField] private float _highActiveIcon = 60f;

    [SerializeField] private int _pixelNumberTransition = 1080;
    [SerializeField] private float _durationTransition = 1;
    [SerializeField] private Ease _kindOfEaseTransition;

    private void Awake()
    {

    }
    private void Start()
    {
        AssignVariable();
        for (int i = 0; i < btaList.Count; i++)
        {
            btaList[i].Initialize(this, i);
        }
        OnClickButton(btaList.FindIndex(x => x.btnID == "btnHome"));
    }
    public void BackToHome(Frame currentPage)
    {
        _currentIndexPage = _pageList.IndexOf(currentPage.GetComponent<RectTransform>());
        OnClickButton(btaList.FindIndex(x => x.btnID == "btnHome"));
    }

    #region ChangePage
    private Tween callHidePage = null;
    private void OnChangePage(int indexCurrentPage, int indexNextPage, float durationTransition, bool useTracking)
    {
        if (indexCurrentPage == -1)
            return;
        if (_pageList == null)
            return;
        if (_pageList.Count > 0)
        {
            _pageList[indexNextPage].GetComponent<Frame>().Show();
        }
        if (callHidePage != null)
        {
            callHidePage.Kill(true);
        }
        callHidePage = DOVirtual.DelayedCall(durationTransition, () =>
        {

        }).OnComplete(() =>
        {
            _pageList[indexCurrentPage].GetComponent<Frame>().Hide();
            callHidePage = null;
        });
    }

    #endregion


    private void AssignVariable()
    {
        _pageList = new List<RectTransform>() {
           // UIHomeManager.Instance.GetFrame<StoragePage>().GetComponent<RectTransform>(),
            UIHomeManager.Instance.GetFrame<MissionPage>().GetComponent<RectTransform>(),
            UIHomeManager.Instance.GetFrame<HomePage>().GetComponent<RectTransform>(),
            //UIHomeManager.Instance.GetFrame<SpinPage>().GetComponent<RectTransform>(),
            UIHomeManager.Instance.GetFrame<ShopPage>().GetComponent<RectTransform>()
        };

        ScaleByScreen();

        _btaOriginScale = btaList[0].myRect.sizeDelta;
        float incsActiveValue = _btaOriginScale.x * _btaActiveScaleIncs;
        float decsDeactiveValue = _btaOriginScale.x - (incsActiveValue - _btaOriginScale.x) / (btaList.Count - 1);
        _incsActiveScale = new Vector3(incsActiveValue, _btaOriginScale.y, _btaOriginScale.z);
        _decsDeactiveValue = new Vector3(decsDeactiveValue, _btaOriginScale.y, _btaOriginScale.z);
    }

    private void ScaleByScreen()
    {
        Vector2 resolutionGeneral = MainCanvasScaler.Instance.main.referenceResolution;
        Vector2 currentResolution = new Vector2(Screen.width, Screen.height);
        float matchCanvasScale = MainCanvasScaler.Instance.main.matchWidthOrHeight;
        if (matchCanvasScale == 0)
        { // max theo height
            float ratioWidth = resolutionGeneral.x / Screen.width;
            currentResolution = new Vector2(resolutionGeneral.x, Screen.height * ratioWidth);
        }
        else if (matchCanvasScale == 1)
        { // max theo width
            float ratioHeight = resolutionGeneral.y / Screen.height;
            currentResolution = new Vector2(Screen.width * ratioHeight, resolutionGeneral.y);
        }

        for (int i = 0; i < btaList.Count; i++)
        {
            var sizeDelta = btaList[i].myRect.sizeDelta;
            btaList[i].myRect.sizeDelta = new Vector2(currentResolution.x / btaList.Count, sizeDelta.y);
        }
    }

    public void OnClickButton(int idIndex, bool useTracking = false)
    {
        ScaleByScreen();
        for (int i = 0; i < btaList.Count; i++)
        {
            if (i == idIndex) { continue; }
            btaList[i].OnUnSelect(_bgDeactive, 36, _decsDeactiveValue, _timeScale);
        }
        btaList[idIndex].OnSelect(_bgActive, 46, _incsActiveScale, _timeScale, _highActiveIcon);


        //ShowNotiInButton(btaList[nextIndexActive], false);
        if (_currentIndexPage != -1)
        {
            var direction = idIndex - _currentIndexPage;
            if (direction > 0)
                direction = 1;
            else if (direction < 0)
                direction = -1;
            else
                return;

            for (int i = 0; i < _pageList.Count; i++)
            {
                var rectTransform = _pageList[i].GetComponent<RectTransform>();
                rectTransform.DOKill();
                rectTransform.anchoredPosition = Vector3.zero;
            }
            var currentRect = _pageList[_currentIndexPage].GetComponent<RectTransform>();
            var nextRect = _pageList[idIndex].GetComponent<RectTransform>();
            Vector2 currentPageAnchorPos = Vector2.right * _pixelNumberTransition * -direction;
            Vector2 nextPageAnchorPos = Vector2.zero;

            nextRect.anchoredPosition = Vector2.right * _pixelNumberTransition * direction;
            DoTranstitionAnchorPage(currentRect, currentPageAnchorPos, _durationTransition, _kindOfEaseTransition);
            DoTranstitionAnchorPage(nextRect, nextPageAnchorPos, _durationTransition, _kindOfEaseTransition);
        }

        //if (_currentIndexPage == btaList.IndexOf(_btaHome)) UIManagerStatic.PageStack.Pop(); // Pop home
        OnChangePage(_currentIndexPage, idIndex, _durationTransition, useTracking);
        _currentIndexPage = idIndex;
    }


    private void DoTranstitionAnchorPage(RectTransform currentPage, Vector2 targetAnchorPos, float duration, Ease kindEase)
    {
        currentPage.DOAnchorPos(targetAnchorPos, duration).SetEase(kindEase);
    }

    private string _notiKey = "INotify";


    [Serializable]
    public class ButtonInformation
    {
        public string btnID;
        public int IndexTransitionEffect;
        public ButtonAnimation MyButton;
        public Sprite sprActive;
        public Sprite sprDeActive;
        public Image imgBGs;
        public Image imgContents;
        public Text txtContentsActive;
        public Text txtContentsDeactive;

    }
}
