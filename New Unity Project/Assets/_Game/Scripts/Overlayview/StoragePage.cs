﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
//using UnityEditorInternal.VersionControl;
using UnityEngine;
using UnityEngine.UI;

public class StoragePage : Frame {
    private List<StorageItem> listItem;
    [SerializeField] private RectTransform contents;
    [SerializeField] private StorageItem itemPrefab;
    [SerializeField] private ButtonAnimation btnClose;

    private void Start() {
        CreateItem(32);
    }


    void CreateItem(int isCount) {
        listItem = new List<StorageItem>();
        for(int i = 0; i < isCount; i++) {
            StorageItem item = itemPrefab.Spawn(contents);
            item.Initialize(this);
            listItem.Add(item);
        }
    }
    //public 
}
