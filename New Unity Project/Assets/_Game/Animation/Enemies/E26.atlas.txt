
E26.png
size: 128,128
format: RGBA8888
filter: Linear,Linear
repeat: none
Body
  rotate: false
  xy: 2, 48
  size: 52, 67
  orig: 52, 67
  offset: 0, 0
  index: -1
L_Wing_01
  rotate: true
  xy: 56, 71
  size: 21, 69
  orig: 21, 69
  offset: 0, 0
  index: -1
L_Wing_11
  rotate: false
  xy: 56, 49
  size: 25, 20
  orig: 25, 20
  offset: 0, 0
  index: -1
L_Wing_21
  rotate: true
  xy: 2, 25
  size: 21, 83
  orig: 21, 83
  offset: 0, 0
  index: -1
R_Wing_01
  rotate: true
  xy: 56, 94
  size: 21, 70
  orig: 21, 70
  offset: 0, 0
  index: -1
R_Wing_11
  rotate: false
  xy: 83, 49
  size: 25, 20
  orig: 25, 20
  offset: 0, 0
  index: -1
R_Wing_21
  rotate: true
  xy: 2, 2
  size: 21, 84
  orig: 21, 84
  offset: 0, 0
  index: -1
