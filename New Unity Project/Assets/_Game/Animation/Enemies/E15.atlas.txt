
E15.png
size: 256,64
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 2, 8
  size: 55, 54
  orig: 55, 54
  offset: 0, 0
  index: -1
10
  rotate: false
  xy: 151, 26
  size: 40, 36
  orig: 40, 36
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 59, 28
  size: 48, 34
  orig: 48, 34
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 232, 39
  size: 22, 23
  orig: 22, 23
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 123, 3
  size: 17, 21
  orig: 17, 21
  offset: 0, 0
  index: -1
5
  rotate: true
  xy: 98, 2
  size: 22, 23
  orig: 22, 23
  offset: 0, 0
  index: -1
6
  rotate: true
  xy: 193, 26
  size: 18, 21
  orig: 18, 21
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 109, 26
  size: 40, 36
  orig: 40, 36
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 59, 10
  size: 37, 16
  orig: 37, 16
  offset: 0, 0
  index: -1
9
  rotate: false
  xy: 193, 46
  size: 37, 16
  orig: 37, 16
  offset: 0, 0
  index: -1
