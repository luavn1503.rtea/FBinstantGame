
E49.png
size: 256,128
format: RGBA8888
filter: Linear,Linear
repeat: none
Belt
  rotate: true
  xy: 91, 2
  size: 46, 72
  orig: 46, 72
  offset: 0, 0
  index: -1
Body
  rotate: true
  xy: 91, 50
  size: 70, 69
  orig: 70, 69
  offset: 0, 0
  index: -1
Head
  rotate: false
  xy: 2, 2
  size: 87, 118
  orig: 87, 118
  offset: 0, 0
  index: -1
foot_L
  rotate: true
  xy: 226, 32
  size: 20, 28
  orig: 20, 28
  offset: 0, 0
  index: -1
foot_R
  rotate: true
  xy: 226, 10
  size: 20, 28
  orig: 20, 28
  offset: 0, 0
  index: -1
glasses
  rotate: false
  xy: 165, 12
  size: 59, 15
  orig: 59, 15
  offset: 0, 0
  index: -1
gunL
  rotate: true
  xy: 162, 88
  size: 32, 80
  orig: 32, 80
  offset: 0, 0
  index: -1
gunR
  rotate: true
  xy: 162, 54
  size: 32, 80
  orig: 32, 80
  offset: 0, 0
  index: -1
nose
  rotate: true
  xy: 165, 29
  size: 23, 59
  orig: 23, 59
  offset: 0, 0
  index: -1
