
E52.png
size: 512,128
format: RGBA8888
filter: Linear,Linear
repeat: none
Body
  rotate: false
  xy: 2, 9
  size: 74, 108
  orig: 74, 108
  offset: 0, 0
  index: -1
Head
  rotate: false
  xy: 336, 71
  size: 67, 46
  orig: 67, 46
  offset: 0, 0
  index: -1
L_Leg_01
  rotate: false
  xy: 405, 68
  size: 31, 49
  orig: 31, 49
  offset: 0, 0
  index: -1
L_Leg_02
  rotate: false
  xy: 471, 75
  size: 31, 42
  orig: 31, 42
  offset: 0, 0
  index: -1
L_Wing_01
  rotate: false
  xy: 78, 46
  size: 81, 71
  orig: 81, 71
  offset: 0, 0
  index: -1
L_Wing_11
  rotate: false
  xy: 244, 36
  size: 44, 81
  orig: 44, 81
  offset: 0, 0
  index: -1
R_Leg_01
  rotate: false
  xy: 438, 68
  size: 31, 49
  orig: 31, 49
  offset: 0, 0
  index: -1
R_Leg_11
  rotate: false
  xy: 78, 2
  size: 31, 42
  orig: 31, 42
  offset: 0, 0
  index: -1
R_Wing_01
  rotate: false
  xy: 161, 46
  size: 81, 71
  orig: 81, 71
  offset: 0, 0
  index: -1
R_Wing_11
  rotate: false
  xy: 290, 36
  size: 44, 81
  orig: 44, 81
  offset: 0, 0
  index: -1
