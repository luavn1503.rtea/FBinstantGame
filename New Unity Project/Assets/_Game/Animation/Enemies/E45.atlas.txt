
E45.png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Body
  rotate: false
  xy: 2, 90
  size: 190, 141
  orig: 190, 141
  offset: 0, 0
  index: -1
Head
  rotate: false
  xy: 307, 146
  size: 96, 85
  orig: 96, 85
  offset: 0, 0
  index: -1
L
  rotate: true
  xy: 194, 109
  size: 33, 62
  orig: 33, 62
  offset: 0, 0
  index: -1
L_Arm
  rotate: false
  xy: 116, 21
  size: 56, 67
  orig: 56, 67
  offset: 0, 0
  index: -1
L_Hand
  rotate: false
  xy: 2, 2
  size: 112, 86
  orig: 112, 86
  offset: 0, 0
  index: -1
R
  rotate: false
  xy: 463, 169
  size: 33, 62
  orig: 33, 62
  offset: 0, 0
  index: -1
R_Arm
  rotate: false
  xy: 405, 164
  size: 56, 67
  orig: 56, 67
  offset: 0, 0
  index: -1
R_Hand
  rotate: false
  xy: 194, 144
  size: 111, 87
  orig: 111, 87
  offset: 0, 0
  index: -1
