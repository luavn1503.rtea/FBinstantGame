
E18.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
10
  rotate: true
  xy: 979, 216
  size: 34, 43
  orig: 34, 43
  offset: 0, 0
  index: -1
11
  rotate: false
  xy: 460, 106
  size: 25, 24
  orig: 25, 24
  offset: 0, 0
  index: -1
12
  rotate: false
  xy: 907, 239
  size: 67, 81
  orig: 67, 81
  offset: 0, 0
  index: -1
13
  rotate: true
  xy: 284, 2
  size: 35, 39
  orig: 35, 39
  offset: 0, 0
  index: -1
14
  rotate: false
  xy: 420, 9
  size: 20, 28
  orig: 20, 28
  offset: 0, 0
  index: -1
15
  rotate: false
  xy: 154, 23
  size: 90, 81
  orig: 90, 81
  offset: 0, 0
  index: -1
16
  rotate: true
  xy: 335, 281
  size: 116, 98
  orig: 116, 98
  offset: 0, 0
  index: -1
17
  rotate: false
  xy: 505, 126
  size: 38, 34
  orig: 38, 34
  offset: 0, 0
  index: -1
18
  rotate: false
  xy: 246, 2
  size: 36, 50
  orig: 36, 50
  offset: 0, 0
  index: -1
18a
  rotate: false
  xy: 386, 39
  size: 56, 58
  orig: 56, 58
  offset: 0, 0
  index: -1
19
  rotate: false
  xy: 2, 8
  size: 150, 176
  orig: 150, 176
  offset: 0, 0
  index: -1
20
  rotate: true
  xy: 586, 364
  size: 33, 94
  orig: 33, 94
  offset: 0, 0
  index: -1
21
  rotate: true
  xy: 950, 323
  size: 74, 59
  orig: 74, 59
  offset: 0, 0
  index: -1
22
  rotate: true
  xy: 976, 252
  size: 69, 29
  orig: 69, 29
  offset: 0, 0
  index: -1
23
  rotate: false
  xy: 487, 119
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
24
  rotate: false
  xy: 821, 166
  size: 77, 77
  orig: 77, 77
  offset: 0, 0
  index: -1
25
  rotate: false
  xy: 970, 415
  size: 40, 95
  orig: 40, 95
  offset: 0, 0
  index: -1
26
  rotate: true
  xy: 682, 364
  size: 33, 94
  orig: 33, 94
  offset: 0, 0
  index: -1
27
  rotate: false
  xy: 384, 3
  size: 34, 34
  orig: 34, 34
  offset: 0, 0
  index: -1
28
  rotate: false
  xy: 900, 160
  size: 77, 77
  orig: 77, 77
  offset: 0, 0
  index: -1
29
  rotate: true
  xy: 432, 215
  size: 40, 95
  orig: 40, 95
  offset: 0, 0
  index: -1
30
  rotate: false
  xy: 2, 186
  size: 169, 195
  orig: 169, 195
  offset: 0, 0
  index: -1
31
  rotate: true
  xy: 173, 219
  size: 162, 160
  orig: 162, 160
  offset: 0, 0
  index: -1
32
  rotate: true
  xy: 529, 220
  size: 35, 32
  orig: 35, 32
  offset: 0, 0
  index: -1
42
  rotate: false
  xy: 429, 132
  size: 74, 78
  orig: 74, 78
  offset: 0, 0
  index: -1
43
  rotate: true
  xy: 154, 2
  size: 19, 11
  orig: 19, 11
  offset: 0, 0
  index: -1
44
  rotate: true
  xy: 154, 161
  size: 23, 17
  orig: 23, 17
  offset: 0, 0
  index: -1
45
  rotate: true
  xy: 180, 3
  size: 18, 24
  orig: 18, 24
  offset: 0, 0
  index: -1
46
  rotate: true
  xy: 335, 212
  size: 67, 95
  orig: 67, 95
  offset: 0, 0
  index: -1
47
  rotate: true
  xy: 353, 2
  size: 35, 29
  orig: 35, 29
  offset: 0, 0
  index: -1
48
  rotate: true
  xy: 735, 322
  size: 40, 36
  orig: 40, 36
  offset: 0, 0
  index: -1
49
  rotate: false
  xy: 505, 162
  size: 48, 51
  orig: 48, 51
  offset: 0, 0
  index: -1
50
  rotate: true
  xy: 301, 40
  size: 57, 83
  orig: 57, 83
  offset: 0, 0
  index: -1
61
  rotate: true
  xy: 246, 54
  size: 50, 53
  orig: 50, 53
  offset: 0, 0
  index: -1
62
  rotate: true
  xy: 206, 3
  size: 18, 23
  orig: 18, 23
  offset: 0, 0
  index: -1
63
  rotate: false
  xy: 970, 399
  size: 11, 14
  orig: 11, 14
  offset: 0, 0
  index: -1
64
  rotate: true
  xy: 1012, 478
  size: 13, 10
  orig: 13, 10
  offset: 0, 0
  index: -1
65
  rotate: true
  xy: 316, 386
  size: 11, 17
  orig: 11, 17
  offset: 0, 0
  index: -1
66
  rotate: true
  xy: 284, 39
  size: 13, 14
  orig: 13, 14
  offset: 0, 0
  index: -1
67
  rotate: false
  xy: 167, 2
  size: 11, 19
  orig: 11, 19
  offset: 0, 0
  index: -1
68
  rotate: false
  xy: 1012, 493
  size: 10, 17
  orig: 10, 17
  offset: 0, 0
  index: -1
69
  rotate: false
  xy: 325, 2
  size: 26, 36
  orig: 26, 36
  offset: 0, 0
  index: -1
70
  rotate: true
  xy: 154, 138
  size: 21, 11
  orig: 21, 11
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 979, 163
  size: 34, 51
  orig: 34, 51
  offset: 0, 0
  index: -1
9
  rotate: false
  xy: 429, 105
  size: 29, 25
  orig: 29, 25
  offset: 0, 0
  index: -1
fx/E_0000_1
  rotate: false
  xy: 563, 287
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
fx/E_0001_2
  rotate: false
  xy: 649, 287
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
fx/E_0002_3
  rotate: false
  xy: 778, 322
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
fx/E_0003_4
  rotate: false
  xy: 864, 322
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
fx/E_0004_5
  rotate: false
  xy: 563, 210
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
fx/E_0005_6
  rotate: false
  xy: 649, 210
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
fx/E_0006_7
  rotate: false
  xy: 735, 245
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
fx/E_0007_8
  rotate: false
  xy: 821, 245
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
fx/E_0008_9
  rotate: false
  xy: 735, 168
  size: 84, 75
  orig: 84, 75
  offset: 0, 0
  index: -1
fx2/5 (1)
  rotate: false
  xy: 316, 399
  size: 126, 111
  orig: 126, 111
  offset: 0, 0
  index: -1
fx2/5 (2)
  rotate: false
  xy: 586, 399
  size: 126, 111
  orig: 126, 111
  offset: 0, 0
  index: -1
fx2/5 (3)
  rotate: false
  xy: 714, 399
  size: 126, 111
  orig: 126, 111
  offset: 0, 0
  index: -1
fx2/5 (4)
  rotate: false
  xy: 842, 399
  size: 126, 111
  orig: 126, 111
  offset: 0, 0
  index: -1
fx2/5 (5)
  rotate: false
  xy: 435, 257
  size: 126, 111
  orig: 126, 111
  offset: 0, 0
  index: -1
fx2/5 (6)
  rotate: false
  xy: 173, 106
  size: 126, 111
  orig: 126, 111
  offset: 0, 0
  index: -1
fx2/5 (7)
  rotate: false
  xy: 301, 99
  size: 126, 111
  orig: 126, 111
  offset: 0, 0
  index: -1
glow1 do
  rotate: false
  xy: 2, 383
  size: 312, 127
  orig: 312, 127
  offset: 0, 0
  index: -1
light1
  rotate: false
  xy: 444, 370
  size: 140, 140
  orig: 140, 140
  offset: 0, 0
  index: -1
