
E54.png
size: 256,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Body
  rotate: false
  xy: 2, 79
  size: 92, 138
  orig: 92, 138
  offset: 0, 0
  index: -1
L_Leg_01
  rotate: false
  xy: 156, 151
  size: 56, 66
  orig: 56, 66
  offset: 0, 0
  index: -1
L_Leg_11
  rotate: false
  xy: 2, 2
  size: 58, 75
  orig: 58, 75
  offset: 0, 0
  index: -1
R_Leg_01
  rotate: false
  xy: 96, 74
  size: 56, 66
  orig: 56, 66
  offset: 0, 0
  index: -1
R_Leg_11
  rotate: false
  xy: 96, 142
  size: 58, 75
  orig: 58, 75
  offset: 0, 0
  index: -1
