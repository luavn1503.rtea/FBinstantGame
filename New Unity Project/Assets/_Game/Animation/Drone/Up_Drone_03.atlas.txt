
Up_Drone_03.png
size: 256,512
format: RGBA8888
filter: Linear,Linear
repeat: none
D3
  rotate: false
  xy: 2, 85
  size: 80, 115
  orig: 80, 115
  offset: 0, 0
  index: -1
D3_L
  rotate: false
  xy: 209, 65
  size: 16, 56
  orig: 16, 56
  offset: 0, 0
  index: -1
D3_R
  rotate: false
  xy: 227, 65
  size: 16, 56
  orig: 16, 56
  offset: 0, 0
  index: -1
Layer 1826
  rotate: false
  xy: 107, 233
  size: 78, 75
  orig: 78, 75
  offset: 0, 0
  index: -1
Lv0_L_01
  rotate: false
  xy: 107, 310
  size: 66, 102
  orig: 66, 102
  offset: 0, 0
  index: -1
Lv0_R_01
  rotate: false
  xy: 84, 91
  size: 61, 109
  orig: 61, 109
  offset: 0, 0
  index: -1
Lv1_21
  rotate: false
  xy: 147, 123
  size: 102, 73
  orig: 102, 73
  offset: 0, 0
  index: -1
Lv2_21
  rotate: false
  xy: 2, 2
  size: 82, 81
  orig: 82, 81
  offset: 0, 0
  index: -1
Lv2_31
  rotate: false
  xy: 126, 417
  size: 112, 83
  orig: 112, 83
  offset: 0, 0
  index: -1
Lv2_L_01
  rotate: false
  xy: 167, 72
  size: 40, 49
  orig: 40, 49
  offset: 0, 0
  index: -1
Lv2_L_21
  rotate: false
  xy: 86, 11
  size: 79, 78
  orig: 79, 78
  offset: 0, 0
  index: -1
Lv2_R_01
  rotate: false
  xy: 167, 21
  size: 40, 49
  orig: 40, 49
  offset: 0, 0
  index: -1
Lv2_R_21
  rotate: false
  xy: 175, 337
  size: 79, 78
  orig: 79, 78
  offset: 0, 0
  index: -1
Lv3_L_01
  rotate: false
  xy: 187, 267
  size: 59, 68
  orig: 59, 68
  offset: 0, 0
  index: -1
Lv4_R_01
  rotate: false
  xy: 187, 198
  size: 59, 67
  orig: 59, 67
  offset: 0, 0
  index: -1
Lv5_01
  rotate: false
  xy: 2, 414
  size: 122, 86
  orig: 122, 86
  offset: 0, 0
  index: -1
Lv5_L_01
  rotate: false
  xy: 2, 308
  size: 103, 104
  orig: 103, 104
  offset: 0, 0
  index: -1
Lv5_R_01
  rotate: false
  xy: 2, 202
  size: 103, 104
  orig: 103, 104
  offset: 0, 0
  index: -1
