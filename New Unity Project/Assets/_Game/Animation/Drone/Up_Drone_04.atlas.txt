
Up_Drone_04.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
D4_01
  rotate: false
  xy: 2, 364
  size: 144, 137
  orig: 144, 137
  offset: 0, 0
  index: -1
D4_11
  rotate: false
  xy: 2, 61
  size: 116, 142
  orig: 116, 142
  offset: 0, 0
  index: -1
Lv1_01
  rotate: false
  xy: 267, 341
  size: 139, 56
  orig: 139, 56
  offset: 0, 0
  index: -1
Lv1_11
  rotate: false
  xy: 228, 198
  size: 48, 88
  orig: 48, 88
  offset: 0, 0
  index: -1
Lv2_01
  rotate: false
  xy: 2, 2
  size: 138, 57
  orig: 138, 57
  offset: 0, 0
  index: -1
Lv2_11
  rotate: false
  xy: 267, 399
  size: 133, 102
  orig: 133, 102
  offset: 0, 0
  index: -1
Lv3_01
  rotate: false
  xy: 119, 288
  size: 119, 70
  orig: 119, 70
  offset: 0, 0
  index: -1
Lv3_11
  rotate: false
  xy: 148, 360
  size: 117, 141
  orig: 117, 141
  offset: 0, 0
  index: -1
Lv3_L_01
  rotate: false
  xy: 408, 323
  size: 56, 84
  orig: 56, 84
  offset: 0, 0
  index: -1
Lv3_R_01
  rotate: false
  xy: 120, 108
  size: 56, 84
  orig: 56, 84
  offset: 0, 0
  index: -1
Lv4_01
  rotate: false
  xy: 2, 205
  size: 115, 157
  orig: 115, 157
  offset: 0, 0
  index: -1
Lv4_L_01
  rotate: false
  xy: 402, 409
  size: 62, 92
  orig: 62, 92
  offset: 0, 0
  index: -1
Lv4_L_11
  rotate: false
  xy: 119, 205
  size: 43, 81
  orig: 43, 81
  offset: 0, 0
  index: -1
Lv4_R_01
  rotate: false
  xy: 164, 194
  size: 62, 92
  orig: 62, 92
  offset: 0, 0
  index: -1
Lv4_R_11
  rotate: false
  xy: 466, 418
  size: 40, 83
  orig: 40, 83
  offset: 0, 0
  index: -1
