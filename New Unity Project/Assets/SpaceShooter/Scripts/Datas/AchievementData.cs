﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Achievement", menuName = "Datas/Achievement")]
public class AchievementData : ScriptableObject {
    public List<QuestData> Mission = new List<QuestData>();
    public List<QuestData> Achievement= new List<QuestData>();
}

