﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BossBattleEditData", menuName = "Datas/BossBattleEditData")]
public class BossBattleEditData : ScriptableObject {

	// BOSS 1
	public Boss1EditData Boss1EData = new Boss1EditData();

	// BOSS 2
	public Boss2EditData Boss2EData = new Boss2EditData();

	// BOSS 3
	public Boss3EditData Boss3EData = new Boss3EditData();

	// BOSS 4
	public Boss4EditData Boss4EData = new Boss4EditData();

	// BOSS 5
	public Boss5EditData Boss5EData = new Boss5EditData();

	// BOSS 6
	public Boss6EditData Boss6EData = new Boss6EditData();

	// BOSS 7
	public Boss7EditData Boss7EData = new Boss7EditData();

	// BOSS 8
	public Boss8EditData Boss8EData = new Boss8EditData();

	// BOSS 9
	public Boss9EditData Boss9EData = new Boss9EditData();

	// BOSS 10
	public Boss10EditData Boss10EData = new Boss10EditData();

	// BOSS 11
	public Boss11EditData Boss11EData = new Boss11EditData();

	public Boss13EditData Boss13EData = new Boss13EditData();


}

#region BossEditData
public class BossBattleEditDataBase {
	[Space(10)]
	public int Key;
	public string NameBoss;
	public float FireRate;
	public float DecsRateShoot;
	public float IncsHealthPoint;
}

[Serializable]
public class Boss1EditData : BossBattleEditDataBase {
	public NumOfBulletAndFireRate Type1;
	public FireRate Type2;

}

[Serializable]
public class Boss2EditData : BossBattleEditDataBase {
	public TimeFireAndFireRate Type1;
	public float Type2TimeLifeLazer;
	public float Type3TimeLifeLazer;

}

[Serializable]
public class Boss3EditData : BossBattleEditDataBase {
	public NumOfBulletAndFireRate Type1;
	public CircleTypeNumberCircleAndNumOfBulletInACircleAndFireRate Type2;
	public NumOfBulletAndFireRate Type3;

}

[Serializable]
public class Boss4EditData : BossBattleEditDataBase {
	public TimeFireAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type2;
	public TimeLifeAndFireRate Type3;

}

[Serializable]
public class Boss5EditData : BossBattleEditDataBase {
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type2;
	public LineTypeNumberLineAndNumOfBulletInALine Type3;

}

[Serializable]
public class Boss6EditData : BossBattleEditDataBase {
	public TimeFireAndFireRate Type1;
	public NumOfLineTimeFireAndFireRate Type2;
	public TimeFireAndFireRate Type3;
}

[Serializable]
public class Boss7EditData : BossBattleEditDataBase {
	public NumOfBulletAndFireRate Type1;
	public NumberOfBullet Type2;
	public TimeFireAndFireRate Type3;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRateAndTimeFire Type4;
}

[Serializable]
public class Boss8EditData : BossBattleEditDataBase {
	public TimeFireAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALine Type2;
	public TimeFireAndFireRate Type3;
}

[Serializable]
public class Boss9EditData : BossBattleEditDataBase {
	public TimeFireAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type2;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type3;
}

[Serializable]
public class Boss10EditData : BossBattleEditDataBase {
	public NumOfBulletAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRateBoss10 Type2;
	public TimeFireAllowAndReverseClockAndFireRate Type3;
}

[Serializable]
public class Boss11EditData : BossBattleEditDataBase {
	public NumOfBulletAndFireRate Type1;
}

[Serializable]
public class Boss12EditData : BossBattleEditDataBase {
	public NumOfBulletAndFireRate Type1;
	public TimeLife Type2;
	public NumOfBulletAndFireRate Type3;
	public SpeedBullet Type4;
}

[Serializable]
public class Boss13EditData : BossBattleEditDataBase {
	public NumOfBulletAndFireRate GiftAtk;
	public NumOfBulletAndFireRate BallAoeAtk;
	public NumberOfBullet SnowBallAndCandyAtk;
	public float GunHp;
}

#endregion
