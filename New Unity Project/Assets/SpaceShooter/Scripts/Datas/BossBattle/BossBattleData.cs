﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "BossBattleData", menuName = "Datas/BossBattleData")]
public class BossBattleData : ScriptableObject {

	public static string LevelOfDiffKey = "LevelOfDifficultBB";

	private static ModeUnlockedBB _modeUnlockedBb;
	public static ModeUnlockedBB ModeUnlockedBb {
		get {
			if (_modeUnlockedBb == null) {
				var modeUnlockedBb = JsonUtility.FromJson<ModeUnlockedBB>(PlayerPrefs.GetString("UnlockedBBKey"));
				if (modeUnlockedBb != null) _modeUnlockedBb = modeUnlockedBb;
				else {
					_modeUnlockedBb = new ModeUnlockedBB();
					PlayerPrefs.SetString(_modeUnlockedBb.UnlockedBBKey, JsonUtility.ToJson(_modeUnlockedBb));
				}
			}
			_modeUnlockedBb = JsonUtility.FromJson<ModeUnlockedBB>(PlayerPrefs.GetString(_modeUnlockedBb.UnlockedBBKey));
			return _modeUnlockedBb;
		}
		set { _modeUnlockedBb = value; }
	}


	//public static string EasyUnlockedIndex = "EasyUnlockedBB";
	//public static string MediumUnlockedIndex = "MediumUnlockedBB";
	//public static string HardUnlockedIndex = "HardUnlockedBB";

	//public static string EasyUnlockedUiIndex = "EasyUnlockedBBUI";
	//public static string MediumUnlockedUiIndex = "MediumUnlockedBBUI";
	//public static string HardUnlockedUiIndex = "HardUnlockedBBUI";

	[Header("Easy Mode")]
	[Space(30)]
	public BossInfoEasyMode BossesInfoEasyMode = new BossInfoEasyMode();

	[Header("Medium Mode")]
	[Space(30)]
	public BossInfoMediumMode BossesInfoMediumMode = new BossInfoMediumMode();

	[Header("Hard Mode")]
	[Space(30)]
	public BossInfoHardMode BossesInfoHardMode = new BossInfoHardMode();

}
[Serializable]
public class BossBattleInfo {
	[Header("BOSS KEY")]
	public int BossKey;
	[Header("-----------------Choose only one of three Rewards-------------------------")]
	public List<int> Gifts;
	public int Gem;
	public int Coin;
	public int GemSkipPrice;
}

[Serializable]
public class BossInforBase {
	public int KindOfGift;
	public int NumOfGift;
	public int FuelNeed;
}

[Serializable]
public class BossInfoEasyMode : BossInforBase {
	public List<BossBattleInfo> BossesInfoEasyMode = new List<BossBattleInfo>();
}
[Serializable]
public class BossInfoMediumMode : BossInforBase {
	public List<BossBattleInfo> BossesInfoMediumMode = new List<BossBattleInfo>();
}
[Serializable]
public class BossInfoHardMode : BossInforBase {
	public List<BossBattleInfo> BossesInfoHardMode = new List<BossBattleInfo>();
}

public class ModeUnlockedBB {
	public string UnlockedBBKey = "UnlockedBBKey";
	public int EasyUnlockedIndex = 0;
	public int MediumUnlockedIndex = 0;
	public int HardUnlockedIndex = 0;
	public int EasyUnlockedUiIndex = 0;
	public int MediumUnlockedUiIndex = 0;
	public int HardUnlockedUiIndex = 0;
}
