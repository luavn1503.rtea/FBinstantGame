﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[CreateAssetMenu(fileName = "BossData", menuName = "Datas/BossData")]
public class BossInfoData : ScriptableObject {

    private static BossInfoData instance;
    public static BossInfoData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<BossInfoData>("BossData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    // BOSS 1
    public Boss1Data Boss1Data = new Boss1Data();

	// BOSS 2
	public Boss2Data Boss2Data = new Boss2Data();

	// BOSS 3
	public Boss3Data Boss3Data = new Boss3Data();

	// BOSS 4
	public Boss4Data Boss4Data = new Boss4Data();

	// BOSS 5
	public Boss5Data Boss5Data = new Boss5Data();

	// BOSS 6
	public Boss6Data Boss6Data = new Boss6Data();

	// BOSS 7
	public Boss7Data Boss7Data = new Boss7Data();

	// BOSS 8
	public Boss8Data Boss8Data = new Boss8Data();

	// BOSS 9
	public Boss9Data Boss9Data = new Boss9Data();

	// BOSS 10
	public Boss10Data Boss10Data = new Boss10Data();

	// BOSS 11
	public Boss11Data Boss11Data = new Boss11Data();

	// BOSS 12
	public Boss12Data Boss12Data = new Boss12Data();

	public Boss13Data Boss13Data = new Boss13Data();

	public string GetNameBoss(int keyBoss) {
		//Debug.LogError("KeyBoss : " + keyBoss);
		//string className = "Boss" + keyBoss + "Data";
		//string nameBossKey = "NameBoss";

		//Type t = Type.GetType(className);

		//FieldInfo namBossField = t.GetField(nameBossKey);

		//foreach (var fieldInfo in t.GetFields()) {
		//	Debug.Log(fieldInfo.GetValue(Boss8Data));
		//}

		//string NameBoss = namBossField.GetValue(t.BaseType).ToString();

		//Debug.LogError(NameBoss);
		//return NameBoss;
		switch (keyBoss) {
			case 1:
				return Boss1Data.NameBoss;
			case 2:
				return Boss2Data.NameBoss;
			case 3:
				return Boss3Data.NameBoss;
			case 4:
				return Boss4Data.NameBoss;
			case 5:
				return Boss5Data.NameBoss;
			case 6:
				return Boss6Data.NameBoss;
			case 7:
				return Boss7Data.NameBoss;
			case 8:
				return Boss8Data.NameBoss;
			case 9:
				return Boss9Data.NameBoss;
			case 10:
				return Boss10Data.NameBoss;
			case 11:
				return Boss11Data.NameBoss;
		}

		return string.Empty;
	}

}

#region Base Value Class

[Serializable]
public class SpeedBullet {
	public float SpeedOfBullet;
}

[Serializable]
public class CircleTypeNumberCircleAndNumOfBulletInACircleAndFireRate : FireRate {
	public int NumOfCircle;
	public int NumOfBulletInACircle;
}

[Serializable]
public class LineTypeNumberLineAndNumOfBulletInALine : SpeedBullet {
	public int NumOfLine;
	public int NumOfBulletInALine;
}

[Serializable]
public class LineTypeNumberLineAndNumOfBulletInALineAndFireRate : FireRate {
	public int NumOfLine;
	public int NumOfBulletInALine;
}
[Serializable]
public class LineTypeNumberLineAndNumOfBulletInALineAndFireRateBoss10 : FireRate {
	public int NumOfLine;
	public int NumOfBulletInALine;
	public float AngleFire;
	public float BulletTight;
}

[Serializable]
public class LineTypeNumberLineAndNumOfBulletInALineAndFireRateAndTimeFire : TimeFireAndFireRate {
	public int NumOfLine;
	public int NumOfBulletInALine;
}

[Serializable]
public class NumberOfBullet : SpeedBullet {
	public int NumOfBullet;
}

[Serializable]
public class FireRate : SpeedBullet {
	public float FireOfRate;
}

[Serializable]
public class TimeFireAndFireRate : FireRate {
	public float TimeOfFire;
}
[Serializable]
public class NumOfLineTimeFireAndFireRate : FireRate {
	public int NumOfLine;
	public float TimeOfFire;
}

[Serializable]
public class TimeFire : SpeedBullet {
	public float TimeOfFire;
}

[Serializable]
public class TimeFireAllowAndReverseClockAndFireRate : FireRate {
	public float TimeOfFireAllow;
	public float TimeOfFireReverse;
}

[Serializable]
public class LineTypeTimeFireAndFireRate : SpeedBullet {
	public int NumOfLine;
	public float TimeOfFire;
}

[Serializable]
public class TimeLife : SpeedBullet {
	public float TimeOfLife;
}

[Serializable]
public class TimeLifeAndFireRate : FireRate {
	public float TimeOfLife;
}
[Serializable]
public class TimeLifeAndNumOfBullet : NumberOfBullet {
	public float TimeOfLife;
}

[Serializable]
public class NumOfBulletAndFireRate : FireRate {
	public int NumOfBullet;
}

#endregion

#region BossData

public class BossDataBase {
	public int Key;
	public string NameBoss;
	public float FireRate;
}


[Serializable]
public class Boss1Data : BossDataBase {
	public NumOfBulletAndFireRate Type1;
	public FireRate Type2;

}

[Serializable]
public class Boss2Data : BossDataBase {
	public TimeFireAndFireRate Type1;
	public float Type2TimeLifeLazer;
	public float Type3TimeLifeLazer;

}

[Serializable]
public class Boss3Data : BossDataBase {
	public NumOfBulletAndFireRate Type1;
	public CircleTypeNumberCircleAndNumOfBulletInACircleAndFireRate Type2;
	public NumOfBulletAndFireRate Type3;

}

[Serializable]
public class Boss4Data : BossDataBase {
	public TimeFireAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type2;
	public TimeLifeAndFireRate Type3;

}

[Serializable]
public class Boss5Data : BossDataBase {
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type2;
	public LineTypeNumberLineAndNumOfBulletInALine Type3;

}

[Serializable]
public class Boss6Data : BossDataBase {
	public TimeFireAndFireRate Type1;
	public NumOfLineTimeFireAndFireRate Type2;
	public TimeFireAndFireRate Type3;
}

[Serializable]
public class Boss7Data : BossDataBase {
	public NumOfBulletAndFireRate Type1;
	public NumberOfBullet Type2;
	public TimeFireAndFireRate Type3;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRateAndTimeFire Type4;
}

[Serializable]
public class Boss8Data : BossDataBase {
	public TimeFireAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALine Type2;
	public TimeFireAndFireRate Type3;
}

[Serializable]
public class Boss9Data : BossDataBase {
	public TimeFireAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type2;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRate Type3;
}

[Serializable]
public class Boss10Data : BossDataBase {
	public NumOfBulletAndFireRate Type1;
	public LineTypeNumberLineAndNumOfBulletInALineAndFireRateBoss10 Type2;
	public TimeFireAllowAndReverseClockAndFireRate Type3;
}

[Serializable]
public class Boss11Data : BossDataBase {
	public NumOfBulletAndFireRate Type1;
}

[Serializable]
public class Boss12Data : BossDataBase {
	public NumOfBulletAndFireRate Type1;
	public TimeLife Type2;
	public NumOfBulletAndFireRate Type3;
	public SpeedBullet Type4;
}

[Serializable]
public class Boss13Data : BossDataBase {
	public NumOfBulletAndFireRate GiftAtk;
	public NumOfBulletAndFireRate BallAoeAtk;
	public NumberOfBullet SnowBallAndCandyAtk;
	public float GunHp;
}

#endregion

