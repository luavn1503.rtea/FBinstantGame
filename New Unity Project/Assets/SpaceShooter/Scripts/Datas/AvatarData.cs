﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AvatarData",menuName = "Datas/AvatarData")]
public class AvatarData : ScriptableObject {

    private static AvatarData instance;
    public static AvatarData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<AvatarData>("AvatarData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<AvatarInfor> AvatarInfor;
}

[Serializable]
public class AvatarInfor {
	public AvatarMissionType Type;
	public Sprite AvatarIcon;
	public string Description;
	public int NumberNeeded;
}

public enum AvatarMissionType {
	Default, DefeatBoss, DefeatHalloweenBoss, BoughtVip
}
