﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static EnumDefine;

[Serializable]
[CreateAssetMenu(fileName = "CardData", menuName = "Datas/CardData")]
public class CardData : ScriptableObject {

    private static CardData instance;
    public static CardData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<CardData>("CardData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<Card> Cards;
	public List<CardUpgrade> CardUpgradeData;

	public CardInfo GetInfo(int index, int level = 0, bool isRandom = true) {
		CardInfo c = new CardInfo();

		if (index < 0 || index >= Cards.Count)
			return null;

		var data = Cards[index];

		c.Icon = data.Icon;
		c.IconInGame = data.IconInGame;
		c.Name = data.Name;
		c.NameText = data.NameText;
		c.Description = data.Description;
		c.Star = data.Star;
		c.Levels = data.Levels;
		c.Numeral = data.GetEffect(level, isRandom).ToList();
		return c;
	}

	public int CardIndex(CardType ct, StarType st) {
		for (int i = 0; i < Cards.Count; i++)
			if (ct == Cards[i].Name && st == Cards[i].Star) {
				//Logs.Log(string.Format("Get data of Card: {0}___{1}", ct, st));
				return i;
			}

		return -1;
	}

	public CardUpgradeNeeded GetIngredient(int index, int level) {
		if (index < Cards.Count)
			for (int i = 0; i < CardUpgradeData.Count; i++)
				if (CardUpgradeData[i].Star == Cards[index].Star)
					return CardUpgradeData[i].Levels[level];

		return null;
	}

	public CardUpgrade GetDataCardUpgrade(int index) {
		if (index < Cards.Count)
			for (int i = 0; i < CardUpgradeData.Count; i++)
				if (CardUpgradeData[i].Star == Cards[index].Star)
					return CardUpgradeData[i];

		return null;
	}

	public int HasCard(CardType ct, StarType st) {
		for (int i = 0; i < Cards.Count; i++)
			if (ct != CardType.Drone) {
				if (ct == Cards[i].Name && st == Cards[i].Star) {
					return i;
				}
			}

		return -1;
	}

	public StarType GetStarType(int index) {
		for (int i = 0; i < Cards.Count; i++) {
			if (index == i) {
				return Cards[i].Star;
			}
		}

		return EnumDefine.StarType.Common;
	}

	public PlayerCard NewPlayerCard(CardType ct, StarType st) {
		int cardIndex = CardIndex(ct, st);
		PlayerCard newCard = new PlayerCard(cardIndex);
		CardInfo cardInfor = GetInfo(cardIndex);
		newCard.Numeral = cardInfor.Numeral;
		return newCard;
	}

	[ContextMenu("Init 7")]
	public void Init() {
		while (Cards.Count < 7) {
			Cards.Add(new Card());
		}
	}
}

[Serializable]
public class Card {
	public Sprite Icon;
	public Sprite IconInGame;
	public CardType Name;
	public string NameText;
	public string Description;
	public StarType Star;
	public List<CardLevel> Levels;

	public bool IsNeedUpdated;

	public Card() {
		Levels = new List<CardLevel>();

		while (Levels.Count < 10) {
			Levels.Add(new CardLevel());
		}
	}

	public List<float> GetEffect(int index, bool isRandom = true) {
		List<float> result = new List<float>();

		if (index < Levels.Count) {
			var dataUse = Levels[index].Numeral;
			for (int i = 0; i < dataUse.Count; i++) {
				result.Add(dataUse[i].Effect +
					(isRandom ? UnityEngine.Random.Range(-dataUse[i].VarianceEffect, dataUse[i].VarianceEffect) : 0));
			}
		}

		return result;
	}

	public List<float> GetRealData(int index) {
		List<float> result = new List<float>();

		if (index < Levels.Count) {
			var dataUse = Levels[index].Numeral;
			for (int i = 0; i < dataUse.Count; i++)
				result.Add(dataUse[i].Effect);
		}

		return result;
	}
}

[Serializable]
public class CardLevel {
	public List<EffectOfCard> Numeral;

	public CardLevel() {
		Numeral = new List<EffectOfCard>();

		for (int i = 0; i < 3; i++)
			Numeral.Add(new EffectOfCard());
	}
}

[Serializable]
public class EffectOfCard {
	public float Effect;
	public float VarianceEffect;
}

[Serializable]
public class CardUpgrade {
	public StarType Star;
	public List<CardUpgradeNeeded> Levels;

	public int GetExpNeeded(int level) {
		return level < Levels.Count ? Levels[level].ExpNeeded : int.MaxValue;
	}
}

[Serializable]
public class CardUpgradeNeeded {
	public int ExpNeeded;
	public int ExpGet;
	public int GoldCost;
}

[Serializable]
public class CardInfo : Card {
	public int Order;
	public int Level;
	public int CurrentExp;
	public List<float> Numeral;
	private string _getBonusLevel;
	public int OrderInList {
		get {
			return Order;
		}
	}

	public Sprite IconShow {
		get {
			return Icon;
		}
	}

	public string NameShow {
		get {
			return NameText ?? "Missing";
		}
	}

	public int BorderShow {
		get { return 0; }
	}

	public string DescriptionShow {
		get {
			return Description;
		}
	}

	public int NumberStarShow {
		get {
			return (int)Star;
		}
	}

	public string LevelShow {
		get {
			return (Level + 1).ToString();
		}
	}

	public string Info {
		get {
			switch (Name) {
				case CardType.CoinBonus:
					return string.Format("Coin bonus +{0:0.##} %", Numeral[0]);
				case CardType.CombatPower:
					return string.Format("Combat power +{0:0}", Numeral[0]);
				case CardType.CooldownBomb:
					return string.Format("Decrease cooldown bomb {0:0.##} s", Numeral[0]);
				case CardType.Shield:
					return string.Format("Duration: {0:0.##} s\nCooldown: {1:0.##} s", Numeral[0], Numeral[1]);
				case CardType.Missiles:
					return string.Format("Damage: {0:0.##}\nNumber Missile: {1:0}\nDuration: {2:0}s", Numeral[0], Numeral[1], Numeral[2]);
				case CardType.POW:
					return string.Format("Damage: {0:0.##}\nEnemy kill: {1:0}\nRadius: {2:0.##}", Numeral[0], Numeral[1], Numeral[2]);
				case CardType.Life:
					return string.Format("Enemy kill: {0:0}", Numeral[0]);
				default:
					return "Dont have data yet";
			}
		}
	}

	public string GetBonusLevel {
		get {
			switch (Name) {
				case CardType.CoinBonus:
					return string.Format("~ {0:0.##} %", Numeral[0]);
				case CardType.CombatPower:
					return string.Format("~ {0:0}", Numeral[0]);
				case CardType.CooldownBomb:
					return string.Format("~ {0:0.##} s", Numeral[0]);
				case CardType.Shield:
					return string.Format("~ {0:0.##} s" + "\n~ {1:0.##} s", Numeral[0], Numeral[1]);
				case CardType.Missiles:
					return string.Format("~ {0:0.##}\n~ {1:0}\n~ {2:0}s", Numeral[0], Numeral[1], Numeral[2]);
				case CardType.POW:
					return string.Format("~ {0:0.##}\n~ {1:0}\n~ {2:0.##}", Numeral[0], Numeral[1], Numeral[2]);
				case CardType.Life:
					return string.Format("~ {0:0}", Numeral[0]);
				default:
					return "Dont have data yet";
			}
		}
	}

	public string GetShortInfo {
		get {
			switch (Name) {
				case CardType.CoinBonus:
					return string.Format("Coin bonus +{0:0.##} %", Numeral[0]);
				case CardType.CombatPower:
					return string.Format("Combat power +{0:0}", Numeral[0]);
				case CardType.CooldownBomb:
					return string.Format("Decrease {0:0.##} s", Numeral[0]);
				case CardType.Shield:
					return string.Format("Duration: {0:0.##} s" + "\nCooldown: {1:0.##} s", Numeral[0], Numeral[1]);
				case CardType.Missiles:
					return string.Format("Damage: {0:0.##}\nNumber Missile: {1:0}\nDuration: {2:0}s", Numeral[0], Numeral[1], Numeral[2]);
				case CardType.POW:
					return string.Format("Damage: {0:0.##}\nEnemy kill: {1:0}\nRadius: {2:0.##}", Numeral[0], Numeral[1], Numeral[2]);
				case CardType.Life:
					return string.Format("Enemy kill: {0:0}", Numeral[0]);
				default:
					return "Dont have data yet";
			}
		}
	}

	public bool IsSelected {
		get; set;
	}
	public CardType CardType {
		get {
			return Name;
		}
	}
	/// <summary>
	/// Equal true when Order = -1 (why???)
	/// </summary>
	public bool IsEquip {
		get {
			return Order == -1;
		}
	}


	public override string ToString() {
		return Name + " | " + Star + " | " + " | " + LevelShow;
	}
}


[Serializable]
public class PlayerCard {
	public PlayerCard(PlayerCard playerCard) {
		Index = playerCard.Index;
		CurrentExp = playerCard.CurrentExp;
		Level = playerCard.Level;
		Numeral = new List<float>(playerCard.Numeral);
		IsFavorite = new List<bool>(playerCard.IsFavorite);
	}

	public PlayerCard(int index) {
		Index = index;
		Numeral = new List<float>();
		IsFavorite = new List<bool>() {false, false, false};
	}

	public PlayerCard(int index, int exp, int level, List<float> numeral) {
		Index = index;
		CurrentExp = exp;
		Level = level;
		Numeral = new List<float>(numeral);
	}

	public int Index;
	public int Level;
	public int CurrentExp;

	public List<float> Numeral;
	public List<bool> IsFavorite;

	public void SetData(int exp, int level, List<float> numeral) {
		CurrentExp = exp;
		Level = level;
		Numeral = new List<float>(numeral);
	}

	public void ChangeFavoriteCard(List<PlayerCard> lsAllCard, int favoriteIndex, int cardId) {
		//for (int i = 0; i < lsAllCard.Count; i++) {
		//	if (lsAllCard[i].FavoriteIndex[favoriteIndex] > -1) {
		//		lsAllCard[i].FavoriteIndex[favoriteIndex] = -1;
		//		break;
		//	}
		//}
		for (int i = 0; i < lsAllCard.Count; i++) {
			if (lsAllCard[i].IsFavorite[favoriteIndex]) {
				lsAllCard[i].IsFavorite[favoriteIndex] = false;
				break;
			}
		}

		lsAllCard[cardId].IsFavorite[favoriteIndex] = true;
	}
}
