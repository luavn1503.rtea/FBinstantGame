﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BackupData {
	private const string Key = "BackupData";
	public List<int> BackupCurrency;
	public List<int> EndlessReward;
	public int EndlessScore;
	public int EndlessRound;
	public bool EndlessIsQuitApp;

	public void SaveData() {
		PlayerPrefs.SetString(Key, JsonUtility.ToJson(this));
	}

	public void LoadData() {
		string data = PlayerPrefs.GetString(Key);
		if (data != string.Empty) {
			var backupData = JsonUtility.FromJson<BackupData>(data);
			BackupCurrency = backupData.BackupCurrency;
			EndlessReward = backupData.EndlessReward;
			EndlessScore = backupData.EndlessScore;
			EndlessRound = backupData.EndlessRound;
			EndlessIsQuitApp = backupData.EndlessIsQuitApp;
		}

		if (BackupCurrency == null || BackupCurrency.Count == 0) {
			BackupCurrency = new List<int>() {0, 0, 0};
		}

		if (EndlessReward == null || EndlessReward.Count == 0) {
			EndlessReward = new List<int>();
			for (int i = 0; i < 9; i++) {
				EndlessReward.Add(0);
			}

			//Debug.Log(EndlessReward.Count);
		}

	}

	public void ClearCurrency() {
		for (int i = 0; i < BackupCurrency.Count; i++) {
			BackupCurrency[i] = 0;
		}
		SaveData();
	}

	public void AddCurrency() {
		//GameData.Instance.PlayerInfo.AddGold(BackupCurrency[0], "backup");
		//GameData.Instance.PlayerInfo.AddGem(BackupCurrency[1], "backup");
		//GameData.Instance.PlayerInfo.AddFuel(BackupCurrency[2], "backup");
		ClearCurrency();
	}

	public void GetCurrency(int index, int number) {
		BackupCurrency[index] += number;
	}

	public void GetEndlessReward(int index, int number) {
		EndlessReward[index] += number;
	}

	public void EarnEndlessReward() {
		PlayerInfo playerInfor = ManagerData.Instance.PlayerInfo;
		Inventory inventory = ManagerData.Instance.Inventory;
		for (int i = 0; i < EndlessReward.Count; i++) {
            if (i <= 5)
            {
                if(EndlessReward[i] > 0)
                {
                    //playerInfor.AddEndlessReward((EndlessRewardType)i, EndlessReward[i], i - 2, "backup");
                }
            }
            else
            {
                if (EndlessReward[i] > 0)
                {
                    inventory.AddTreasure((TreasureType)(i - 6), EndlessReward[i], "backup");
                }
            }
            //switch (i)
            //{
            //    case 0:
            //        playerInfor.GetAllRewardType(AllRewardType.Coin, -1, EndlessReward[i], ChangeCurrencyArea.Endless);
            //        break;
            //    case 1:
            //        playerInfor.GetAllRewardType(AllRewardType.Gem, -1, EndlessReward[i], ChangeCurrencyArea.Endless);
            //        break;
            //    case 2:
            //        playerInfor.GetAllRewardType(AllRewardType.IngameItem, 1, EndlessReward[i], ChangeCurrencyArea.Endless);
            //        break;
            //    case 3:
            //        playerInfor.GetAllRewardType(AllRewardType.IngameItem, 2, EndlessReward[i], ChangeCurrencyArea.Endless);
            //        break;
            //    case 4:
            //        playerInfor.GetAllRewardType(AllRewardType.IngameItem, 3, EndlessReward[i], ChangeCurrencyArea.Endless);
            //        break;
            //    case 5:
            //        playerInfor.GetAllRewardType(AllRewardType.IngameItem, 0, EndlessReward[i], ChangeCurrencyArea.Endless);
            //        break;
            //    case 6:
            //        playerInfor.GetAllRewardType(AllRewardType.Box, 0, EndlessReward[i], ChangeCurrencyArea.Endless);
            //        break;
            //    case 7:
            //        playerInfor.GetAllRewardType(AllRewardType.Box, 1, EndlessReward[i], ChangeCurrencyArea.Endless);
            //        break;
            //    case 8:
            //        playerInfor.GetAllRewardType(AllRewardType.Box, 2, EndlessReward[i], ChangeCurrencyArea.Endless);
            //        break;
            //}
            EndlessReward[i] = 0;
		}

      //  playerInfor.SaveData();
        inventory.SaveData();
        SaveData();
	}

	public void ClearEndless() {
		EndlessRound = 0;
		EndlessScore = 0;
		EndlessIsQuitApp = false;
		SaveData();
	}

	public void OnEndlessPause(bool isQuitApp) {
		EndlessIsQuitApp = isQuitApp;
		SaveData();
	}

	public bool IsHasReward() {
		for (int i = 0; i < EndlessReward.Count; i++) {
			if (EndlessReward[i] > 0) {
				return true;
			}
		}

		return false;
	}

	// for test
	public void SeeReward() {
		for (int i = 0; i < EndlessReward.Count; i++) {
			Debug.Log(EndlessReward[i]);
		}
	}
}

