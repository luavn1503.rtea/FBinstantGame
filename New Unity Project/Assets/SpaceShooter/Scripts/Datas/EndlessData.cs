﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EndlessData", menuName = "Datas/EndlessData")]
public class EndlessData : ScriptableObject {

    private static EndlessData instance;
    public static EndlessData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<EndlessData>("EndlessData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<EndlessRankInfor> RankInfor = new List<EndlessRankInfor>();
	public List<EndlessRoundReward> RoundRewards = new List<EndlessRoundReward>();
	public List<Sprite> RewardIcons = new List<Sprite>();
	public List<EndlessSkipItem> SkipItems = new List<EndlessSkipItem>();
}

[Serializable]
public class EndlessRewardInfor {
	public EndlessRewardType Type;
	public Sprite Icon;
	public int Number;
}

[Serializable]
public class EndlessRankInfor {
	public EndlessRank Title;
	public Sprite RankIcon;
	public int NeededScore;
	public List<EndlessRewardInfor> Rewards = new List<EndlessRewardInfor>();
}

[Serializable]
public class EndlessRoundReward {
	public int InRound;
	public List<EndlessRewardInfor> Infor = new List<EndlessRewardInfor>();
}

[Serializable]
public class EndlessSkipItem {
	public int Wave;
	public int Score;
}

public enum EndlessRewardType {
	Coin, Gem, LvlUp3, LvlUp5, LvlUp10, Bomb, BadBox, NormalBox, GoodBox
}

public enum EndlessRank {
	Rookie, Airman, Sergeant, Lieutenant, Captain, Major, Colonel, General
}
