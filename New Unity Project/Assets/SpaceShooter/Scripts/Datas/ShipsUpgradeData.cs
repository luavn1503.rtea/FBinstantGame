﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using Gemmob;
using UnityEngine;

[CreateAssetMenu(fileName = "ShipsUpgradeData", menuName = "Datas/ShipsUpgradeData")]
public class ShipsUpgradeData : ScriptableObject {

    private static ShipsUpgradeData instance;
    public static ShipsUpgradeData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<ShipsUpgradeData>("ShipsUpgradeData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<ShipUpgradeData> Ships = new List<ShipUpgradeData>();

	public int GetIndex(string shipName) {
		return Ships.FindIndex(ship => String.Compare(ship.Name, shipName, StringComparison.Ordinal) == 0);
	}


}


[Serializable]
public class ShipUpgradeData {
    public int Id;
    public bool isShip;
	public Sprite Icon;
	public List<Sprite> Icons = new List<Sprite>();
	public Sprite ClassIcon;
	public Sprite EvolveItemIcon;
	public string Name;
	public string Description;
	public float BulletDame;
	public int Star = 1;
	public Region Region;
	public SpecialEvent Event;
	public List<hipLevelData> Levels = new List<hipLevelData>();
	public List<hipLevelData> OldLevels = new List<hipLevelData>();
	public int GemPrice = 5;
	public int CoinPrice = 100;
	public int VideoCount = 0;
	public int CombatPower;
	public int LevelUnlock;
	public string BuyDescription;
	public List<AircraftEvolveData> EvolveDatas = new List<AircraftEvolveData>();

	public ShipUpgradeData() {
		Levels = new List<hipLevelData>();
		for (int i = 0; i < 20; i++) {
			Levels.Add(new hipLevelData());
		}

		//List<ShipLevelData> x = new List<ShipLevelData>(Levels);
	}

	public string GetBase(int currentLevel) {
		int realLevel = currentLevel > Levels.Count ? Levels.Count : currentLevel;
		return currentLevel == -1 || currentLevel == 0 ? CombatPower.ToString() : Levels[realLevel - 1].CombatPower.ToString();
	}

	public string ToString(int level) {
		var head = "";
		var tail = string.Empty;
		if (level == -1) {
			head = Description;
			tail = "Base power: " + CombatPower;
		} else {
			var data = Levels[level >= Levels.Count ? Levels.Count - 1 : level];
			tail = (level >= Levels.Count ? "Power: " : "Next power: ") + data.CombatPower;
		}

		return head + "\n" + tail;
	}

	protected string GetCombatPower(int level) {
		level--;
		//return (level < 0 ? CombatPower : Levels[level >= Levels.Count ? Levels.Count - 1 : level].CombatPower).ToString();
		return (level < 0 ? Levels[0].CombatPower : Levels[level >= Levels.Count ? Levels.Count - 1 : level].CombatPower).ToString();
	}
}

[Serializable]
public class hipLevelData {
	public int CombatPower;
	public int Coin;
	public int Gem;
}
