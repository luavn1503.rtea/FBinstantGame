﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData : ScriptableObject {
    public int FuelCost = 2;
    public int CoinReward = 1000;
    public int ReFightCoinReward = 1000;
    public int CombatPower = 1000;
    public int EvolveShipNum;
    public int EvolveShipId;
    public int EvolveDroneNum;
    public int EvolveDroneId;

    public List<WaveData> Waves = new List<WaveData>();

    public int GetPartNum(int curLvlIndex, int curLvl, bool isShip) {
        if(isShip) {
            return curLvl == curLvlIndex ? EvolveShipNum : 0;
        }

        return curLvl == curLvlIndex ? EvolveDroneNum : 0;
    }

    public int GetPartNum(bool isSameLvl, bool isShip) {
        if(isShip) {
            return isSameLvl ? EvolveShipNum : 0;
        }

        return isSameLvl ? EvolveDroneNum : 0;
    }
}

[Serializable]
public class CombatDiff {
    // Data modifi
    public float TimeFireRate;
    public float FireRateDecrease;

    public float TimeEnemyAttack;
    public float EnemyAttackDecease;

    public bool IsIncreaseNumberFire;
    public int TimeIncreaseNumberFire;
}

[Serializable]
public class WaveData {
    //public float DamScale = 1;
    public WaveType WaveType = WaveType.Enemy;

    #region EnemyData
    // Fire rate
    public float FireRate = 1;
    // Scale HP
    public float Hp = 1;
    // Number items of wave
    public ItemsWaveData Items = new ItemsWaveData();
    public bool HasPower;
    // No dam when appear
    public bool NoDam;
    // Xếp đội hình
    public bool IsTeam = true;
    // Auto destroy when moving finish
    public bool DestroyOnFinish = false;
    // Idle move type
    public IdleType IdleType;
    // Time idle move
    public float TimeMove = 1;
    // Idle move easy
    public DG.Tweening.Ease Ease;
    // List path appear
    public List<PathInfor> Paths = new List<PathInfor>();
    // List enemies
    public List<EnemyRowData> Enemies;
    // Gift box data
    public GiftBoxWaveData GiftBox = new GiftBoxWaveData();
    // Number of enemies attact when idle state
    public int NumEnemyAttack;
    // Tốc độ di chuyển khi tấn công
    public float SpeedMoveAttack = 5;
    // Khoảng cách thời gian giữa 2 con tấn công
    public float DistanceTimeAttack = .3f;
    // Khoảng thời gian giữa 2 đợt tấn công
    public float LoopTimeAttack = 2;

    public CombatDiff CombatDiff;
    #endregion

    #region Boss Data
    // BOSS
    public BossHpsInfor Boss = new BossHpsInfor();
    #endregion

    #region RockData
    // ROCK
    public RockData Rock = new RockData();

    #endregion

    #region Warning
    // Warning
    public bool UseWarning;
    public List<Vector3> WarningPositions;

    #endregion


    // Other
    // - Use for editer (Collslap)
    public bool IsShow = true;

    /// <summary>
    /// Enemy wave data
    /// </summary>
    public WaveData() {
        Enemies = new List<EnemyRowData>();
        for(int i = 0; i < 7; i++) {
            EnemyRowData r = new EnemyRowData();
            for(int j = 0; j < 7; j++)
                r.Rows.Add(new EnemyWaveData());
            Enemies.Add(r);
        }
        CombatDiff = new CombatDiff();
    }
}
[Serializable]
public class EnemyRowData {
    public List<EnemyWaveData> Rows = new List<EnemyWaveData>();
}

[Serializable]
public class EnemyWaveData {
    public int EnemyId = -1;
    public int PathId;
}

[Serializable]
public class RockData {
    public Vector3 StartPosition;
    public Vector3 EndPosition;
    public float Range = 3;
    public float Speed = 3;

    //// Falling angle
    //public float Angle;
    // Total small rocks
    public int NumSmall;
    // Total normal rocks
    public int NumNormal;
    // Total big rocks
    public int NumBig;
    // Total time to spawn rock
    public float Duration = 5;
    
    public int GetTotalRock() {
        return NumSmall + NumNormal + NumBig;
    }
}

public enum WaveType {
    Enemy, Rock, Boss, GiftBox
}

public enum IdleType {
    NotMove, StayAndJump, TwoPartX, TwoPartY, WaveJump, Random, Switch, RandomSwitch, Around, MoveX, MoveXY
}


public enum ItemType {
    Power, LevelUp, Ship, Halloween
}

public enum RockType {
    Small,
    Normal,
    Big
}

[Serializable]
public class PathInfor {
    public float DelayTime;
    public float Distance = .3f;
    public float MoveSpeed = 4;
    public int Index;
    public Vector2 Offset;
    public bool FlipX;
    public bool FlipY;
    public List<Cell> Cells = new List<Cell>();

    public void AddCell(Cell cell) {
        if(Cells.Find(c => c.Row == cell.Row && c.Coll == cell.Coll) == null)
            Cells.Add(cell);
    }

    public void RemoveCell(Cell cell) {
        Cell rCell = Cells.Find(c => c.Row == cell.Row && c.Coll == cell.Coll);
        if(rCell != null)
            Cells.Remove(rCell);
    }
    public void RemoveCell(int row, int coll) {
        Cell rCell = Cells.Find(c => c.Row == row && c.Coll == coll);
        if(rCell != null)
            Cells.Remove(rCell);
    }

    public bool HasCell(int row, int coll) {
        Cell rCell = Cells.Find(c => c.Row == row && c.Coll == coll);
        if(rCell != null)
            return true;
        return false;
    }

    public int GetIndex(int row, int coll) {
        Cell rCell = Cells.Find(c => c.Row == row && c.Coll == coll);
        if(rCell != null)
            return Cells.IndexOf(rCell);
        return -1;
    }
}

[Serializable]
public class Cell {
    public int Row = 0;
    public int Coll = 0;
}


[Serializable]
public class ItemsWaveData {
    public List<ItemWaveData> List = new List<ItemWaveData>();
    // Số lượng enemy có thể có item, tính theo thứ tự enemy chết
    public int NumEnemiesCanHaveItem;
}


[Serializable]
public class ItemWaveData {
    public ItemType Type;
    public int Num;
}

[Serializable]
public class GiftBoxWaveData {
    public int NumGiftBox;
    public float Duration;
    public float Speed;
}

[Serializable]
public class BossHpsInfor {
    public int BossIndex = 1;
    public List<BossHpInfor> Hps = new List<BossHpInfor>();
}

[Serializable]
public class BossHpInfor {
    public int Hp;
    public int NumItem;
}