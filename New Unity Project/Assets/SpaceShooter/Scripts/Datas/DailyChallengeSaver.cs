﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyChallengeSaver {
	private const string Key = "DailyChallengeSaver";
	public List<DailyChallengeInfor> DailyInfor;

	public void SaveData() {
		PlayerPrefs.SetString(Key, JsonUtility.ToJson(this));
	}

	public void LoadData() {
		string data = PlayerPrefs.GetString(Key);
		if (data != string.Empty) {
			var saver = JsonUtility.FromJson<DailyChallengeSaver>(data);
			DailyInfor = saver.DailyInfor;
		}

		if (DailyInfor == null || DailyInfor.Count == 0) {
			DailyInfor = new List<DailyChallengeInfor>();
			for (int i = 0; i < ManagerData.Instance.DailyChallengeData.LsChallenge.Count; i++) {
				DailyInfor.Add(new DailyChallengeInfor());
			}
		}
	}

	public void SetDailyChallenge() {
		for (int i = 0; i < DailyInfor.Count; i++) {
			DailyInfor[i].Lvl = ManagerData.Instance.DailyChallengeData.RandomLvl(i);
			DailyInfor[i].IsDone = false;
		}
		SaveData();
	}

	public void SetDoneChallenge(int curLvl) {
		for (int i = 0; i < DailyInfor.Count; i++) {
			if (curLvl == DailyInfor[i].Lvl && !DailyInfor[i].IsDone) {
				DailyInfor[i].IsDone = true;
				ManagerData.Instance.Inventory.AddExTicket(ManagerData.Instance.DailyChallengeData.LsChallenge[i].Reward, "Ingame", "Campagin", curLvl.ToString());
				break;
			}
		}
		SaveData();
	}

	public bool IsChallenge(int lvl) {
		return DailyInfor.Exists(infor => infor.Lvl == lvl);
	}
}

[Serializable]
public class DailyChallengeInfor {
	public int Lvl;
	public bool IsDone;
}
