﻿using Gemmob;
using Gemmob.Common;
using UnityEngine;

public class CountDownController : SingletonFreeAlive<CountDownController>
{
    public LocalCountDown CountDownFree;
    public LocalCountDown CountDownVideo;
    public LocalCountDown CountDownFuel;
    public LocalCountDown CountDownBuyShip;
    public LocalCountDown CountDownBox;
    public LocalCountDown CountDownPacks;
	public LocalCountDown CountDownHalloween;
	public LocalCountDown CountDownResetMarket;
	public LocalCountDown CountDownResetMarketByAds;
	public LocalCountDown CountDownHomeRandomReward;
	public LocalCountDown CountDownFreeGold;
    public LocalCountDown CountDownFreeGem;
    public LocalCountDown CountDownFreeFuel;
    internal LocalCountDown countDownFree;
    internal LocalCountDown countDownVideo;

    void Start()
    {
        if (ManagerData.Instance)
        {
            ManagerData.Instance.CurrentLoad();
         //   CountDownFuel.OnComplete += GameData.Instance.PlayerInfo.FuelRecovery;
        }
    }

    public void LoadData()
    {
        if (CountDownVideo)
            CountDownVideo.LoadData();
        if (CountDownFree)
            CountDownFree.LoadData();
        if (CountDownFuel)
            CountDownFuel.LoadData();
        if (CountDownBuyShip)
            CountDownBuyShip.LoadData();
        if (CountDownFreeGold)
            CountDownFreeGold.LoadData();
        if (CountDownFreeGem)
            CountDownFreeGem.LoadData();
        if (CountDownFreeFuel)
            CountDownFreeFuel.LoadData();
        if (CountDownBox)
            CountDownBox.LoadData();
        if(CountDownPacks)
            CountDownPacks.LoadData();
	    if (CountDownHalloween)
		    CountDownHalloween.LoadData();
	    if (CountDownResetMarket) 
			CountDownResetMarket.LoadData();
	    if (CountDownResetMarketByAds) 
			CountDownResetMarketByAds.LoadData();
	    if (CountDownHomeRandomReward) {
			CountDownHomeRandomReward.LoadData();
	    }
    }
    
}
