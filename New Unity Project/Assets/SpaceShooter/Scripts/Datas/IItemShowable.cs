﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemShowable {
    int OrderInList { get; }
    Sprite IconShow { get; }
    string NameShow { get; }
    string DescriptionShow { get; }
    int NumberStarShow { get; }
    string LevelShow { get; }
    string Info { get; }
    string GetBonusLevel { get; }
    string GetShortInfo { get; }
    bool IsSelected { set; get; }
    CardType CardType { get; }
    bool IsEquip { get; }
    int BorderShow { get; }
}
