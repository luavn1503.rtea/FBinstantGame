﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackMarketSlotData {
	private const string Key = "CurBlackMarketSlot";
	public List<bool> IsUnlockSlot;
	public List<int> SlotInforId;
	public List<int> AdsWatched;
	public List<bool> IsSold;

	public void SaveData() {
		PlayerPrefs.SetString(Key, JsonUtility.ToJson(this));
	}

	public void LoadData() {
		string data = PlayerPrefs.GetString(Key);
		if (data != string.Empty) {
			var slotData = JsonUtility.FromJson<BlackMarketSlotData>(data);
			IsUnlockSlot = slotData.IsUnlockSlot;
			SlotInforId = slotData.SlotInforId;
			AdsWatched = slotData.AdsWatched;
			IsSold = slotData.IsSold;
		}
		BlackMarketData bmData = ManagerData.Instance.BlackMarketData;
		if (IsUnlockSlot == null || IsUnlockSlot.Count == 0) {
			IsUnlockSlot = new List<bool>();
			SlotInforId = new List<int>();
			AdsWatched = new List<int>();
			IsSold = new List<bool>();
			for (int i = 0; i < bmData.Slot.Count; i++) {
				IsUnlockSlot.Add(bmData.Slot[i].NoAdsUnlockNum < 0 || bmData.Slot[i].UnlockNum < 0);
				SlotInforId.Add(bmData.Slot[i].GetInforId());
				AdsWatched.Add(0);
				IsSold.Add(false);
			}
			SaveData();
		}

		if (bmData.Slot.Count > IsUnlockSlot.Count) {
			for (int i = IsUnlockSlot.Count; i < bmData.Slot.Count; i++) {
				IsUnlockSlot.Add(bmData.Slot[i].NoAdsUnlockNum < 0 || bmData.Slot[i].UnlockNum < 0);
				SlotInforId.Add(bmData.Slot[i].GetInforId());
				AdsWatched.Add(0);
				IsSold.Add(false);
			}
			SaveData();
		}

		//if (SlotInforId == null || SlotInforId.Count == 0) {
		//	SlotInforId = new List<int>();
		//	for (int i = 0; i < bmData.Slot.Count; i++) {
		//		SlotInforId.Add(bmData.Slot[i].GetInforId());
		//	}
		//	SaveData();
		//}

		//if (AdsWatched == null || AdsWatched.Count == 0) {
		//	for (int i = 0; i < bmData.Slot.Count; i++) {
		//		AdsWatched.Add(0);
		//	}
		//}
	}

	public void ResetMarket() {
		BlackMarketData data = ManagerData.Instance.BlackMarketData;
		for (int i = 0; i < SlotInforId.Count; i++) {
			SlotInforId[i] = data.Slot[i].GetInforId();
			IsSold[i] = false;
			AdsWatched[i] = 0;
		}

		SaveData();
	}

	public int GetSlotInforId(int id) {
		return SlotInforId[id];
	}

	public bool CheckUnlock(int id) {
		return IsUnlockSlot[id];
	}

	public int GetAdsWatched(int id) {
		return AdsWatched[id];
	}

	public void UnlockSlot(int id) {
		IsUnlockSlot[id] = true;
		SaveData();
	}

	public void WatchAds(int id) {
		AdsWatched[id] += 1;
		SaveData();
	}

	public void Sold(int id) {
		IsSold[id] = true;
		SaveData();
	}

	public bool GetSold(int id) {
		return IsSold[id];
	}
}
