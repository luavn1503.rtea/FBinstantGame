﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillsInfo {
    private readonly string skillsKey = "skillsData";
    public List<SkillDataInfo> skillsInfo = new List<SkillDataInfo>();
    public SkillDataInfo GetSkillInfo(EnumDefine.IDSkill idSkill) {
        var dataInfo = skillsInfo.Find(x => x.IDSkill == idSkill);
        if(dataInfo == null) {
            Debug.Log(string.Format("Skill {0} chưa có data", idSkill));
            return new SkillDataInfo(idSkill, -1);
        }
        return dataInfo;
    }

    public bool BuySkill(EnumDefine.IDSkill idSkill, bool save = true) {
        var dataInfo = GetSkillInfo(idSkill);
        var result = dataInfo.Buy();
        if(save) { Save(); }
        return result;
    }

    public void UpgradeSkill(EnumDefine.IDSkill idSkill, int value = 1, bool save = true) {
        var dataInfo = GetSkillInfo(idSkill);
        dataInfo.Upgrade(value);
        if(save) { Save(); }
    }

    public SkillsInfo LoadData() {
        SkillsInfo data = this;
        if(PlayerPrefs.HasKey(skillsKey)) {
            string jsonData = PlayerPrefs.GetString(skillsKey);
            data = JsonUtility.FromJson<SkillsInfo>(jsonData);
        }
        else {
            Save();
        }
        return data;
    }


    public SkillsInfo() {
        skillsInfo = new List<SkillDataInfo>();
        foreach(var item in SkillsData.Instance.Skills) {
            skillsInfo.Add(new SkillDataInfo(item.ID, -1));
        }
    }

    public void Save() {
        PlayerPrefs.SetString(skillsKey, JsonUtility.ToJson(this));
    }
}

[Serializable]
public class SkillDataInfo {
    [SerializeField] private EnumDefine.IDSkill idSkill = EnumDefine.IDSkill.Shield;
    [SerializeField] private int level = -1;
    public SkillDataInfo(EnumDefine.IDSkill idSkill, int level) {
        this.idSkill = idSkill;
        this.level = level;
    }
    public EnumDefine.IDSkill IDSkill => idSkill;
    public int Level => level;

    public bool IsUnlock() {
        return level >= 0;
    }

    public bool Buy() {
        if(level > 0) { return false; }
        level = 0;
        return true;
    }

    public void Upgrade(int value) {
        level += value;
    }
}