﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[CreateAssetMenu(fileName = "UnlockLevelModeData", menuName = "Datas/UnlockLevelModeData")]
public class UnlockLevelModeData : ScriptableObject
{
    private static UnlockLevelModeData instance;
    public static UnlockLevelModeData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<UnlockLevelModeData>("UnlockLevelModeData");
                return instance;
            }
            return instance;
        }

        set {
            instance = value;

        }
    }

    public UnlockLevelMode UnlockLevelEndlessMode;
    public UnlockLevelMode UnlockLevelTrialMode;
    public UnlockLevelMode UnlockLevelHalloweenMode;
    public UnlockLevelMode UnlockLevelBossBattleMode;
}

[Serializable]
public class UnlockLevelMode
{
    public int LevelUnlock;
    public Sprite Icon;
}
