﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "IapShipData", menuName = "Datas/IapShipData")]
public class IapShipData : ScriptableObject
{
    private static IapShipData instance;
    public static IapShipData Instance {
        get {
            if (instance == null) {
                instance = Resources.Load<IapShipData>("IapShipData");
                return instance;
            }
            return instance;
        }

        set {
            instance = value;
        }
    }

    public List<ShopShipInfo> shopShipInfos;
}

[System.Serializable]
public class ShopShipInfo {
    public AllRewardType EarnType;
    public int ExtendId;
    public int Value;
    public string IapKey;
    public string Content;
    public List<RewardUnlockedInfo> rewards;
}

[System.Serializable]
public class RewardUnlockedInfo {
    public AllRewardType RewardType;
    public int ExtendId;
    public string Name;
    public Sprite Icon;
    public Sprite CategoryIcon;
    public int Number;
}


