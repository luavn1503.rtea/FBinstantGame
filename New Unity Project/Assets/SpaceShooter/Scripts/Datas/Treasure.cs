﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public static class Treasure {
	public const string BadTimeKey = "BadTimeKey";
	public const string BadNumKey = "BadNumKey";
	public const string NormalTimeKey = "NormalTimeKey";
	public const string NormalNumKey = "NormalNumKey";
	public const string GoodTimeKey = "GoodTimeKey";
	public const string GoodNumKey = "NgonNumKey";

	public const string FirstTimeKey = "FirstTimeKey";

	public static int isFirstTime;

	public static PlayerTreasure bad = new PlayerTreasure();
	public static PlayerTreasure normal = new PlayerTreasure();
	public static PlayerTreasure good = new PlayerTreasure();

	//public Dictionary<TreasureType, OwnedTreasure> dicTreasure = new Dictionary<TreasureType, OwnedTreasure>()
	//{
	//	{TreasureType.Bad, new OwnedTreasure()},
	//	{TreasureType.Normal, new OwnedTreasure()},
	//	{TreasureType.Good, new OwnedTreasure()}
	//};

	public static void IsFirstTime() {
		isFirstTime = PlayerPrefs.GetInt(FirstTimeKey, 0);
		if (isFirstTime == 0) {
			isFirstTime = 1;
			PlayerPrefs.SetInt(FirstTimeKey, isFirstTime);
			PlayerPrefs.Save();
		}
	}

	private static void LoadData(ref PlayerTreasure player, string timeKey, string numKey) {
		player.time = PlayerPrefs.GetInt(timeKey);
		player.num = PlayerPrefs.GetInt(numKey);
	}

	public static void LoadData() {
		LoadData(ref bad, BadTimeKey, BadNumKey);
		LoadData(ref normal, NormalTimeKey, NormalNumKey);
		LoadData(ref good, GoodTimeKey, GoodNumKey);
		isFirstTime = PlayerPrefs.GetInt(FirstTimeKey);
	}

	private static void SaveData(PlayerTreasure player, string timeKey, string numKey) {
		PlayerPrefs.SetInt(timeKey, player.time);
		PlayerPrefs.SetInt(numKey, player.num);
	}

	public static void SaveData() {
		SaveData(bad, BadTimeKey, BadNumKey);
		SaveData(normal, NormalTimeKey, NormalNumKey);
		SaveData(good, GoodTimeKey, GoodNumKey);
		PlayerPrefs.Save();
	}

	public static void ResetTime() {
		bad.time = 0;
		normal.time = 0;
		good.time = 0;
	}

	public static void AddBox(PlayerTreasure kindTreasure, int num) {
		PlayerTreasure playerTreasure = kindTreasure.Equals(bad) ? bad :
			kindTreasure.Equals(normal) ? normal :
			kindTreasure.Equals(good) ? good : null;
		if (playerTreasure != null) {
			LoadData();
			playerTreasure.num += num;
            Debug.LogFormat("Add Kind Treasure {0} , num {1}", kindTreasure.ToString(), num);
			SaveData();
		}
	}
}


