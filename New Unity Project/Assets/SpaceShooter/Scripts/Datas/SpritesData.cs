﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpritesData", menuName = "Data/SpritesData")]
public class SpritesData : ScriptableObject {
    public Sprite GoldIcon;
    public Sprite GemIcon;
    public Sprite FuelIcon;
    public Sprite Item3;
    public Sprite Item5;
    public Sprite Item10;
    public Sprite Bomb;
    [SerializeField] private List<BorderRank> borderRanks = new List<BorderRank>();
    private static SpritesData instance;
    public static SpritesData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<SpritesData>("SpritesData");
                return instance;
            }
            return instance;
        }

        set {
            instance = value;
        }
    }

    public Sprite GetBorderRank(EnumDefine.Rank rank) {
        var data = borderRanks.Find(x => x.Rank == rank);
        if (data == null) {
            Debug.Log(string.Format("Border Rank {0} not set DATA", rank));
            return null;
        }
        return data.SpriteBorder;
    }

    [Serializable]
    public class BorderRank {
        [SerializeField] private EnumDefine.Rank rank = EnumDefine.Rank.D;
        [SerializeField] private Sprite sprBorder = null;
        public EnumDefine.Rank Rank => rank;
        public Sprite SpriteBorder => sprBorder;
    }
}
