﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Xml.Linq;
using Gemmob;
using UnityEngine;

[CreateAssetMenu(fileName = "ShipsData", menuName = "Data/ShipsData")]
public class ShipsData : ScriptableObject {
    private static ShipsData instance;
    public List<ShipData> Ships = new List<ShipData>();
    [SerializeField] private ShipsUpgradeData ship = null;
    [SerializeField] private DronesUpgradeData drone = null;
    public static ShipsData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<ShipsData>("Data/ShipsData");
                return instance;
            }
            return instance;
        }
        
    }
    public ShipData GetUpgradeShip(EnumDefine.IDShip IDShip) {
        var data = Ships.Find(x => x.ID == IDShip);
        if(data == null) {
            Debug.Log(string.Format("Drone {0} not yet", IDShip));
            return new ShipData();
        }
        return data;
    }
    

    [ContextMenu("adw")]
    private void ConvertD() {
        for(int i = 0; i < ship.Ships.Count; i++) {
            for(int ki = 0; ki < ship.Ships[i].Levels.Count; ki++) {
                Ships[i].Levels[ki].statses = new Statses();
                Ships[i].Levels[ki].statses.data = new List<Stats>() { new Stats(EnumDefine.IDStats.Power, ship.Ships[i].Levels[ki].CombatPower) };
            }
        }
        for(int i = ship.Ships.Count; i < drone.Drones.Count + ship.Ships.Count; i++) {
            for(int ki = 0; ki < drone.Drones[i - ship.Ships.Count].Levels.Count; ki++) {
                Ships[i].Levels[ki].statses = new Statses();
                Ships[i].Levels[ki].statses.data = new List<Stats>() { new Stats(EnumDefine.IDStats.Power, drone.Drones[i - ship.Ships.Count].Levels[ki].CombatPower) };
            }
        }
    }
}


[Serializable]
public class ShipData {
    [SerializeField] private EnumDefine.IDShip IDShip = EnumDefine.IDShip.Ship_01;
    [SerializeField] private EnumDefine.TypeShip TypeShip = EnumDefine.TypeShip.Ship;
    [SerializeField] private List<Sprite> Icons = new List<Sprite>();
    [SerializeField] private Sprite ClassIcon;
    [SerializeField] private Sprite EvolveItemIcon;
    [SerializeField] private string Name;
    [SerializeField] private string Description;
    [SerializeField] private float BulletDame;
    [SerializeField] private int Star = 1;
    [SerializeField] private Region Region;
    [SerializeField] private SpecialEvent Event;
    /*[SerializeField] private*/ public List<ShipLevelData> Levels = new List<ShipLevelData>();
    [SerializeField] private int GemPrice = 5;
    [SerializeField] private int CoinPrice = 100;
    [SerializeField] private int VideoCount = 0;
    [SerializeField] private int LevelUnlock;
    [SerializeField] private string BuyDescription;
    [SerializeField] private UIShipPreview shipPreview = null;
    [SerializeField] private List<AircraftEvolveData> EvolveDatas = new List<AircraftEvolveData>();

    public ShipData() {
        Levels = new List<ShipLevelData>();
        for(int i = 0; i < 20; i++) {
            Levels.Add(new ShipLevelData());
        }
    }

    public ShipData(EnumDefine.IDShip IDShip, EnumDefine.TypeShip TypeShip, List<Sprite> Icons, Sprite ClassIcon, Sprite EvolveItemIcon,
        string Name, string Description, float BulletDame, int Star, List<ShipLevelData> Levels, List<ShipLevelData> OldLevels, Region Region, SpecialEvent Event,
        int GemPrice, int CoinPrice, int VideoCount, int LevelUnlock, string BuyDescription, UIShipPreview shipPreview, List<AircraftEvolveData> EvolveDatas) {
        this.IDShip = IDShip;
        this.TypeShip = TypeShip;
        this.Icons = Icons;
        this.ClassIcon = ClassIcon;
        this.EvolveItemIcon = EvolveItemIcon;
        this.Name = Name;
        this.Description = Description;
        this.BulletDame = BulletDame;
        this.Star = Star;
        this.Levels = Levels;
        this.Region = Region;
        this.Event = Event;
        this.GemPrice = GemPrice;
        this.CoinPrice = CoinPrice;
        this.VideoCount = VideoCount;
        this.LevelUnlock = LevelUnlock;
        this.BuyDescription = BuyDescription;
        this.shipPreview = shipPreview;
        this.EvolveDatas = EvolveDatas;
    }

    public EnumDefine.IDShip ID => IDShip;

    public EnumDefine.TypeShip TYPE => TypeShip;

    public string GetName => Name;

    public int GetGemPrice => GemPrice;

    public int GetCoinPrice => CoinPrice;

    public int GetLevelUnlock => LevelUnlock;

    public int TotalLevel => Levels.Count;

    public ShipLevelData GetLevelData(int level) => Levels[level];

    public UIShipPreview GetShipPreview => shipPreview;

    public Sprite GetIcon(EnumDefine.Rank rank) {
        return Icons[(int)rank];
    }

    public AircraftEvolveData GetDrebrisEvolve(EnumDefine.Rank rank) {
        return EvolveDatas[(int)rank];
    }

    public int MaxLevel => Levels.Count - 1;

    public EnumDefine.Rank MaxRank => (EnumDefine.Rank)(EvolveDatas.Count - 1);

    public int MaxLevelInRank(EnumDefine.Rank rank) {
        var maxLevel = 0;
        for(int i = 0; i < EvolveDatas.Count; i++) {
            if(i <= (int)rank) {
                maxLevel += EvolveDatas[i].MaxLvl;
            }
        }
        return maxLevel;
    }
}

public class AircraftDataShow : ShipData {
    public int Order;
    public int CurrentLevel;
    public int VideoSeen;
    public bool IsUnlock;
    public EnumDefine.Rank Rank;
}

public enum Region {
    All,
    USA
}

public enum SpecialEvent {
    None = -1,
    Halloween,
    DailyChallenge,
    Christmas
}

[Serializable]
public class ShipLevelData {
    [SerializeField] private int coin  = default;
    [SerializeField] private int gem  = default;
    public Statses statses = new Statses();
    public ShipLevelData() {

    }
    public ShipLevelData(int coin, int gem, Statses statses) {
        this.coin = coin;
        this.gem = gem;
        this.statses = statses;
    }
    public int Coin => coin;
    public int Gem => gem;
}

[Serializable]
public class AircraftEvolveData {
    public int MaxLvl;
    public int ItemNum;
    public int GemNum;
    public List<AircraftEvolveInfor> EvolveInfor = new List<AircraftEvolveInfor>();

    public AircraftEvolveData() {

    }

    public AircraftEvolveData(int maxLvl, int itemNum, int gemNum) {
        MaxLvl = maxLvl;
        ItemNum = itemNum;
        GemNum = gemNum;
    }

    public AircraftEvolveData(int maxLvl) {
        MaxLvl = maxLvl;
    }

    public AircraftEvolveData(int maxLvl, int itemNum, int gemNum, int count, bool isShip = true) {
        MaxLvl = maxLvl;
        ItemNum = itemNum;
        GemNum = gemNum;
        if(isShip) {
            switch(count) {
                case 0:
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpMaxLvl));
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpStar));
                    break;
                case 1:
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpMaxLvl));
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpStar));
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.ChangeBullet));
                    break;
                case 2:
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpMaxLvl));
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpStar));
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.ChangeSuper));
                    break;
                case 3:
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpMaxLvl));
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpStar));
                    EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpSuperDur));
                    break;
            }
        }
        else {
            EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpMaxLvl));
            EvolveInfor.Add(new AircraftEvolveInfor(EvolveInforType.UpStar));
        }
    }
}

[Serializable]
public class AircraftEvolveInfor {
    public EvolveInforType Type;

    public AircraftEvolveInfor() {

    }

    public AircraftEvolveInfor(EvolveInforType type) {
        Type = type;
    }
}

public enum EvolveInforType {
    ChangeBullet, ChangeSuper, UpSuperDur, UpDam, UpSpeed,
    UpMaxLvl = 99,
    UpStar = 100
}
