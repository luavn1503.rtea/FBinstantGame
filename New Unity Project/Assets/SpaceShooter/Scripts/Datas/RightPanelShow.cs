﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using DG.Tweening;
//using Gemmob.Common;
//using UnityEngine;
//using UnityEngine.UI;
//using Gemmob;

//public class RightPanelShow : MonoBehaviour {

//	[Serializable]
//	public class BtnEvent {
//		public Sprite Icon;
//		public string Title;
//	}

//	private IItemShowable _data;

//	[Header("Text infor")]
//	[SerializeField] private Text Name;
//	[SerializeField] private Text BuyDescription;
//	[SerializeField] private Text CardDescription;
//	[SerializeField] private Text Level;
//	[SerializeField] private Transform Star;
//	[SerializeField] private Text _txtCurCp;
//	[SerializeField] private Text _txtNextCp;
//	[SerializeField] private GameObject _cpGroup;

//	[Header("Text upgrade")]
//	[SerializeField] private GameObject TxtUpgrade;
//	[SerializeField] private GameObject TxtMax;
//	[SerializeField] private GameObject TxtBuy;

//	//[Header("Button")]
//	//[SerializeField] private GameObject BtnGem;
//	//[SerializeField] private GameObject BtnCoin;
//	//[SerializeField] private GameObject BtnVideo;
//	//[SerializeField] private GameObject BtnUpgrade;

//	private Text _txtCoinUpgrade;
//	private Text _txtGemUpgrade;
//	private Text _txtVideoUpgrade;
//	private Text _txtBuyShip;
//	[SerializeField] private Text _txtCountDownVideo;

//	[Header("Button Event")]
//	[SerializeField] private ButtonAnimation _btaCoinUpgrade;
//	[SerializeField] private ButtonAnimation _btaGemUpgrade;
//	[SerializeField] private ButtonAnimation _btaVideoUpgrade;
//	[SerializeField] private ButtonAnimation _btaCardUpgrade;
//	[SerializeField] private ButtonAnimation _btaEvent;
//	[SerializeField] private ButtonAnimation _btaDescript;
//	[SerializeField] private ButtonAnimation _btaAircraftEvolve;
//	[SerializeField] private ButtonAnimation _btaBuyShip;
//	[SerializeField] private GameObject _descriptPanel;

//	[SerializeField] private Sprite _inforImg;
//	[SerializeField] private Sprite _closeImg;

//	[Header("DescriptGroup")]
//	[SerializeField] private Text _txtName;
//	[SerializeField] private Image _classIcon;
//	[SerializeField] private Text _txtDescript;
//	[SerializeField] private Image _classIconInfor;

//	private Image _videImage;
//	private LocalCountDown _cdVideo;
//	public Action UpdateContentViewInfo;
//	public Action ShowUpgradeEfx;
//	[SerializeField] private List<BtnEvent> _lsBtnEvent;

//	private AircraftDataShow _aircraftData;
//	private ShopShipInfo _shopShipInfo;


//	private void AssignTextInButton() {
//		_txtCoinUpgrade = _btaCoinUpgrade.gameObject.GetComponentInChildren<Text>();
//		_txtCoinUpgrade.color = DataConfig.Instance.DolarColor;
//		_txtGemUpgrade = _btaGemUpgrade.gameObject.GetComponentInChildren<Text>();
//		_txtGemUpgrade.color = DataConfig.Instance.BlueTxtColor;
//		_txtVideoUpgrade = _btaVideoUpgrade.gameObject.GetComponentInChildren<Text>();
//		_txtBuyShip = _btaBuyShip.gameObject.GetComponentInChildren<Text>();
//	}
//	private void AssignEventToButton() {

//		_btaCoinUpgrade.onClick.AddListener(ButtonUpgradeCoin);
//		_btaGemUpgrade.onClick.AddListener(ButtonUpgradeGem);
//		_btaVideoUpgrade.onClick.AddListener(ButtonUpgradeVideo);
//		//_btaCardUpgrade.onClick.AddListener(ButtonUpgradeCard);
//		//_btaDescript.onClick.AddListener(ButtonDescriptClick);
//		//_btaAircraftEvolve.onClick.AddListener(ButtonEvolveClick);
//		//      _btaBuyShip.onClick.AddListener(ButtonBuyShip);
//		AssignTextInButton();
//	}

//	private void Awake() {
//		AssignEventToButton();
//	}

//	void Start() {
//		_videImage = _btaVideoUpgrade.GetComponent<Image>();
//		_cdVideo = CountDownController.Instance.CountDownBuyShip;

//		_cdVideo.OnTick += () => {
//			_btaVideoUpgrade.interactable = _cdVideo.Done;
//			_videImage.color = _cdVideo.Done ? Color.white : Color.gray;
//			_txtCountDownVideo.text = _cdVideo.Done ? "" : _cdVideo.GetTime();
//			//if (_aircraftData != null)
//			//	_txtVideoUpgrade.text = _aircraftData.VideoSeen + " / " + _aircraftData.VideoCount;
//		};

//		_cdVideo.OnStateChanged += () => {
//			_btaVideoUpgrade.interactable = _cdVideo.Done;
//			_videImage.color = _cdVideo.Done ? Color.white : Color.gray;
//			_txtCountDownVideo.text = _cdVideo.Done ? "" : _cdVideo.GetTime();
//		};
//	}

//	public void UpdateInformation(IItemShowable newData) {
//		_data = newData;
//		//ClearAllButton();
//		//ClearAllText();

//		if(newData == null) {
//			Name.text = string.Empty;
//			BuyDescription.text = string.Empty;
//			Level.text = string.Empty;
//			Star.gameObject.SetActive(false);
//			_cpGroup.SetActive(false);
//			_classIconInfor.gameObject.SetActive(false);
//			_classIconInfor.transform.parent.gameObject.SetActive(false);
//			Level.transform.parent.gameObject.SetActive(false);
//			Star.parent.gameObject.SetActive(false);
//			CardDescription.gameObject.SetActive(false);
//			return;
//		}

//		_classIconInfor.gameObject.SetActive(true);
//		Level.transform.parent.gameObject.SetActive(true);
//		Star.parent.gameObject.SetActive(true);
//		//_txtCurCp.text = newData.GetCurCp;
//		//_txtNextCp.text = newData.GetNextCp;
//		Name.text = newData.NameShow;
//		//BuyDescription.text = newData.DescriptionShow;


//		if(newData is CardInfo) {
//			//BtnUpgrade.SetActive(true);
//			_btaCardUpgrade.gameObject.SetActive(true);
//			_btaDescript.gameObject.SetActive(false);
//			_cpGroup.SetActive(false);
//			//TxtUpgrade.SetActive(true);
//			_classIconInfor.transform.parent.gameObject.SetActive(false);
//			CardDescription.gameObject.SetActive(true);
//			BuyDescription.gameObject.SetActive(false);
//			CardDescription.text = newData.DescriptionShow;
//			Level.text = string.Format("Lvl {0}/10", newData.LevelShow);
//			if(newData.LevelShow.Equals("10")) {
//				//	SetCardUpgradeBtn(Color.clear, "MAX", Color.green, false);
//			}
//			else {
//				//SetCardUpgradeBtn(Color.white, "UPGRADE", new Color(140 / 255f, 59 / 255f, 0, 1), true);
//			}
//		} else {
//			var data = (AircraftDataShow)newData;
//			_btaDescript.gameObject.SetActive(true);
//			CardDescription.gameObject.SetActive(false);
//			//	SetTxtDescriptGroup(newData.NameShow, data.ClassIconShow, newData.DescriptionShow);
//			Level.text = newData.LevelShow;
//			if(data == null)
//				return;

//			if(data.CurrentLevel == -1) {
//				ShowBuy(data);
//				//SetCpShow(false, data);
//				BuyDescription.gameObject.SetActive(true);
//				BuyDescription.text = data.BuyDescriptionShow;
//			}
//			else if(data.CurrentLevel < data.Levels.Count) {
//				ShowUpgrade(data);
//				//SetCpShow(true, data);
//				BuyDescription.gameObject.SetActive(false);
//			}
//			else {
//				//	SetCpShow(false, data);
//				ShowMax(data);
//			}
//			ShowBuyShip(data);


//		}

//		Star.gameObject.SetActive(true);
//		for(int i = 0; i < Star.childCount; i++)
//			Star.GetChild(i).gameObject.SetActive(i < newData.NumberStarShow);
//	}

//	#region Aircraft function
//	private void ShowMax(AircraftDataShow data) {
//		TxtMax.SetActive(true);
//		BuyDescription.gameObject.SetActive(false);
//	}

//	private void ShowBuy(AircraftDataShow data) {
//		//TxtBuy.SetActive(true);
//		if(data.VideoCount > 0 && Config.IsAdsEnable) {
//			//TxtBuy.SetActive(true);
//			_btaVideoUpgrade.gameObject.SetActive(true && false);

//			//BtnVideo.SetActive(true);

//			_btaVideoUpgrade.interactable = _cdVideo.Done;
//			_videImage.color = _cdVideo.Done ? Color.white : Color.gray;
//			_txtVideoUpgrade.text = data.VideoSeen + " / " + data.VideoCount;
//		} else if(data.Event != SpecialEvent.None) {
//			//TxtBuy.SetActive(true);
//			//_btaEvent.gameObject.SetActive(true);
//			//SetBtnEvent(data.Event);
//			if(GameData.Instance.CurrentAircraftData.Ships[6].Level < 0) {
//				_btaGemUpgrade.gameObject.SetActive(false);
//				_btaCoinUpgrade.gameObject.SetActive(false);
//			}

//		} else {
//			//_btaGemUpgrade.gameObject.SetActive(true);
//			//_btaCoinUpgrade.gameObject.SetActive(true);

//			_btaGemUpgrade.gameObject.SetActive(data.CurrentLevel >= 0 || data.VideoCount > 0);
//			_btaCoinUpgrade.gameObject.SetActive(data.CurrentLevel >= 0 || data.VideoCount > 0);

//			//_txtCoinUpgrade.text = data.CoinPrice.ToString();
//			//_txtGemUpgrade.text = data.GemPrice.ToString();
//			_txtCoinUpgrade.text = PriceShower.ShowPrice(data.CoinPrice);
//			_txtGemUpgrade.text = PriceShower.ShowPrice(data.GemPrice);
//		}
//	}

//	private void ShowUpgrade(AircraftDataShow data) {
//		//TxtUpgrade.SetActive(true);
//		//_btaGemUpgrade.gameObject.SetActive(true);
//		//_btaCoinUpgrade.gameObject.SetActive(true);

//		_btaGemUpgrade.gameObject.SetActive(data.Levels[data.CurrentLevel].Gem > 0 && !data.CheckCanEvolve());
//		_btaCoinUpgrade.gameObject.SetActive(data.Levels[data.CurrentLevel].Coin > 0 && !data.CheckCanEvolve());
//		_btaAircraftEvolve.gameObject.SetActive(data.CheckCanEvolve());

//		//_txtCoinUpgrade.text = data.Levels[data.CurrentLevel].Coin.ToString();
//		//_txtGemUpgrade.text = data.Levels[data.CurrentLevel].Gem.ToString();
//		_txtCoinUpgrade.text = PriceShower.ShowPrice(data.Levels[data.CurrentLevel].Coin);
//		_txtGemUpgrade.text = PriceShower.ShowPrice(data.Levels[data.CurrentLevel].Gem);

//	}

//	private void ShowBuyShip(AircraftDataShow data) {
//		int idShipSelected = EquipmentPage.OldIndexItemSelect;
//		_btaBuyShip.gameObject.SetActive(false);
//		_shopShipInfo = null;
//		foreach(var item in GameData.Instance.IapShipData.shopShipInfos) {
//			if(idShipSelected == item.ExtendId && data.CurrentLevel < 0 && data.isShip) {
//				_btaBuyShip.gameObject.SetActive(true);
//				_shopShipInfo = item;
//				break;
//			}
//		}
//		if(_shopShipInfo != null) {
//			if(Application.internetReachability != NetworkReachability.NotReachable) {
//				_txtBuyShip.text = UnityIAP.Instance.GetLocalPrice(_shopShipInfo.IapKey).localizedPriceString;
//			} else {
//				_txtBuyShip.text = _shopShipInfo.Content;
//			}
//		}
//	}


//	public void ButtonUpgradeCoin() {
//		//[Tracking - Position]-[ButtonPress]
//		// string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
//		string btnID = "btn_buyGold";
//		//  GameTracking.Instance.TrackButtonPress(location, btnID);

//		_aircraftData = (AircraftDataShow)_data;

//		if(_aircraftData == null) return;

//		if(GameData.Instance.Upgrade(EquipmentPage.TypeItemSelected, _aircraftData.Order, true)) {
//			Debug.Log("Upgrade success");
//			UpdateContentViewInfo();
//			if(PlayerInfo.AfterLoadData != null) {
//				PlayerInfo.AfterLoadData();
//			}
//			ShowUpgradeEfx();
//		} else {
//			//UIManager.Instance.Announce.StartAnnounce("You don't have enough funds! \nDo you want to get more ?", TypeReward.Coin);
//			Debug.Log("Upgrade fail");

//			//[Tracking - Position]-[MenuShowup]
//			// GameTracking.Instance.TrackMenuShowup(UIManager.Instance.Announce.CurrentNameMenu());
//		}
//	}

//	public void ButtonUpgradeGem() {
//		//[Tracking - Position]-[ButtonPress]
//		//  string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
//		string btnID = "btn_buyGem";
//		//GameTracking.Instance.TrackButtonPress(location, btnID);

//		_aircraftData = (AircraftDataShow)_data;

//		if(_aircraftData == null) return;

//		if(GameData.Instance.Upgrade(EquipmentPage.TypeItemSelected, _aircraftData.Order, false)) {
//			Debug.Log("Upgrade success");
//			UpdateContentViewInfo();
//			if(PlayerInfo.AfterLoadData != null) {
//				PlayerInfo.AfterLoadData();
//			}
//			ShowUpgradeEfx();
//		} else {
//			//UIManager.Instance.Announce.StartAnnounce("You don't have enough funds! \nDo you want to get more ?", TypeReward.Gem);
//			Debug.Log("Upgrade fail");

//			//[Tracking - Position]-[MenuShowup]
//			//  GameTracking.Instance.TrackMenuShowup(UIManager.Instance.Announce.CurrentNameMenu());
//		}
//	}

//	public void ButtonUpgradeVideo() {
//		//[Tracking - Position]-[ButtonPress]
//		// string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
//		string btnID = "btn_watchAds";
//		// GameTracking.Instance.TrackButtonPress(location, btnID);

//		_aircraftData = (AircraftDataShow)_data;

//		if(_aircraftData == null)
//			return;
//		bool isSkipOrFail = false;
//		//[Tracking-Position]-[VideoAds]
//		//	AdsModify.ShowRewardVideo("GetShip_" + _aircraftData.Order, AdsType.VideoReward, location, "null", "null",
//		() => {
//			// complete
//			//Debug.LogErrorFormat("Before Order {0} VideoSeen {1} VideoCount {2}", _aircraftData.Order, _aircraftData.VideoSeen, _aircraftData.VideoCount);
//			GameData.Instance.WatchVideo(EquipmentPage.TypeItemSelected, _aircraftData.Order);
//			//Debug.LogErrorFormat("After Order {0} VideoSeen {1} VideoCount {2}", _aircraftData.Order, _aircraftData.VideoSeen, _aircraftData.VideoCount);
//			UpdateContentViewInfo();
//			_btaVideoUpgrade.interactable = _cdVideo.Done;
//			_videImage.color = _cdVideo.Done ? Color.white : Color.gray;
//			_txtVideoUpgrade.text = ((AircraftDataShow)_data).VideoSeen + " / " + ((AircraftDataShow)_data).VideoCount;
//			//},
//			() => {
//				// skip or fail
//				isSkipOrFail = true;
//				_cdVideo.StopImmediately();
//				_btaVideoUpgrade.interactable = _cdVideo.Done;
//				_videImage.color = _cdVideo.Done ? Color.white : Color.gray;
//				//});
//				if(isSkipOrFail)
//					return;
//				_cdVideo.StartCountDown();
//			};

//			//public void ButtonBuyShip() {
//			//    //[Tracking - Position]-[ButtonPress]
//			//   // string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
//			//    string btnID = "btn_buyIAP";
//			//   // GameTracking.Instance.TrackButtonPress(location, btnID);

//			//    UnityIAP.Instance.Buy(_shopShipInfo.IapKey, _shopShipInfo.IapKey, OnBuyShipSucess);
//			//}

//			//private void OnBuyShipSucess() {
//			//    _aircraftData = (AircraftDataShow)_data;
//			//    // unlock ship
//			//   // string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
//			//  //  GameData.Instance.PlayerInfo.GetAllRewardType(_shopShipInfo.EarnType, _shopShipInfo.ExtendId, _shopShipInfo.Value, location);
//			//    Debug.Log("buy Sucess full");
//			//    // announce
//			//  //  UIManager.Instance.Announce.StartAnnounce(_aircraftData.NameShow, "You have had unlock new ship", _aircraftData.Icon);
//			//    //[Tracking - Position]-[MenuShowup]
//			//   // GameTracking.Instance.TrackMenuShowup(UIManager.Instance.Announce.CurrentNameMenu());
//			//}
//			#endregion

//			void ButtonUpgradeCard() {
//				//[Tracking - Position]-[ButtonPress]
//				//   string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
//				string btnID = "btn_watchAds";
//				// GameTracking.Instance.TrackButtonPress(location, btnID);

//				//  UIManager.Instance.EquipmentPage.ButtonUpgrade();

//				//[Tracking - Position]-[MenuShowup]
//				string locationMenu = "MenuFuseCards";
//				GameTracking.Instance.TrackMenuShowup(locationMenu);
//			}

//			//private void ClearAllText() {

//			//	TxtUpgrade.SetActive(false);
//			//	TxtMax.SetActive(false);
//			//	TxtBuy.SetActive(false);
//			//	if (!_txtCoinUpgrade || !_txtGemUpgrade || !_txtVideoUpgrade) AssignTextInButton();
//			//	_txtGemUpgrade.text = String.Empty;
//			//	_txtCoinUpgrade.text = String.Empty;
//			//	_txtVideoUpgrade.text = String.Empty;
//			//}


//			//private void ClearAllButton() {
//			//BtnGem.SetActive(false);
//			//BtnCoin.SetActive(false);
//			//BtnVideo.SetActive(false);
//			//BtnUpgrade.SetActive(false);
//			_btaCoinUpgrade.gameObject.SetActive(false);
//			_btaGemUpgrade.gameObject.SetActive(false);
//			_btaVideoUpgrade.gameObject.SetActive(false);
//			_btaCardUpgrade.gameObject.SetActive(false);
//			_btaEvent.gameObject.SetActive(false);
//			_btaAircraftEvolve.gameObject.SetActive(false);
//			_btaDescript.gameObject.SetActive(false);
//			_btaBuyShip.gameObject.SetActive(false);
//			//}

//			void SetBtnEventSkin(Text title, bool unlockMode) {
//				//_btaEvent.GetComponent<_2dxFX_GrayScale>()._EffectAmount = unlockMode ? 0 : 1;
//				_btaEvent.interactable = unlockMode;
//				title.color = unlockMode ? DataConfig.Instance.DolarColor : DataConfig.Instance.ColorLevelCantPlay;
//			}

//			void SetBtnEvent(SpecialEvent sEvent) {
//				_btaEvent.gameObject.SetActive(true);
//				_btaEvent.GetComponent<Image>().sprite = _lsBtnEvent[(int)sEvent].Icon;
//				Text title = _btaEvent.transform.GetChild(0).GetComponent<Text>();
//				title.text = _lsBtnEvent[(int)sEvent].Title;
//				_btaEvent.onClick.RemoveAllListeners();
//				switch(sEvent) {
//					case SpecialEvent.Halloween:
//						//SetBtnEventSkin(title, GameData.Instance.PlayerInfo.UnlockModeData.isUnlockHalloweenMode);
//						//_btaEvent.onClick.AddListener(ButtonHalloweenClick);
//						break;
//					case SpecialEvent.DailyChallenge:
//						//SetBtnEventSkin(title, GameData.Instance.PlayerInfo.UnlockModeData.isUnlocDailyMode);
//						//	_btaEvent.onClick.AddListener(ButtonDailyChallengeClick);
//						break;
//				}
//			}

//			void SetCardUpgradeBtn(Color btnColor, string descript, Color txtColor, bool interactable) {
//				_btaCardUpgrade.GetComponent<Image>().color = btnColor;
//				Text txt = _btaCardUpgrade.GetComponentInChildren<Text>();
//				txt.text = descript;
//				txt.color = txtColor;
//				_btaCardUpgrade.interactable = interactable;
//			}

//			void ButtonDescriptClick() {
//				//[Tracking - Position]-[ButtonPress]
//				//   string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
//				string btnID = "btn_info";
//				//  GameTracking.Instance.TrackButtonPress(location, btnID);

//				if(_descriptPanel.transform.localScale.x < 1) {
//					DescriptPanelSetting(_closeImg, Vector2.one);
//				}
//				else {
//					DescriptPanelSetting(_inforImg, Vector2.zero);
//				}
//			}

//			void DescriptPanelSetting(Sprite img, Vector2 scale) {
//				_btaDescript.GetComponent<Image>().sprite = img;
//				_descriptPanel.transform.DOScale(scale, 0.2f);
//			}

//			void SetTxtDescriptGroup(string name, Sprite classIcon, string descript) {
//				_classIconInfor.transform.parent.gameObject.SetActive(true);
//				_txtName.text = string.Format("Name: {0}", name);
//				_classIconInfor.sprite = _classIcon.sprite = classIcon;
//				_classIcon.SetNativeSize();
//				_classIconInfor.SetNativeSize();
//				_txtDescript.text = descript;
//			}

//			//void SetCpShow(bool isShowCp, AircraftDataShow data) {
//			//	_cpGroup.SetActive(isShowCp);
//			//	if(isShowCp) {
//			//		if(data.CheckCanEvolve()) {
//			//			_txtCurCp.text = "MAX LVL";
//			//			_txtNextCp.text = data.GetEvolveData(1).MaxLvl.ToString();
//			//		}
//			//		else {
//			//			_txtCurCp.text = data.GetCp(0);
//			//			_txtNextCp.text = data.GetCp(1);
//			//		}
//			//	}
//			//}
//		}
//	}
//}


		


		


		


//			//void ButtonEvolveClick() {
//			//       //[Tracking - Position]-[ButtonPress]
//			//       string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
//			//       string btnID = "btn_upgrade";
//			//       GameTracking.Instance.TrackButtonPress(location, btnID);

//			//       UIManager.Instance.EquipmentPage.AircraftEvolvePopup.gameObject.SetActive(true);
//			//	UIManager.Instance.EquipmentPage.BottomPanelCard.DeactiveBtn();

//			//       //[Tracking - Position]-[MenuShowup]
//			//       string locationMenu = "MenuEvolve";
//			//       GameTracking.Instance.TrackMenuShowup(locationMenu);
//			//   }

//			//void ButtonHalloweenClick() {
//			//	UIManager.Instance.HalloweenPage.ShowPage();
//			//	UIManager.Instance.EquipmentPage.gameObject.SetActive(false);
//			//	UIManager.Instance.HomePage.gameObject.SetActive(false);
//			//	UIManager.Instance.HalloweenPage.exchangePage.MoveToShip();
//			//	UIManager.Instance.HalloweenPage.isFromShip = true;
//			//}

//			//void ButtonDailyChallengeClick() {
//			//	UIManager.Instance.DailyChallengePage.OpenFromShip();
//			//	GamePlayState.Instance.ModePlay = ModePlay.Normal;
//			//	UIManager.Instance.EquipmentPage.gameObject.SetActive(false);
//			//	UIManager.Instance.HomePage.gameObject.SetActive(false);
//			//}

		
		


