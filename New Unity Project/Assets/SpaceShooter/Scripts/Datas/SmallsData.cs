﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static EnumDefine;
[CreateAssetMenu(fileName = "SmallsData", menuName = "Data/SmallsData")]
public class SmallsData : ScriptableObject {
    public ChestsData chests = new ChestsData();
    public ModesPlayData modesPlay = new ModesPlayData();
    private static SmallsData instance = null;
    public static SmallsData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<SmallsData>("Data/SmallsData");
                return instance;
            }
            return instance;
        }
    }


}

[Serializable]
public class ModesPlayData {
    public List<ModePlayData> modesPlay = new List<ModePlayData>();
    public ModePlayData GetModePlayData(EnumDefine.IDModePlay ID) {
        var data = modesPlay.Find(x => x.ID == ID);
        if(data == null) {
            Debug.Log(string.Format("ModePlay {0} not yet", ID));
            return new ModePlayData(ID);
        }
        return data;
    }
}

[Serializable]
public class ModePlayData {
    [SerializeField] private EnumDefine.IDModePlay idMode = EnumDefine.IDModePlay.Easy;
    [SerializeField] private string name = string.Empty;
    [SerializeField] private Sprite icon = null;

    public ModePlayData(EnumDefine.IDModePlay idMode) {
        this.idMode = idMode;
    }

    public EnumDefine.IDModePlay ID => idMode;

    public string Name => name;

    public Sprite Icon => icon;
}

[Serializable]
public class ChestsData {
    public List<ChestData> chestNormal = new List<ChestData>();
    public List<ChestData> chestUtimate = new List<ChestData>();
}


[Serializable]
public class ChestData {
    [SerializeField] private int id = 0;
    [SerializeField] private string name = string.Empty;
    [SerializeField] private EnumDefine.IDCurrency idCurrency = IDCurrency.Coin;
    [SerializeField] private int price = 0;
    [SerializeField] private Sprite icon = null;
    public int ID => id;
    public string Name => name;
    public EnumDefine.IDCurrency IdCurrency => idCurrency;
    public int Price => price;
    public Sprite Icon => icon;

}

