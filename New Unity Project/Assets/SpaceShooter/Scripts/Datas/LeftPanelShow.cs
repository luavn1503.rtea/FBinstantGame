﻿using UnityEngine;
using UnityEngine.UI;

public class LeftPanelShow : MonoBehaviour {
	//public LeftPanelCard CardShow;
	//public LeftPanelAircraft AircraftShow;

	[SerializeField] private ButtonAnimation _btaEquip;

	public void UpdateInformation(IItemShowable item) {
		//if (item is AircraftDataShow) {
		//	CardShow.ShowInformation(null);
		//	AircraftShow.ShowInformation(item);
		//} else if (item is CardInfo) {
		//	CardShow.ShowInformation(item);
		//	AircraftShow.ShowInformation(null);
		//} else {
		//	CardShow.ShowInformation(null);
		//	AircraftShow.ShowInformation(null);
		//}

		_btaEquip.gameObject.SetActive(item != null);

		var isLock = item == null;

		var aircraft = item as AircraftDataShow;
		if (aircraft != null) {
			isLock = aircraft.CurrentLevel <= -1;
		}

		//_btaEquip.interactable = !(isLock || item.IsEquip);
		//if (isLock) {
		//	_btaEquip.GetComponent<Image>().color = Color.gray;
		//	_btaEquip.GetComponentInChildren<Text>().text = "LOCKED";
		//} else {
		//	_btaEquip.GetComponentInChildren<Text>().text = "EQUIP";
		//	//Debug.Log("have Equipped : " + item.IsEquip);
		//	_btaEquip.GetComponent<Image>().color = item.IsEquip ? Color.gray : Color.white;
		//}
		//_btaEquip.GetComponent<Image>().SetNativeSize();

		//if (isLock) {
		//	SetEquipBtn(Color.gray, "LOCKED", false);
		//} 
		//else {
		//	if (EquipmentPage.TypeItemSelected == EquipmentType.Ship) {
		//		SetEquipBtn(item.IsEquip ? Color.gray : Color.white, "EQUIP", !item.IsEquip);
		//	} 
		//	else {
		//		SetEquipBtn(Color.white, item.IsEquip ? "UNEQUIP" : "EQUIP", true);
		//	}
		//}
	}

	void SetEquipBtn(Color color, string descript, bool interactable) {
		_btaEquip.GetComponent<Image>().color = color;
		_btaEquip.GetComponentInChildren<Text>().text = descript;
		_btaEquip.interactable = interactable;
	}

}
