﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveAwaySaver {
	private const string Key = "GiveAwaySaver";
	public List<string> GiveAwayEarned;
	public string CurGiftCode;

	public void SaveData() {
		PlayerPrefs.SetString(Key, JsonUtility.ToJson(this));
	}

	public void LoadData() {
		string data = PlayerPrefs.GetString(Key);
		if (data != string.Empty) {
			var saver = JsonUtility.FromJson<GiveAwaySaver>(data);
			GiveAwayEarned = saver.GiveAwayEarned;
			CurGiftCode = saver.CurGiftCode;
		}

		if (GiveAwayEarned == null || GiveAwayEarned.Count == 0) {
			GiveAwayEarned = new List<string>();
		}
	}

	public void AddSaver(GiveAwayEvent GAEvent) {
		GiveAwayEarned.Add(GAEvent.ToString());
		SaveData();
	}

	public bool IsEarnedGiveAway(GiveAwayEvent GAEvent) {
		return GiveAwayEarned.Exists(item => string.Equals(item, GAEvent.ToString()));
	}

	public void SetCurGiftCode(string code) {
		CurGiftCode = code;
		SaveData();
	}
}

public enum GiveAwayEvent {
	BoxingDay
}
