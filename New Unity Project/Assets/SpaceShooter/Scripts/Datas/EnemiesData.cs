﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemiesData", menuName = "Datas/EnemiesData")]
public class EnemiesData : ScriptableObject {

    private static EnemiesData instance;
    public static EnemiesData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<EnemiesData>("EnemiesData");
                return instance;
            }
            return instance;
        }

        set {
            instance = value;
        }
    }

    public List<EnemyData> Datas;

    public List<EnemyData> Rocks;

    public List<EnemyData> Gifts;
}

[Serializable]
public class EnemyData {
    public string EnemyName;
    public float Hp = 100;
    public float MinHp = 1;
    public int NumberBarHp = 1;
    public float BodyDam = 10;
    public float BulletDam = 10;
    public int Score = 10;
    public bool CanAttack;
}


