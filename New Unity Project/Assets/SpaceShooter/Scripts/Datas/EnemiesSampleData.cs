﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemiesSampleData",menuName = "Datas/EnemiesSampleData")]
public class EnemiesSampleData : ScriptableObject {
	public List<GameObject> Enemies;
	public List<GameObject> Bosses;
	public List<GameObject> Rocks;

	[ContextMenu("Load enemies")]
	void LoadEnemies() {
		Enemies.Clear();
		for (int i = 0; i < 46; i++) {
			GameObject enemy = Resources.Load<GameObject>("Prefabs/Enemies/Enemy" + (i + 1));
			Enemies.Add(enemy);
		}
	}

	[ContextMenu("Load boses")]
	void LoadBosses() {
		Bosses.Clear();
		Bosses = Resources.LoadAll<GameObject>("Prefabs/Bosses").ToList();
	}

	public GameObject GetEnemyById(int id) {
		return Enemies[id];
	}

	public GameObject GetBossById(int id) {
		return Bosses[id];
	}
}
