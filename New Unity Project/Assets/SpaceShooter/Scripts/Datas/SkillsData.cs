﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkillsData", menuName = "Data/SkillsData")]
public class SkillsData : ScriptableObject {
    private static SkillsData instance = null;
    public List<SkillData> Skills = new List<SkillData>();
    public static SkillsData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<SkillsData>("SkillsData");
                return instance;
            }
            return instance;
        }
    }
    public SkillData GetSkillData(EnumDefine.IDSkill idSkill) {
        var data = Skills.Find(x => x.ID == idSkill);
        if(data == null) {
            Debug.Log(string.Format("Drone {0} not yet", idSkill));
            return new SkillData();
        }
        return data;
    }
}

[Serializable]
public class SkillData {
    [SerializeField] private EnumDefine.TypeSkill typeSkill = EnumDefine.TypeSkill.Talen;
    [SerializeField] private EnumDefine.IDSkill idSkill = EnumDefine.IDSkill.Shield;
    [SerializeField] private string name = string.Empty;
    [SerializeField] private string description = string.Empty;
    [SerializeField] private Sprite icon = null;
    public List<SkillLevelData> Levels = new List<SkillLevelData>();
    public EnumDefine.TypeSkill TYPE => typeSkill;
    public EnumDefine.IDSkill ID => idSkill;
    public string Name => name;
    public string Decription => description;
    public Sprite Icon => icon;
    public int MaxLevel => Levels.Count;

    public SkillLevelData GetLevelData(int level) {
        return Levels[level];
    }
}

[Serializable]
public class SkillLevelData {
    [SerializeField] private int coin = 0;
    [SerializeField] private int combatPower = 0;
    [SerializeField] private float timeDuration = 0f;
    [SerializeField] private float timeCountdown = 0f;
    [SerializeField] private float mainStats = 0f;
    public int Coin => coin;
    public int CombatPower => combatPower;
    public float TimeDuration => timeDuration;
    public float TimeCountdown => timeCountdown;
    public float MainStats => mainStats;
}
