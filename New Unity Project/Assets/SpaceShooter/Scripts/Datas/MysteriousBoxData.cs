﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "MysteriousBoxData", menuName = "Data/MysteriousBoxData")]
public class MysteriousBoxData : ScriptableObject 
{
    public RewardMysterious[] mysteriousRewards;


    public RewardMysterious GetRandom()
    {
        int random = Random.Range(0, mysteriousRewards.Length);
        return mysteriousRewards[random];
    }

    [System.Serializable]
    public class RewardMysterious
    {
        public Reward[] rewards;
    }

    [System.Serializable]
    public class Reward
    {
        public AllRewardType rewardType;
        public int extID;
        public int amount;
        public Sprite icon;
    }
}
