﻿using UnityEngine;

public class CardPageUpper : MonoBehaviour {
	//public RightPanelShow RightPanel;
	public LeftPanelShow LeftPanel;
	public CardUpgradePanel UpgradePanel;

	public void UpdateInformation(IItemShowable item) {
		//RightPanel.UpdateInformation(item);
		LeftPanel.UpdateInformation(item);
	}

	public bool AddItemToIngredient(IItemShowable item) {
		return UpgradePanel.AddItemToIngredient(item);
	}


	public void OpenUpgrade() {
		//UIManager.Instance.EquipmentPage.ShowUperInfomation(false);
		//var item = LeftPanel.CardShow.Data;

		UpgradePanel.gameObject.SetActive(true);
		//UpgradePanel.SetMainData(item);

		//RightPanel.gameObject.SetActive(false);
		LeftPanel.gameObject.SetActive(false);
	}

	public void OpenInfomation() {
		//UIManager.Instance.EquipmentPage.ShowUperInfomation(true);
		var item = UpgradePanel.MainItem.Data;

		//RightPanel.UpdateInformation(item);
		LeftPanel.UpdateInformation(item);

		UpgradePanel.gameObject.SetActive(false);

		//RightPanel.gameObject.SetActive(true);
		LeftPanel.gameObject.SetActive(true);
	}

}
