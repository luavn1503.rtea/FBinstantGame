﻿using Gemmob.API.Analytis;
using Gemmob.API.IAP;
using System;

public class UnityIAP : BaseIap<UnityIAP> {
    public const string RemoveAdsKey = "removeads";

    public bool IsRemovedAds => IsOwned(RemoveAdsKey);

    private const string CompareKey = "com.galaxyattack.compare";
    private const string SpenderKey = "Spender";
    private const string BigSpenderKey = "BigSpender";
    private const string EarlySpenderKey = "EarlySpender";
    private const string SaleLoverKey = "SaleLover";

#if IAP_ENABLE
    protected override void IapInit(ConfigurationBuilder builder) {
        //-------------------------

        foreach (var infor in GameData.Instance.ShopCoinData.Coins)
            if (infor.Iap != string.Empty) {
                for (int i = 0; i < 100; i += 10) {
                    builder.AddProduct(infor.Iap + (i == 0 ? "" : i.ToString()), ProductType.Consumable);
                }
            }

        foreach (var infor in GameData.Instance.ShopCoinData.Gem)
            if (infor.Iap != string.Empty) {
                for (int i = 0; i < 100; i += 10) {
                    builder.AddProduct(infor.Iap + (i == 0 ? "" : i.ToString()), ProductType.Consumable);
                }
            }

        if (GameData.Instance.PacksItemData.Packs != null)
            foreach (var infor in GameData.Instance.PacksItemData.Packs)
                if (!string.IsNullOrEmpty(infor.Iap))
                    builder.AddProduct(infor.Iap, ProductType.Consumable);

        if (GameData.Instance.ShopCoinData.C.BuyType == ShopCoinBuyType.Dolar)
            builder.AddProduct(GameData.Instance.ShopCoinData.C.Iap, ProductType.Consumable);
        if (GameData.Instance.ShopCoinData.F.BuyType == ShopCoinBuyType.Dolar)
            builder.AddProduct(GameData.Instance.ShopCoinData.F.Iap, ProductType.Consumable);
        if (GameData.Instance.ShopCoinData.G.BuyType == ShopCoinBuyType.Dolar)
            builder.AddProduct(GameData.Instance.ShopCoinData.G.Iap, ProductType.Consumable);

        builder.AddProduct(SettingPopup.RemoveAdsIap, ProductType.Consumable);

        builder.AddProduct(GameData.Instance.PackData.VipPack.Key, ProductType.Consumable);
        builder.AddProduct(GameData.Instance.PackData.VipPack.oldKey, ProductType.Consumable);
        builder.AddProduct(CompareKey, ProductType.Consumable);

        foreach (var infor in GameData.Instance.PackData.PackInfor) {
            builder.AddProduct(infor.Key, ProductType.Consumable);
            builder.AddProduct(infor.OldKey, ProductType.Consumable);
        }

        foreach (var item in GameData.Instance.IapShipData.shopShipInfos) {
            builder.AddProduct(item.IapKey, ProductType.Consumable);
        }

        Logs.Log("Add Iap key done");

        UnityPurchasing.Initialize(this, builder);

        Preload();
    }

#endif

    public void Buy(string productId, string name, Action onBuyCompleted = null, Action onBuyFailed = null,
        bool isSale = false, int salePercent = 0) {
        Action newCompleted = () => {
            if (onBuyCompleted != null) onBuyCompleted();
         //   GameData.Instance.PlayerInfo.AddBoughtIapPack(productId);
            SetUserProperties(isSale);
            //Tracking.LogIAP(productId, salePercent);
            GameTracking.Instance.TrackingIapPurchased(productId, name, isSale ? IapCategory.Discount : IapCategory.Null);
        };
        base.Buy(productId, newCompleted, onBuyFailed);
    }

    void SetUserProperties(bool isSale) {
        Users.SetUserProperty(SpenderKey, true.ToString());
       // decimal allSpend = GameData.Instance.PlayerInfo.GetAllSpent();
        //if (allSpend > GetLocalPrice(CompareKey).localizedPrice) {
        //    Users.SetUserProperty(BigSpenderKey, true.ToString());
        //}
        Users.SetUserProperty(SaleLoverKey, isSale.ToString());
     //   TimeSpan delta = DateTime.Today - DateTime.Parse(GameData.Instance.PlayerInfo.OpenDay);
        //if (delta.TotalDays <= 3) {
        //    Users.SetUserProperty(EarlySpenderKey, true.ToString());
        //}
    }

}
