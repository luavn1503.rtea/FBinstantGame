﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpinData", menuName = "Datas/SpinData")]
public class SpinData : ScriptableObject
{
    private static SpinData instance;
    public static SpinData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<SpinData>("SpinData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<SpinItemData> Items = new List<SpinItemData>();

}
[Serializable]
public class SpinItemData
{
    public Sprite Icon;
	public AllRewardType Type;
    public int ExtendId = 1;
    public int Number = 1;
    public int Rate;
    public int IconSize = 60;
}

public enum TypeReward
{
    Coin,
    Gem,
    Fuel,
    Item3,
    Item5,
    Item10,
    Bomb,
    Ship
}

