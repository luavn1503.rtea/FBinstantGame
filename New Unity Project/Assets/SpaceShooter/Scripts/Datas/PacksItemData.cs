﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PacksItemData", menuName = "Datas/PacksItemData")]
public class PacksItemData: ScriptableObject {

    private static PacksItemData instance;
    public static PacksItemData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<PacksItemData>("PacksItemData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<PackItemIap> Packs;
}

[System.Serializable]
public class ItemGetIap {
    public Sprite ItemIcon;
    public TypeReward TypeReward;
    public int Number;
}

[Serializable]
public class PackItemIap {
    public string Iap;
    public List<ItemGetIap> Packs;
    public float Price;
    public float Percent;
}
