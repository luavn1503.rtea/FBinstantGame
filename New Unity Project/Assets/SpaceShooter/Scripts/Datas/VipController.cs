﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class VipController {
	private const string Key = "VipController";
	public int DayCount;
	public string LastDayClaim;
	public List<string> ClaimDays;
	public bool IsClaimDaily;
	public bool IsBoughtRemoveAds;

	public void SaveData() {
		PlayerPrefs.SetString(Key, JsonUtility.ToJson(this));
	}

	public void LoadData() {
		string data = PlayerPrefs.GetString(Key);
		if (data != string.Empty) {
			var vipController = JsonUtility.FromJson<VipController>(data);
			DayCount = vipController.DayCount;
			IsClaimDaily = vipController.IsClaimDaily;
			LastDayClaim = vipController.LastDayClaim;
			ClaimDays = vipController.ClaimDays;
			IsBoughtRemoveAds = vipController.IsBoughtRemoveAds;
		}
		if (ClaimDays == null || ClaimDays.Count == 0) {
			ClaimDays = new List<string>();
		}
	}

	public void CheckOutDate() {
		if (DayCount > ManagerData.Instance.PackData.VipPack.Days) {
		//	GameData.Instance.PlayerInfo.SetVip(false);
			if (!IsBoughtRemoveAds) {
				ManagerData.IsRemoveAds = false;
			}
			//TopBarController.Instance.Fuel.SetTxtFuel();
			//UIManager.Instance.HomePage.VipPopup.SetActiveVipBtn();
			//UIManager.Instance.HomePage.VipPopup.SetPrice();
		}
	}

	public void SetBoughtDay() {
		LastDayClaim = DateTime.Today.ToShortDateString();
		IsBoughtRemoveAds = false;
		ClaimDays.Clear();
		DayCount = 1;
		SaveData();
	}

	public void GetDayCount() {
		if (DayCount <= ManagerData.Instance.PackData.VipPack.Days && ManagerData.Instance.PlayerInfo.PlayerProfile.IsVip) {
			DateTime today = DateTime.Today;
			DateTime lastDayClaim = DateTime.Parse(LastDayClaim);
			int deltaDay = Mathf.Abs((int) (today - lastDayClaim).TotalDays);
			//Debug.Log(deltaDay);
			DayCount += deltaDay;
			LastDayClaim = today.ToShortDateString();
			IsClaimDaily = false;
			if (!ManagerData.IsRemoveAds) {
				ManagerData.IsRemoveAds = true;
			}
			SaveData();
		}
	}

	public void SetClaimDaily(bool isClaimDayly) {
		IsClaimDaily = isClaimDayly;
		string today = DateTime.Today.ToShortDateString();
		ClaimDays.Add(today);
		SaveData();
	}

	public void BoughtRemoveAds() {
		IsBoughtRemoveAds = true;
		SaveData();
	}
}
