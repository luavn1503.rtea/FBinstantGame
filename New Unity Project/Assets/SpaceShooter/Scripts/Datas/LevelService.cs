﻿using System.Collections.Generic;
using Gemmob;

namespace Gemmob {
    public class LevelService : Singleton<LevelService> {
        private const string ResourcePath = "Levels";
        public const string FullPath = "Assets/SpaceShooter/Resources/Levels";

        public Dictionary<int, LevelData> LevelData;

        public Dictionary<int, LevelData> LoadAllLevelData() {
            var levels = new List<LevelData>(ResourceLogger.LoadAll<LevelData>(ResourcePath));
            for (int i = 0; i < levels.Count; i++) {
                if (LevelData.ContainsKey(i))
                    continue;

                LevelData.Add(i, levels[i]);
            }

            return LevelData;
        }

        //private LevelData LoadLevel(int index) {
        //}

        protected override void Initialize() {
            LevelData = new Dictionary<int, LevelData>();
        }

        public LevelData GetById(int id) {
			if (!LevelData.ContainsKey(id)) {
                var level = ResourceLogger.Load<LevelData>(ResourcePath + "/Level " + (id + 1).ToString("00"));
                if(level != null) { LevelData.Add(id, level); }
            }
            return LevelData[id];
        }

	    public LevelData GetByIdForTool(int id) {
		    if (!LevelData.ContainsKey(id)) {
			    var level = ResourceLogger.Load<LevelData>(ResourcePath + "/Level " + (id + 1).ToString("00"));
                if(level != null) { LevelData.Add(id, level); }
		    }
		    return LevelData[id];
		}

	    public LevelData GetLvlById(int id) {
		    return ResourceLogger.Load<LevelData>(ResourcePath + "/Level " + (id + 1).ToString("00"));
	    }
    }
}