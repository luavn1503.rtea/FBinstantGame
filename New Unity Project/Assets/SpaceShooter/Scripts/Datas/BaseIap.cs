﻿using System;
using UnityEngine;

#if IAP_ENABLE
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

#endif

namespace Gemmob.API.IAP {
#if IAP_ENABLE
    public abstract class BaseIap<T> : SingletonFreeAlive<T>, IStoreListener where T : Component {
#else
    public abstract class BaseIap<T> : SingletonFreeAlive<T> where T : Component {
#endif
        public bool isRequesting;
        public Action onBuyFailed;
        public Action onBuyCompleted;
        public event Action<string> OnPurchasingComplete;
        private const char DefaultSymbol = '$';
        private const string DefaultIsoCurrencyCode = "USD";

        public class Meta {
            public readonly string isoCurrencyCode;
            public readonly string localizedPriceString;
            public readonly Decimal localizedPrice;
            public readonly char symbol;

            public Meta(decimal localizedPrice, char symbol, string isoCurrencyCode) {
                this.isoCurrencyCode = isoCurrencyCode;
                this.localizedPrice = localizedPrice;
                if (!string.IsNullOrEmpty(isoCurrencyCode)) {
                    localizedPriceString = this.localizedPrice + " " + isoCurrencyCode;
                }

                this.symbol = symbol;
            }

            public Meta(decimal localizedPrice, string localizedPriceString, string isoCurrencyCode) {
                this.isoCurrencyCode = isoCurrencyCode;
                this.localizedPriceString = localizedPriceString;
                this.localizedPrice = localizedPrice;

                if (string.IsNullOrEmpty(this.localizedPriceString)) {
                    symbol = DefaultSymbol;
                }
                else {
                    if (!char.IsDigit(this.localizedPriceString[0])) {
                        symbol = this.localizedPriceString[0];
                    }
                    else if (!char.IsDigit(this.localizedPriceString[this.localizedPriceString.Length - 1])) {
                        symbol = this.localizedPriceString[this.localizedPriceString.Length - 1];
                    }
                    else {
                        symbol = DefaultSymbol;
                    }
                }
            }
        }

        private static readonly Meta emptyMeta = new Meta(0, DefaultSymbol, DefaultIsoCurrencyCode);
#if IAP_ENABLE
        private IStoreController storeController;
        private IExtensionProvider storeExtensionProvider;
#endif

        public void RegisterOnPurchasingComplete(Action<string> onPurchasingComplete) {
            OnPurchasingComplete += onPurchasingComplete;
        }

        public void UnRegisterOnPurchasingComplete(Action<string> onPurchasingComplete) {
            OnPurchasingComplete -= onPurchasingComplete;
        }

        public virtual void OnPurchaseStart() { }

        public virtual void OnPurchaseEnd() { }

        protected override void OnAwake() {
#if IAP_ENABLE
            StandardPurchasingModule module = StandardPurchasingModule.Instance();
#if UNITY_EDITOR
            module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
#endif
            var builder = ConfigurationBuilder.Instance(module);
            IapInit(builder);
            UnityPurchasing.Initialize(this, builder);
#endif
        }
#if IAP_ENABLE
        protected abstract void IapInit(ConfigurationBuilder builder);


        private bool IsInitialized() {
            var isInitialized = storeController != null && storeExtensionProvider != null;
            if (!isInitialized) {
                Logs.LogErrorFormat("[IAP] Not initialized.");
            }

            return isInitialized;
        }


        public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
            Logs.Log("[IAP]  OnInitialized");
            storeController = controller;
            storeExtensionProvider = extensions;
        }

        public void OnInitializeFailed(InitializationFailureReason error) {
            Logs.LogErrorFormat("[IAP] OnInitializeFailed error: {0}", error);
            isRequesting = false;
        }


        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) {
            var validPurchase = true;

#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX


            var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                AppleTangle.Data(), Application.identifier);

            try {
                var result = validator.Validate(args.purchasedProduct.receipt);
                foreach (IPurchaseReceipt productReceipt in result) {
                    Logs.LogFormat("productID:={0}, purchaseDate={1}, transactionID={2}", productReceipt.productID,
                        productReceipt.purchaseDate, productReceipt.transactionID);
                }
            }
            catch (IAPSecurityException) {
#if !UNITY_EDITOR
                Logs.LogError("Invalid receipt, not unlocking content");
                validPurchase = false;
#endif
            }
#else
            Logs.LogError("Please ask Pham Dinh Cuong skype:phamcuongt2 to get google public key for unity purchase");
#endif

            if (validPurchase) {
                Events.LogIap(args.purchasedProduct.definition.id, "completed");
                //Users.IncrementIapCounter();
                if (OnPurchasingComplete != null) {
                    OnPurchasingComplete.Invoke(args.purchasedProduct.definition.id);
                }

                if (onBuyCompleted != null) {
                    onBuyCompleted.Invoke();
                }
            }

            isRequesting = false;

            OnPurchaseEnd();
            return PurchaseProcessingResult.Complete;
        }


        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) {
            Events.LogIapStats(product.definition.id, "failed");
            if (onBuyFailed != null) {
                onBuyFailed.Invoke();
            }

            OnPurchaseEnd();
            isRequesting = false;
        }
#endif
        public Meta GetLocalPrice(string id, Decimal defaultPrice = 0, string defaultSymbol = "$", string defaultCurencyCode = DefaultIsoCurrencyCode) {
#if IAP_ENABLE
            if (storeController != null) {
                var productMetadata = storeController.products.WithID(id).metadata;
                return new Meta(productMetadata.localizedPrice, productMetadata.localizedPriceString,
                    productMetadata.isoCurrencyCode);
            }
            else if (defaultPrice > 0) {
                return new Meta(defaultPrice, defaultSymbol, defaultCurencyCode);
            }

            return emptyMeta;
#else
#if UNITY_EDITOR
            Logs.LogError("IAP is disable please check build config");
#endif
            if (defaultPrice > 0) {
                return new Meta(defaultPrice, defaultSymbol, defaultCurencyCode);
            }

            return emptyMeta;
#endif
        }

        public void RestorePurchases(System.Action success = null) {
#if IAP_ENABLE
            if (IsInitialized()) {
                if (Application.platform == RuntimePlatform.IPhonePlayer ||
                    Application.platform == RuntimePlatform.OSXPlayer) {
                    Logs.Log("RestorePurchases started ...");

                    var apple = storeExtensionProvider.GetExtension<IAppleExtensions>();

                    apple.RestoreTransactions(result => {
                        if (result && success != null) {
                            success.Invoke();
                        }
                        Logs.LogFormat(
                            "RestorePurchases continuing: {0}. If no further messages, no purchases available to restore.",
                            result);
                    });
                }
                else {
                    Logs.LogFormat("RestorePurchases FAIL. Not supported on this platform. Current = {0}",
                        Application.platform);
                }

                isRequesting = false;
            }
#elif UNITY_EDITOR
            Logs.LogError("IAP is disable please check build config");
#endif
        }

        public virtual void Buy(string productId, Action onBuyCompleted = null, Action onBuyFailed = null) {
#if IAP_ENABLE
            if (isRequesting)
                return;
            this.onBuyCompleted = onBuyCompleted;
            this.onBuyFailed = onBuyFailed;
            isRequesting = true;
            if (IsInitialized()) {
                Product product = storeController.products.WithID(productId);
                if (product != null) {
                    Events.LogIapStats(product.definition.id, "click");
                    if (product.availableToPurchase) {
                        Logs.LogFormat("Purchasing product asychronously: '{0}'", product.definition.id);
                        storeController.InitiatePurchase(product);
                        OnPurchaseStart();
                    }
                    else {
                        Events.LogIapStats(product.definition.id, "unavailable");
                        Logs.Log(
                            "BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                    }
                }
            }
#else
            if (Config.IsDebug) {
                if (onBuyCompleted != null) {
                    onBuyCompleted.Invoke();
                }

                Logs.LogError("IAP is disable please check build config");
            }
#endif
        }

        public bool IsOwned(string productID) {
#if IAP_ENABLE
            if (!IsInitialized()) return false;
            var product = storeController.products.WithID(productID);
            if (product == null) return false;
            return product.hasReceipt;
#else
            Logs.Log("IAP is disable. Please enableIAP at BuildConfig.");
            return false;
#endif
        }
    }
}