﻿using System;
using System.Collections.Generic;
using Gemmob;
using UnityEngine;
using static EnumDefine;

[Serializable]
public class AircraftData {
    public List<ShipDataInfo> Ships = new List<ShipDataInfo>();

    private string shipDataKey = "shipData";

    public AircraftData() {
        Ships = new List<ShipDataInfo>();
        foreach(var item in ShipsData.Instance.Ships) {
            if(item.ID == EnumDefine.IDShip.Ship_01 || item.ID == IDShip.Drone_01) {
                Ships.Add(new ShipDataInfo(item.ID, 0, 10000));
            }
            else {
                Ships.Add(new ShipDataInfo(item.ID, -1, 0));
            }
        }
    }

    public AircraftData LoadShips() {
        AircraftData data = this;
        if(PlayerPrefs.HasKey(shipDataKey)) {
            string dataJson = PlayerPrefs.GetString(shipDataKey);
            data = JsonUtility.FromJson<AircraftData>(dataJson);
        }
        else {
            Save();
        }
        return data;
    }

    public int AddShipDebris(IDShip idShip, int amount) {
        var shipInfo = Ships.Find(x => x.ID == idShip);
        if(shipInfo == null) {
            shipInfo = new ShipDataInfo(idShip, -1, amount);
        }
        else {
            shipInfo.AddDebris(amount);
        }
        return shipInfo.Debris;
    }


    public void Save() {
        PlayerPrefs.SetString(shipDataKey, JsonUtility.ToJson(this));
    }


    public void BackupDataOnline(ref PlayerServerInfo info) {
        //info.Ships = Ships;
        //info.Drones = Drones;
    }

    public float WinRateShip(int indexShip) {
        if(indexShip < Ships.Count && indexShip >= 0)
            return (float)Ships[indexShip].Win / Ships[indexShip].Fight;

        return 0;
    }

    public bool BuyShip(EnumDefine.IDShip idShip, bool isSave = true) {
        var shipInfo = GetDataShipInfo(idShip);
        var result = shipInfo.Buy();
        if(isSave) { Save(); }
        return result;
    }


    public ShipDataInfo GetDataShipInfo(EnumDefine.IDShip idShip) {
        var dataInfo = Ships.Find(x => x.ID == idShip);
        if(dataInfo == null) {
            Logs.Log(string.Format("Ship {0} chưa có data", idShip));
            return new ShipDataInfo(idShip, -1, 0);
        }
        return dataInfo;
    }

    public bool IsShip(int index) {
        return index < Ships.Count && index >= 0;
    }


    public bool IsAircraft(List<ShipDataInfo> LsInfor, int index) {
        return index < LsInfor.Count && index >= 0;
    }

    public bool UpgradeShip(EnumDefine.IDShip idShip, bool isSave = true) {
        var softData = GetDataShipInfo(idShip);
        if(softData == null) { return false; }
        softData.Upgrade();
        if(isSave) { Save(); }
        return true;
    }

    public void EvolveAircraft(EnumDefine.IDShip idShip) {
        var softData = GetDataShipInfo(idShip);
        if(softData == null) { return; }
        softData.Evolve();
        Save();
    }

    #region For editor

    public void LoadDataForEditor() {
        string data = PlayerPrefs.GetString(shipDataKey);
        if(data != string.Empty)
            Ships = JsonHelper.FromJson<ShipDataInfo>(data);
    }

    #endregion
}

[Serializable]
public class ShipDataInfo {
    [SerializeField] private EnumDefine.IDShip idShip = EnumDefine.IDShip.Ship_01;
    [SerializeField] private EnumDefine.Rank rank = EnumDefine.Rank.D;
    [SerializeField] private int level = -1;
    [SerializeField] private int debris = 0;
    [SerializeField] private int win = 0;
    [SerializeField] private int fight = 0;
    [SerializeField] private int videoSeen = 0;
    public ShipDataInfo(EnumDefine.IDShip idShip, int level, int debris) {
        this.idShip = idShip;
        this.level = level;
        this.debris = debris;
    }
    public EnumDefine.IDShip ID => idShip;
    public int Level => level;
    public EnumDefine.Rank Rank => rank;
    public int Debris => debris;
    public int Win => win;
    public int Fight => fight;
    public int VideoSeen => videoSeen;
    public bool IsUnlock() {
        return Level >= 0;
    }

    public EnumDefine.Rank Evolve(int value = 1) {
        rank += 1;
        return rank;
    }

    public int Upgrade(int value = 1) {
        level += value;
        return level;
    }

    public int AddDebris(int amount) {
        debris += amount;
        return debris;
    }

    public bool Buy() {
        if(level > 0) { return false; }
        level = 0;
        return true;
    }
}
