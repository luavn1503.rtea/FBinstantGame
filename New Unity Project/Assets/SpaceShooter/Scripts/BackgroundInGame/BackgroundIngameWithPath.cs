﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Gemmob;
using UnityEngine;

public class BackgroundIngameWithPath : BackgroundIngame {
	[SerializeField] protected List<int> _pathId;

	protected override void SetBg() {
		for (int i = 0; i < _layers.Count; i++) {
			_layers[i].SetSpeed(_layerInfors[i].Speed, _layerInfors[i].GameSpeed);
		}

		for (int i = 0; i < _objs.Count; i++) {
			_objs[i].SetSpeed(_objSpeed[i]);
			if (_objs[i].ObjMoveType == BackgroundObject.MoveType.Path) {
				SetPath(_objs[i].transform, _objSpeed[i]);
			}
		}
	}

	protected virtual void SetPath(Transform ship, float speed) {

	}

	protected virtual IEnumerator WaitNextPath(Transform ship, float speed, float time) {
		yield return Yielder.Wait(time);
		SetPath(ship, speed);
	}

	void OnDisable() {
		StopAllCoroutines();
	}

}
