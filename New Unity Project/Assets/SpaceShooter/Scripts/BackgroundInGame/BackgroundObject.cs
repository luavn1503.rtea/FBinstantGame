﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundObject : MonoBehaviour {
	[SerializeField] private MoveType _moveType; 
	[SerializeField] private float _speed;
	[SerializeField] private bool _isChild;
	private Action _doMove;

	public MoveType ObjMoveType {
		get { return _moveType; }
	}

	public float Speed {
		get { return _speed; }
	}

	public void SetSpeed(float speed) {
		_speed = speed;
	}

	void Start() {
		//SetMove();
		StartCoroutine(DoSetMove());
	}

	void SetMove() {
		switch (_moveType) {
			case MoveType.Normal:
				_doMove = NormalMove;
				break;
			case MoveType.Rotate:
				_doMove = () => RotateMove(_speed);
				break;
			case MoveType.MoveRotate:
				_doMove = () => {
					NormalMove();
					RotateMove(15);
				};
				break;
		}
	}

	IEnumerator DoSetMove() {
		if (!_isChild) {
			transform.position = new Vector3(transform.position.x, 40, transform.position.z);
		}
		yield return Yielder.Wait(3);
		SetMove();
	}

	void NormalMove() {
		//transform.Translate(_speed * Vector2.down * Time.deltaTime);
		transform.position = transform.position + Vector3.down * _speed * Time.deltaTime;
		if (transform.position.y < -40) {
			transform.position = new Vector3(transform.position.x, 40, transform.position.z);
		}
	}

	void PathMove() {

	}

	void RotateMove(float speed) {
		transform.Rotate(0, 0, -speed * Time.deltaTime);
	}

	void Update() {
		if (_doMove != null) {
			_doMove();
		}
	}

	public enum MoveType {
		Normal, Path, Rotate, MoveRotate
	}
}
