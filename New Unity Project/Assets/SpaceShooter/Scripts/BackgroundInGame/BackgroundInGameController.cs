﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Gemmob;
using UnityEngine;

public class BackgroundInGameController : MonoBehaviour {
	[SerializeField] private GameObject[] allBg;

    private void Start() {
        LoadBackground();
    }

    //private void OnStartGame(GameEvent.OnStartGame eventParam) {
    //	var modePlay = GamePlayState.Instance.ModePlay;
    //	switch (modePlay) {
    //		case ModePlay.Boss:
    //			Instantiate(_bbBg, transform);
    //			break;
    //		case ModePlay.Normal:
    //			LoadBackground();
    //			break;
    //		default:
    //			Instantiate(_originBg, transform);
    //			break;
    //	}
    //}

    void LoadBackground() {
		//int id = lvl % 6 == 0 ? (lvl / 6) - 1 : lvl / 6;
		var bgInGame = Instantiate(allBg[/*id*/7], transform);
        bgInGame.GetComponent<BackGround>().Init();
	}
}
