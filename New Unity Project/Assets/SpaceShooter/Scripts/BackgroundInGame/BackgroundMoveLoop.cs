﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMoveLoop : MonoBehaviour {

    public enum Type { Horizontal, Vertical }

    [SerializeField] protected Type type;
    [SerializeField] protected SpriteRenderer bg;
    [SerializeField] protected float speedMove = 5;
    [SerializeField] protected bool flip;

    protected GameObject joinObject;

    public static Vector3 BottomLeft
    {
        get
        {
            return Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        }
    }

    public static Vector3 UpRight
    {
        get
        {
            return Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        }
    }

    protected float SizeY
    {
        get
        {
            return bg.sprite.bounds.size.y * bg.transform.localScale.y;
        }
    }

    protected float SizeX
    {
        get
        {
            return bg.sprite.bounds.size.x * bg.transform.localScale.x;
        }
    }

    protected virtual float Offset
    {
        get
        {
            return Time.deltaTime * speedMove * flipValue;
        }
    }

    protected float OffsetX
    {
        get
        {
            return type == Type.Horizontal ? Offset : 0;
        }
    }

    protected float OffsetY
    {
        get
        {
            return type == Type.Vertical ? Offset : 0;
        }
    }

    protected int flipValue
    {
        get
        {
            return flip ? -1 : 1;
        }
    }

    protected virtual void Start()
    {
        if (bg == null)
        {
            bg = GetComponent<SpriteRenderer>();
        }
    }

    protected virtual void Update()
    {
        transform.position = new Vector2(transform.position.x + OffsetX, transform.position.y - OffsetY);

        if (type == Type.Vertical)
        {
            if (CanGenerate())
            {
                Generate(new Vector2(transform.position.x, transform.position.y + SizeY * flipValue));
            }

            if (CanDestroy())
            {
                Destroy(gameObject);
            }
        }

        if (type == Type.Horizontal)
        {
            if (CanGenerate())
            {
                Generate(new Vector2(transform.position.x - SizeX * flipValue, transform.position.y));
            }

            if (CanDestroy())
            {
                Destroy(gameObject);
            }
        }
    }

    protected virtual void Generate(Vector2 position)
    {
        if (joinObject == null)
        {
            joinObject = Instantiate(gameObject, base.transform.parent);
            joinObject.transform.SetSiblingIndex(base.transform.GetSiblingIndex());
            joinObject.transform.position = position;
        }
    }

    protected virtual bool CanGenerate()
    {
        if (type == Type.Vertical)
        {
            if (transform.position.y + (SizeY / 2) < UpRight.y && !flip)
            {
                return true;
            }

            if (transform.position.y - (SizeY / 2) > BottomLeft.y && flip)
            {
                return true;
            }
        }

        if (type == Type.Horizontal)
        {
            if (transform.position.x - (SizeX / 2) > BottomLeft.x && !flip)
            {
                return true;
            }

            if (transform.position.x + (SizeX / 2) < UpRight.x && flip)
            {
                return true;
            }
        }
        return false;
    }

    protected virtual bool CanDestroy()
    {
        if (type == Type.Vertical)
        {
            if (transform.position.y + SizeY / 2 < BottomLeft.y && !flip)
            {
                return true;
            }

            if (transform.position.y - SizeY / 2 > UpRight.y && flip)
            {
                return true;
            }
        }

        if (type == Type.Horizontal)
        {
            if (transform.position.x - (SizeX / 2) > UpRight.x && !flip)
            {
                return true;
            }

            if (transform.position.x + (SizeX / 2) < BottomLeft.x && flip)
            {
                return true;
            }
        }
        return false;
    }
}
