﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillToScreen : MonoBehaviour {
    public SpriteRenderer sprite;
    public Camera mainCamera;
    public bool FitX;
    public bool FitY;
    public bool ratio;

    private void Start() {
        if(sprite == null)
            sprite = GetComponent<SpriteRenderer>();
        if(mainCamera == null)
            mainCamera = MainCamera.Instance.main;
        ResizeSpriteToScreen();
    }

    [ContextMenu("Resize")]
    public void ResizeSpriteToScreen() {
        if(sprite == null)
            return;

        sprite.transform.localScale = new Vector3(1, 1, 1);

        var width = sprite.sprite.bounds.size.x;
        var height = sprite.sprite.bounds.size.y;

        var worldScreenHeight = mainCamera.orthographicSize * 2.0f;
        var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        float x = FitX ? worldScreenWidth / width : sprite.transform.localScale.x;
        float y = FitY ? worldScreenHeight / height : sprite.transform.localScale.y;

        float r = 1;
        float px = x;
        float py = y;

        if(FitX) {
            r = x / sprite.transform.localScale.x;
            py = ratio ? y * sprite.transform.localScale.y * r : y;
        }

        if(FitY) {
            r = y / sprite.transform.localScale.y;
            px = ratio ? sprite.transform.localScale.x * r : x;
        }

        if(FitX && FitY) {
            px = x;
            py = y;
        }

        sprite.transform.localScale = new Vector2(px, py);
    }
}
