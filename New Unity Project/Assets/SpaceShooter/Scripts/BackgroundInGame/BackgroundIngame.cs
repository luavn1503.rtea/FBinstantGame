﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundIngame : MonoBehaviour {
	[SerializeField] protected List<BackgroundMoveLoopExtend> _layers;
	[SerializeField] protected List<BackgroundObject> _objs;
	[SerializeField] protected List<BackgroundInfo> _layerInfors;
	[SerializeField] protected List<float> _objSpeed;

	[ContextMenu("Init")]
	void Init() {
		_layerInfors = new List<BackgroundInfo>();
		_layers = new List<BackgroundMoveLoopExtend>();
		_objs = new List<BackgroundObject>();
		_objSpeed = new List<float>();
		for (int i = 0; i < transform.childCount; i++) {
			if (transform.GetChild(i).GetComponent<BackgroundMoveLoopExtend>()) {
				_layers.Add(transform.GetChild(i).GetComponent<BackgroundMoveLoopExtend>());
				_layerInfors.Add(new BackgroundInfo());
			}

			if (transform.GetChild(i).GetComponent<BackgroundObject>()) {
				_objs.Add(transform.GetChild(i).GetComponent<BackgroundObject>());
				_objSpeed.Add(0);
			}
		}
	}

	[ContextMenu("Reset layer")]
	void ResetLayer() {
		_layerInfors = new List<BackgroundInfo>();
		_layers = new List<BackgroundMoveLoopExtend>();
		for (int i = 0; i < transform.childCount; i++) {
			if (transform.GetChild(i).GetComponent<BackgroundMoveLoopExtend>()) {
				_layers.Add(transform.GetChild(i).GetComponent<BackgroundMoveLoopExtend>());
				_layerInfors.Add(new BackgroundInfo());
			}
		}
	}

	void Start() {
		SetBg();
	}

	protected virtual void SetBg() {
		for (int i = 0; i < _layers.Count; i++) {
			_layers[i].SetSpeed(_layerInfors[i].Speed, _layerInfors[i].GameSpeed);
		}

		for (int i = 0; i < _objs.Count; i++) {
			_objs[i].SetSpeed(_objSpeed[i]);
		}
	}

}

[Serializable]
public class BackgroundInfo {
	public float Speed;
	public float GameSpeed;
}
