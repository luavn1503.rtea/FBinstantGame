﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BackgroundMoveLoopExtend : BackgroundMoveLoop {
	[SerializeField] private float _gameSpeed;

	public void SetSpeed(float speed, float gameSpeed) {
		speedMove = speed;
		_gameSpeed = gameSpeed;
		DOVirtual.Float(_gameSpeed, 1, 3f, value => { _gameSpeed = value; });
	}

	protected override float Offset {
		get { return Time.deltaTime * speedMove * _gameSpeed * flipValue; }
	}
}
