﻿using global::System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Gemmob;
using Google.GData.Spreadsheets;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class LevelToolEditor : EditorWindow {
	private string[] _gameMode = new string[] {"Level", "Halloween", "Tutorial"};
    private int[] _enemyBoss = new int[] { 4, 7, 10, 13, 17, 20, 24, 28, 32, 36, 40, 46 };
    private bool IsShowOrder;
    private Vector2 _menuScroll;
    private Vector2 _inforScroll;
    private Vector2 _wavePathScroll;
    private int _levelSelectIndex;
    private int _enemySelectIndex;
    private int _wavePathSelectIndex;


    public string[] LevelNames;
    public string[] PathNames;

    private LevelData _levelData;
    //private WaveData _wave;
    private int _waveIndex;

    private PathManager _pathManager;
    public PathManager Paths {
        get {
            if (!_pathManager)
                _pathManager = FindObjectOfType<PathManager>();
            return _pathManager;
        }
    }

    [MenuItem("Game Tools/Level Editor")]
    static void Init() {
        var w = GetWindow<LevelToolEditor>();
        w.titleContent.text = "Level Tool";
        w.RefreshEditor();
        w.Show();
    }

    private int _curMode;
    private int _mode;

    void OnGUI() {
        Time.timeScale = EditorGUILayout.Slider("Time Scale", Time.timeScale, 0, 10);
        //CustomGUI.HeaderButton("LEVEL TOOL", Color.white);
        GUILayout.BeginHorizontal();
        DisplayMenu();
        DisplayWaves();
        DisplayWaveInfor();
        GUILayout.EndHorizontal();
    }

    void DisplayMenu() {
        Rect r = EditorGUILayout.BeginVertical(GUILayout.ExpandHeight(true), GUILayout.Width(200));
        GUI.Box(r, GUIContent.none);
        _mode = GUILayout.Toolbar(_mode, _gameMode);
        if (_mode != _curMode) {
            _curMode = _mode;
            RefreshEditor();
        }
        _levelSelectIndex = EditorGUILayout.Popup(_levelSelectIndex, LevelNames, GUILayout.Height(30));
        _menuScroll = GUILayout.BeginScrollView(_menuScroll);

        if (GUILayout.Button("Create new", GUILayout.Height(50)))
            CreateNewLevel();
        if (GUILayout.Button("Load", GUILayout.Height(50))) {
            LoadLevel();
            CreateOrderWaveList();
        }
        if (GUILayout.Button("Save", GUILayout.Height(50)))
            SaveLevel();
        if (GUILayout.Button("Clear", GUILayout.Height(50)))
            ClearLevel();
        if (GUILayout.Button("Update UI", GUILayout.Height(50)))
            UpdateUi();
        if (GUILayout.Button("Refresh", GUILayout.Height(50)))
            RefreshEditor();
        if (GUILayout.Button("Create new path", GUILayout.Height(50)))
            CreateNewPath();
		if(GUILayout.Button("Read google",GUILayout.Height(50)))
			ReadGoogle();
	    if (GUILayout.Button("Check item in wave", GUILayout.Height(50))) {
			CheckItemInWave();
	    }

        if(GUILayout.Button("PLAY", GUILayout.Height(100))) { PlayGame(); }

        ViewOrderWaves();

        GUILayout.EndScrollView();
        GUILayout.EndVertical();
    }

    void ViewOrderWaves() {
        if (_waveReorderableList == null)
            CreateOrderWaveList();
        if (_waveReorderableList != null)
            _waveReorderableList.DoLayoutList();
    }

    void CreateOrderWaveList() {
        if (_levelData == null)
            return;
        _waveReorderableList = new ReorderableList(_levelData.Waves, typeof(WaveData));
        _waveReorderableList.drawElementCallback += (rect, index, active, focused) => {
            EditorGUI.LabelField(rect, "Wave " + index);
        };
    }

    private ReorderableList _waveReorderableList;

    void DisplayWaves() {
        EditorGUILayout.BeginVertical(GUILayout.Width(300));
        if (_levelData != null) {
            EditorGUILayout.BeginVertical();
            _levelData.FuelCost = EditorGUILayout.IntField("Fuel : ", _levelData.FuelCost);
            _levelData.CoinReward = EditorGUILayout.IntField("Reward : ", _levelData.CoinReward);
            _levelData.ReFightCoinReward = EditorGUILayout.IntField("Replay Reward : ", _levelData.ReFightCoinReward);
            _levelData.CombatPower = EditorGUILayout.IntField("Combat Power : ", _levelData.CombatPower);
	        _levelData.EvolveShipId = EditorGUILayout.IntField("Evolve ship id :", _levelData.EvolveShipId);
	        _levelData.EvolveShipNum = EditorGUILayout.IntField("Evolve ship num :", _levelData.EvolveShipNum);
	        _levelData.EvolveDroneId = EditorGUILayout.IntField("Evolve ship id :", _levelData.EvolveDroneId);
	        _levelData.EvolveDroneNum = EditorGUILayout.IntField("Evolve ship id :", _levelData.EvolveDroneNum);

            EditorGUILayout.EndVertical();

            _inforScroll = GUILayout.BeginScrollView(_inforScroll);
            int waveCount = 0;
            GUIStyle waveStyle = new GUIStyle(EditorStyles.helpBox);
            GUIStyle titleStyle = new GUIStyle(EditorStyles.foldout);
            titleStyle.fontSize = 16;
            int width = 80;
            foreach (var wave in _levelData.Waves) {
                waveCount++;
                EditorGUILayout.BeginHorizontal(waveStyle, GUILayout.ExpandWidth(true));

                if (waveCount == _waveIndex + 1) {
                    titleStyle.normal.textColor = Color.yellow;
                    if (GUILayout.Button("Wave " + waveCount, titleStyle))
                        wave.IsShow = !wave.IsShow;
                } else {
                    titleStyle.normal.textColor = Color.white;
                    if (GUILayout.Button("Wave " + waveCount, titleStyle))
                        wave.IsShow = !wave.IsShow;
                }

                // Load wave
                if (GUILayout.Button("Load", GUILayout.ExpandWidth(false)))
                    _waveIndex = waveCount - 1;

                // Delete wave
                if (GUILayout.Button("Delete", GUILayout.ExpandWidth(false))) {
                    _levelData.Waves.Remove(wave);
                    break;
                }

                EditorGUILayout.EndHorizontal();

                if (wave.IsShow) {
                    wave.Hp = EditorGUITool.FloatField("HpScale", wave.Hp, width, GUILayout.ExpandWidth(false));
                    wave.WaveType = (WaveType)EditorGUITool.EnumPopup("Wave type", wave.WaveType, width, GUILayout.ExpandWidth(false));

                    if (wave.WaveType == WaveType.Enemy) {
                        wave.FireRate = EditorGUITool.FloatField("Fire rate", wave.FireRate, width, GUILayout.ExpandWidth(false));

                        wave.NoDam = EditorGUITool.Toggle("No dam", wave.NoDam, width, GUILayout.ExpandWidth(false));
                        wave.IsTeam = EditorGUITool.Toggle("IsTeam", wave.IsTeam, width, GUILayout.ExpandWidth(false));
                        if (wave.IsTeam) {
                            GUILayout.Space(10);
                            GUILayout.Label("IDLE SETTING");

                            wave.IdleType = (IdleType)EditorGUITool.EnumPopup("* Idle type", wave.IdleType, width, GUILayout.ExpandWidth(false));
                            wave.Ease = (Ease)EditorGUITool.EnumPopup("* Easy", wave.Ease, width, GUILayout.ExpandWidth(false));
                            wave.TimeMove = EditorGUITool.FloatField("* Time move", wave.TimeMove, width, GUILayout.ExpandWidth(false));
                        } else {
                            wave.DestroyOnFinish = EditorGUITool.Toggle("Destroy on finish", wave.DestroyOnFinish, width, GUILayout.ExpandWidth(false));
                        }

                        GUILayout.Space(10);
                        GUILayout.Label("ENEMY ATTACK");
                        wave.NumEnemyAttack = EditorGUITool.IntField("* Number", wave.NumEnemyAttack, width, GUILayout.ExpandWidth(false));
                        wave.SpeedMoveAttack = EditorGUITool.FloatField("* Speed", wave.SpeedMoveAttack, width, GUILayout.ExpandWidth(false));
                        wave.DistanceTimeAttack = EditorGUITool.FloatField("* Distance", wave.DistanceTimeAttack, width, GUILayout.ExpandWidth(false));
                        wave.LoopTimeAttack = EditorGUITool.FloatField("* LoopTime", wave.LoopTimeAttack, width, GUILayout.ExpandWidth(false));

                        GUILayout.Space(10);
                        GUILayout.Label("DIFFERENT COMBAT POWER");

                        wave.CombatDiff.TimeFireRate = EditorGUITool.FloatField("# Time (FR)", wave.CombatDiff.TimeFireRate, width, GUILayout.ExpandWidth(false));
                        wave.CombatDiff.FireRateDecrease = EditorGUITool.FloatField("# FireRate D", wave.CombatDiff.FireRateDecrease, width, GUILayout.ExpandWidth(false));
                        wave.CombatDiff.TimeEnemyAttack = EditorGUITool.FloatField("# Time (e)", wave.CombatDiff.TimeEnemyAttack, width, GUILayout.ExpandWidth(false));
                        wave.CombatDiff.EnemyAttackDecease = EditorGUITool.FloatField("# Number e D", wave.CombatDiff.EnemyAttackDecease, width, GUILayout.ExpandWidth(false));
                        wave.CombatDiff.IsIncreaseNumberFire = EditorGUITool.Toggle("# Number e D", wave.CombatDiff.IsIncreaseNumberFire, width, GUILayout.ExpandWidth(false));
                        if (wave.CombatDiff.IsIncreaseNumberFire)
                            wave.CombatDiff.TimeIncreaseNumberFire = EditorGUITool.IntField("# Time increase:", wave.CombatDiff.TimeIncreaseNumberFire, width, GUILayout.ExpandWidth(false));

                        GUILayout.Space(10);
                        GUILayout.Label("ITEMS");
                        // ItemsWave
                        if (wave.Items == null)
                            wave.Items = new ItemsWaveData();
                        for (int i = 0; i < wave.Items.List.Count; i++) {
                            GUILayout.BeginHorizontal();
                            wave.Items.List[i].Type = (ItemType)EditorGUILayout.EnumPopup(wave.Items.List[i].Type);
                            wave.Items.List[i].Num = EditorGUILayout.IntField(wave.Items.List[i].Num);
                            if (GUILayout.Button("Del")) {
                                wave.Items.List.RemoveAt(i);
                                break;
                            }

                            GUILayout.EndHorizontal();
                        }

                        wave.Items.NumEnemiesCanHaveItem = EditorGUILayout.IntField("NumECanHaveItem", wave.Items.NumEnemiesCanHaveItem);

                        if (GUILayout.Button("Add Item"))
                            wave.Items.List.Add(new ItemWaveData());
                    } else if (wave.WaveType == WaveType.Rock) {
                        GUILayout.Space(10);
                        GUILayout.Label("ROCK SETTING");
                        wave.Rock.StartPosition = EditorGUILayout.Vector3Field("* Start ", wave.Rock.StartPosition);
                        wave.Rock.EndPosition = EditorGUILayout.Vector3Field("* End ", wave.Rock.EndPosition);
                        wave.Rock.Range = EditorGUITool.FloatField("* Range", wave.Rock.Range, width, GUILayout.ExpandWidth(false));
                        wave.Rock.Speed = EditorGUITool.FloatField("* Speed", wave.Rock.Speed, width, GUILayout.ExpandWidth(false));
                        wave.Rock.NumBig = EditorGUITool.IntField("* Big", wave.Rock.NumBig, width, GUILayout.ExpandWidth(false));
                        wave.Rock.NumNormal = EditorGUITool.IntField("* Normal", wave.Rock.NumNormal, width, GUILayout.ExpandWidth(false));
                        wave.Rock.NumSmall = EditorGUITool.IntField("* Small", wave.Rock.NumSmall, width, GUILayout.ExpandWidth(false));
                        wave.Rock.Duration = EditorGUITool.FloatField("* Duration", wave.Rock.Duration, width, GUILayout.ExpandWidth(false));
                    } else if (wave.WaveType == WaveType.Boss) {
                        GUILayout.Space(10);
                        GUILayout.Label("BOSS SETTING");
                        wave.Boss.BossIndex = EditorGUITool.IntField("Boss", wave.Boss.BossIndex, width, GUILayout.ExpandWidth(false));

                        GUILayout.Space(10);
                        GUILayout.Label("IDLE SETTING");

                        wave.IdleType = (IdleType)EditorGUITool.EnumPopup("* Idle type", wave.IdleType, width, GUILayout.ExpandWidth(false));
                        wave.Ease = (Ease)EditorGUITool.EnumPopup("* Easy", wave.Ease, width, GUILayout.ExpandWidth(false));
                        wave.TimeMove = EditorGUITool.FloatField("* Time move", wave.TimeMove, width, GUILayout.ExpandWidth(false));

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Hp");
                        GUILayout.Label("Item");
                        GUILayout.EndHorizontal();
                        // View hps boss
                        for (int i = 0; i < wave.Boss.Hps.Count; i++) {
                            GUILayout.BeginHorizontal();
                            wave.Boss.Hps[i].Hp = EditorGUITool.IntField(string.Empty, wave.Boss.Hps[i].Hp);
                            wave.Boss.Hps[i].NumItem = EditorGUITool.IntField(string.Empty, wave.Boss.Hps[i].NumItem);
                            if (GUILayout.Button("Del")) {
                                wave.Boss.Hps.RemoveAt(i);
                                break;
                            }
                            GUILayout.EndHorizontal();
                        }

                        if (GUILayout.Button("Add Hp")) {
                            wave.Boss.Hps.Add(new BossHpInfor());
                        }
                    } else if (wave.WaveType == WaveType.GiftBox) {
                        GUILayout.Space(10);
                        GUILayout.Label("GIFTBOX SETTING");
                        wave.GiftBox.Duration = EditorGUITool.FloatField("Duration", wave.GiftBox.Duration, width, GUILayout.ExpandWidth(false));
                        wave.GiftBox.NumGiftBox = EditorGUITool.IntField("Number", wave.GiftBox.NumGiftBox, width, GUILayout.ExpandWidth(false));
                        wave.GiftBox.Speed = EditorGUITool.FloatField("Speed", wave.GiftBox.Speed, width, GUILayout.ExpandWidth(false));
                    }
                }
                GUILayout.Space(30);

                // Warning setting
                GUILayout.Label("WARNING SETTING");
                wave.UseWarning = EditorGUITool.Toggle("Warning", wave.UseWarning, width);
                if (wave.UseWarning) {
                    for (int i = 0; i < wave.WarningPositions.Count; i++) {
                        GUILayout.BeginHorizontal();
                        wave.WarningPositions[i] = EditorGUILayout.Vector3Field(string.Empty, wave.WarningPositions[i]);
                        if (GUILayout.Button("Del")) {
                            wave.WarningPositions.RemoveAt(i);
                            break;
                        }
                        GUILayout.EndHorizontal();
                    }
                    if (GUILayout.Button("Add warning postion"))
                        wave.WarningPositions.Add(new Vector3());
                }
            }

            GUILayout.EndScrollView();
            if (GUILayout.Button("Add Wave")) {
                _levelData.Waves.Add(new WaveData());
            }
        }
        EditorGUILayout.EndVertical();
    }

    void OnFocus() {
        // Remove delegate listener if it has previously
        // been assigned.
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
        // Add (or re-add) the delegate.
        SceneView.onSceneGUIDelegate += this.OnSceneGUI;
    }
    void OnDestroy() {
        // When the window is destroyed, remove the delegate
        // so that it will no longer do any drawing.
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
    }

    void OnSceneGUI(SceneView sceneView) {
        if (_levelData == null || _levelData.Waves == null || _waveIndex > _levelData.Waves.Count - 1)
            return;

        // Draw rock setting
        if (_levelData.Waves[_waveIndex].WaveType == WaveType.Rock) {
            _levelData.Waves[_waveIndex].Rock.StartPosition = Handles.PositionHandle(_levelData.Waves[_waveIndex].Rock.StartPosition, Quaternion.identity);
            _levelData.Waves[_waveIndex].Rock.EndPosition = Handles.PositionHandle(_levelData.Waves[_waveIndex].Rock.EndPosition, Quaternion.identity);

            _levelData.Waves[_waveIndex].Rock.Range = (Handles.PositionHandle(_levelData.Waves[_waveIndex].Rock.StartPosition + Vector3.right * _levelData.Waves[_waveIndex].Rock.Range, Quaternion.identity) - _levelData.Waves[_waveIndex].Rock.StartPosition).x;

            Handles.Label(_levelData.Waves[_waveIndex].Rock.StartPosition, "Start Rock");
            Handles.DrawLine(_levelData.Waves[_waveIndex].Rock.StartPosition, _levelData.Waves[_waveIndex].Rock.EndPosition);

            Handles.color = Color.red;
            Handles.DrawLine(_levelData.Waves[_waveIndex].Rock.StartPosition + Vector3.left * _levelData.Waves[_waveIndex].Rock.Range,
                _levelData.Waves[_waveIndex].Rock.StartPosition + Vector3.right * _levelData.Waves[_waveIndex].Rock.Range);

            Handles.DrawLine(_levelData.Waves[_waveIndex].Rock.EndPosition + Vector3.left * _levelData.Waves[_waveIndex].Rock.Range,
                _levelData.Waves[_waveIndex].Rock.EndPosition + Vector3.right * _levelData.Waves[_waveIndex].Rock.Range);

            Handles.DrawLine(_levelData.Waves[_waveIndex].Rock.StartPosition + Vector3.right * _levelData.Waves[_waveIndex].Rock.Range,
                _levelData.Waves[_waveIndex].Rock.EndPosition + Vector3.right * _levelData.Waves[_waveIndex].Rock.Range);

            Handles.DrawLine(_levelData.Waves[_waveIndex].Rock.StartPosition + Vector3.left * _levelData.Waves[_waveIndex].Rock.Range,
                _levelData.Waves[_waveIndex].Rock.EndPosition + Vector3.left * _levelData.Waves[_waveIndex].Rock.Range);
        }

        // Draw warning setting
        if (_levelData.Waves[_waveIndex].UseWarning && _levelData.Waves[_waveIndex].WarningPositions.Count > 0)
            for (int i = 0; i < _levelData.Waves[_waveIndex].WarningPositions.Count; i++) {
                _levelData.Waves[_waveIndex].WarningPositions[i] = Handles.PositionHandle(_levelData.Waves[_waveIndex].WarningPositions[i], Quaternion.identity);
                Handles.Label(_levelData.Waves[_waveIndex].WarningPositions[i], "WarPos " + (i + 1));
            }
    }

    void DisplayWaveInfor() {
        GUILayout.BeginVertical();

        if (_levelData != null && _levelData.Waves != null && _levelData.Waves.Count > 0) {
            //            if (GameData != null)
            //            {
            // View list enemies
            GUILayout.BeginHorizontal();
            GUIStyle s = new GUIStyle(EditorStyles.miniButton);

            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            for (int i = 0; i < 55; i++) {
                if (_enemyBoss.Contains(i))
                    continue;

                if (_enemySelectIndex == i) {
                    s.normal.textColor = Color.yellow;
                    s.fontSize = 16;
                } else {
                    s.normal.textColor = Color.white;
                    s.fontSize = 12;
                }
                if (GUILayout.Button("E" + (i + 1), s, GUILayout.Width(40), GUILayout.Height(40))) {
                    _enemySelectIndex = i;
                }

                if (i % 26 == 0 && i != 0) {
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                }
            }

            if (_enemySelectIndex == -1) {
                s.normal.textColor = Color.yellow;
                s.fontSize = 16;
            } else {
                s.normal.textColor = Color.white;
                s.fontSize = 12;
            }
            if (GUILayout.Button("Null", s, GUILayout.Width(40), GUILayout.Height(40))) {
                _enemySelectIndex = -1;
            }

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            GUILayout.EndHorizontal();
            //            }

            GUILayout.BeginVertical();
            GUILayout.Label("Wave " + (_waveIndex + 1));
            WaveData _wave = _levelData.Waves[_waveIndex];
            string cellName;
            for (int i = 0; i < 7; i++) {
                GUILayout.BeginHorizontal();
                for (int j = 0; j < 7; j++) {
                    {
                        if (_wave.Enemies[i].Rows[j].EnemyId >= 0) {
                            cellName = "E" + (_wave.Enemies[i].Rows[j].EnemyId + 1);
                            int pathIndex = 0;
                            // Show enemy path
                            foreach (var pathInfor in _wave.Paths) {
                                if (pathInfor.HasCell(i, j)) {
                                    cellName += "-" + (pathIndex + 1) + "\n" + pathInfor.GetIndex(i, j);
                                    break;
                                }
                                pathIndex++;
                            }
                        } else {
                            cellName = string.Empty;
                        }
                    }
                    if (GUILayout.Button(cellName, GUILayout.Width(60), GUILayout.Height(40))) {
                        // Add enemy to path
                        if (Event.current.alt) {
                            // remove enemy form others path
                            foreach (var pathInfor in _wave.Paths)
                                pathInfor.RemoveCell(i, j);
                            // add enemy to selected path
                            if (_wave.Enemies[i].Rows[j].EnemyId >= 0)
                                _wave.Paths[_wavePathSelectIndex].AddCell(new Cell { Row = i, Coll = j });
                        } else
                            // Set enemy to matrix
                            _wave.Enemies[i].Rows[j].EnemyId = _enemySelectIndex;
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.BeginHorizontal();

            GUILayout.Label("Paths");
            if (GUILayout.Button("Add Path", GUILayout.ExpandWidth(false))) {
                if (_wave.Paths == null)
                    _wave.Paths = new List<PathInfor>();
                _wave.Paths.Add(new PathInfor());
                _wavePathSelectIndex = _wave.Paths.Count - 1;
            }
            GUIStyle style = new GUIStyle(EditorStyles.toolbar);
            style.border = new RectOffset(10, 10, 10, 10);
            GUILayout.EndHorizontal();
            int pathCount = 0;
            GUIStyle pathTitleStyle1 = new GUIStyle(EditorStyles.label);
            pathTitleStyle1.normal.textColor = Color.yellow;
            pathTitleStyle1.fontSize = 16;

            _wavePathScroll = GUILayout.BeginScrollView(_wavePathScroll);
            if (_wave.Paths != null && _wave.Paths.Count > 0)
                foreach (var pathInfor in _wave.Paths) {
                    Rect r = EditorGUILayout.BeginVertical();
                    GUI.Box(r, GUIContent.none, style);
                    EditorGUILayout.BeginHorizontal();


                    if (_wavePathSelectIndex == pathCount)
                        GUILayout.Label("" + (pathCount + 1), pathTitleStyle1, GUILayout.ExpandWidth(false));
                    else
                        GUILayout.Label("" + (pathCount + 1), GUILayout.ExpandWidth(false));

                    if (GUILayout.Button("Select", GUILayout.ExpandWidth(false))) {
                        _wavePathSelectIndex = pathCount;
                    }

                    pathInfor.Index = EditorGUILayout.Popup(pathInfor.Index, PathNames, GUILayout.Width(100));
                    // Focus path
                    if (GUILayout.Button("Focus")) {
                        Selection.activeObject = Paths.transform.GetChild(pathInfor.Index);
                        EditorGUIUtility.PingObject(Paths.transform.GetChild(pathInfor.Index));
                    }
                    if (GUILayout.Button("Reset", GUILayout.ExpandWidth(false))) {
                        pathInfor.Cells.Clear();
                        break;
                    }
                    // Delete path
                    if (GUILayout.Button("Del", GUILayout.ExpandWidth(false))) {
                        _wave.Paths.Remove(pathInfor);
                        _wavePathSelectIndex = -1;
                        break;
                    }
                    GUILayout.EndHorizontal();

                    GUILayout.BeginVertical();
                    pathInfor.DelayTime = EditorGUITool.FloatField("Delay Time", pathInfor.DelayTime, 90, GUILayout.ExpandWidth(false));
                    GUILayout.BeginHorizontal();
                    pathInfor.Offset = EditorGUITool.Vecter2Field("Offset", pathInfor.Offset, 90);
                    pathInfor.FlipX = EditorGUITool.Toggle("FlipX", pathInfor.FlipX, 30);
                    pathInfor.FlipY = EditorGUITool.Toggle("FlipY", pathInfor.FlipY, 30);
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    pathInfor.Distance = EditorGUITool.FloatField("Distance", pathInfor.Distance, 60);
                    pathInfor.MoveSpeed = EditorGUITool.FloatField("Speed", pathInfor.MoveSpeed, 60);
                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                    GUILayout.BeginHorizontal();
                    GUILayout.EndVertical();
                    pathCount++;
                    EditorGUILayout.EndVertical();
                    GUILayout.Space(10);
                }
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        } else {
            GUILayout.Label("No wave");
        }
        GUILayout.EndVertical();
    }

    #region Menu buttons

    /// <summary>
    /// Create new level
    /// </summary>
    void CreateNewLevel() {
        var LevelPath = LevelService.FullPath;
        var EndLessPath = EndlessLevelService.FullPath;
        if (!AssetDatabase.IsValidFolder(LevelService.FullPath + "/")) {
            var lv = CreateInstance<LevelData>();

            if (_mode == 0) {
                LevelService.Instance.LoadAllLevelData();
                lv.name = "Level " + (LevelService.Instance.LevelData.Count + 1);
                AssetDatabase.CreateAsset(lv, LevelPath + "/" + lv.name + ".asset");
                AssetDatabase.SaveAssets();
                LevelService.Instance.LoadAllLevelData();
            } 
            else if (_mode == 2) {
	            lv.name = "Level Tutorial";
	            AssetDatabase.CreateAsset(lv, LevelPath + "/" + lv.name + ".asset");
				AssetDatabase.SaveAssets();
            }
            else {
                lv.name = "HalloweenLevel " + (EndlessLevelService.Instance.Levels.Count + 1);
                AssetDatabase.CreateAsset(lv, EndLessPath + "/" + lv.name + ".asset");
                AssetDatabase.SaveAssets();
                EndlessLevelService.Instance.Reload();
            }
            RefreshEditor();
            UpdateUi();
            //            EditorUtility.SetDirty(LevelManager);
        } else {
            Debug.LogWarning("Folder not found, press again to create new.");
            AssetDatabase.CreateFolder("Assets", "Levels");
        }
    }

    /// <summary>
    /// Load level with index of level selected
    /// </summary>
    void LoadLevel() {
        if (_mode == 0) {
            _levelData = LevelService.Instance.GetByIdForTool(_levelSelectIndex);
            Notify("Level " + (_levelSelectIndex + 1) + " loaded");
        }

	    else if (_mode == 2) {
		    _levelData = ResourceLogger.Load<LevelData>("Levels/Level Tutorial");
		    Notify("Level Tutorial" + " loaded");
	    }
        else {
            _levelData = EndlessLevelService.Instance.GetById(_levelSelectIndex);
            Notify("HalloweenLevel " + (_levelSelectIndex + 1) + " loaded");
        }
        _waveIndex = 0;
    }

    /// <summary>
    /// Create new path
    /// </summary>
    void CreateNewPath() {
        if (_curMode == 0) {
            var prefab = Paths.transform.GetChild(0);
            var newPath = Instantiate(prefab, prefab.transform.parent);
            newPath.name = "Path" + (Paths.transform.childCount);
            Selection.activeObject = newPath;
            EditorGUIUtility.PingObject(newPath);
            UpdatePaths();
        } else {
            var prefab = Paths.transform.GetChild(1);
            var newPath = Instantiate(prefab, prefab.transform.parent);
            newPath.name = "Path" + (Paths.transform.childCount);
            Selection.activeObject = newPath;
            EditorGUIUtility.PingObject(newPath);
            UpdatePaths();
        }
    }

    /// <summary>
    /// Save level
    /// </summary>
    void SaveLevel() {
        if (_mode == 0) {
            EditorUtility.SetDirty(LevelService.Instance.GetByIdForTool(_levelSelectIndex));
            Notify("Level " + (_levelSelectIndex + 1) + " saved");
        }
        else if (_mode == 2) {
	        EditorUtility.SetDirty(ResourceLogger.Load<LevelData>("Levels/Level Tutorial"));
	        Notify("Level Tutorial" + " saved");
        }
        else {
            EditorUtility.SetDirty(EndlessLevelService.Instance.GetById(_levelSelectIndex));
            Notify("HalloweenLevel " + (_levelSelectIndex + 1) + " saved");
        }
        AssetDatabase.SaveAssets();

    }

    /// <summary>
    /// Reset level
    /// </summary>
    void ClearLevel() {
        Notify("Cleared");
    }

    void Notify(string content) {
        ShowNotification(new GUIContent { text = content });
    }

    /// <summary>
    /// Update UI References
    /// </summary>
    void UpdateUi() {
        LevelService.Instance.LoadAllLevelData();
        EndlessLevelService.Instance.Reload();
    }

    void RefreshEditor() {
        LevelService.Instance.LoadAllLevelData();
        //LevelNames = _mode == 0 ?
        //    LevelService.Instance.LevelData.Values.Select(s => s.name).ToArray() :
        //    EndlessLevelService.Instance.Levels.ToList().Select(s => s.name).ToArray();
	    switch (_mode) {
			case 0:
				LevelNames = LevelService.Instance.LevelData.Values.Select(s => s.name).ToArray();
				break;
			case 1:
				LevelNames = EndlessLevelService.Instance.Levels.ToList().Select(s => s.name).ToArray();
				break;
			case 2:
				LevelData tutorial = ResourceLogger.Load<LevelData>("Levels/Level Tutorial");
				LevelNames = tutorial == null ? new[] {string.Empty} : new[] {tutorial.name};
				break;
	    }

        UpdatePaths();

        Paths.InitPathNormal();

        Paths.InitPathNormal();
    }

    void UpdatePaths() {
        PathNames = Paths.transform.GetAllChilds().Select(s => s.name).ToArray();
    }

    void PlayGame() {
        if(EditorApplication.isPlaying) { EditorApplication.isPlaying = false; }
        else {
            Logs.Log("TO DO: Set Level Here");
            EditorApplication.isPlaying = true;
        }
    }

	void ReadGoogle() {
		Debug.Log("Read google sheet");
		List<CellEntry> data = ReadGoogleSheet.DoCellQuery("Evolve ship data", "CP Level");
		int startRow = 2;
		int limitRow = 79;
		int count = 0;
		for (int i = startRow; i <= limitRow; i++) {
			LevelData lvlData = LevelService.Instance.GetByIdForTool(count);
			lvlData.CombatPower = ReadGoogleSheet.GetIntFromCell(data, i, 2);
			lvlData.CoinReward = ReadGoogleSheet.GetIntFromCell(data, i, 3);
			lvlData.ReFightCoinReward = ReadGoogleSheet.GetIntFromCell(data, i, 4);
			lvlData.EvolveShipNum = ReadGoogleSheet.GetIntFromCell(data, i, 5);
			lvlData.EvolveShipId = ReadGoogleSheet.GetIntFromCell(data, i, 6) - 1;
			lvlData.EvolveDroneNum = ReadGoogleSheet.GetIntFromCell(data, i, 7);
			lvlData.EvolveDroneId = ReadGoogleSheet.GetIntFromCell(data, i, 8) - 1;
			EditorUtility.SetDirty(lvlData);
			AssetDatabase.SaveAssets();
			count += 1;
		}
	}

	void CheckNormalLevel() {
		for (int i = 0; i < 78; i++) {
			LevelData lvlData = LevelService.Instance.GetByIdForTool(i);
			for (int wave = 0; wave < lvlData.Waves.Count; wave++) {
				if (lvlData.Waves[wave].WaveType == WaveType.Enemy) {
					int countEnemy = 0;
					int countItem = 0;
					foreach (var col in lvlData.Waves[wave].Enemies) {
						foreach (var row in col.Rows) {
							if (row.EnemyId >= 0) {
								countEnemy += 1;
							}
						}
					}

					foreach (var item in lvlData.Waves[wave].Items.List) {
						countItem += item.Num;
					}

					if (countItem > lvlData.Waves[wave].Items.NumEnemiesCanHaveItem) {
						Debug.LogFormat("Tong so item lon hon so ma enemy co\nLevel {0}\nWave {1}", i + 1, wave + 1);
					}

					if (lvlData.Waves[wave].Items.NumEnemiesCanHaveItem > countEnemy) {
						Debug.LogFormat("Tong so item lon hon so enemy\nLevel {0}\nWave {1}", i + 1, wave + 1);
					}
				}
			}
		}
	}

	void CheckHalloween() {
		LevelData lvlData = Resources.Load<LevelData>("Halloween/HalloweenLevel 1");
		for (int wave = 0; wave < lvlData.Waves.Count; wave++) {
			if (lvlData.Waves[wave].WaveType == WaveType.Enemy) {
				int countEnemy = 0;
				int countItem = 0;
				foreach (var col in lvlData.Waves[wave].Enemies) {
					foreach (var row in col.Rows) {
						if (row.EnemyId >= 0) {
							countEnemy += 1;
						}
					}
				}

				foreach (var item in lvlData.Waves[wave].Items.List) {
					countItem += item.Num;
				}

				if (countItem > lvlData.Waves[wave].Items.NumEnemiesCanHaveItem) {
					Debug.LogFormat("Tong so item lon hon so ma enemy co\nWave {1}",wave + 1);
				}

				if (lvlData.Waves[wave].Items.NumEnemiesCanHaveItem > countEnemy) {
					Debug.LogFormat("Tong so item lon hon so enemy\nWave {1}", wave + 1);
				}
			}
		}
	}

	void CheckItemInWave() {
		if (_mode == 0) {
			Debug.Log("Normal");
			CheckNormalLevel();
		}
		else if (_mode == 1) {
			Debug.Log("Halloween");
			CheckHalloween();
		}
	}

    #endregion
}
