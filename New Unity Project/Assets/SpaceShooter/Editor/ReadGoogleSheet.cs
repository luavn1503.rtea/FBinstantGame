﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GDataDB;
using GDataDB.Impl;
using Google.GData.Client;
using Google.GData.Spreadsheets;
using UnityEngine;

public class ReadGoogleSheet {
	public static List<CellEntry> DoCellQuery(string spreadSheetName, string workSheetName) {
		var client = new DatabaseClient("", "");
		if (string.IsNullOrEmpty(spreadSheetName))
			return null;
		if (string.IsNullOrEmpty(workSheetName))
			return null;
		var error = string.Empty;
		var db = client.GetDatabase(spreadSheetName, ref error);
		if (db == null) {
			Debug.Log("Db null");
			return null;
		}
		var worksheet = ((Database)db).GetWorksheetEntry(workSheetName);
		var cellQuery = new CellQuery(worksheet.CellFeedLink);
		var cellFeed = client.SpreadsheetService.Query(cellQuery);

		return cellFeed.Entries.Cast<CellEntry>().ToList();
	}

	public static string GetStringCell(List<CellEntry> data, int row, int col) {
		row += 1;
		col += 1;
		var x = data.Find(s => s.Row == row && s.Column == col);
		return x != null ? x.Value : string.Empty;
	}

	public static int GetIntFromCell(List<CellEntry> data, int row, int col) {
		row += 1;
		col += 1;
		var x = data.Find(s => s.Row == row && s.Column == col);
		return x == null ? 0 : int.Parse(x.Value);
	}

	public static float GetFloatFromCell(List<CellEntry> data, int row, int col) {
		row += 1;
		col += 1;
		var x = data.Find(s => s.Row == row && s.Column == col);
		return x == null ? 0f : float.Parse(x.Value);
	}
}
