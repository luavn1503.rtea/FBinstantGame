﻿using UnityEngine;

public class SoundEffect : SoundItem {
    /**<summary> Play one shot and forget it. Can not stop the played clip.</summary>*/
    public override void Play() {
        SFX.Instance.PlaySoundEffect(audioClip);
    }

    public override void Stop() {
    }
}
