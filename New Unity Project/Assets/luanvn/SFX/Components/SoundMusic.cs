﻿using UnityEngine;

public class SoundMusic : SoundItem {
    public float delayAutoPlay = 0;
    public float fadeDuration = SFX.FadeDuration;
    public bool fadein = true;
    public bool fadeout = true;

    /**<summary>Play a loop ambient sound like background music.</summary>*/
    public override void Play() {
        if (delayAutoPlay == 0) {
            SFX.Instance.PlayBackgroundMusic(audioClip, fadein, fadeout);
        }
        else {
            DG.Tweening.DOVirtual.DelayedCall(delayAutoPlay, () => {
                SFX.Instance.PlayBackgroundMusic(audioClip, fadein, fadeout);
            });
        }
    }

    public override void Stop() {
        SFX.Instance.StopBackgroundMusic(fadeout);
    }
    
    public void Pause() {
        SFX.Instance.PauseBackgroundMusic(fadeout);
    }

    public void Resume() {
        SFX.Instance.ResumeBackgroundMusic(fadein);
    }
}
