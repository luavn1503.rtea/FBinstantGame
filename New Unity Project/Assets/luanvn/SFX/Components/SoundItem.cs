﻿using UnityEngine;

public abstract class SoundItem : MonoBehaviour {
    [SerializeField] protected AudioClip audioClip;

    public enum AutoPlay { None, Awake, Start, Enable }
    public AutoPlay autoPlay = AutoPlay.None;

    private void Awake() {
        if (autoPlay == AutoPlay.Awake) Play();
    }

    private void OnEnable() {
        if (autoPlay == AutoPlay.Enable) Play();
    }

    void Start() {
        if (autoPlay == AutoPlay.Start) Play();
    }

    public abstract void Play();
    public abstract void Stop();
}
