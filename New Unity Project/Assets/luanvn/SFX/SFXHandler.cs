﻿using UnityEngine;
using System.Collections;
using System;

public partial class SFX {

    public event Action<bool> OnBackgroundMusicEnableHandler;
    public event Action<float> OnBackgroundMusicVolumeHandler;
    public event Action<bool> OnSoundEffectEnableHandler;
    public event Action<float> OnSoundEffectVolumeHandler;

    private void RegisterListener() {
        OnBackgroundMusicEnableHandler += HandleBackgroundMusicEnable;
        OnBackgroundMusicVolumeHandler += HandleBackgroundMusicVolumeChange;
    }

    #region Event Handler
    private void HandleBackgroundMusicEnable(bool enable) {
        if (enable) SFX.Instance.PlayBackgroundMusic();
        else SFX.Instance.PauseBackgroundMusic();
    }

    private void HandleBackgroundMusicVolumeChange(float volume) {
        SFX.Instance.SetAudioSourceVolume(volume);
    }

    private void HandleSoundEffectEnable(bool enable) {

    }

    private void HandleSoundEffectVolumeChange(float volume) {

    }
    #endregion
}
