using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SFX))]
public class SFXEditor : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        SFXEditor.DrawSFXEditor();
    }

    public static void DrawSFXEditor() { 
        if (!Application.isPlaying) return;

        
		SFX sfx = SFX.Instance;

        GUILayout.Space(20);
        EditorGUILayout.BeginVertical();

        EditorGUILayout.BeginHorizontal();
        {
            if (GUILayout.Button(string.Format("BgMusicEnable: {0}", sfx.BackgroundMusicEnable))) {
                sfx.BackgroundMusicEnable = !sfx.BackgroundMusicEnable;
            }
            
            if (GUILayout.Button("Play")) {
                if (sfx.BackgroundMusicEnable) sfx.PlayBackgroundMusic();
            }

            if (GUILayout.Button("Stop")) {
                if (sfx.BackgroundMusicEnable) sfx.StopBackgroundMusic();
            }
            EditorGUILayout.EndHorizontal();
        }

        GUILayout.Space(10);
        if (GUILayout.Button(string.Format("SoundEffectEnable: {0}", sfx.SoundEffectEnable))) {
            sfx.SoundEffectEnable = !sfx.SoundEffectEnable;
        }

        EditorGUILayout.EndVertical();
	}
}