﻿using System.Collections;
using UnityEngine;
using System;
using Gemmob;

/**<summary>Controller of the sound and music method in the game. DO NOT CHANGE THIS.</summary>*/
public sealed partial class SFX : SingletonFreeAlive<SFX> {
    public const float FadeDuration = 1.0f;

    private AudioSource audioSource;
    
    #region Initialize
    protected override void OnAwake() {
        InitAudioSource();
        RegisterListener();
    }

    private void InitAudioSource() {
        if (audioSource == null) {
            audioSource = new GameObject("AudioSource", typeof(AudioSource)).GetComponent<AudioSource>();
            audioSource.transform.SetParent(transform);
        }
        audioSource.playOnAwake = false;
        audioSource.loop = true;
    }

    public void SetAudioSourceVolume(float volume) {
        if (audioSource == null) return;
        audioSource.volume = volume;
    }
    #endregion

    #region Background Music
    public void PlayBackgroundMusic(AudioClip music, bool fadein = true, bool fadeoutCurrent = true, float fadeDuration = FadeDuration) {
        if (music == null || audioSource == null || !BackgroundMusicEnable || (audioSource.isPlaying && audioSource.clip.Equals(music))) return;

        System.Action play = () => {
            if (audioSource.isPlaying) audioSource.Stop();
            audioSource.clip = music;
            audioSource.Play();
            if (!fadein) audioSource.volume = BackgroundVolume;
            else FadeInBackgroundMusic(fadeDuration);
        };

        if (!audioSource.isPlaying) {
            play.Invoke();
            return;
        }

        if (!fadeoutCurrent) {
            play.Invoke();
            return;
        }

        FadeOutBackgroundMusic(fadeDuration, play);
    }

    public void PlayBackgroundMusic(bool fadein = true, float fadeDuration = FadeDuration) {
        if (audioSource == null || !BackgroundMusicEnable || audioSource.isPlaying) return;
        if (audioSource.clip == null) {
            Logs.Log("[SFX] Null audio clip! Play BackgroundMusic Failed!");
            return;
        }

        audioSource.Play();

        if (!fadein) audioSource.volume = BackgroundVolume;
        else FadeInBackgroundMusic(fadeDuration);
    }

    public void ResumeBackgroundMusic(bool fadein = true, float fadeDuration = FadeDuration) {
        if (audioSource == null || audioSource.clip == null || !BackgroundMusicEnable) return;

        audioSource.UnPause();

        if (!fadein) audioSource.volume = BackgroundVolume;
        else FadeInBackgroundMusic(fadeDuration);
    }

    public void PauseBackgroundMusic(bool fadeout = true, float fadeDuration = FadeDuration) {
        if (audioSource == null || !audioSource.isPlaying) return;

        if (!fadeout) { audioSource.Pause(); }
        else FadeOutBackgroundMusic(fadeDuration, () => { audioSource.Pause(); });
    }

    public void StopBackgroundMusic(bool fadeout = true, float fadeDuration = FadeDuration) {
        if (audioSource == null || !audioSource.isPlaying) return;

        if (!fadeout) { audioSource.Stop(); }
        else FadeOutBackgroundMusic(fadeDuration, () => { audioSource.Stop(); });
    }
    #endregion

    #region Sound Effect
    public void PlaySoundEffect(AudioClip audio) {
        if (audioSource == null || audio == null || !SoundEffectEnable) return;
        audioSource.PlayOneShot(audio, SoundEffectVolume);
    }
    #endregion

    #region Other Audio Source
    public void StopAudio(AudioSource audioSource) {
        if (audioSource == null || !audioSource.isPlaying) return;
        audioSource.Stop();
    }

    public AudioSource CloneAudio(bool removeOnDone = false) {
        return CloneAudio(audioSource, removeOnDone);
    }

    public AudioSource CloneAudio(AudioSource source, bool removeOnDone = false) {
        if (source == null) return null;
        AudioSource dup = source.Spawn(source.transform.parent);
        if (removeOnDone) StartCoroutine(IEWaitForDone(dup, RemoveAudio));
        return dup;
    }

    private void RemoveAudio(AudioSource audioSource) {
        if (audioSource == null) return;
        audioSource.Recycle();
    }

    IEnumerator IEWaitForDone(AudioSource audioSource, System.Action<AudioSource> callback, float delayCallback = 0) {
        if (audioSource == null || callback == null) yield break;

        yield return null;

        while (audioSource != null && audioSource.isPlaying) yield return null;
        if (delayCallback > 0) yield return Yielder.Wait(delayCallback);

        Callback.Call(callback, audioSource);
    }

    #endregion

    #region Volume Fade Effect
    private void FadeInBackgroundMusic(float duration = FadeDuration, System.Action callback = null) {
        FadeInSound(audioSource, BackgroundVolume, duration, callback);
    }

    private void FadeOutBackgroundMusic(float duration = FadeDuration, System.Action callback = null) {
        FadeOutSound(audioSource, duration, callback);
    }

    private void FadeInSound(AudioSource audio, float toVolume, float duration = FadeDuration, System.Action callback = null) {
        StartCoroutine(IEFadeSound(audio, 0, toVolume, duration, callback));
    }

    private void FadeOutSound(AudioSource audio, float duration = FadeDuration, System.Action callback = null) {
        StartCoroutine(IEFadeSound(audio, audio.volume, 0, duration, callback));
    }

    IEnumerator IEFadeSound(AudioSource audio, float froVolume, float toVolume, float duration = FadeDuration, System.Action callback = null) {
        if (audio == null) {
            Logs.LogWarning(string.Format("[SFX] Fade Sound Fall! Null audio {0}", audio.name));
            yield break;
        }
        float t = 0;
        while (t < duration && audio != null) {
            t += Time.deltaTime;
            audio.volume = Mathf.Lerp(froVolume, toVolume, t / duration);
            yield return null;
        }
        if (audio) audio.volume = toVolume;
        else Logs.LogWarning("[SFX] audio was broke while running fade coroutine!");

        Callback.Call(callback);
    }
    #endregion
}