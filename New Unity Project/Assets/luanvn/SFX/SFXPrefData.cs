﻿using UnityEngine;
using System;

public partial class SFX {
    private class PrefKey {
        public const string BackgroundMusicEnable = "BackgroundMusicEnable";
        public const string SoundEffectEnable = "SoundEffectEnable";
        public const string BackgroundMusicVolume = "BackgroundMusicVolume";
        public const string SoundEffectVolume = "SoundEffectVolume";
    }

    public bool BackgroundMusicEnable {
        get { return PlayerPrefs.GetInt(PrefKey.BackgroundMusicEnable, 1) == 1; }
        set {
            if (value == BackgroundMusicEnable) return;
            PlayerPrefs.SetInt(PrefKey.BackgroundMusicEnable, value == true ? 1 : 0);
            Callback.Call(SFX.Instance.OnBackgroundMusicEnableHandler, value);
        }
    }

    public float BackgroundVolume {
        get { return PlayerPrefs.GetFloat(PrefKey.BackgroundMusicVolume, 1f); }
        set {
            float v = Mathf.Clamp01(value);
            if (v == BackgroundVolume) return;
            PlayerPrefs.SetFloat(PrefKey.BackgroundMusicVolume, v);
            Callback.Call(SFX.Instance.OnBackgroundMusicVolumeHandler, v);
        }
    }

    public bool SoundEffectEnable {
        get { return PlayerPrefs.GetInt(PrefKey.SoundEffectEnable, 1) == 1; }
        set { PlayerPrefs.SetInt(PrefKey.SoundEffectEnable, value == true ? 1 : 0); }
    }

    public float SoundEffectVolume {
        get { return PlayerPrefs.GetFloat(PrefKey.SoundEffectVolume, 1); }
        set {
            float v = Mathf.Clamp01(value);
            PlayerPrefs.SetFloat(PrefKey.SoundEffectVolume, v);
        }
    }
}
