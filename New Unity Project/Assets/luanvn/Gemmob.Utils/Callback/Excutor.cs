﻿using System.Collections;
using System;
using UnityEngine;
using System.Collections.Generic;
using Gemmob;

public class Excutor : SingletonFreeAlive<Excutor> {
    #region private Queue Excute
    private readonly Queue<Action> tasks = new Queue<Action>();

    private void Update() {
        this.ExcuteTasks();
    }

    private void ExcuteTasks() {
        while (tasks.Count > 0) {
            Action task = null;

            lock (tasks) {
                if (tasks.Count > 0) {
                    task = tasks.Dequeue();
                }
            }

            task.Invoke();
        }
    }

    private IEnumerator IEDelay(Action task, float delayTime, bool ignoreTimeScale = false) {
        float elapsed = 0;
        while (elapsed < delayTime) {
            yield return null;
            elapsed += ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
        }
        Schedule(task);
    }
    #endregion

    /**<summary>The task will be add into a queue after 'delayTime' seconds, then be excute from main thread (on Update) </summary>*/
    public static void Schedule(Action task, float delayTime = 0, bool ignoreTimeScale = false) {
        if (delayTime == 0) {
            lock (Instance.tasks) {
                Instance.tasks.Enqueue(task);
            }
        }
        else {
            Instance.StartCoroutine(Instance.IEDelay(task, delayTime, ignoreTimeScale));
        }
    }

    public static void RunCoroutine(IEnumerator enumerator) {
        Instance.StartCoroutine(enumerator);
    }
}
