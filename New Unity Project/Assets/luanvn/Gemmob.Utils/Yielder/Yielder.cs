﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Gemmob {
    public class Yielder {
        private static readonly Dictionary<float, WaitForSeconds> waitCache = new Dictionary<float, WaitForSeconds>(50);

        public static readonly WaitForEndOfFrame EndOfFrame = new WaitForEndOfFrame();

        public static readonly WaitForFixedUpdate FixedUpdate = new WaitForFixedUpdate();

        public static WaitForSeconds Wait(float seconds) {
            if (waitCache.ContainsKey(seconds)) return waitCache[seconds];
            WaitForSeconds value = new WaitForSeconds(seconds);
            waitCache.Add(seconds, value);
            return value;
        }

        public static WaitForSeconds WaitForMiliseconds(uint miliseconds) {
            float seconds = miliseconds / 1000f;
            return Wait(seconds);
        }

        #region Extend Wait
        public class WaitForTaskCompletion : CustomYieldInstruction {
            Task task;
            Action onCompleted;
            Action<string> onFailed;

            public WaitForTaskCompletion(Task task, Action onCompleted = null, Action<string> onFailed = null) {
                this.task = task;
                this.onCompleted = onCompleted;
                this.onFailed = onFailed;
            }

            public override bool keepWaiting {
                get {
                    if (task == null) {
                        Callback.CallSchedule(onFailed, "Task is null");
                        return false;
                    }

                    if (task.IsCompleted) {
                        Callback.CallSchedule(onCompleted);
                        return false;
                    }
                    else if (task.IsFaulted || task.IsCanceled) {
                        Callback.CallSchedule(onFailed, task.Exception.ToString());
                        return false;
                    }

                    return true;
                }
            }
        }

        public class WaitForTaskCompletion<T> : CustomYieldInstruction {
            Task<T> task;
            Action<T> onCompleted;
            Action<string> onFailed;

            public WaitForTaskCompletion(Task<T> task, Action<T> onCompleted = null, Action<string> onFailed = null) {
                this.task = task;
                this.onCompleted = onCompleted;
                this.onFailed = onFailed;
            }

            public override bool keepWaiting {
                get {
                    if (task == null) {
                        Callback.CallSchedule(onFailed, "Task is null");
                        return false;
                    }

                    if (task.IsCompleted) {
                        Callback.CallSchedule(onCompleted, task.Result);
                        return false;
                    }
                    else if (task.IsFaulted || task.IsCanceled) {
                        Callback.CallSchedule(onFailed, task.Exception.ToString());
                        return false;
                    }

                    return true;
                }
            }
        }
        #endregion
    }
}