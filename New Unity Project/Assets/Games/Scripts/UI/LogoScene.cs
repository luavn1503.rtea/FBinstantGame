﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoScene : MonoBehaviour {

    private void Awake() {
        Time.timeScale = 1f;
    }

    public void ShowGameScene()
    {
        //UnityEngine.SceneManagement.SceneManager.LoadScene("_SwordManNewStyle/Scenes/Splash");
        this.DoTransition(() => {
            SceneManager.LoadScene(1);
        });
	}
}
